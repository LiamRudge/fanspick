﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Helper
{
    public static class FanspickStrings
    {
        #region Validation engine
        public const String Validation_Failed = "VALIDATION_FAILED";
        #endregion

        #region Alert titles
        public const String GenericDialogTitle = "Fanspick";
        #endregion

        #region Alert actions
        public const String Alert_OK = "OK";
        #endregion

        #region Colours
        public const String Color_FpDarkBlue = "#1D3046";
        public const String Color_FpWhite = "#FFFFFF";        
        #endregion
    }
}
