﻿using System;

namespace Fanspick.Shared.Helper
{
    public static class FanspickSettings
    {
        public static Models.AppModels.RuntimePurpose RuntimePurpose { get; set; }

        public static String ApiUrl
        {
            get
            {
                return string.Format(BaseUrl, 8000) + "/api";
            }
        }

        public static String SocialUrl
        {
            get
            {
                return string.Format(BaseUrl, 8080);
            }
        }

        public static String BaseUrl
        {
            get
            {
                return "http://52.163.48.178:{0}";
                //if (RuntimePurpose == Models.AppModels.RuntimePurpose.Development)
                //{
                //    return "http://52.163.48.178:8000/api";
                //}
                //else if (RuntimePurpose == Models.AppModels.RuntimePurpose.Live)
                //{
                //    return "http://13.94.139.68:8000/api";
                //}
                //else if (RuntimePurpose == Models.AppModels.RuntimePurpose.UAT)
                //{
                //    return "http://51.141.0.91:8000/api";
                //}
                // new server 51.141.45.249


                //return string.Empty;
                // fplive.ukwest.cloudapp.azure.com
            }
        } 

        public static String TeamShirtBaseURL
        {
            get
            {
                return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamShirts/";
            }
        }

        public static String TeamLogoBaseURL
        {
            get
            {
                return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamLogos/";
            }
        }
    }
}