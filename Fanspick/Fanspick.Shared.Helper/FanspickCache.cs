﻿﻿using Fanspick.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Repository.SQLite;
namespace Fanspick.Shared.Helper
{
    public class FanspickCache
    {

        public FanspickCache()
        {

        }

        public static Models.AppModels.FanspickScope CurrentFanspickScope { get; set; } = new Models.AppModels.FanspickScope();

        private static FanspickUser userData { get; set; } = new FanspickUser() { };

        public static FanspickUser UserData
        {
            get
            {
                if (userData != null)
                {
                    if (string.IsNullOrEmpty(userData.AccessToken))
                    {
                        try
                        {
                            FanspickUser userFromDatabase = FanspickDatabase.Get<FanspickUser>().FirstOrDefault();
                            if (userFromDatabase != null)
                            {
                                userData = userFromDatabase;
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }

                return userData;
            }
            set
            {
                try
                {
                    if (value != null)
                    {
                        FanspickDatabase.DropAndCreateTableIfNotExists<FanspickUser>();
                        FanspickDatabase.InsertEntity(value);
                    }
                    else
                    {
                        FanspickDatabase.DropAndCreateTableIfNotExists<FanspickUser>();
                    }
                }
                catch (Exception ex)
                {

                }

                userData = value;
            }
        }

        public static List<Country> CountryData { get; set; } = new List<Country>();

        public static List<Competition> CompetitionData { get; set; } = new List<Competition>();

        public static List<Formation> Formations { get; set; } = new List<Formation>();

        public static List<SelectUserPickPlayer> SelectUserPick { get; set; } = new List<SelectUserPickPlayer>();

        public static List<SelectUserPickPlayer> SelectUserPickLive { get; set; } = new List<SelectUserPickPlayer>();

        public static List<SelectUserPickPlayer> SelectManagerPick { get; set; } = new List<SelectUserPickPlayer>();

        public static List<SelectUserPickPlayer> SelectFansPick { get; set; } = new List<SelectUserPickPlayer>();

        public static RecentChatToShow chatPersonDetails ; 
        public static string SelectedGroupId, SelectedName, SelectedImage;

        public static string IsNewUser ;

        public static string BannerImage;
        public static string BannerPopUp;
        public static string BannerUrl;

		public static string PitchImage;
		public static string PitchPopUp;
		public static string PitchUrl;

        public static bool isPitch = false;
        public static bool isphoneValid = false;
		public static bool showLIve = false;
        public static bool isNewMessageRecived = false;

        public static int PageIndex;

    }


}
