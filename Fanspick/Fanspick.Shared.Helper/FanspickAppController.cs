﻿using System;
using System.Linq;
using Fanspick.Shared.Models;

namespace Fanspick.Shared.Helper
{
    public class FanspickAppController
    {
        public FanspickAppController()
        {
        }

        public static Func<bool> CacheRoutine()
        {
            try
            {
                CreateNotificationsPreMatch();
            }
            catch (Exception ex)
            {

            }

            try
            {
                CreateNotificationLiveMatch();
            }
            catch (Exception ex)
            {

            }

            return () => true;
        }

        private static void CreateNotificationLiveMatch()
        {
			if (FanspickCache.CurrentFanspickScope != null)
			{
				if (FanspickCache.CurrentFanspickScope.UpComingFixtures != null)
				{
                    var upcomingFixturesInLive = FanspickCache.CurrentFanspickScope.UpComingFixtures.Where(f => Convert.ToDateTime(f.FixtureDate) >= DateTime.Now && Convert.ToDateTime(f.FixtureDate).AddMinutes(90) <= DateTime.Now);
					foreach (var fixture in upcomingFixturesInLive)
					{
						Xamarin.Forms.DependencyService.Get<IFanspickLocalNotification>().TriggerNotification($"Fanspick", $"{fixture.LocalTeam.Name} vs. {fixture.VisitorTeam.Name} now Live!");
					}
				}
			}
        }

        #region
        private static void CreateNotificationsPreMatch()
        {
            if (FanspickCache.CurrentFanspickScope != null)
            {
                if (FanspickCache.CurrentFanspickScope.UpComingFixtures != null)
                {
                    var upcomingFixturesInPreMatch = FanspickCache.CurrentFanspickScope.UpComingFixtures.Where(f => Convert.ToDateTime(f.FixtureDate).AddHours(-2) >= DateTime.Now && Convert.ToDateTime(f.FixtureDate).AddHours(-1) <= DateTime.Now);
                    foreach (var fixture in upcomingFixturesInPreMatch)
                    {
                        Xamarin.Forms.DependencyService.Get<IFanspickLocalNotification>().TriggerNotification($"Fanspick", $"Don't forget to make a YourPick for {fixture.LocalTeam.Name} vs. {fixture.VisitorTeam.Name}!");
                    }
                }
            }
        }
        #endregion
    }
}
