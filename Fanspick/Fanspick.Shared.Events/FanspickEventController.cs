﻿using System;
using Fanspick.Shared.Events.Events;

namespace Fanspick.Shared.Events
{
    public class FanspickEventController
    {
        private FanspickPitchEventHandler fanspickPitchEvents;
        public FanspickPitchEventHandler FanspickPitchEvents
        {
            get{
                if (fanspickPitchEvents==null)
                {
                    return new FanspickPitchEventHandler();
                }
                else
                {
                    return fanspickPitchEvents;
                }
            }

            set{
                fanspickPitchEvents = value;
            }
        }

        public FanspickEventController()
        {
        }
    }
}
