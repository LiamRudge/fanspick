﻿﻿﻿using System;
using System.Threading;
using Fanspick.Shared.Components.Components;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.Service;

using Xamarin.Forms;
using static Fanspick.Shared.Models.DTO.MatchCommentriesDTO;

namespace Fanspick.Shared.Events.Events
{
    public class FanspickPitchEventHandler
    {
        public static bool calledOnRunUp;
        public static bool calledOnPreMatch;
        public static bool calledOnPreMatchEnd;
        public static bool calledOnLiveMatchStart;
        public static bool calledManOfTheMatchStart;
        public static bool calledOnLiveMatchFinish;
        public static bool calledOn12HourCutOff;

        public static PitchType CurrentlyViewing { get; set; }
        public static MatchStage MatchStage { get; set; }
        public static CancellationTokenSource cancellation { get; set; } = new CancellationTokenSource();
        public static CancellationTokenSource commentryCancellation { get; set; } = new CancellationTokenSource();
        public string MatchStatusText { get; private set; }
        public static bool CanDoManOfTheMatch { get; set; }
        public string CounterTimeText { get; private set; }
        public static bool CallTimer { get; set; }
        public bool _Stopped = false;
        public bool _Resumed = false;
        public bool isDelayed = false;
       
        public string matchApistatus = "";
        public DateTime fixtureDateNew;
        public bool secondHalfDone = false;
        public static DateTime StartcurMills;
        public static DateTime StopcurMills;
        TimeSpan difference;
        public static void ResetPitchEvents()
        {
            calledOnRunUp = false;
            calledOnPreMatch = false;
            calledOnPreMatchEnd = false;
            calledOnLiveMatchStart = false;
            calledOnLiveMatchFinish = false;
            MatchStage = MatchStage.RunUp;
            cancellation = new CancellationTokenSource();
            commentryCancellation = new CancellationTokenSource();
            CallTimer = true;
        }

        public FanspickPitchEventHandler()
        {

            Start();


        }
        public void Stop(int counter)
        {

            _Stopped = true;
            stopEvents(counter);

        }


        public void stopEvents(int counter)
        {
            if (counter == 1)
            { StartcurMills = DateTime.Now; }

            MatchStatusText = "Half Time";
            RaiseUpdateCounterText(MatchStatusText);
            RaiseUpdateCounterTimeText(CounterTimeText);


        }

        public void Resume(int counter1)
        {
            _Stopped = false;
            _Resumed = true;
            resumeEvents(counter1);

        }
        public bool resumeEvents(int counter1)
        {
            if (counter1 == 1)
            {
                difference = DateTime.Now.Subtract(StartcurMills); ;

            }
            TimeSpan RunEveryX = new TimeSpan(0, 0, 1);
            Device.StartTimer(RunEveryX, RaiseEvents);
            return true;
        }
        public void Start()
        {
            _Stopped = false;
            _Resumed = false;
            TimeSpan RunEveryX = new TimeSpan(0, 0, 1);
            Device.StartTimer(RunEveryX, RaiseEvents);
        }

        ~FanspickPitchEventHandler()
        {
            ResetPitchEvents();

            cancellation.Cancel();
        }

        public bool RaiseEvents()
        {
            if (!cancellation.IsCancellationRequested)
            {
                try
                {
                    Models.Fixture currentFixture = Fanspick.Shared.Helper.FanspickCache.CurrentFanspickScope.SelectedFixture;
                                        if (currentFixture != null)
                    {
                        DateTime fixtureDate;
                        DateTime.TryParse(currentFixture.FixtureDate, out fixtureDate);

                        if (fixtureDate != null)
                        {

                            // before pre match
                            if (DateTime.Now < fixtureDate.AddHours(-1))
                            {
                                CounterTimeText = FormatTime((fixtureDate.AddHours(-1) - DateTime.Now).TotalSeconds);

                                MatchStage = MatchStage.RunUp;

                                MatchStatusText = "Deadline for Changes";

                            }
                            // pre match
                            else if (DateTime.Now < fixtureDate && DateTime.Now >= fixtureDate.AddHours(-1))
                            {
                                CounterTimeText = FormatForLive((fixtureDate - DateTime.Now).TotalSeconds);

                                MatchStage = MatchStage.PreMatch;

                                MatchStatusText = "KickOff Time";
                            }
                            // live match
                            else if (DateTime.Now >= fixtureDate && DateTime.Now <= fixtureDate.AddMinutes(120))
                            {
                                MatchStage = MatchStage.PreMatchEnd;
                                RaiseGetMatchStatus(EventArgs.Empty);

                                    if (isDelayed)
                                    {

                                        MatchStatusText = "Match delayed";
                                        CounterTimeText = "";
                                    } 

                                if (fixtureDateNew != null)
                                {

                                    if (matchApistatus.Contains("First Half"))
                                    {  
                                        
                                        CounterTimeText = FormatForLive((fixtureDateNew.AddMinutes(90) - DateTime.Now).TotalSeconds);

                                        MatchStage = MatchStage.Live;

                                        MatchStatusText = "Live - First Half";
                                    }
                                    if (matchApistatus.Contains("45+"))
                                    {
                                        CounterTimeText = "0h: 45m: 00s";
                                        MatchStage = MatchStage.Live;
                                        MatchStatusText = "Live - " + matchApistatus;
                                    }

                                    
                                        if (matchApistatus.Contains("Halftime"))
                                        {
                                            CounterTimeText = "0h: 45m: 00s";

                                            MatchStage = MatchStage.Live;

                                            MatchStatusText = "Half Time";
                                        }
                                    if (matchApistatus.Contains("Second Half") && secondHalfDone == false)
                                        {
	                                            CounterTimeText = FormatForLive((fixtureDateNew.AddMinutes(45) - DateTime.Now).TotalSeconds);
                                            if(CounterTimeText.Contains(": -"))
		                                        {
		                                            secondHalfDone = true;
		                                        }
                                            MatchStage = MatchStage.Live;

                                           if (DateTime.Now >= fixtureDateNew && DateTime.Now >= fixtureDateNew.AddMinutes(30) && DateTime.Now <= fixtureDateNew.AddMinutes(45))
                                            {

                                                MatchStatusText = "Live - M.O.M now available!";
                                                CanDoManOfTheMatch = true;
                                            }
                                            else
                                            {

                                                MatchStatusText = "Live - Second Half";
                                                CanDoManOfTheMatch = false;
                                            }
                                        }
                                    if (matchApistatus.Contains("Second Half") && secondHalfDone == true)
                                    {
										CounterTimeText = "0h: 00m: 00s";
										MatchStage = MatchStage.Live;
										CanDoManOfTheMatch = true;
										MatchStatusText = "Live - " + matchApistatus;
                                    }
                                        if (matchApistatus.Contains("90'+"))
                                        {
                                        
                                            CounterTimeText = "0h: 00m: 00s";
                                            MatchStage = MatchStage.Live;
										CanDoManOfTheMatch = true;
										MatchStatusText = "Live - " + matchApistatus;
                                        }
                                        if (matchApistatus.Contains("Full"))
                                        {
                                            CanDoManOfTheMatch = false;

                                            MatchStage = MatchStage.Past;

                                            MatchStatusText = "Full Time";

                                            CounterTimeText = "Dont forget to make YourPick now on the upcoming fixture!";
                                        }
                                    }
                            }
                            else if (DateTime.Now >= fixtureDate.AddMinutes(105) && DateTime.Now <= fixtureDate.AddMinutes(135))
                            {
                                CanDoManOfTheMatch = false;

                                //                        MatchStatusText = "Extra Time";

                                //MatchStage = MatchStage.Live;

                                //CounterTimeText = FormatForLive((fixtureDate.AddMinutes(120) - DateTime.Now).TotalSeconds);

                                MatchStage = MatchStage.Past;

                                MatchStatusText = "Full Time";

                                CounterTimeText = "Dont forget to make YourPick now on the upcoming fixture!";
                            }
                            else if (DateTime.Now >= fixtureDate.AddMinutes(135))
                            {
                                MatchStage = MatchStage.Past;

                                MatchStatusText = "Full Time";

                                CounterTimeText = "Dont forget to make YourPick now on the upcoming fixture!";
                            }
                            else if (DateTime.Now >= fixtureDate.AddHours(12))
                            {
                                MatchStage = MatchStage.CutOff;

                                MatchStatusText = "Full Time";

                                CounterTimeText = "Dont forget to make YourPick now on the upcoming fixture!";
                            }

                            RaiseUpdateCounterText(MatchStatusText);

                            RaiseUpdateCounterTimeText(CounterTimeText);

                            /*
                             * run as iteration
                             * this is because the user may click on an already player match
                             * but we still need all of the pre match and live data
                             * so iterate
                             */
                            if (MatchStage == MatchStage.Locked)
                            {
                                // do nothing
                            }
                            else if (MatchStage == MatchStage.CutOff)
                            {
                                RaiseOnRunUp(EventArgs.Empty);

                                RaiseOnPreMatch(EventArgs.Empty);

                                RaiseOnLiveMatchStart(EventArgs.Empty);

                                RaiseOnLiveMatchFinish(EventArgs.Empty);

                                // cutoff functionality
                                MatchStage = MatchStage.Locked;
                            }
                            else if (MatchStage == MatchStage.Past)
                            {
                                RaiseOnRunUp(EventArgs.Empty);

                                RaiseOnPreMatch(EventArgs.Empty);

                                RaiseOnLiveMatchStart(EventArgs.Empty);

                                RaiseOnLiveMatchFinish(EventArgs.Empty);

                                // lock functionality
                                MatchStage = MatchStage.Locked;
                            }
                            else if (MatchStage == MatchStage.Live)
                            {
                                RaiseOnRunUp(EventArgs.Empty);

                                RaiseOnPreMatch(EventArgs.Empty);

                                RaiseOnLiveMatchStart(EventArgs.Empty);
                            }
                            else if (MatchStage == MatchStage.PreMatch)
                            {
                                RaiseOnRunUp(EventArgs.Empty);

                                RaiseOnPreMatch(EventArgs.Empty);

                            }

                            else if (MatchStage == MatchStage.PreMatchEnd)
							{
                                RaiseOnPreMatchEnd(EventArgs.Empty);


							}

                            else if (MatchStage == MatchStage.RunUp)
                            {
                                RaiseOnRunUp(EventArgs.Empty);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }

                return true;
            }
            else
            {
                return false;
            }
        }

    
        #region event triggers
        private void RaiseOnRunUp(EventArgs e)
        {
            try
            {
                if (!calledOnRunUp)
                {
                    calledOnRunUp = true;

                    OnRunUp(this, e);
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void RaiseOnPreMatch(EventArgs e)
        {
            try
            {
                if (!calledOnPreMatch)
                {
                    calledOnPreMatch = true;

                    OnPreMatch(this, e);
                }
            }
            catch (Exception ex)
            {

            }
        }

		private void RaiseOnPreMatchEnd(EventArgs e)
		{
			try
			{
				if (!calledOnPreMatchEnd)
				{
					calledOnPreMatchEnd = true;

					OnPreMatchEnd(this, e);
				}
			}
			catch (Exception ex)
			{

			}
		}





        private void RaiseOnLiveMatchStart(EventArgs e)
        {
            try
            {
                if (!calledOnLiveMatchStart)
                {
                    calledOnLiveMatchStart = true;

                    OnLiveMatchStart(this, e);
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void RaiseOnLiveMatchFinish(EventArgs e)
        {
            try
            {
                if (!calledOnLiveMatchFinish)
                {
                    calledOnLiveMatchFinish = true;

                    OnLiveMatchFinish(this, e);
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void RaiseUpdateCounterText(String CounterText)
        {
            try
            {
                UpdateCounterText(CounterText, EventArgs.Empty);
            }
            catch (Exception ex)
            {

            }
        }
        private void RaiseGetMatchStatus(EventArgs e)
        {
            try
            {
                GetMatchStatus(this, e);
            }
            catch (Exception ex)
            {

            }
        }
        private void RaiseUpdateCounterTimeText(String CounterTimeText)
        {
            try
            {
                UpdateCounterTimeText(CounterTimeText, EventArgs.Empty);
            }
            catch (Exception ex)
            {

            }
        }
        private void RaiseOn12HourCutOFf(EventArgs e)
        {
            try
            {
                if (!calledOn12HourCutOff)
                {
                    calledOn12HourCutOff = true;

                    On12HourCutOff(this, e);
                }
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region events
        public event EventHandler OnRunUp;
        public event EventHandler OnPreMatch;
        public event EventHandler OnPreMatchEnd;
        public event EventHandler OnLiveMatchStart;
        public event EventHandler OnLiveMatchFinish;
        public event EventHandler UpdateCounterText;
        public event EventHandler GetMatchStatus;
        public event EventHandler UpdateCounterTimeText;
        public event EventHandler On12HourCutOff;
        public event EventHandler StopTimer;
        #endregion

        private string FormatTime(double Seconds)
        {
            var timespan = TimeSpan.FromSeconds(Seconds);
            return string.Format("{0:D2}d : {1:D2}h : {2:D2}m : {3:D2}s", timespan.Days, timespan.Hours, timespan.Minutes, timespan.Seconds);
        }

        private string FormatForLive(double Seconds)
        {
            var timespan = TimeSpan.FromSeconds(Seconds);
            //int actualMinutes = 0;
            //if (timespan.Hours>0)
            //{
            //    actualMinutes = (60 * timespan.Hours);
            //}
            //actualMinutes += timespan.Minutes;
            return string.Format("{0:D2}h : {1:D2}m : {2:D2}s", timespan.Hours, timespan.Minutes, timespan.Seconds);
        }
    }

    public enum MatchStage
    {
        RunUp = 1,
        PreMatch = 2,
        PreMatchEnd= 8,
        Live = 3,
        Past = 4,
        CutOff = 5,
        Locked = 6,
        FullyLocked = 7
    }
    public enum PitchType
    {
        Nothing,
        YourPick,
        ManagersPick,
        Fanspick
    }
}
