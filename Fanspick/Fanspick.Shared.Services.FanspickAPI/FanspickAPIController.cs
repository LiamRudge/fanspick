﻿using Fanspick.Shared.Models.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models;

namespace Fanspick.Shared.Services.FanspickAPI
{
    public class FanspickAPIController
    {
        public String AccessToken { get; set; } = String.Empty;

        public dynamic Data { get; set; } = null;

        public String Target { get; set; } = String.Empty;

        public ApiType Api { get; set; } = ApiType.Fanspick;

        public MethodType Method { get; set; } = MethodType.Post;

        public Dictionary<String, String> Queries { get; set; } = new Dictionary<string, string>();

        public async Task<Models.FanspickAPI.FanspickAPIResponse> Call()
        {
            Models.FanspickAPI.FanspickAPIResponse response = new Models.FanspickAPI.FanspickAPIResponse();

            string content_json = string.Empty;

            try
            {
                String trueTarget = string.Empty;
                switch (this.Api)
                {
                    case ApiType.Fanspick:
                        trueTarget = $"{Helper.FanspickSettings.ApiUrl}/{this.Target}";
                        break;
                    case ApiType.Social:
                        trueTarget = $"{Helper.FanspickSettings.SocialUrl}/{this.Target}";
                        break;
                    default:
                        trueTarget = $"{Helper.FanspickSettings.ApiUrl}/{this.Target}";
                        break;
                }


                if (Queries.Count > 0)
                {
                    trueTarget += "?";

                    foreach (var query in Queries)
                    {
                        trueTarget += $"{query.Key}={query.Value}&";
                    }
                }

                var uri = new Uri(string.Format(trueTarget, string.Empty));

                HttpClientHandler handler = new HttpClientHandler();

                HttpClient httpClient = new HttpClient(handler);

                HttpResponseMessage httpResponse = null;

                if (this.Method == MethodType.Post)
                {
                    // is post

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, uri);
                    if (!String.IsNullOrEmpty(this.AccessToken))
                    {
                        request.Headers.Add("Authorization", $"bearer {this.AccessToken}");
                    }


                    String json = Newtonsoft.Json.JsonConvert.SerializeObject(Data, Newtonsoft.Json.Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                    content_json = json;

                    HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

                    request.Content = content;

                    if (handler.SupportsTransferEncodingChunked())
                    {
                        request.Headers.TransferEncodingChunked = true;
                    }

                    httpResponse = await httpClient.SendAsync(request);
                }
                else
                {
                    // is get

                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, uri);

                    System.Diagnostics.Debug.WriteLine(request == null ? "Request is NULL" : "Request has data");

                    if (!String.IsNullOrEmpty(this.AccessToken))
                    {
                        request.Headers.Add("Authorization", $"bearer {this.AccessToken}");
                    }

                    httpResponse = await httpClient.SendAsync(request);
                }

                if (httpResponse.IsSuccessStatusCode)
                {
                    response.DataAsJsonString = await httpResponse.Content.ReadAsStringAsync();

                    response.OK = true;
                }
                else
                {
                    response.ReasonPhrase = JsonConvert.DeserializeObject<ErrorMessageResponse>(httpResponse.Content.ReadAsStringAsync().Result).Message;
                    Fanspick.Shared.Models.ErrorMessageResponse.responseMessage = response.ReasonPhrase;
                    response.OK = false;
                }
            }
            catch (Exception error)
            {
                // TODO record error when calling the api fails
            }

            return response;
        }
    }
}
 