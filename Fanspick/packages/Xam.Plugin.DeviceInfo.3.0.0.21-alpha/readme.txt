Device Info Readme


   .-"-.
 _/.-.-.\_
( ( o o ) )
 |/  "  \| 
  \  -  /
  /'"""'\
 /       \

## Ensure you install the NuGet into ALL projects

## Get started by adding a using statement:
using Plugin.DeviceInfo;

Then access any method or property such as:

var platform = CrossDeviceInfo.Current.Platform;



Find the most up to date information at: https://github.com/jamesmontemagno/DeviceInfoPlugin
