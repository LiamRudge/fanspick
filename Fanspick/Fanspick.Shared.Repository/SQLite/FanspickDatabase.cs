﻿using System;
using SQLite.Net;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;
using Fanspick.Shared.Models;

namespace Fanspick.Shared.Repository.SQLite
{
    public static class FanspickDatabase
    {

        const string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";

        private static SQLiteConnection _connection;
        private static SQLiteConnection Connection
        {
            get
            {
                if (_connection == null)
                {
                    _connection = DependencyService.Get<ISQLite>().GetConnection();
                }

                return _connection;
            }
            set
            {
                _connection = value;
            }
        }

        public static void CreateDatabaseSchema()
        {
            CreateTableIfNotExist<RandomThought>();
        }

        public static void DropAllTables()
        {
            Connection.DropTable<RandomThought>();
        }

        private static bool CreateTableIfNotExist<T>() where T : class
        {
            bool created = false;

            try
            {
                if (!TableExists<T>())
                {
					Connection.CreateTable<T>();

					created = true;
                }


            }
            catch (Exception ex)
            {
                // todo log error
            }

            return created;
        }

        public static bool TableExists<T>(){
            var cmd = Connection.CreateCommand(cmdText, typeof(T).Name);
			if (cmd.ExecuteScalar<string>() == null)
			{
                return false;
			}
            else
            {
                return true;
            }
        }

		public static IEnumerable<T> Get<T>() where T : class
		{
			
		   return Connection.Table<T>();
			
		}
        public static IEnumerable<T> Get<T>(Func<T, bool> predicate) where T : class
        {
           return Connection.Table<T>().Where(predicate);
        }

        public static bool DropAndCreateTableIfNotExists<T>()
        {

			bool created = false;
			try
			{
				if (TableExists<T>())
				{
                    Connection.DropTable<T>();
				}
                Connection.CreateTable<T>();
				created = true;
			}
			catch (Exception ex)
			{
				// todo log error
			}
			return created;
        }

        public static bool CreateTableIfNotExists<T>()
        {
            bool created =false;
           try
            {
                if (!TableExists<T>())
                {
                    Connection.CreateTable<T>();
                }
                created = true;
            }
            catch (Exception ex)
            {
				// todo log error
			}
            return created;
        }

        public static bool InsertEntity<T>(T ObjectToInsert){
            bool inserted = false;

            try
            {
                Connection.Insert(ObjectToInsert);
                inserted=true;
            }
            catch (Exception ex)
            {
                // todo log error
            }

            return inserted;
        }

        public static bool UpdateEntity<T>(T ObjectToUpdate){
            bool updated = false;
            try
            {
				if (TableExists<T>())
				{
                    Connection.Update(ObjectToUpdate);
                    updated = true;
				}
            }
            catch (Exception ex)
            {
                // todo log error 
            }
            return updated;
        }

        public static bool DeleteEntity<T>(T ObjectToDelete)
        {
            bool deleted = false;
            try
            {
                Connection.Delete(ObjectToDelete);
            }
            catch (Exception ex)
            {
                // todo log error
            }
            return deleted;
        }
	}
}