﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models
{
    public class TeamStats
    {
		[JsonProperty(PropertyName = "_id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "teamKnownName")]
		public string KnownName { get; set; }

		[JsonProperty(PropertyName = "points")]
		public string Points { get; set; }

		[JsonProperty(PropertyName = "imageURL")]
		public string ImageURLteam { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public string TeamID { get; set; }
		public string TeamLogoAsImage
		{
			get
			{
                if (this.ImageURLteam != null)
				{
                    return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamLogos/" + this.ImageURLteam;
				}
				else
				{
					return "no_image";
				}
			}
		}

		[JsonProperty(PropertyName = "overallStats")]
		public OverAllTeamStats OverallStats { get; set; }


		

    }
}