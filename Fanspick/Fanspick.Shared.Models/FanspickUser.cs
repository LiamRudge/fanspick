﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;

namespace Fanspick.Shared.Models
{
    public class FanspickUser
    {

        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "fcmId")]
        public string FcmId { get; set; }

        [JsonProperty(PropertyName = "profileComplete")]
        public int ProfileComplete { get; set; }

        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "firstname")]
        public string Firstname { get; set; }

        [JsonProperty(PropertyName = "lastname")]
        public string Lastname { get; set; }

        [JsonProperty(PropertyName = "emailId")]
        public string EmailId { get; set; }

		[JsonProperty(PropertyName = "address")]
		public string Address { get; set; }

        [JsonProperty(PropertyName = "lat")]
        public string Latitude { get; set; }

		[JsonProperty(PropertyName = "photo")]
		public string Photo { get; set; }

        [JsonProperty(PropertyName = "long")]
        public string Longitude { get; set; }

        [JsonProperty(PropertyName = "rejection")]
        public bool Rejection { get; set; }

        [JsonProperty(PropertyName = "deviceType")]
        public string DeviceType { get; set; }

        [JsonProperty(PropertyName = "deviceToken")]
        public string DeviceToken { get; set; }

        [JsonProperty(PropertyName = "appVersion")]
        public string AppVersion { get; set; }

        [JsonProperty(PropertyName = "dob")]
        public DateTime? Dob { get; set; }

		[JsonProperty(PropertyName = "phoneNumber")]
		public string PhoneNumber { get; set; }

        [JsonProperty(PropertyName = "gender")]
        public string Gender { get; set; }

        [JsonProperty(PropertyName = "onceLogin")]
        public string OnceLogin { get; set; }

        [Ignore]
        [JsonProperty(PropertyName = "ageVerification")]
        public List<object> AgeVerification { get; set; }

        [Ignore]
        [JsonProperty(PropertyName = "communityId")]
        public List<object> CommunityId { get; set; }

        [Ignore]
        [JsonProperty(PropertyName = "teamFavourite")]
        public List<object> TeamFavourite { get; set; }

        [JsonProperty(PropertyName = "gradeValue")]
        public string GradeValue { get; set; }

        [JsonProperty(PropertyName = "gradePoint")]
        public string GradePoint { get; set; }

        [JsonProperty(PropertyName = "active")]
        public bool Active { get; set; }

        [Ignore]
        [JsonProperty(PropertyName = "defaultTeam")]
        public List<DefaultTeam> DefaultTeam { get; set; }

        [JsonProperty(PropertyName = "deleteByAdmin")]
        public bool DeleteByAdmin { get; set; }

        [JsonProperty(PropertyName = "loginType")]
        public string LoginType { get; set; }

		[JsonProperty(PropertyName = "city")]
		public string City { get; set; }

		[JsonProperty(PropertyName = "zipcode")]
		public string Zip { get; set; }

    }
}

public class DefaultTeam
{
    [JsonProperty(PropertyName = "_id")]
    public string Id { get; set; }

    [JsonProperty(PropertyName = "favouriteTeam")]
    public String FavouriteTeam { get; set; }

    [JsonProperty(PropertyName = "FavouriteTeamCountry")]
    public string FavouriteTeamCountry { get; set; }

	//[JsonProperty(PropertyName = "favouriteTeam")]
	//public TeamData Teamdata { get; set; }

}


public class TeamData
{
	[JsonProperty(PropertyName = "_id")]
	public string FavTeamId { get; set; }

	[JsonProperty(PropertyName = "knownName")]
	public string FavTeamKnownName { get; set; }

}

