﻿using System;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models
{
    public interface IGoogleLogin
    {
		Task<GoogleLoginResult> SignIn();
		Task<GoogleLoginResult> SignOut();
    }
}
