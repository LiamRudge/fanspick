﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models
{
    public class Position
    {
        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "PosX")]
        public string PosX { get; set; }

        [JsonProperty(PropertyName = "PosY")]
        public string PosY { get; set; }

        [JsonProperty(PropertyName = "Role")]
        public string Role { get; set; }

        [JsonProperty(PropertyName = "Key")]
        public string Key { get; set; }
        public string PlayerImage { get; set; }
    }
}
