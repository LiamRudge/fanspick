﻿using System;
using Newtonsoft.Json;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace Fanspick.Shared.Models
{
    public class Commentry
    {

        [JsonProperty(PropertyName = "commentaryId")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "Type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "eventTime")]
        public string EventTime { get; set; }

        public DateTime EventTimeAsDateTime
        {
            get
            {
                DateTime dt;
                DateTime.TryParse(EventTime, out dt);
                return dt;
            }
        }

        [JsonProperty(PropertyName = "minsInMatch")]
        public string MinsInMatch { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        public string TypeImage
        {
            get
            {
                String toShowFavImage = "";
                String imageType = this.Type;

                if (imageType == "Goal")
                {
                    toShowFavImage = "commentary_goal.png";
                }
                else if (imageType == "Yellow_Card")
                {
                    toShowFavImage = "commentary_yellow.png";
                }
                else if (imageType == "Red_Card")
                {
                    toShowFavImage = "commentary_red.png";
                }
                else if (imageType == "Substitution")
                {
                    toShowFavImage = "commentary_sub.png";
                }
                else
                {
                    toShowFavImage = "commentary_other.png";
                }

                return toShowFavImage;
            }
        }

        public string TypeInfo
		{
			get
			{
				string typeinf = "";

                if (this.Type == "Others")
				{
					typeinf = "Activity";
				}
				else
				{
					typeinf = this.Type;
				}
				return typeinf;

			}
		}

		public GridLength Div1
		{
			get
			{
				var height = new GridLength(100);
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					height = new GridLength(160);
				}

				return height;
			}
		}

		public GridLength Div2
		{
			get
			{
				var height = new GridLength(30);
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					height = new GridLength(50);
				}

				return height;
			}
		}



		public double TypeImageTextSize
		{
			get
			{
				var size = 10;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 14;
				}

				return size;
			}
		}



		public double DescriptionSize
		{
			get
			{
				var size = 12;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 16;
				}

				return size;
			}
		}

		public double circleSize
		{
			get
			{
				var size = 40;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 50;
				}

				return size;
			}
		}


		public double MinsInMatchSize
		{
			get
			{
				var size = 9;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 13;
				}

				return size;
			}
		}


        public double CellImageSize
		{
			get
			{
				var size = 20;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 30;
				}

				return size;
			}
		}

    }
}