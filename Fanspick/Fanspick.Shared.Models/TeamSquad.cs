﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Net;
using Plugin.DeviceInfo;

namespace Fanspick.Shared.Models
{
    public class Squad
    {

        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

		[JsonProperty(PropertyName = "playerId")]
		public string PlayerId { get; set; }

        [JsonProperty(PropertyName = "name")]
		public string Name
		{
			get
			{
				return WebUtility.HtmlDecode(decodedKnownName);
			}
			set
			{
				decodedKnownName = WebUtility.HtmlDecode(value);
			}
		}

		private string decodedKnownName;

		[JsonProperty(PropertyName = "position")]
		public string Role { get; set; }

        public ImageSource shirt { get; set; }

		public double PlayerShirtSize
		{
			get
			{
                double size = 65;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 90;
				}

				return size;
			}
		}

		public double CloseIconSize
		{
			get
			{
				double size = 30;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 45;
				}

				return size;
			}
		}

	}
}