﻿using System;
using System.Collections.Generic;

namespace Fanspick.Shared.Models
{
    public class CCodes
    {
        public List<CCode> Codes { get; set; }
    }
    public class CCode
    {
        public string Code { get; set; }
		public string CodeP { get; set; }
        public string CodeF { get; set; }
        public string CodeN { get; set; }
	}
	public class PCode
	{
        public string Code { get; set; }
	}
}
