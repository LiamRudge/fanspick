﻿using System;
namespace Fanspick.Shared.Models
{
    public interface IKeyboardService
    {
		event EventHandler KeyboardIsShown;
		event EventHandler KeyboardIsHidden;
    }
}
