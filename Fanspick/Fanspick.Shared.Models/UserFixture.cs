﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models
{
    public class UserFixture
    {
        [JsonProperty(PropertyName = "currentFormation")]
        public Formation1 CurrentFormation { get; set; }

        [JsonProperty(PropertyName = "isLive")]
        public bool IsLive { get; set; }
    }
}
