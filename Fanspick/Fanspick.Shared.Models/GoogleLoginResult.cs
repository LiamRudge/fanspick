﻿using System;
namespace Fanspick.Shared.Models
{
    public class GoogleLoginResult
    {
		public bool IsSuccess { get; set; }
		public string UserId { get; set; }
		public string Name { get; set; }
		public string Image { get; set; }
		public string Email { get; set; }
		public string Message { get; set; }
    }
}
