﻿using System;
namespace Fanspick.Shared.Models
{
    public interface IFanspickLocalNotification
    {
        void TriggerNotification(String Title, String Message);
    }
}
