﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace Fanspick.Shared.Models
{
    public class Predictions
	{
		[JsonProperty("status")]
		public string status { get; set; }

		[JsonProperty("predictions")]
		public List<Prediction> predictions { get; set; }
    }
	public class Prediction
	{
		[JsonProperty("description")]
		public string Description { get; set; }

		[JsonProperty("place_id")]
		public string Place_id { get; set; }

		[JsonProperty("reference")]
		public string Reference { get; set; }

		public List<PredictionMatchedSubstring> matched_substrings { get; set; }

		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("terms")]
		public List<PredictionTerm> Terms { get; set; }

		[JsonProperty("types")]
		public List<string> Types { get; set; }
	}
	public class PredictionMatchedSubstring
	{
		[JsonProperty("length")]
		public int Length { get; set; }

		[JsonProperty("offset")]
		public int Offset { get; set; }
	}
	public class PredictionTerm
	{
		[JsonProperty("offset")]
		public int Offset { get; set; }

		[JsonProperty("value")]
		public string Value { get; set; }
	}
}
