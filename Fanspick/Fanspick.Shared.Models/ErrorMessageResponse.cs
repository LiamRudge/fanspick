﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models
{
    public class ErrorMessageResponse
    {
		[JsonProperty(PropertyName = "statusCode")]
		public int StatusCode { get; set; }

		[JsonProperty(PropertyName = "error")]
		public string error { get; set; }

		[JsonProperty(PropertyName = "message")]
		public string Message { get; set; }

		[JsonProperty(PropertyName = "data")]
		public Resp Data { get; set; }

        public static string responseMessage = "";


    }

	public class ErrorMessageUserPick
	{
		[JsonProperty(PropertyName = "statusCode")]
		public int StatusCode { get; set; }


	}

	public class Resp
	{
		[JsonProperty(PropertyName = "filename")]
		public string Filename { get; set; }

	}
}
