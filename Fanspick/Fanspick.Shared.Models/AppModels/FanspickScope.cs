﻿using Fanspick.Shared.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Fanspick.Shared.Models.DTO.MyCommunityDTO;
using Xamarin.Forms;

namespace Fanspick.Shared.Models.AppModels
{
    public class FanspickScope
    {
        public FanspickScope()
        {
            
        }

        public Fanspick.Shared.Models.Team SelectedTeam { get; set; }

        public Fanspick.Shared.Models.DefaultTeamFavorite DefaultTeam { get; set; }

        public List<Fanspick.Shared.Models.Fixture> UpComingFixtures { get; set; }

        public Fanspick.Shared.Models.Fixture SelectedFixture { get; set; }

        public ResponsePickData YourPickData { get; set; } 

        public Fanspick.Shared.Models.Fixture NextFixture { get; set; }

        public String SelectTopicID { get; set; }

        public String SelectTopicName { get; set; }

        public ResponsePickData ManagerPickData { get; set; }

        public String SelectedPlayerId { get; set; }

        public String SelectedPlayerName { get; set; }

        public String SelectedPositionId { get; set; }

        public ResponsePickData FanspickPickData { get; set; }

        public List<Squad> TeamSquadData { get; set; }

        public ResponseMyCommunityData SelectedCommunity { get; set; }

        public string TeamShirtImage { get; set; }

        public UserNotificationDTO.ResponseUserNotificationData SelectedAlert { get; set; }

        public String CurrentlyUrlToVisit { get; set; }

        public string FCMId { get; set; }

        public bool HasUpcomingFixture { get; set; }
        public String NewPlayerId { get; set; }

        public String twMaxId { get; set; }
        public String fbNextTS { get; set; }
                          

        public List<String> FavoritePrimaryTeamID { get; set; }
    }
}
