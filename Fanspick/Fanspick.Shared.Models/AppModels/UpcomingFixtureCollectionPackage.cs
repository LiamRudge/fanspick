﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.AppModels
{
    public class UpcomingFixtureCollectionPackage
    {
        public String AccessToken { get; set; }

        public String TeamId { get; set; }
    }
}
