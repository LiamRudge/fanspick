﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.AppModels
{
    public class SignUpModel
    {
        [Services.ValidationEngine.Conditions.IsRequired]
        public String Username { get; set; }
        [Services.ValidationEngine.Conditions.IsRequired]
        public String EmailAddress { get; set; }
        [Services.ValidationEngine.Conditions.IsRequired]
        public String Password { get; set; }
        [Services.ValidationEngine.Conditions.IsRequired]
        public String PasswordConfirmation { get; set; }
        [Services.ValidationEngine.Conditions.IsRequired]
        public String Gender { get; set; }

        //public String FirstName { get; set; }
        //public String SecondName { get; set; }
        //public DateTime DOB { get; set; }
        //public String City { get; set; }
        //public String FavouriteTeam { get; set; }
    }
}
