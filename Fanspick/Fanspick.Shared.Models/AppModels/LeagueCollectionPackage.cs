﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.AppModels
{
    public class LeagueCollectionPackage
    {
        public String AccessToken { get; set; }

        public String CountryId { get; set; }
    }
}
