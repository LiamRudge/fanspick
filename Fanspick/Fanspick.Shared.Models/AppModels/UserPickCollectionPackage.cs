﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.AppModels
{
    public class UserPickCollectionPackage
    {
        public String AccessToken { get; set; }
        public String FixtureId { get; set; }
        public Boolean IsLive { get; set; }
        public String TeamId { get; set; }
    }
}
