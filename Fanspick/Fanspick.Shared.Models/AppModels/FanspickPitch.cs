﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.FanspickAPI.ResponseModels.YourPick;

namespace Fanspick.Shared.Models.AppModels
{
    public class FanspickPitch
    {
        public bool CollectedFromServer { get; set; }

        private Formation formation;
        public Formation Formation
        {
            get
            {
                return formation;
            }

            set
            {
                formation = value;

                for (int i = 0; i < formation.type.Length; i++)
                {
                    // add goalkeeper
                    if (i == 0)
                    {
                        PitchLine goalKeeperLine = new PitchLine();
                        PitchPlayer goalKeeper = new PitchPlayer();
                        goalKeeperLine.LinePlayers.Add(goalKeeper);
                        Lines.Add(goalKeeperLine);
                    }


                    PitchLine playerLine = new PitchLine();
                    int noPlayersInRow;
                    int.TryParse(formation.type[i].ToString(), out noPlayersInRow);
                    for (int a = 0; a < noPlayersInRow; a++)
                    {
                        PitchPlayer player = new PitchPlayer();
                        playerLine.LinePlayers.Add(player);
                    }
                    Lines.Add(playerLine);
                }
            }
        }

        public List<LineUpPlayer> Players { get; set; } = new List<LineUpPlayer>();

        public List<PitchLine> Lines { get; set; } = new List<PitchLine>();
    }
}
