﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.AppModels
{
    public class TeamCollectionPackage
    {
        public String AccessToken { get; set; }

        public String CompetitionId { get; set; }
    }
}
