﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.AppModels
{
    public class PitchPlayer
    {
        public String TeamShirt { get; set; }

        public Xamarin.Forms.ImageSource PlayerImage
        {
            get
            {
                if (!String.IsNullOrEmpty(TeamShirt))
                {
                    using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(TeamShirt)))
					{
						return Xamarin.Forms.ImageSource.FromStream(() => ms);
					}
                }
                else
                {
                    return Xamarin.Forms.ImageSource.FromResource("gk_grey_plus.png");
                }
            }
        }
    }
}
