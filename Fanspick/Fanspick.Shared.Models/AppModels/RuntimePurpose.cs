﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.AppModels
{
    public enum RuntimePurpose
    {
        Development,
        Live,
        UAT
    }
}
