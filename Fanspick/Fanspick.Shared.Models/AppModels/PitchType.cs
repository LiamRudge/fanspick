﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.AppModels
{
    public enum PitchType1
    {
        YourPick_Runup,
        YourPick_Prematch,
        YourPick_Match,
        ManagersPick,
        Fanspick_Runup,
        Fanspick_Match
    }
}
