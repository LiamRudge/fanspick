﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models
{
    public class OverAllTeamStats
    {
        public int ItemIndex { get; set; }

		[JsonProperty(PropertyName = "goalDifference")]
		public string GoalDifference { get; set; }

		[JsonProperty(PropertyName = "goalsConceded")]
		public string GoalsConceded { get; set; }

		[JsonProperty(PropertyName = "goalsScored")]
		public string GoalsScored { get; set; }

		[JsonProperty(PropertyName = "Losses")]
		public string Losses { get; set; }

		[JsonProperty(PropertyName = "Draws")]
		public string Draws { get; set; }

		[JsonProperty(PropertyName = "Wins")]
		public string Wins { get; set; }

		[JsonProperty(PropertyName = "GamesPlayed")]
		public string GamesPlayed { get; set; }
    }
}