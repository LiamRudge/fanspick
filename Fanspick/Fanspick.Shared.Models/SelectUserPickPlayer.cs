﻿using System;
namespace Fanspick.Shared.Models
{
    public class SelectUserPickPlayer
	{
		public string PositionId { get; set; }
        public string PosX { get; set; }
        public string PosY { get; set; }
        public string PlayerName { get; set; }
        public string PLayerId { get; set; }
        public string Action { get; set; }

	}
}
