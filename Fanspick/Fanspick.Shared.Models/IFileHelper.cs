﻿using System;
namespace Fanspick.Shared.Models
{
	public interface IFileHelper
	{
		string GetLocalFilePath(string filename);
	}
}
