﻿using System;
using Newtonsoft.Json;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace Fanspick.Shared.Models
{
    public class CommunityPosts
    {
        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "time")]
        public string Time { get; set; }

        [JsonProperty(PropertyName = "userId")]
        public User UserId { get; set; }

        [JsonIgnore]
        public string CurrentUserId { get; set; }

		[JsonIgnore]
		public string UserPic { get; set; }

        public bool ViewMyImage
		{
			get
			{
                bool isMyPic = false;
                if(this.UserPic !=null){
                    isMyPic = true;
                }

                return isMyPic;
				
			}
		}


		public Color color
		{
			get
			{
                var send = Color.FromRgb(220, 220, 223);
				
                return send;
			}
		}

		public Color colorBlue
		{
			get
			{
				var send = Color.FromRgb(49, 144, 220);

				return send;
			}
		}

        public LayoutOptions TimeOption
		{
			get
			{
				if (this.UserId.Sender_id == this.CurrentUserId)
				{
                    return LayoutOptions.End;
				}
				else
				{
                    return LayoutOptions.Start;
				}

			}
		}


        public bool VisibilityItemOne
        {
            get
            {
                if (this.UserId.Sender_id == this.CurrentUserId)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }

        public bool VisibilityItemTwo
        {
            get
            {
                if (this.UserId.Sender_id == this.CurrentUserId)
                {
                    return true;
                }

                return false;
            }

        }

		[JsonIgnore]
		public Boolean isConnect { get; set; }

		public bool isConnectedShow
		{
			get
			{
				if (isConnect)
				{
                    return false;
				}

                return true;
			}

		}

		

		public Thickness Margin
		{
			get
			{
                var size = new Thickness(20,0,0,0);
				String device = CrossDeviceInfo.Current.Model;
				if (device == "iPad")
				{
					size = new Thickness(70, 0, 0, 0);
				}
				return size;
			}
		}


        public double BackgroundImageSize
        {
            get{
                double size = 45;
                String device = CrossDeviceInfo.Current.Model;
                if (device == "iPad")
                {
                    size = 90;
                }
                return size;
            }
        }

		public double Textsize
		{
			get
			{
				double size = 10;
				String device = CrossDeviceInfo.Current.Model;
				if (device == "iPad")
				{
					size = 15;
				}
				return size;
			}
		}
		public double TextsizeTime
		{
			get
			{
				double size = 6;
				String device = CrossDeviceInfo.Current.Model;
				if (device == "iPad")
				{
					size = 10;
				}
				return size;
			}
		}

		public double CircleSize
		{
			get
			{
				double size = 35;
				String device = CrossDeviceInfo.Current.Model;
				if (device == "iPad")
				{
					size = 50;
				}
				return size;
			}
		}

        public String TimeToShow
        {
            get
            {
                String time = "";
                if (this.Time == null)
                {
                    time = "00:00";
                }
                else
                {
                    DateTime parsedFixtureDate = DateTime.Parse(this.Time);
                    TimeZoneInfo timeZoneInformation = TimeZoneInfo.Local;
                    DateTimeOffset timeOffset = new DateTimeOffset(parsedFixtureDate, timeZoneInformation.GetUtcOffset(parsedFixtureDate));
                    time = parsedFixtureDate.ToString("d MMM yyyy, HH: mm");
                }


                return time;
            }

        }

		public String OnlyTime
		{
			get
			{
				String time = "";
				if (this.Time == null)
				{
					time = "00:00";
				}
				else
				{
					DateTime parsedFixtureDate = DateTime.Parse(this.Time);
					TimeZoneInfo timeZoneInformation = TimeZoneInfo.Local;
					DateTimeOffset timeOffset = new DateTimeOffset(parsedFixtureDate, timeZoneInformation.GetUtcOffset(parsedFixtureDate));
					time = parsedFixtureDate.ToString("HH: mm");
				}


				return time;
			}

		}
    }


	public class User
	{
		[JsonProperty(PropertyName = "_id")]
		public string Sender_id { get; set; }

		[JsonProperty(PropertyName = "username")]
		public string Username { get; set; }

        public string UserAlphabet
		{
			get
			{
                if(this.Username == null){
                    return " ";
                }

                return this.Username.ToUpper()[0].ToString();
			}
		}


	}
}
