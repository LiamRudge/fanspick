﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Fanspick.Shared.Models
{
    public class DefaultTeamFavorite
	{

		[JsonProperty(PropertyName = "favouriteTeam")]
        public Team FavouriteTeam { get; set; }

		}
}


