﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.Service
{
    public class ServiceResponse
    {
        public ServiceResponse()
        {
            this.MessageStack = new List<string>();
        }
        public bool OK { get; set; }
        public List<String> MessageStack { get; set; }
        public dynamic Data { get; set; }
        public String ReasonPhrase { get; set; }
        public String signUpReasonPhrase { get; set; }


        public string InlineErrorMessage
        {
            get
            {
                string _errorMessage = string.Empty;
                string stringSeparator = "\n";
                if (this.MessageStack.Count() > 0)
                {
                    _errorMessage = this.MessageStack.Aggregate((i, j) => i + stringSeparator + j);
                }
                return _errorMessage;
            }
        }

    }
}
