﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.DeviceInfo;

namespace Fanspick.Shared.Models
{
	public class Topics
	{

		[JsonProperty(PropertyName = "_id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "fixturedate")]
		public string FixtureDate { get; set; }

		public String FixtureDateToShow
		{
			get
			{
				String time = "";
				if (this.FixtureDate == null)
				{
					time = " ";
				}
				else
				{
					DateTime parsedFixtureDate = DateTime.Parse(this.FixtureDate);
					TimeZoneInfo timeZoneInformation = TimeZoneInfo.Local;
					DateTimeOffset timeOffset = new DateTimeOffset(parsedFixtureDate, timeZoneInformation.GetUtcOffset(parsedFixtureDate));
					time = parsedFixtureDate.ToString("ddd, d MMM yyyy, HH:mm");
				}

				return time;
			}

		}


		public double CellHeight
		{
			get
			{

				double size = 150;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 250;
				}

				return size;
			}
		}

		public double TextSize
		{
			get
			{

				double size = 13;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 20;
				}

				return size;
			}
		}

	}
}


