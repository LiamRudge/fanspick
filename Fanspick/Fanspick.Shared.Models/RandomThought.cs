﻿using System;
using SQLite.Net.Attributes;

namespace Fanspick.Shared.Models
{
	public class RandomThought
	{
		[PrimaryKey]
        public String AccessToken { get; set; }
		public string SelectedTeamId { get; set; }
		public string FavoriteTeamId { get; set; }
        public string SelectedFixtureId { get; set; }

		public RandomThought()
		{
		}
	}
}