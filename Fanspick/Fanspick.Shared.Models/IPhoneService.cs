﻿using System;
using System.Collections.Generic;
using SQLite.Net;

namespace Fanspick.Shared.Models
{
	public interface IPhoneService
	{
		List<ContactModal> GetContacts();
	}
}