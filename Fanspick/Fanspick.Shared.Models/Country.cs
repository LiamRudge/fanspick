﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models
{
    public class Country
    {
        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "countryName")]
        public string CountryName { get; set; }

        [JsonProperty(PropertyName = "SportID")]
        public string SportId { get; set; }

        [JsonProperty(PropertyName = "competitionMini")]
        public List<CompetitionMini> CompetitionMini { get; set; }
    }
}






