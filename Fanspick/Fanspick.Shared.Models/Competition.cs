﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models
{   
    public class Competition
    {
        public Competition()
        {
            this.Teams = new List<Team>();
        }
        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "competitionFeedId")]
        public int CompetitionFeedId { get; set; }

        [JsonProperty(PropertyName = "yearOfStart")]
        public int YearOfStart { get; set; }

        [JsonProperty(PropertyName = "endDate")]
        public string EndDate { get; set; }

        [JsonProperty(PropertyName = "startDate")]
        public string StartDate { get; set; }

        [JsonProperty(PropertyName = "competitionFormatType")]
        public string CompetitionFormatType { get; set; }

        [JsonProperty(PropertyName = "isContinentalCup")]
        public bool IsContinentalCup { get; set; }

        [JsonProperty(PropertyName = "isInternationalCompetition")]
        public bool IsInternationalCompetition { get; set; }

        [JsonProperty(PropertyName = "pyramidStructureRank")]
        public int PyramidStructureRank { get; set; }

        [JsonProperty(PropertyName = "isNationalLeague")]
        public bool IsNationalLeague { get; set; }

        [JsonProperty(PropertyName = "countryId")]
        public string CountryId { get; set; }

        [JsonProperty(PropertyName = "sportId")]
        public object SportId { get; set; }

        [JsonProperty(PropertyName = "competitionName")]
        public string CompetitionName { get; set; }

        [JsonProperty(PropertyName = "teamMini")]
        public List<object> TeamMini { get; set; }

        [JsonProperty(PropertyName = "Teams")]
        public List<Team> Teams { get; set; }
    }
    

    //public class GetLeaguesForCountryModel
    //{
    //    public int statusCode { get; set; }
    //    public string message { get; set; }
    //    public List<Datum> data { get; set; }
    //}
}


