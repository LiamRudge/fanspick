﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models
{
    public interface ICountryCodes
    {
		List<CCode> DisplayCountryCodes(string input);
    }
}
