﻿﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Globalization;
using Plugin.DeviceInfo;

namespace Fanspick.Shared.Models
{

    public class PlayerDetails
    {
        [JsonProperty(PropertyName = "playerPos")]
        public string PlayerPos { get; set; }

        [JsonProperty(PropertyName = "playerName")]
        public string PlayerName { get; set; }

        [JsonProperty(PropertyName = "playerId")]
        public string PlayerId { get; set; }
    }

    public class Substitute
    {
        [JsonProperty(PropertyName = "role")]
        public string Role { get; set; }

        [JsonProperty(PropertyName = "playerPositionY")]
        public int PlayerPositionY { get; set; }

        [JsonProperty(PropertyName = "playerPositionX")]
        public int PlayerPositionX { get; set; }

        [JsonProperty(PropertyName = "playerName")]
        public string PlayerName { get; set; }

        [JsonProperty(PropertyName = "playerId")]
        public string PlayerId { get; set; }
    }

    public class FanspickTeam
    {
        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "imageURL")]
        public string ImageURL { get; set; }
    }

    public class TeamDetails
    {
        [JsonProperty(PropertyName = "summary")]
        public List<object> Summary { get; set; }

        [JsonProperty(PropertyName = "coaches")]
        public List<object> Coaches { get; set; }

        [JsonProperty(PropertyName = "stats")]
        public List<object> Stats { get; set; }

        [JsonProperty(PropertyName = "players")]
        public List<PlayerDetails> Players { get; set; }

        [JsonProperty(PropertyName = "substitutes")]
        public List<Substitute> Substitutes { get; set; }

        [JsonProperty(PropertyName = "substitutions")]
        public List<object> Substitutions { get; set; }

        [JsonProperty(PropertyName = "managerPick")]
        public List<object> ManagerPick { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "goals")]
        public object Goals { get; set; }

        [JsonProperty(PropertyName = "feedServerId")]
        public string FeedServerId { get; set; }

        [JsonProperty(PropertyName = "franspickTeamId")]
        public FanspickTeam FanspickTeam { get; set; }


    }

    public class Fixture
    {
		public string dateMsg = "";
		public string timeMsg = "";
		public string venueMsg = "";
		public string dateTimemsg = "";
		public string dateVenuemsg = "";
		public string dateTimeVenuemsg = "";
		public string timeVenuemsg = "";

		public Boolean Played
        {
            get
            {
                DateTime convertedFixtureDate = DateTime.Parse(this.FixtureDate);

                return (convertedFixtureDate.AddMinutes(120) <= DateTime.Now);
            }
        }

        public Double Intensity
        {
            get
            {
                if (Played)
                {
                    return 0.4;
                }
                else
                {
                    return 1;
                }
            }
        }

        public String FixtureName
        {
            get
            {
                return $"{LocalTeam.Name} vs {VisitorTeam.Name}";
            }
        }

		public String FixtureDisplayStatus
		{
			get
			{

				DateTime convertedFixtureDate = DateTime.Parse(this.FixtureDate);

				if (this.FixtureDate.Contains("1970"))
				{
					dateMsg = "Date not announced yet";
				}
				if (this.Time.Contains("TBA"))
				{
					timeMsg = "Time not announced yet";
				}
				if (this.Venue == "")
				{
					venueMsg = "Venue not announced yet";
				}
				if (this.FixtureDate.Contains("1970") && this.Time.Contains("TBA"))
				{
					dateTimemsg = "Date and time not announced yet";
				}

				if (this.FixtureDate.Contains("1970") && this.Venue == "" && this.Time.Contains("TBA"))
				{
					dateTimeVenuemsg = "Venue, date and time not announced yet";
				}
				if (!this.FixtureDate.Contains("1970") && this.Venue == "" && this.Time.Contains("TBA"))
				{
					timeVenuemsg = "Venue and time not announced yet";
				}
				if (this.FixtureDate.Contains("1970") && this.Venue == "" && !this.Time.Contains("TBA"))
				{
					dateVenuemsg = "Venue and date not announced yet";
				}

				if (convertedFixtureDate < DateTime.Now)
				{

					if (dateMsg != "" && timeMsg != "" && venueMsg != "")
					{
						return $"{dateTimeVenuemsg}";
					}
					else if (dateMsg != "" && timeMsg == "" && venueMsg != "")
					{
						return $"At {convertedFixtureDate.ToString("HH:mm")} , {dateVenuemsg}";
					}

					else if (dateMsg == "" && timeMsg != "" && venueMsg != "")
					{
						return $"On {convertedFixtureDate.ToString("ddd, d MMM yyyy")} , {timeVenuemsg}";
					}
					else if (dateMsg != "" && timeMsg != "" && venueMsg == "")
					{
						return $"Venue: {this.Venue} , {dateTimemsg}";
					}
					else if (dateMsg == "" && timeMsg == "" && venueMsg != "")
					{
						return $"On {convertedFixtureDate.ToString("ddd, d MMM yyyy, HH:mm")} , {venueMsg}";
					}
					else
					{
						return $"{convertedFixtureDate.ToString("ddd, d MMM yyyy, HH:mm")} at {this.Venue}";
					}
				}
				else if (DateTime.Now >= convertedFixtureDate && convertedFixtureDate <= DateTime.Now.AddMinutes(120))
				{
					return "Live";
				}
				else
				{


					if (dateMsg != "" && timeMsg != "" && venueMsg != "")
					{
						return $"{dateTimeVenuemsg}";
					}
					else if (dateMsg != "" && timeMsg == "" && venueMsg != "")
					{
						return $"At {convertedFixtureDate.ToString("HH:mm")} , {dateVenuemsg}";
					}

					else if (dateMsg == "" && timeMsg != "" && venueMsg != "")
					{
						return $"On {convertedFixtureDate.ToString("ddd, d MMM yyyy")} , {timeVenuemsg}";
					}
					else if (dateMsg != "" && timeMsg != "" && venueMsg == "")
					{
						return $"Venue: {this.Venue} , {dateTimemsg}";
					}
					else if (dateMsg == "" && timeMsg == "" && venueMsg != "")
					{
						return $"On {convertedFixtureDate.ToString("ddd, d MMM yyyy, HH:mm")} , {venueMsg}";
					}
					else
					{
						return $"{convertedFixtureDate.ToString("ddd, d MMM yyyy, HH:mm")} at {this.Venue}";
					}
				}
			}
		}


		public String FixtureDateForHeader
		{
			get
			{
                DateTime convertedFixtureDate = DateTime.Parse(this.FixtureDate);

				return $"Kickoff: {convertedFixtureDate.ToString("ddd, d MMM yyyy, HH:mm")}";

			}
		}
		public String FixtureDateToShow
        {
            get
            {
                DateTime convertedFixtureDate = DateTime.Parse(this.FixtureDate);

                return $"{convertedFixtureDate.ToString("dd/MM/yyyy HH:mm")}";

            }
        }

        public String FixtureDisplayScores
        {
            get
            {

                if(HomeTeamScore == null){
                    HomeTeamScore = "0";
                }
				if (AwayTeamScore == null)
				{
					AwayTeamScore = "0";
				}

                return $"{HomeTeamScore} - {AwayTeamScore}";
            }
        }



        public String FixtureDisplayLeague
        {
            get
            {
                return this.CompetitionName;
            }
        }

        public string LocalTeamImage
        {
			get
			{

                if (this.LocalTeam.FanspickTeam.ImageURL != null)
				{
					return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamLogos/" + this.LocalTeam.FanspickTeam.ImageURL;
				}
				else
				{
					return "no_image";
				}


			}

        }

        public string VisitorTeamImage
        {
			get
			{

                if (this.VisitorTeam.FanspickTeam.ImageURL != null)
				{
					return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamLogos/" + this.VisitorTeam.FanspickTeam.ImageURL;
				}
				else
				{
					return "no_image";
				}


			}
        }

        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "feedServerId")]
        public string FeedServerId { get; set; }

        [JsonProperty(PropertyName = "competitionName")]
        public string CompetitionName { get; set; }

        [JsonProperty(PropertyName = "fixtureType")]
        public string FixtureType { get; set; }

        [JsonProperty(PropertyName = "visitorTeam")]
        public TeamDetails VisitorTeam { get; set; }

        [JsonProperty(PropertyName = "localTeam")]
        public TeamDetails LocalTeam { get; set; }

        [JsonProperty(PropertyName = "competitionId")]
        public string CompetitionId { get; set; }

        [JsonProperty(PropertyName = "countryId")]
        public string CountryId { get; set; }

        [JsonProperty(PropertyName = "static_id")]
        public string StaticId { get; set; }

        [JsonProperty(PropertyName = "venue_city")]
        public string VenueCity { get; set; }

        [JsonProperty(PropertyName = "venue_id")]
        public string VenueId { get; set; }

        [JsonProperty(PropertyName = "venue")]
        public string Venue { get; set; }

        [JsonProperty(PropertyName = "matchStatus")]
        public string MatchStatus { get; set; }

        [JsonProperty(PropertyName = "time")]
        public string Time { get; set; }

        [JsonProperty(PropertyName = "date")]
        public string Date { get; set; }

        [JsonProperty(PropertyName = "homeTeamScore")]
        public string HomeTeamScore { get; set; }

        [JsonProperty(PropertyName = "awayTeamScore")]
        public string AwayTeamScore { get; set; }

        [JsonProperty(PropertyName = "feedDate")]
        public string FeedDate { get; set; }

        private string fixtureDate;
        [JsonProperty(PropertyName = "fixtureDate")]

        public string FixtureDate
        {
            get
            {
                try
                {
                    DateTime parsedFixtureDate = DateTime.Parse(fixtureDate);
                    TimeZoneInfo timeZoneInformation = TimeZoneInfo.Local;
                    DateTimeOffset timeOffset = new DateTimeOffset(parsedFixtureDate, timeZoneInformation.GetUtcOffset(parsedFixtureDate));
                    string newTime = parsedFixtureDate.ToString("ddd dd MMM yyyy h:mm tt");
                    return newTime;
                }
                catch (Exception ex)
                {
                    return fixtureDate;
                }
            }
            set { fixtureDate = value; }
        }

        public string FixtureImage
        {

            get
            {
                string image = "";
                string fixtureType = this.CompetitionName;

               
                if (fixtureType == "FA Cup")
                {
                    image = "icon_cup_large";
                }
                else if (fixtureType == "UEFA Champions League")
                {
                    image = "icon_world_large";
                }
				else if (fixtureType == "UEFA Europa League")
				{
					image = "icon_world_large";
				}
                else
				{
					image = "icon_ball_large";
				}

                return image;
            }
        }


        public double CellHeight
		{

			get
			{
                double height = 120;
                    String device = CrossDeviceInfo.Current.Model;

                    if (device == "iPad")
                    {
                        height = 180;
                    }

                return height;
			}
		}

		public double ImageHeight
		{

			get
			{
				double height = 100;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					height = 180;
				}

				return height;
			}
		}


        public GridLength BlockHeight1
		{
			get
			{
                var height = new GridLength(40);
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					height = new GridLength(50);
				}

				return height;
			}
		}

		public GridLength BlockHeight2
		{
			get
			{
				var height = new GridLength(40);
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					height = new GridLength(80);
				}

				return height;
			}
		}

		public GridLength BlockHeight3
		{
			get
			{
				var height = new GridLength(40);
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					height = new GridLength(50);
				}

				return height;
			}
		}

		public double FontLine1
		{

			get
			{
				double height = 12;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					height = 16;
				}

				return height;
			}
		}

		public double FontLine2
		{

			get
			{
				double height = 10;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					height = 15;
				}

				return height;
			}
		}

		public double FontLine3
		{

			get
			{
				double height = 13;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					height = 18;
				}

				return height;
			}
		}

		public int ItemIndex
		{
			get;
			set;
		}

		public LayoutOptions ShiftImage
		{

			get
			{

                if(val){
					val = false;
                    return LayoutOptions.Start;

                }else{
                    val = true;
                    return LayoutOptions.End;

                }

			}
		}

        public bool val
		{
			get;
			set;
		}


       


	}
}