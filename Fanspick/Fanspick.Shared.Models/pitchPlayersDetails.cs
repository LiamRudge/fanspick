﻿using System;
using System.Net;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Fanspick.Shared.Models
{
    public class pitchPlayersDetails
    {
        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }



        [JsonProperty(PropertyName = "knownName")]
        public string Name { get; set; }

        public ImageSource shirt { get; set; }
    }
}
