﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Fanspick.Shared.Models
{
    public interface IScreenCapture
    {
        Task<bool> Share(string subject, string message, ImageSource image);
    }
}
