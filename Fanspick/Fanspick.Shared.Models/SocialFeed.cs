﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.DeviceInfo;

namespace Fanspick.Shared.Models
{
    public class SocialFeed
    {
        [JsonProperty(PropertyName = "created_at")]
        public string Created { get; set; }

        [JsonProperty(PropertyName = "refrer")]
        public string Refrer { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [JsonIgnore]
        public HtmlWebViewSource HTMLText
        {
            get
            {
                return new HtmlWebViewSource() { Html = this.Text.Replace("color:fff", "color:#000") };
            }
        }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "hashtags")]
        public List<string> Hashtags { get; set; }

        [JsonProperty(PropertyName = "expanded_url")]
        public string Expanded_URL { get; set; }

        [JsonProperty(PropertyName = "is_media")]
        public bool IsMedia { get; set; }

        [JsonProperty(PropertyName = "media")]
        public SocialMedia Media { get; set; }

        [JsonProperty(PropertyName = "username")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "userpic")]
        public string UserPic { get; set; }


		public double Height
		{

			get
			{
				double height = 175;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					height = 370;
				}

				return height;
			}
		}



		public double FontSize
		{

			get
			{
				double height = 11;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					height = 16;
				}

				return height;
			}
		}


        [JsonProperty(PropertyName = "timestamp")]
        public string Timestamp { get; set; }

        [JsonProperty(PropertyName = "ago")]
        public DateTime Ago { get; set; }

        [JsonIgnore]
        public bool IsVisibleTwitterIcon
        {
            get { return this.Refrer == "twitter"; }
        }

        [JsonIgnore]
        public bool IsVisibleFBIcon
        {
            get { return this.Refrer == "facebook"; }
        }

        [JsonIgnore]
        public LayoutOptions HorizontalOption { get; set; }


    }

    public class SocialMedia
    {
        [JsonProperty(PropertyName = "url")]
        public string URL { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
    }

}
