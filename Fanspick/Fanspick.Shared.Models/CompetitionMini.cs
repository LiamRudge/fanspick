﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models
{
    public class CompetitionMini
    {
        [JsonProperty(PropertyName = "CompetitionId")]
        public string competitionId { get; set; }

        [JsonProperty(PropertyName = "competitionFixtureFeedUrl")]
        public string CompetitionFixtureFeedUrl { get; set; }

        [JsonProperty(PropertyName = "competitionStandingsFeedUrl")]
        public string CompetitionStandingsFeedUrl { get; set; }

        [JsonProperty(PropertyName = "competitionCommentaryFeedUrl")]
        public string CompetitionCommentaryFeedUrl { get; set; }

        [JsonProperty(PropertyName = "pyramidStructureRank")]
        public string PyramidStructureRank { get; set; }

        [JsonProperty(PropertyName = "isNationalLeague")]
        public string IsNationalLeague { get; set; }

        [JsonProperty(PropertyName = "isInternationalCompetition")]
        public string IsInternationalCompetition { get; set; }

        [JsonProperty(PropertyName = "isContinentalCup")]
        public string IsContinentalCup { get; set; }

        [JsonProperty(PropertyName = "competitionFormatType")]
        public string CompetitionFormatType { get; set; }
    }
}
