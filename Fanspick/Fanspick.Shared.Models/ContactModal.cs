﻿using System;
namespace Fanspick.Shared.Models
{
    public class ContactModal
    {
		public string Name { get; set; }

        public string PhoneNumber { get; set; }

    }
}
