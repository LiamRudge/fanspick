﻿﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Fanspick.Shared.Models
{
    public class Player
    {
        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "knownName")]
        public string KnownName
        {
            get
            {
                return WebUtility.HtmlDecode(decodedKnownName);
            }
            set
            { 
                decodedKnownName = WebUtility.HtmlDecode(value);
            }
        }
		public string color
		{
			get;
			set;
		}

		public string PosColor
		{
			get;
			set;
		}

		public int ItemIndex
		{
			get;
			set;
		}

        private string decodedKnownName;

        [JsonProperty(PropertyName = "team")]
        public string TeamName { get; set; }

        [JsonProperty(PropertyName = "imageURL")]
        public string ImageURL { get; set; }

        [JsonProperty(PropertyName = "firstname")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "lastname")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "nationality")]
        public string Nationality { get; set; }

        [JsonProperty(PropertyName = "birthdate")]
        public string Birthdate { get; set; }

        [JsonProperty(PropertyName = "position")]
        public string Position { get; set; }


		public string PositionShow
		{
			get
			{
				return "Posiiton: " + this.Position;
			}
		}

		public string PlaceShow
		{
			get
			{
                return "Birthplace: " + this.Birthplace +","+this.Birthcountry;
			}
		}


		[JsonProperty(PropertyName = "height")]
        public string HeightRecieved { get; set; }

		[JsonProperty(PropertyName = "weight")]
		public string WeightRecieved { get; set; }

        [JsonProperty(PropertyName = "age")]
        public string AgeRecieved { get; set; }

		[JsonProperty(PropertyName = "yellowredTotal")]
		public int YellowredTotal { get; set; }

        [JsonProperty(PropertyName = "redcardsTotal")]
        public int RedCardsRecieved { get; set; }

        [JsonProperty(PropertyName = "yellowcardsTotal")]
        public int YellowCardsRecieved { get; set; }

        [JsonProperty(PropertyName = "goalsTotal")]
        public int Goals { get; set; }

		[JsonProperty(PropertyName = "goals")]
		public int GoalsPlayer { get; set; }

        [JsonProperty(PropertyName = "birthplace")]
        public string Birthplace { get; set; }

        [JsonProperty(PropertyName = "birthcountry")]
        public string Birthcountry { get; set; }

        [JsonProperty(PropertyName = "userActions")]
        public List<UserActions> userActions { get; set; } = new List<UserActions>();

		[JsonProperty(PropertyName = "substitutes_on_benchTotal")]
		public int Substitutes_on_benchTotal { get; set; }

		[JsonProperty(PropertyName = "substitute_outTotal")]
		public int Substitute_outTotal { get; set; }

		[JsonProperty(PropertyName = "substitute_inTotal")]
		public int Substitute_inTotal { get; set; }

		[JsonProperty(PropertyName = "appearencesTotal")]
		public int AppearencesTotal { get; set; }

		[JsonProperty(PropertyName = "lineups")]
		public int Lineups { get; set; }

		[JsonProperty(PropertyName = "minutesTotal")]
		public int MinutesTotal { get; set; }

		public class UserActions
        {
            [JsonProperty(PropertyName = "action")]
            public string Action { get; set; }
        }


        public string PlayerImage
        {
            get
            {

                if(this.ImageURL==null){
                    return "no_image";

                }else{
                    return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/playerImages/" + this.ImageURL;
                }


            }
        }

		public int PlayerImageThickness
		{
			get
			{

				if (this.ImageURL == null)
				{
					return 0;

				}
				else
				{
					return 5;
				}


			}
		}




        public string PlayerWeight{
            get{

                string weight = "Weight: N/A";

                if(this.WeightRecieved != null){
                    weight = "Weight: "+ this.WeightRecieved ;
                }

                return weight;
            }
        }

		public string PlayerHeight
		{
			get
			{

				string height = "Height: N/A";

                if (this.HeightRecieved != null)
				{
					height = "Height: "+this.HeightRecieved;
				}

				return height;
			}
		}

		public string Age
		{
			get
			{
                string age = "Age: "+this.AgeRecieved + " Yrs";

				return age;
			}
		}

		public string RedCards
		{
			get
			{
                string age = "Red Cards:" + this.RedCardsRecieved;
				return age;
			}
		}

		public string YellowCards
		{
			get
			{
                string age = "Yellow Cards:" +this.YellowCardsRecieved;

				return age;
			}
		}




	}
}

//         "statistics":[
//            {  
//               "redcards":0,
//               "yellowred":0,
//               "yellowcards":0,
//               "goals":0,
//               "substitutes_on_bench":0,
//               "substitute_out":0,
//               "substitute_in":0,
//               "lineups":0,
//               "appearences":0,
//               "minutes":0,
//               "competitionId":"596887db71f4e76ba53ef492",
//               "teamId":"5968959971f4e76ba53ef4b3",
//               "season":"2017/2018",
//               "competitonFeedId":"1204",
//               "competitionName":"Premier League",
//               "teamFeedId":"9406",
//               "teamName":"Tottenham Hotspur"
//            }
//         ]
//      }
//   ]
//}"