﻿using System;
using SQLite.Net;

namespace Fanspick.Shared.Models
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();
	}
}