﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.FanspickAPI.ResponseModels.Fixtures
{
    public class FranspickTeamId
    {
        public string _id { get; set; }
        public string teamLogo { get; set; }
    }

    public class VisitorTeam
    {
        public FranspickTeamId franspickTeamId { get; set; }
        public string feedServerId { get; set; }
        public string name { get; set; }
    }

    public class FranspickTeamId2
    {
        public string _id { get; set; }
        public string teamLogo { get; set; }
    }

    public class LocalTeam
    {
        public FranspickTeamId2 franspickTeamId { get; set; }
        public string feedServerId { get; set; }
        public string name { get; set; }
    }

    public class Datum
    {
        public string _id { get; set; }
        public string feedServerId { get; set; }
        public string competitionName { get; set; }
        public string fixtureType { get; set; }
        public VisitorTeam visitorTeam { get; set; }
        public LocalTeam localTeam { get; set; }
        public string competitionId { get; set; }
        public string countryId { get; set; }
        public string static_id { get; set; }
        public string venue_city { get; set; }
        public string venue_id { get; set; }
        public string venue { get; set; }
        public string matchStatus { get; set; }
        public string time { get; set; }
        public string date { get; set; }
        public int __v { get; set; }
        public string feedDate { get; set; }
        public string fixtureDate { get; set; }
    }

    public class AllFixtureModel
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public List<Datum> data { get; set; }
    }
}
