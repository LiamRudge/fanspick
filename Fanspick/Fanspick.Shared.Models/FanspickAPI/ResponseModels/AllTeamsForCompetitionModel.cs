﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Fanspick.Shared.Models.FanspickAPI.ResponseModels.Teams
{
    public class Datum
    {
        public string _id { get; set; }
        public string coachName { get; set; }
        public string knownName { get; set; }
        public string sportId { get; set; }
        public string countryId { get; set; }
        public string teamLogo { get; set; }
        public int __v { get; set; }
        public string founded { get; set; }
        public string country { get; set; }
        public string fullName { get; set; }
        public bool isNationalTeam { get; set; }
        public List<Models.FanspickAPI.ResponseModels.Leagues.Datum> RelatedLeagues { get; set; } = new List<Leagues.Datum>();


        public Xamarin.Forms.ImageSource teamLogoAsImage
        {
            get
            {

                return GetImage();
            }
        }

        private ImageSource GetImage()
        {
            ImageSource imageSource = null;
            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(this.teamLogo)))
            {
                return imageSource = Xamarin.Forms.ImageSource.FromStream(()=>ms);
            }
        }
    }

    public class AllTeamsForCompetitionModel
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public List<Datum> data { get; set; }
    }
}
