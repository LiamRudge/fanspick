﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.FanspickAPI.ResponseModels.Country
{

    public class CompetitionMini
    {
        public string competitionFixtureFeedUrl { get; set; }
        public string competitionStandingsFeedUrl { get; set; }
        public string competitionCommentaryFeedUrl { get; set; }
        public int pyramidStructureRank { get; set; }
        public bool isNationalLeague { get; set; }
        public bool isContinentalCup { get; set; }
        public bool isInternationalCompetition { get; set; }
        public string competitionFormatType { get; set; }
        public string competitionId { get; set; }
    }

    public class Datum
    {
        public string _id { get; set; }
        public string countryName { get; set; }
        public string SportID { get; set; }
        public List<CompetitionMini> competitionMini { get; set; }
    }

    public class GetCountriesForSportModel
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public List<Datum> data { get; set; }
    }
}
