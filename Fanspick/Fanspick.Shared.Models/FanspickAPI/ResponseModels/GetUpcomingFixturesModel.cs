﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.FanspickAPI.ResponseModels.UpcomingFixtures
{
    public class LivePlayer
    {
        public string playerPos { get; set; }
        public string playerName { get; set; }
        public string playerId { get; set; }
    }

    public class Substitute
    {
        public string role { get; set; }
        public int playerPositionY { get; set; }
        public int playerPositionX { get; set; }
        public string playerName { get; set; }
        public string playerId { get; set; }
    }

    public class FranspickTeamId
    {
        public string _id { get; set; }
        public string teamLogo { get; set; }
    }

    public class VisitorTeam
    {
        public List<object> summary { get; set; }
        public List<object> coaches { get; set; }
        public List<object> stats { get; set; }
        public List<LivePlayer> livePlayers { get; set; }
        public List<Substitute> substitutes { get; set; }
        public List<object> substitutions { get; set; }
        public List<object> managerPick { get; set; }
        public string name { get; set; }
        public object goals { get; set; }
        public string feedServerId { get; set; }
        public FranspickTeamId franspickTeamId { get; set; }
    }

    public class LivePlayer2
    {
        public string playerPos { get; set; }
        public string playerName { get; set; }
        public string playerId { get; set; }
    }

    public class Substitute2
    {
        public string role { get; set; }
        public int playerPositionY { get; set; }
        public int playerPositionX { get; set; }
        public string playerName { get; set; }
        public string playerId { get; set; }
    }

    public class FranspickTeamId2
    {
        public string _id { get; set; }
        public string teamLogo { get; set; }
    }

    public class LocalTeam
    {
        public List<object> summary { get; set; }
        public List<object> coaches { get; set; }
        public List<object> stats { get; set; }
        public List<LivePlayer2> livePlayers { get; set; }
        public List<Substitute2> substitutes { get; set; }
        public List<object> substitutions { get; set; }
        public List<object> managerPick { get; set; }
        public string name { get; set; }
        public object goals { get; set; }
        public string feedServerId { get; set; }
        public FranspickTeamId2 franspickTeamId { get; set; }
    }

    public class Datum
    {
        public Boolean Played
        {
            get
            {
                DateTime convertedFixtureDate = DateTime.Parse(this.fixtureDate);

                return (convertedFixtureDate.AddMinutes(120) <= DateTime.Now);
            }
        }

        public Double Intensity
        {
            get
            {
                if (Played)
                {
                    return 0.4;
                }
                else
                {
                    return 1;
                }
            }
        }

        public String fixtureName
        {
            get
            {
                return $"{localTeam.name} vs {visitorTeam.name}";
            }
        }

        public String fixtureDisplayStatus
        {
            get
            {
                DateTime convertedFixtureDate = DateTime.Parse(this.fixtureDate);

                if (convertedFixtureDate < DateTime.Now)
                {
                    return $"{convertedFixtureDate.ToString("dd/MM/yyyy HH:mm")} at {this.venue}";
                }
                else if (DateTime.Now >= convertedFixtureDate && convertedFixtureDate <= DateTime.Now.AddMinutes(120))
                {
                    return "Live";
                }
                else
                {
                    return $"{convertedFixtureDate.ToString("dd/MM/yyyy HH:mm")} at {this.venue}";
                }
            }
        }

        public String fixtureDisplayScores
        {
            get
            {
                return $"{homeTeamScore} - {awayTeamScore}";
            }
        }

        public String fixtureDisplayLeague
        {
            get
            {
                return this.competitionName;
            }
        }

        public Xamarin.Forms.ImageSource localTeamImage
        {
            get
            {
				using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(this.localTeam.franspickTeamId.teamLogo)))
				{
					return Xamarin.Forms.ImageSource.FromStream(() => ms);
				}
            }
        }

        public Xamarin.Forms.ImageSource visitorTeamImage
        {
            get
            {
                using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(this.visitorTeam.franspickTeamId.teamLogo)))
				{
					return Xamarin.Forms.ImageSource.FromStream(() => ms);
				}
            }
        }

        public string _id { get; set; }
        public string feedServerId { get; set; }
        public string competitionName { get; set; }
        public string fixtureType { get; set; }
        public VisitorTeam visitorTeam { get; set; }
        public LocalTeam localTeam { get; set; }
        public string competitionId { get; set; }
        public string countryId { get; set; }
        public string static_id { get; set; }
        public string venue_city { get; set; }
        public string venue_id { get; set; }
        public string venue { get; set; }
        public string matchStatus { get; set; }
        public string time { get; set; }
        public string date { get; set; }
        public int __v { get; set; }
        public int homeTeamScore { get; set; }
        public int awayTeamScore { get; set; }
        public string feedDate { get; set; }
        public string fixtureDate { get; set; }
        public string convertedDate { get; set; }
    }

    public class GetUpcomingFixturesModel
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public List<Datum> data { get; set; }
    }
}
