﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.FanspickAPI.ResponseModels.YourPick
{
    public class Formation
    {
        public string _id { get; set; }
        public string activeImage { get; set; }
        public string disableImage { get; set; }
        public string type { get; set; }
        public bool isActive { get; set; }
        public bool isDeleted { get; set; }
        public Formation Empty
        {
            get
            {
                return new Formation() { type = "4321" };
            }
        }
    }

    public class PositionId
    {
        public string _id { get; set; }
        public int PosX { get; set; }
        public int PosY { get; set; }
        public string Role { get; set; }
        public string Key { get; set; }
    }

    public class PlayerId
    {
        public string _id { get; set; }
        public string knownName { get; set; }
    }

    public class LineUpPlayer
    {
        public PositionId positionId { get; set; }
        public PlayerId playerId { get; set; }
        public string _id { get; set; }
    }

    public class Position
    {
        public string _id { get; set; }
        public int PosX { get; set; }
        public int PosY { get; set; }
        public string Role { get; set; }
        public string Key { get; set; }
    }

    public class Data
    {
        public Formation formation { get; set; }
        public List<LineUpPlayer> lineUpPlayers { get; set; }
        public List<Position> positions { get; set; }
    }

    public class GetUserPickModel
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public Data data { get; set; }
    }
}
