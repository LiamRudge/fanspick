﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.FanspickAPI.ResponseModels.Account
{
    public class UserDetails
    {
        public int __v { get; set; }
        public string createdOn { get; set; }
        public string loggedInOn { get; set; }
        public string emailId { get; set; }
        public string username { get; set; }
        public string gender { get; set; }
        public string deviceType { get; set; }
        public string deviceToken { get; set; }
        public string appVersion { get; set; }
        public string passwordHash { get; set; }
        public string _id { get; set; }
        public bool active { get; set; }
        public string updatedOn { get; set; }
        public List<object> ageVerification { get; set; }
        public List<object> communityId { get; set; }
        public List<object> defaultTeam { get; set; }
        public List<object> teamFavourite { get; set; }
        public bool deleteByAdmin { get; set; }
        public string loginType { get; set; }
        public string onceLogin { get; set; }
        public bool rejection { get; set; }
        public int profileComplete { get; set; }
    }

    public class Data
    {
        public string accessToken { get; set; }
        public UserDetails userDetails { get; set; }
    }

    public class RegisterModel
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public Data data { get; set; }
    }
}
