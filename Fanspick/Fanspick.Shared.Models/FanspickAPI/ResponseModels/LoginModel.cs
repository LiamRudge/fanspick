﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.FanspickAPI.ResponseModels.Login
{
    public class DefaultTeam
    {
        public string favouriteTeam { get; set; }
        public string favouriteTeamCountry { get; set; }
        public string _id { get; set; }
    }

    public class UserDetails
    {
        public string _id { get; set; }
        public object fcmId { get; set; }
        public string emailId { get; set; }
        public string lat { get; set; }
        public string @long { get; set; }
        public string username { get; set; }
        public string gender { get; set; }
        public bool active { get; set; }
        public List<object> ageVerification { get; set; }
        public List<object> communityId { get; set; }
        public List<DefaultTeam> defaultTeam { get; set; }
        public List<object> teamFavourite { get; set; }
        public bool deleteByAdmin { get; set; }
        public string loginType { get; set; }
        public bool rejection { get; set; }
        public int profileComplete { get; set; }
    }

    public class Data
    {
        public string accessToken { get; set; }
        public UserDetails userDetails { get; set; }
    }

    public class LoginModel
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public Data data { get; set; }
    }
}
