﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.FanspickAPI.ResponseModels.Leagues
{
    public class Datum
    {
        public string _id { get; set; }
        public int competitionFeedId { get; set; }
        public int yearOfStart { get; set; }
        public string endDate { get; set; }
        public string startDate { get; set; }
        public string competitionFormatType { get; set; }
        public bool isContinentalCup { get; set; }
        public bool isInternationalCompetition { get; set; }
        public int pyramidStructureRank { get; set; }
        public bool isNationalLeague { get; set; }
        public string countryId { get; set; }
        public object sportId { get; set; }
        public string competitionName { get; set; }
        public int __v { get; set; }
        public List<object> teamMini { get; set; }
    }

    public class GetLeaguesForCountryModel
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public List<Datum> data { get; set; }
    }
}
