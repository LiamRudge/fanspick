﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.FanspickAPI
{
    public class FanspickAPIResponse
    {
        public dynamic Data { get; set; }
        public String DataAsJsonString { get; set; }
        public Boolean OK { get; set; }
        public String ReasonPhrase { get; set; }
    }
}
