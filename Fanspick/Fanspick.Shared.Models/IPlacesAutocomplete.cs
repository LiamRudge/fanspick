﻿﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models
{
    public interface IPlacesAutocomplete
    {
		Task<Predictions> GetAutocomplete(string s, string c);
		List<string> PostalCodes(string c, string ci, string i);
       
     
    }
}
