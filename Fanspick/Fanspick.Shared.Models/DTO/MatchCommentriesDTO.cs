﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class MatchCommentriesDTO
	{
		public class RequestMatchCommentriesDTO
		{
			[JsonIgnore]
			[JsonProperty(PropertyName = "accessToken")]
			public String AccessToken { get; set; }

			[JsonProperty(PropertyName = "fixtureId")]
			public String FixtureId { get; set; }

		}


		public class ResponseMatchCommentries : ResponseBaseDTO
		{
			[JsonProperty(PropertyName = "data")]
			public ResponseMatchCommentriesData Data { get; set; } 
		}

		public class ResponseMatchCommentriesData
		{
            [JsonProperty(PropertyName = "commentaries")]
            public List<Commentry> Commentry { get; set; } = new List<Commentry>();

			[JsonProperty(PropertyName = "matchStatus")]
			public String MatchStatus { get; set; }

			[JsonProperty(PropertyName = "statusTime")]
			public String StatusTime { get; set; }
		}

	}
}