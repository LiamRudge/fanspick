﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetUpcomingFixturesDTO
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "accessToken")]
        public String AccessToken { get; set; }

        [JsonProperty(PropertyName = "teamId")]
        public String TeamId { get; set; }
    }

    public class RequestGetUpcomingFixturesRefitDTO
	{
		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }
	}

    public class ResponseGetUpcomingFixturesDTO: ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public List<Fixture> Data { get; set; }
    }
}
