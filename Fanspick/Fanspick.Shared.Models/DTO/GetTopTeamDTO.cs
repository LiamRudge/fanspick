﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace Fanspick.Shared.Models.DTO
{
	public class GetTopTeamDTO
	{

		public class RequestGetTopTeamDTO
		{
			[JsonIgnore]
			[JsonProperty(PropertyName = "accessToken")]
			public String AccessToken { get; set; }

			[JsonIgnore]
			[JsonProperty(PropertyName = "competitionId")]
			public String CompetitionId { get; set; }

		}


		public class ResponseGetTopTeamDTO : ResponseBaseDTO
		{
			[JsonProperty(PropertyName = "data")]
            public List<ResponseGetTopTeamData> Data { get; set; } = new List<ResponseGetTopTeamData>();
		}

		public class ResponseGetTopTeamData
		{

			[JsonProperty(PropertyName = "_id")]
			public string Id { get; set; }

			[JsonProperty(PropertyName = "teamMini")]
            public TeamStats Stats { get; set; }

            public int ItemIndex
            {
                get;
                set;
            }
			public string color
			{
				get;
				set;
			}

			public double TextSize
			{
				get
				{
					double cell = 10;

					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						cell = 15;
					}

					return cell;
				}
			}

			public double ImageSize
			{
				get
				{
					double cell = 36;

					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						cell = 55;
					}

					return cell;
				}
			}


			public GridLength DivLength2
			{
				get
				{
                    var cell = new GridLength(45);
					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
					  cell = new GridLength(60);
					}

					return cell;
				}
			}

            public Thickness ImageMargin
			{
				get
				{
                    var cell = new Thickness(5);
					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
                        cell = new Thickness(15);
					}

					return cell;
				}
			}

			public GridLength DivLength3
			{
				get
				{
					var cell = new GridLength(70);
					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						cell = new GridLength(110);
					}

					return cell;
				}
			}


		}

	}
}