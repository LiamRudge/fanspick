﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetUserPickPercentageDTO
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "accessToken")]
        public String AccessToken { get; set; }

        [JsonProperty(PropertyName = "fixtureId")]
        public String FixtureId { get; set; }


        [JsonProperty(PropertyName = "teamId")]
        public String TeamId { get; set; }
    }

    public class ResponseGetUserPickPercentageDTO : ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public ResponsePickPecentageData Data { get; set; }
    }   

    public class ResponsePickPecentageData
    {

		[JsonProperty(PropertyName = "percentage")]
		public String Percentage { get; set; }
    }
}//{"statusCode":200,"message":"Success","data":9}