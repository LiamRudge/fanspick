﻿using Newtonsoft.Json;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestGetTopicByFixtureDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }
	}

	public class ResponseGetTopicByFixtureDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseGetTopicByFixtureData Data { get; set; }
	}

	public class ResponseGetTopicByFixtureData
	{

	}


}