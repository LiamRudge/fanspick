﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestDeleteMessageDTO
	{

		[JsonProperty(PropertyName = "groupId")]
		public string GroupId { get; set; }

		[JsonProperty(PropertyName = "type")]
		public string Type { get; set; } //All

	}

	public class RequestDeleteMessageArrayDTO
	{
		[JsonProperty(PropertyName = "groupId")]
		public string GroupId { get; set; }

		[JsonProperty(PropertyName = "type")]
		public string Type { get; set; } //Selected

		[JsonProperty(PropertyName = "messageIds")]
		public string MessageIds { get; set; }
	}

	public class ResponseDeleteMessageDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseAgeVerificationData Data { get; set; }
	}

	public class ResponseDeleteMessageData
	{
		// api returns null
	}
}