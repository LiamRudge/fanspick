﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetBillBoardDTO
	{
	
		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "sponsorImageType")]
		public String SponsorImageType { get; set; }
        // Billboard
        //Pitch
	}

	public class ResponseGetBillBoardDTO : ResponseBaseDTO
	{
        [JsonProperty(PropertyName = "data")]
        public List<ResponseGetBillBoardData> Data { get; set; } = new List<ResponseGetBillBoardData>();
	}

	public class ResponseGetBillBoardData
	{
        [JsonProperty(PropertyName = "_id")]
        public String BillBoardId { get; set; }

		[JsonProperty(PropertyName = "sponsorName")]
		public String SponsorName { get; set; }

		[JsonProperty(PropertyName = "banner")]
		public String BillBoardBanner { get; set; }

		[JsonProperty(PropertyName = "link")]
		public String BillBoardLink { get; set; }

		[JsonProperty(PropertyName = "image")]
		public String BillBoardImage { get; set; }

		//http://fanspick-admin-test.5sol.co.uk/SponsorUploads/
		// banner WnARtt4-Sign.jpg

		// api returns null
	}
}
