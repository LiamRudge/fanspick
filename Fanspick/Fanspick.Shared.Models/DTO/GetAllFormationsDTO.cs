﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetFormationByTypeDTO
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "accessToken")]
        public String AccessToken { get; set; }

        [JsonProperty(PropertyName = "type")]
        public String Type { get; set; }

    }

    public class RequestGetFormationByTypeRefitDTO
	{
		[JsonProperty(PropertyName = "type")]
		public String Type { get; set; }

	}

    public class ResponseGetFormationByTypeDTO : ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public ResponsePickData Data { get; set; }
    }

}
