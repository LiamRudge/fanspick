﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Plugin.DeviceInfo;

namespace Fanspick.Shared.Models.DTO
{
    public class MyCommunityDTO
    {
        public class RequestMyCommunityDTO : RequestBaseDTO
        {
            [JsonIgnore]
            [JsonProperty(PropertyName = "accessToken")]
            public String AccessToken { get; set; }

            [JsonProperty(PropertyName = "teamId")]
            public string TeamId { get; set; }

        }

    }

    public class RequestMyCommunityRefitDTO : RequestBaseDTO
	{
		
		[JsonProperty(PropertyName = "teamId")]
		public string TeamId { get; set; }

	}

		public class ResponseMyCommunityDTO : ResponseBaseDTO
		{
			[JsonProperty(PropertyName = "data")]
            public List<ResponseMyCommunityData> Data { get; set; } = new List<ResponseMyCommunityData>();
		}

		public class ResponseMyCommunityData
		{
			[JsonProperty(PropertyName = "_id")]
			public string Id { get; set; }

			[JsonProperty(PropertyName = "Name")]
			public string CommunityName { get; set; }

			[JsonProperty(PropertyName = "topics")]
			public List<Topics> Data { get; set; }

			public string NumberPost
			{
				get
				{
                    return this.Data.Count.ToString();
				}
			}

            public double CellHeight
			{
				get
				{

                    double size = 180;
					String device = CrossDeviceInfo.Current.Model;

                    if (device == "iPad")
                    {
                        size = 350;
                    }

					return size;
				}
			}

       public double LineFont1
			{
				get
				{

                    double size = 9;
                    String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						size = 16;
					}

					return size;
				}
			}

			public double LineFont2
			{
				get
				{

					double size = 12;
					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						size = 22;
					}

					return size;
				}
			}

			public double LineFont3
			{
				get
				{

					double size = 10;
					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						size = 18;
					}

					return size;
				}
			}


		}

}