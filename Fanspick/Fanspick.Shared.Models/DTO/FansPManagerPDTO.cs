﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestFansPManagerPDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

	}

	public class ResponseFansPManagerPDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseFansPManagerPData Data { get; set; }
	}

	public class ResponseFansPManagerPData
	{
		[JsonProperty(PropertyName = "percentage")]
		public String Percentage { get; set; }
	}


}