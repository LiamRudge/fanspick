﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestCreateActionDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "userId")]
		public String userId { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

		[JsonProperty(PropertyName = "playerId")]
		public String PlayerId { get; set; }

		[JsonProperty(PropertyName = "action")]
		public String Action { get; set; }

		[JsonProperty(PropertyName = "time")]
		public String Time { get; set; }


	}

	public class ResponseCreateUserActionDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
        public String Data { get; set; }


	}

	
}