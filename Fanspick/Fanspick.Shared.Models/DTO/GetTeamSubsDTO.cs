﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestGetTeamSubsDTO : RequestBaseDTO
	{

		[JsonIgnore]
		public override String APITarget { get { return "fanspick/getTeamSubstitutes"; } }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

		[JsonProperty(PropertyName = "isLive")]
		public bool IsLive { get; set; }

	}

	public class ResponseGetTeamSubsDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public List<Squad> Data { get; set; }
	}

}