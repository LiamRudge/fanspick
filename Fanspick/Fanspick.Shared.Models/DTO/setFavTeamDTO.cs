﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestSetFavTeamDTO
    {
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public string TeamId { get; set; }

		[JsonProperty(PropertyName = "type")]
		public string Type { get; set; }

    }

    public class RequestSetFavTeamRefitDTO
	{
		[JsonProperty(PropertyName = "teamId")]
		public string TeamId { get; set; }

		[JsonProperty(PropertyName = "type")]
		public string Type { get; set; }

	}

	public class ResponseSetFavTeamDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseSetFavTeamData Data { get; set; }
	}

	public class ResponseSetFavTeamData
	{
		// api returns null
	}
}
