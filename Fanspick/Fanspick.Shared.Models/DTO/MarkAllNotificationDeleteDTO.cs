﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class MarkAllNotificationDeleteDTO
	{
		public class RequestMarkAllNotificationDeleteDTO : RequestBaseDTO
		{
			[JsonIgnore]
			public override MethodType Method { get { return MethodType.Get; } }
		}

		public class ResponseMarkAllNotificationDeleteDTO : ResponseBaseDTO
		{
			[JsonProperty(PropertyName = "data")]
			public ResponseMarkAllNotificationDeleteData Data { get; set; }
		}

		public class ResponseMarkAllNotificationDeleteData
		{

		}

	}
}
