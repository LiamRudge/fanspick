﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestChatHistoryDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "topicId")]
		public String TopicId { get; set; }

	}

	public class ResponseChatHistoryDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public List<ResponseChatHistoryData> Data { get; set; }
	}

	public class ResponseChatHistoryData
	{
		[JsonProperty(PropertyName = "communityPosts")]
        public List<CommunityPosts> CommunityPosts { get; set; }

		[JsonProperty(PropertyName = "fanspickPosts")]
		public List<CommunityPosts> FanspickPosts { get; set; }

		[JsonProperty(PropertyName = "_id")]
        public String _id { get; set; }

	}

}
