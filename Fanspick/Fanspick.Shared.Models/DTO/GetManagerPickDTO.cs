﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetManagerPickDTO
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "accessToken")]
        public String AccessToken { get; set; }

        [JsonProperty(PropertyName = "fixtureId")]
        public String FixtureId { get; set; }

        [JsonProperty(PropertyName = "teamId")]
        public String TeamId { get; set; }
    }

    public class RequestGetManagerPickRefitDTO
	{

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }
	}

    public class ResponseGetManagerPickDTO : ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public ResponsePickData Data { get; set; }
    }

}
