﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.DeviceInfo;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetALlFormationDTO
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "accessToken")]
        public String AccessToken { get; set; }
    }

    public class ResponseGetAllFormationsDTO : ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public List<Formation> Data { get; set; }
    }

    public class Formation
    {
        public string _id { get; set; }
        public string activeImage { get; set; }
        public string safeActiveImage
        {
            get
            {
                string safeString = $"http://fanspick-admin-test.5sol.co.uk{activeImage}.png";
                safeString = safeString.Replace("\\", "/");
                return safeString;
            }
        }
        public string disableImage { get; set; }
        public string type { get; set; }
        public string safeType { get{
                string sType = string.Empty;
                for (int i = 0; i < type.Length; i++)
                {
                    sType += type[i] + "-";
                }
                sType= sType.Remove(sType.Length-1);
                return sType;
            } }
        public bool isActive { get; set; }
        public bool isDeleted { get; set; }


		public double ItemWidth
		{
			get
			{
				double size = 100;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 100;//200
				}

				return size;
			}
		}


		public double ItemHeight
		{
			get
			{
				double size = 90;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 90;//180
				}

				return size;
			}
		}

		public double ItemSize
		{
			get
			{
				double size = 60;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 60;//100
				}

				return size;
			}
		}

		public double TextHeight
		{
			get
			{
				double size = 13;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 30;
				}

				return size;
			}
		}

		public double TextSize
		{
			get
			{
				double size = 11;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 16;
				}

				return size;
			}
		}

		public double CloseBtnSize
		{
			get
			{
				double size = 15;
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 35;
				}

				return size;
			}
		}




	}
}
