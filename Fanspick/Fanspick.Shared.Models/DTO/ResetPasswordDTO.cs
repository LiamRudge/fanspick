﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestResetPasswordDTO
	{
		[JsonProperty(PropertyName = "phoneNumber")]
		public string PhoneNumber { get; set; }

		[JsonProperty(PropertyName = "newPassword")]
		public string NewPassword { get; set; }

	}

	public class ResponseResetPasswordDTO : ResponseBaseDTO
	{
        [JsonProperty(PropertyName = "data")]
        public List<ResponseResetPasswordData> Data { get; set; } = new List<ResponseResetPasswordData>();
	}

	public class ResponseResetPasswordData
	{
		// api returns null
	}
}
