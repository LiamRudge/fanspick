﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestBaseDTO
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "accessToken")]
        public String AccessToken { get; set; }

        [JsonIgnore]
        public virtual String APITarget { get; }

        [JsonIgnore]
        public virtual ApiType API { get { return ApiType.Fanspick; } }

        [JsonIgnore]
        public virtual MethodType Method { get { return MethodType.Post; } }
    }


    public class ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "statusCode")]
        public int StatusCode { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }

    public enum ApiType
    {
        Fanspick,
        Social
    }

    public enum MethodType
    {
        Get,
        Post
    }
}
