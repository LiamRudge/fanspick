﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestUserPManagerPDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

	}

	public class ResponseUserPManagerPDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseUserPManagerPData Data { get; set; }
	}

	public class ResponseUserPManagerPData
	{
		[JsonProperty(PropertyName = "percentage")]
		public String Percentage { get; set; }
	}


}