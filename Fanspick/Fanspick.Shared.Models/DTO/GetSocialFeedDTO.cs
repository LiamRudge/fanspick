﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetSocialFeedDTO : RequestBaseDTO
    {
        [JsonIgnore]
        public override String APITarget
        {
            get
            {
                return string.Format("?q={0}", this.TeamId);
            }
        }

        [JsonIgnore]
        public override ApiType API
        {
            get
            {
                return ApiType.Social;
            }
        }

        [JsonIgnore]
        public override MethodType Method
        {
            get { return MethodType.Get; }
        }
        
        [JsonIgnore]
        public String TeamId { get; set; }
    }


	public class RequestGetSocialFeedDTOReload : RequestBaseDTO
	{
		[JsonIgnore]
		public override String APITarget
		{
			get
			{
                String url = string.Format("?q={0}", this.TeamId)+"&twMaxId="+this.fbNextTS + "&fbNextTS=" + this.fbNextTS;
				return url;
			}
		}

		[JsonIgnore]
		public override ApiType API
		{
			get
			{
				return ApiType.Social;
			}
		}

		[JsonIgnore]
		public override MethodType Method
		{
			get { return MethodType.Get; }
		}

		[JsonIgnore]
		public String TeamId { get; set; }

		[JsonIgnore]
		public String twMaxId { get; set; }

		[JsonIgnore]
		public String fbNextTS { get; set; }


	}

    public class ResponseGetSocialFeedDTO : ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public DataResponse Data { get; set; }


    }

	public class DataResponse
	{
		[JsonProperty(PropertyName = "feeds")]
		public List<SocialFeed> Data { get; set; }

		[JsonProperty(PropertyName = "twMaxId")]
		public String twMaxId { get; set; }

		[JsonProperty(PropertyName = "fbNextTS")]
		public String fbNextTS { get; set; }
	}


}
