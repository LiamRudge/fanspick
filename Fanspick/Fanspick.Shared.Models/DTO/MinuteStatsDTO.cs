﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using System.Linq;
using System.Net;
using Plugin.DeviceInfo;

namespace Fanspick.Shared.Models.DTO
{
	public class MinuteStatsDTO
	{
		public class RequestMinuteStatsDTO
		{
			[JsonIgnore]
			[JsonProperty(PropertyName = "accessToken")]
			public String AccessToken { get; set; }

			[JsonProperty(PropertyName = "fixtureId")]
			public String FixtureId { get; set; }

			[JsonProperty(PropertyName = "teamId")]
			public String TeamId { get; set; }

			[JsonProperty(PropertyName = "isLive")]
            public Boolean isLive { get; set; }

		}


		public class ResponseMinuteStatsDTO : ResponseBaseDTO
		{
			[JsonProperty(PropertyName = "data")]
			public ResponseMinuteStatsData Data { get; set; }
		}

		public class ResponseMinuteStatsData
		{
			[JsonProperty(PropertyName = "actionStats")]
			public List<ActionStats> action { get; set; } = new List<ActionStats>();


            public List<ActionStats> Action15{
                get{
                    if(this.action!=null){
                        return this.action.Where(x => x.Minutes == "15").ToList();

                    }
                    return new List<ActionStats>();

                }

            }

			public List<ActionStats> Action30
			{
				get
				{
					if (this.action != null)
					{
						return this.action.Where(x => x.Minutes == "30").ToList();

					}
					return new List<ActionStats>();

				}

			}


			public List<ActionStats> Action45
			{
				get
				{
					if (this.action != null)
					{
						return this.action.Where(x => x.Minutes == "45").ToList();

					}
					return new List<ActionStats>();

				}

			}

			public List<ActionStats> Action60
			{
				get
				{
					if (this.action != null)
					{
						return this.action.Where(x => x.Minutes == "60").ToList();

					}
					return new List<ActionStats>();

				}

			}

			public List<ActionStats> Action75
			{
				get
				{
					if (this.action != null)
					{
						return this.action.Where(x => x.Minutes == "75").ToList();

					}
					return new List<ActionStats>();

				}

			}

			public List<ActionStats> Action90
			{
				get
				{
					if (this.action != null)
					{
						return this.action.Where(x => x.Minutes == "90").ToList();

					}
					return new List<ActionStats>();

				}

			}
		}

		public class ActionStats 
		{
			[JsonProperty(PropertyName = "action")]
            public String action { get; set; }

            public string FormattedMinutes{
                get{
                    int minute = 0;
                    int.TryParse(Minutes, out minute);
                    int lastminute = minute - 14;
                    return string.Format("{0} - {1}", lastminute, minute);
                }

            }

			[JsonProperty(PropertyName = "minutes")]
			public String Minutes { get; set; }

			[JsonProperty(PropertyName = "playerId")]
			public PlayerDetailAction playerDetails { get; set; }

			public double SubTextSize
			{
				get
				{
					double size = 10;
					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						size = 15;
					}
					return size;
				}
			}


			public double SubImageSize
			{
				get
				{
					double size = 15;
					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						size = 17;
					}
					return size;
				}
			}

			public double SubBackImage
			{
				get
				{
					double size = 19;

					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						size = 25;
					}

					return size;
				}
			}

			public string ImageFav
			{
				get
				{
			
					String toShowFavImage = "";
                    if (this.action == "star")
					{
						toShowFavImage = "gold_star.png";
					}
					else if(this.action == "hairdryer")

					{
						toShowFavImage = "hairdrayer.png";
					}
					else if (this.action == "manofmatch")

					{
						toShowFavImage = "icon_mom.png";
					}


					return toShowFavImage;
				}
			}

		}

		public class PlayerDetailAction
		{
			[JsonProperty(PropertyName = "knownName")]
			public string KnownName
			{
				get
				{
					return WebUtility.HtmlDecode(decodedKnownName);
				}
				set
				{
					decodedKnownName = WebUtility.HtmlDecode(value);
				}
			}

                private string decodedKnownName;

		}

	}
}