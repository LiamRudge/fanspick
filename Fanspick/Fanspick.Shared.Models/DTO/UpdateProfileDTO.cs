﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
    public class UpdateProfileDTO
    {
        public class RequestUpdateProfileDTO
		{
            [JsonIgnore]
			[JsonProperty(PropertyName = "accessToken")]
			public String AccessToken { get; set; }

            [JsonProperty(PropertyName = "firstname")]
            public string FirstName { get; set; }

			[JsonProperty(PropertyName = "lastname")]
			public string LastName { get; set; }

			[JsonProperty(PropertyName = "dob")]
            public string DateOfBirth { get; set; }

			[JsonProperty(PropertyName = "lat")]
            public string Latitude { get; set; }

            [JsonProperty(PropertyName = "lon")]
            public string Longitude { get; set; }

			[JsonProperty(PropertyName = "photo")]
            public string Photo { get; set; }

			[JsonProperty(PropertyName = "address")]
            public string Address { get; set; }

			[JsonProperty(PropertyName = "city")]
			public string City { get; set; }

			[JsonProperty(PropertyName = "zipcode")]
			public string Zip { get; set; }

            public RequestUpdateProfileDTO()
            {
                this.DateOfBirth = DateTime.Now.ToString("yyyy-MM-dd");
            }

		}


    }

    public class RequestUpdateProfileRefitDTO
	{
	
		[JsonProperty(PropertyName = "firstname")]
		public string FirstName { get; set; }

		[JsonProperty(PropertyName = "lastname")]
		public string LastName { get; set; }

		[JsonProperty(PropertyName = "dob")]
		public string DateOfBirth { get; set; }

		[JsonProperty(PropertyName = "lat")]
		public string Latitude { get; set; }

		[JsonProperty(PropertyName = "lon")]
		public string Longitude { get; set; }

		[JsonProperty(PropertyName = "photo")]
		public string Photo { get; set; }

		[JsonProperty(PropertyName = "address")]
		public string Address { get; set; }

		[JsonProperty(PropertyName = "city")]
		public string City { get; set; }

		[JsonProperty(PropertyName = "phoneNumber")]
		public string PhoneNumber { get; set; }

		[JsonProperty(PropertyName = "zipcode")]
		public string Zip { get; set; }

		public RequestUpdateProfileRefitDTO()
		{
			this.DateOfBirth = DateTime.Now.ToString("yyyy-MM-dd");
		}

	}

	public class RequestUpdateProfileRefitPRoDTO
	{

		[JsonProperty(PropertyName = "firstname")]
		public string FirstName { get; set; }

		[JsonProperty(PropertyName = "lastname")]
		public string LastName { get; set; }

		[JsonProperty(PropertyName = "dob")]
		public string DateOfBirth { get; set; }

		[JsonProperty(PropertyName = "photo")]
		public string Photo { get; set; }

		[JsonProperty(PropertyName = "address")]
		public string Address { get; set; }

		[JsonProperty(PropertyName = "city")]
		public string City { get; set; }

		[JsonProperty(PropertyName = "zipcode")]
		public string Zip { get; set; }
		public RequestUpdateProfileRefitPRoDTO()
		{
			this.DateOfBirth = DateTime.Now.ToString("yyyy-MM-dd");
		}

	}


	public class ResponseUpdateProfileDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseUpdateProfileData Data { get; set; }
	}

	public class ResponseUpdateProfileData
	{
		// api returns null
	}
}
