﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestChangeFormationDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "newFormationId")]
		public String NewFormationId { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

		[JsonProperty(PropertyName = "isLive")]
		public bool IsLive { get; set; }
	}

    public class RequestChangeFormationRefitDTO
	{

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "newFormationId")]
		public String NewFormationId { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

		[JsonProperty(PropertyName = "isLive")]
		public bool IsLive { get; set; }
	}

	public class ResponseChangeFormationDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseChangeFormationData Data { get; set; }
	}

	public class ResponseChangeFormationData
	{
		// api returns null
	}
}