﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
    public class SwapPitchPlayersDTO
    {
		public class RequestSwapPitchPlayersDataDTO : RequestBaseDTO
		{
			[JsonIgnore]
			public override String APITarget { get { return "fanspick/swapPlayers"; } }


			[JsonProperty(PropertyName = "fixtureId")]
			public String FixtureId { get; set; }

			[JsonProperty(PropertyName = "teamId")]
			public String TeamId { get; set; }

			[JsonProperty(PropertyName = "oldPlayerId")]
			public String OldPlayerId { get; set; }

			[JsonProperty(PropertyName = "newPlayerId")]
			public String NewPlayerId { get; set; }

			[JsonProperty(PropertyName = "isLive")]
			public Boolean IsLive { get; set; }

		}

    }



        public class RequestSwapPitchPlayersDataRefitDTO : RequestBaseDTO
        {
        
            [JsonProperty(PropertyName = "fixtureId")]
            public String FixtureId { get; set; }

            [JsonProperty(PropertyName = "teamId")]
            public String TeamId { get; set; }

            [JsonProperty(PropertyName = "oldPlayerId")]
            public String OldPlayerId { get; set; }

            [JsonProperty(PropertyName = "newPlayerId")]
            public String NewPlayerId { get; set; }

            [JsonProperty(PropertyName = "isLive")]
            public Boolean IsLive { get; set; }

        }

        public class ResponseSwapPitchPlayersDTO : ResponseBaseDTO
        {

            [JsonProperty(PropertyName = "data")]
            public List<ResponseSwapPitchPlayersData> Data { get; set; } = new List<ResponseSwapPitchPlayersData>();
        }

        public class ResponseSwapPitchPlayersData
        {


        }

}
