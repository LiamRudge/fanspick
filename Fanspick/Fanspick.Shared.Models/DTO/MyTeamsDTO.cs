﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace Fanspick.Shared.Models.DTO
{
	public class MyTeamsDTO
	{
		public class RequestMyTeamsDTO
		{
			[JsonIgnore]
			[JsonProperty(PropertyName = "accessToken")]
			public String AccessToken { get; set; }

		}

	}

	public class ResponseMyTeamsDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public List<ResponseMyTeamsData> Data { get; set; } = new List<ResponseMyTeamsData>();
	}

	public class ResponseMyTeamsData
	{
		[JsonProperty(PropertyName = "_id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "knownName")]
		public string KnownName { get; set; }

		[JsonProperty(PropertyName = "isPrimaryFavouriteTeam")]
		public Boolean IsPrimaryFavouriteTeam { get; set; }


		[JsonProperty(PropertyName = "imageURL")]
		public string ImageURL { get; set; }

		public string TeamLogo
		{
			get
			{
				if (this.ImageURL != null)
				{
					return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamLogos/" + this.ImageURL;
				}
				else
				{
					return "no_image";
				}
			}
		}

		public double ItemHeight
		{
			get
			{
				double cell = 50;

				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					cell = 80;
				}

				return cell;
			}
		}

		public double TextSize
		{
			get
			{
				double size = 15;

				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 21;
				}

				return size;
			}
		}


		public GridLength Division1
		{
			get
			{
				GridLength size = new GridLength(15, GridUnitType.Star);
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = new GridLength(20, GridUnitType.Star);
				}
				return size;
			}
		}


		public GridLength Division2
		{
			get
			{
				var size = new GridLength(70, GridUnitType.Star);
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = new GridLength(70, GridUnitType.Star);
				}
				return size;
			}
		}

		public GridLength Division3
		{
			get
			{
				var size = new GridLength(15, GridUnitType.Star);
				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = new GridLength(10, GridUnitType.Star);
				}
				return size;
			}
		}


		public string ImageFav
		{
			get
			{
				Boolean isPrime = this.IsPrimaryFavouriteTeam;
				String toShowFavImage = "";
				if (isPrime)
				{
					toShowFavImage = "star.png";
				}
				else
				{
					toShowFavImage = "silver_star.png";
				}

				return toShowFavImage;
			}
		}


	}



}