﻿﻿using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestLoginDTO
    {
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }

        [JsonProperty(PropertyName = "deviceToken")]
        public string DeviceToken { get; set; }

        [JsonProperty(PropertyName = "deviceType")]
        public string DeviceType { get; set; }

        [JsonProperty(PropertyName = "appVersion")]
        public string AppVersion { get; set; }

        [JsonProperty(PropertyName = "loginType")]
        public string LoginType { get; set; }

		[JsonProperty(PropertyName = "fcmId")]
		public string FcmId { get; set; }
	

	}
	public class RequestFacebookLoginDTO
	{
		[JsonProperty(PropertyName = "email")]
		public string Email { get; set; }

		
		[JsonProperty(PropertyName = "deviceToken")]
		public string DeviceToken { get; set; }

		[JsonProperty(PropertyName = "deviceType")]
		public string DeviceType { get; set; }

		[JsonProperty(PropertyName = "appVersion")]
		public string AppVersion { get; set; }

		[JsonProperty(PropertyName = "loginType")]
		public string LoginType { get; set; }

		[JsonProperty(PropertyName = "facebookId")]
		public string FacebookID { get; set; }


	}


    public class ResponseLoginDTO : ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public ResponseLoginData Data { get; set; }
    }

    public class ResponseLoginData
    {
        [JsonProperty(PropertyName = "accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "userDetails")]
        public FanspickUser UserDetails { get; set; }

    }
	public class ResponseFBLoginDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseFBLoginData Data { get; set; }
	}

	public class ResponseFBLoginData
	{
		[JsonProperty(PropertyName = "accessToken")]
		public string AccessToken { get; set; }

		[JsonProperty(PropertyName = "userDetails")]
		public FanspickUser UserDetails { get; set; }

	}
}
