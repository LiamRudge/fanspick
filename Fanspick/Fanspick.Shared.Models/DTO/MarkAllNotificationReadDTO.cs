﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class MarkAllNotificationReadDTO
	{
		public class RequestMarkAllNotificationReadDTO : RequestBaseDTO
		{
			[JsonIgnore]
			public override MethodType Method { get { return MethodType.Get; } }
		}

		public class ResponseMarkAllNotificationReadDTO : ResponseBaseDTO
		{
			[JsonProperty(PropertyName = "data")]
			public ResponseMarkAllNotificationReadData Data { get; set; } 
		}

		public class ResponseMarkAllNotificationReadData
		{

		}

	}
}
