﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetTeamSquadDTO:RequestBaseDTO
    {

        [JsonIgnore]
        public override String APITarget { get{ return "fanspick/getTeamSquad"; } }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

		[JsonProperty(PropertyName = "isLive")]
		public bool IsLive { get; set; }

    }

    public class RequestGetTeamSquadRefitDTO : RequestBaseDTO
	{

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

		[JsonProperty(PropertyName = "isLive")]
		public bool IsLive { get; set; }

	}

    public class ResponseGetTeamSquadDTO : ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public List<Squad> Data { get; set; }
    }

}
