﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class EventLoggingDTO
	{
		public class RequestEventLoggingDTO
		{

            [JsonProperty(PropertyName = "lat")]
            public string Latitude { get; set; }

			[JsonProperty(PropertyName = "lon")]
			public string Longitude { get; set; }

			[JsonProperty(PropertyName = "eventType")]
			public string EventType { get; set; }

			[JsonProperty(PropertyName = "eventDescription")]
			public string EventDescription { get; set; }

			[JsonProperty(PropertyName = "eventAdditionalInfoID")]
			public string EventAdditionalInfoID { get; set; }

			[JsonProperty(PropertyName = "userId")]
			public string UserId { get; set; }

			[JsonProperty(PropertyName = "userAgent")]
			public string UserAgent { get; set; }

			[JsonProperty(PropertyName = "deviceType")]
			public string DeviceType { get; set; }

			[JsonProperty(PropertyName = "deviceToken")]
			public string DeviceToken { get; set; }

			[JsonProperty(PropertyName = "appVersion")]
			public string AppVersion { get; set; }




		}


		public class ResponseEventLoggingDTO : ResponseBaseDTO
		{
			[JsonProperty(PropertyName = "data")]
			public ResponseEventLoggingData Data { get; set; }
		}

		public class ResponseEventLoggingData
		{
			// api returns null
		}
	}
}