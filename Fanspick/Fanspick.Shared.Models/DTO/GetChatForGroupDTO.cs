﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetChatForGroupDTO
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "accessToken")]
        public String AccessToken { get; set; }

        [JsonProperty(PropertyName = "groupId")]
        public string GroupId { get; set; }

    }

    public class ResponseGetChatForGroupDTO : ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public ResponseGetChatForGroupData Data { get; set; }
    }

    public class ResponseGetChatForGroupData
    {
        [JsonProperty(PropertyName = "groupId")]
        public String GroupId { get; set; }

		[JsonProperty(PropertyName = "reciever")]
		public Reciever reciever { get; set; }

        [JsonProperty(PropertyName = "chats")]
        public ObservableCollection<ChatMessages> Chats { get; set; } = new ObservableCollection<ChatMessages>();
    }

    public class Reciever
    {

        [JsonProperty(PropertyName = "groupMembers")]
        public List<GroupMembers> groupMembers { get; set; } = new List<GroupMembers>();

        [JsonProperty(PropertyName = "admin")]
		public List<AdminMembers> admin { get; set; } = new List<AdminMembers>();

    }

	public class GroupMembers
	{
    
        public String MemberId { get; set; }
        public String MemberName { get; set; }

	}

    public class AdminMembers
    {
        public String AdminId { get; set; }
        public String AdminName { get; set; }
    }


	public class MemberWithAdmin
	{
		public String MemberId { get; set; }
        public String MemberName { get; set; }
        public Boolean isAdmin { get; set; }
        public String UserImage { get; set; }

        public string AdminText
		{
			get
			{
                if(isAdmin){
                    return "Remove Admin";
                }else{
                    return "Add Admin";
                }

			}
		}


	}

    public class ChatMessages
    {

        [JsonProperty(PropertyName = "time")]
        public String Time { get; set; }

        [JsonProperty(PropertyName = "message")]
        public String Message { get; set; }


        [JsonProperty(PropertyName = "sender")]
        public SenderDetails Sender { get; set; }

		[JsonProperty(PropertyName = "type")]
        public String type { get; set; }

		[JsonProperty(PropertyName = "statusType")]
		public String statusType { get; set; }

		[JsonIgnore]
		public string CurrentUserId { get; set; }

		[JsonIgnore]
		public int imageType { get; set; }
		// 0 = clock
		// 1 = singletick
		// 2 = doubletick
		// 3 = Bluetick

        public String ReciptImage
		{
			get
			{
                String image = "";

                if(this.imageType ==0){
                    image =  "clock";
                }
				else if(this.imageType == 1){
					image = "tick_sent";
				}
				else if (this.imageType == 2)
				{
					image = "tick_double";
				}
				else if (this.imageType == 3)
				{

                    if(this.statusType == null){
                    image = "tick_sent";
					}
                    else if(this.statusType =="notdelivered"){
                    image = "tick_sent";    
                    }
                    else{
                    image = "tick_blue";    
                    }

					
				}




                return image;



			}
		}

		public Color color
		{
			get
			{
				var send = Color.FromRgb(220, 220, 223);
				return send;
			}
		}

		public Color colorBlue
		{
			get
			{
				var send = Color.FromRgb(49, 144, 220);
				return send;
			}
		}

		[JsonIgnore]
		public Boolean isConnect { get; set; }

		public bool isConnectedShow
		{
			get
			{
				if (isConnect)
				{
					return false;
				}

				return true;
			}

		}

		public LayoutOptions TimeOption
		{
			get
			{
                if (this.Sender.Sender_id == this.CurrentUserId)
				{
					return LayoutOptions.End;
				}
				else
				{
					return LayoutOptions.Start;
				}

            }
		}

		public String TimeToShow
		{
			get
			{
				String time = "";
				if (this.Time == null)
				{
					time = "00:00";
				}
				else
				{
					DateTime parsedFixtureDate = DateTime.Parse(this.Time);
					TimeZoneInfo timeZoneInformation = TimeZoneInfo.Local;
					DateTimeOffset timeOffset = new DateTimeOffset(parsedFixtureDate, timeZoneInformation.GetUtcOffset(parsedFixtureDate));
					time = parsedFixtureDate.ToString("d MMM yyyy, HH: mm");
				}

				return time;
			}
		}

		public double TextsizeTime
		{
			get
			{
				double size = 6;
				String device = CrossDeviceInfo.Current.Model;
				if (device == "iPad")
				{
					size = 10;
				}
				return size;
			}
		}

		public bool VisibilityItemOne
		{
			get
			{
				if (this.Sender.Sender_id == this.CurrentUserId)
				{
					return false;
				}
				else
				{
                    if (this.type == "Notification")
					{
						return false;
					}
					else
					{
						return true;
					}
				}

			}
		}

		public bool VisibilityItemTwo
		{
			get
			{
				if (this.Sender.Sender_id == this.CurrentUserId)
				{
					if (this.type == "Notification")
					{
                        return false;
                    }else{
                        return true;
                    }

				}

				return false;
			}

		}

		public bool UpdateMessageVisibility
		{
			get
			{
				if (this.type == "Notification")
				{
					return true;
                }else{
                return false;    
                }

				
			}

		}

		public bool UpdateMessageVisibilityTime
		{
			get
			{
				if (this.type == "Notification")
				{
                    return false;
				}
				else
				{
                    return true;
				}


			}

		}

        public String UpdateMessage
		{
			get
			{
				if (this.type == "Notification")
				{
                    return this.Message;
                }else{
                    return "";
                }

			}

		}

    }

    public class SenderDetails
    {
		[JsonProperty(PropertyName = "_id")]
		public String Sender_id { get; set; }

		[JsonProperty(PropertyName = "username")]
		public String SenderUsername { get; set; }

		[JsonProperty(PropertyName = "phoneNumber")]
		public String SenderPhoneNumber { get; set; }
    }

}
