﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestTopicByFixtureDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

	}

	public class ResponseTopicByFixtureDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public List<ResponseTopicByFixtureData> Data { get; set; }
	}

	public class ResponseTopicByFixtureData
	{
		[JsonProperty(PropertyName = "_id")]
		public String _id { get; set; }

	}

}
