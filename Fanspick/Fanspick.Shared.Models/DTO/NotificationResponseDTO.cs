﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestNotificationResponseDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "notificationId")]
		public String NotificationId { get; set; }

		[JsonProperty(PropertyName = "questionId")]
		public String QuestionId { get; set; }

		[JsonProperty(PropertyName = "questionText")]
		public String QuestionText { get; set; }

		[JsonProperty(PropertyName = "response")]
		public String Response { get; set; }
	}

	public class ResponseNotificationResponseDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseNotificationResponseData Data { get; set; }
	}

	public class ResponseNotificationResponseData
	{
		
	}


}
