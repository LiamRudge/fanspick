﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGenerateOtpDTO
	{
        [JsonProperty(PropertyName = "phoneNumber")]
		public string PhoneNumber { get; set; }

		[JsonProperty(PropertyName = "signUp")]
		public Boolean signUp { get; set; }


    }

	public class ResponseGenerateOtpDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseAgeVerificationData Data { get; set; }
	}

	public class ResponseGenerateOtpData
	{
        
		// api returns null
	}

}
