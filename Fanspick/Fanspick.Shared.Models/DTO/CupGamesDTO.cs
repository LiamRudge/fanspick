﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace Fanspick.Shared.Models.DTO
{
	public class CupGamesDTO
	{
		public class RequestCupGameDTO
		{
			[JsonIgnore]
			[JsonProperty(PropertyName = "accessToken")]
			public String AccessToken { get; set; }

			[JsonProperty(PropertyName = "competitionName")]
			public String CompetitionName { get; set; }

		}

	}

    public class RequestCupGameRefitDTO
	{
	
		[JsonProperty(PropertyName = "competitionName")]
		public String CompetitionName { get; set; }

	}


        public class ResponseCupGamesDTO : ResponseBaseDTO
        {
            [JsonProperty(PropertyName = "data")]
            public List<ResponseCupGameData> Data { get; set; } = new List<ResponseCupGameData>();
        }

        public class ResponseCupGameData
        {
            [JsonProperty(PropertyName = "_id")]
            public string Id { get; set; }

            [JsonProperty(PropertyName = "knownName")]
            public string KnownName { get; set; }


            [JsonProperty(PropertyName = "imageURL")]
            public string ImageURL { get; set; }

            [JsonProperty(PropertyName = "founded")]
            public string Founded { get; set; }

            [JsonProperty(PropertyName = "country")]
            public string Country { get; set; }

            [JsonProperty(PropertyName = "fullName")]
            public string FullName { get; set; }

            [JsonProperty(PropertyName = "isNationalTeam")]
            public bool IsNationalTeam { get; set; }

            [JsonProperty(PropertyName = "isPrimaryFavouriteTeam")]
            public Boolean IsPrimaryFavouriteTeam { get; set; }

            [JsonProperty(PropertyName = "isSecondaryFavouriteTeam")]
            public Boolean IsSecondaryFavouriteTeam { get; set; }


            public string TeamLogo
            {
                get
                {

                    if (this.ImageURL != null)
                    {
                        return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamLogos/" + this.ImageURL;
                    }
                    else
                    {
                        return "no_image";
                    }


                }
            }

            public string ImageFav
            {
                get
                {
                    Boolean isPrime = this.IsPrimaryFavouriteTeam;
                    Boolean isSilver = this.IsSecondaryFavouriteTeam;
                    String toShowFavImage = "";
                    if (isPrime)
                    {
                        toShowFavImage = "star.png";
                    }
                    else
                    {

                        if (isSilver)
                        {
                            toShowFavImage = "silver_star.png";
                        }
                        else
                        {
                            toShowFavImage = "";
                        }

                    }

                    return toShowFavImage;
                }
            }


            public double ItemHeight
            {
                get
                {
                    double cell = 50;

                    String device = CrossDeviceInfo.Current.Model;

                    if (device == "iPad")
                    {
                        cell = 80;
                    }

                    return cell;
                }
            }

            public GridLength Col1
            {
                get
                {
                    var size = new GridLength(14.85, GridUnitType.Star);

                    String device = CrossDeviceInfo.Current.Model;

                    if (device == "iPad")
                    {
                        size = new GridLength(10, GridUnitType.Star);
                    }

                    return size;
                }
            }

            public GridLength Col2
            {
                get
                {
                    var size = new GridLength(0.15, GridUnitType.Star);

                    String device = CrossDeviceInfo.Current.Model;

                    if (device == "iPad")
                    {
                        size = new GridLength(0.10, GridUnitType.Star);
                    }

                    return size;
                }
            }

            public GridLength Col3
            {
                get
                {
                    var size = new GridLength(52, GridUnitType.Star);

                    String device = CrossDeviceInfo.Current.Model;

                    if (device == "iPad")
                    {
                        size = new GridLength(58.90, GridUnitType.Star);
                    }

                    return size;
                }
            }


            public GridLength Col4
            {
                get
                {
                    var size = new GridLength(7, GridUnitType.Star);

                    String device = CrossDeviceInfo.Current.Model;

                    if (device == "iPad")
                    {
                        size = new GridLength(5.5, GridUnitType.Star);
                    }

                    return size;
                }
            }

            public GridLength Col5
            {
                get
                {
                    var size = new GridLength(6, GridUnitType.Star);

                    String device = CrossDeviceInfo.Current.Model;

                    if (device == "iPad")
                    {
                        size = new GridLength(4.5, GridUnitType.Star);
                    }

                    return size;
                }
            }

            public Thickness MarginSize
            {
                get
                {

                    var thickness = new Thickness(0, 10, 0, 10);
                    String device = CrossDeviceInfo.Current.Model;

                    if (device == "iPad")
                    {
                        thickness = new Thickness(0, 30, 0, 30);
                    }


                    return thickness;
                }

            }

            public GridLength IconDisplayGap1
            {
                get
                {
                    var size = new GridLength(15, GridUnitType.Star);

                    String device = CrossDeviceInfo.Current.Model;

                    if (device == "iPad")
                    {
                        size = new GridLength(25, GridUnitType.Star);
                    }

                    return size;
                }
            }

            public GridLength IconDisplayGap3
            {
                get
                {
                    var size = new GridLength(15, GridUnitType.Star);

                    String device = CrossDeviceInfo.Current.Model;

                    if (device == "iPad")
                    {
                        size = new GridLength(25, GridUnitType.Star);
                    }

                    return size;
                }
            }

            public GridLength IconDisplayGap2
            {
                get
                {
                    var size = new GridLength(70, GridUnitType.Star);

                    String device = CrossDeviceInfo.Current.Model;

                    if (device == "iPad")
                    {
                        size = new GridLength(50, GridUnitType.Star);
                    }

                    return size;
                }
            }

            //Resizing ends here



        }



}