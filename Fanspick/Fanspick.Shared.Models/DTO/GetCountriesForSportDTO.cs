﻿using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetCountriesForSportDTO
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "accessToken")]
        public String AccessToken { get; set; }

        [JsonProperty(PropertyName = "sportId")]
        public string SportId { get; set; }
    }

    public class RequestGetCountriesForSportRefitDTO
	{
		
		[JsonProperty(PropertyName = "sportId")]
		public string SportId { get; set; }
	}

    public class ResponseGetCountriesForSportDTO: ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public List<Country> Data { get; set; }
    }
}
