﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class ViewProfileDTO
	{
		public class RequestViewProfileDTO: RequestBaseDTO
		{
            [JsonIgnore]
            public override MethodType Method { get { return MethodType.Get; } }

        }	

	}

	public class ResponseViewProfileDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public List<ResponseViewProfileData> Data { get; set; } = new List<ResponseViewProfileData>();
	}

	public class ResponseViewProfileData
	{
		[JsonProperty(PropertyName = "gradeValue")]
		public string GradeValue { get; set; }

		[JsonProperty(PropertyName = "gradePoint")]
		public string GradePoint { get; set; }

		[JsonProperty(PropertyName = "dob")]
		public string DateOfBirth { get; set; }

		[JsonProperty(PropertyName = "address")]
		public string Address { get; set; }

		[JsonProperty(PropertyName = "onceLogin")]
		public string OnceLogin { get; set; }

		[JsonProperty(PropertyName = "firstname")]
		public string FirstName { get; set; }

		[JsonProperty(PropertyName = "phoneNumber")]
		public string PhoneNumber { get; set; }

		[JsonProperty(PropertyName = "lastname")]
		public string LastName { get; set; }

		[JsonProperty(PropertyName = "photo")]
		public string UserPhoto { get; set; }

		[JsonProperty(PropertyName = "city")]
		public string City { get; set; }

		[JsonProperty(PropertyName = "zipcode")]
		public string Zip { get; set; }

		[JsonProperty(PropertyName = "defaultTeam")]
		public List<DefaultTeamFavorite> TeamData { get; set; }

	}
}
