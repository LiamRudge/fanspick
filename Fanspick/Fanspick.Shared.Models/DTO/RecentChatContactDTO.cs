﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class RecentChatContactDTO
	{
		public class RequestRecentChatContactDTO : RequestBaseDTO
		{
			[JsonIgnore]
			public override MethodType Method { get { return MethodType.Post; } }
		}


		public class ResponseRecentChatContactDTO : ResponseBaseDTO
		{
			[JsonProperty(PropertyName = "data")]
			public ResponseRecentChatContactData Data { get; set; }
		}

		public class ResponseRecentChatContactData
		{
			[JsonProperty(PropertyName = "chats")]
			public List<RecentChat> Recentchat { get; set; } = new List<RecentChat>();
		}


		public class RecentChat
		{

			[JsonProperty(PropertyName = "lastActivatedTime")]
			public string lastActivatedTime { get; set; }

			[JsonProperty(PropertyName = "isDeleted")]
			public Boolean IsDeleted { get; set; }

			[JsonProperty(PropertyName = "type")]
			public string Type { get; set; } //oneToOne

			[JsonProperty(PropertyName = "groupMembers")]
			public List<Members> GroupMembers { get; set; } = new List<Members>();

			[JsonProperty(PropertyName = "_id")]
			public string _idChat { get; set; }

			[JsonProperty(PropertyName = "chats")]
			public Chat chat { get; set; }

			[JsonProperty(PropertyName = "name")]
			public String GroupName { get; set; }



		}

		public class Chat
		{
			[JsonProperty(PropertyName = "time")]
			public String Time { get; set; }

			[JsonProperty(PropertyName = "message")]
			public String Message { get; set; }

			[JsonProperty(PropertyName = "sender")]
			public String Sender { get; set; }

			[JsonProperty(PropertyName = "_id")]
			public String _id { get; set; }

		}


		public class Members
		{
			[JsonProperty(PropertyName = "memberId")]
			public MemberDetails memberDetails { get; set; }
		}


		public class MemberDetails
		{
			[JsonProperty(PropertyName = "_id")]
			public String _id { get; set; }

			[JsonProperty(PropertyName = "username")]
			public String Username { get; set; }

			[JsonProperty(PropertyName = "phoneNumber")]
			public String PhoneNumber { get; set; }

			[JsonProperty(PropertyName = "firstname")]
			public String Firstname { get; set; }

			[JsonProperty(PropertyName = "photo")]
			public String PhotoLink { get; set; }

            public String UserPhoto
			{
				get
				{
					String image = "sample_player_image";

                    if(this.PhotoLink !=null ){

                        if(this.PhotoLink != "null"){
                        image = "http://fanspick-admin-test.5sol.co.uk/Uploads/" + this.PhotoLink;    
                        }else{
                            image = "sample_player_image";
                        }


                    }else{

						image = "sample_player_image";
					}

					return image;
				}

			}

		}

	}


	public class RecentChatToShow
	{
		public String _idUser { get; set; }
		public String PhoneNumber { get; set; }
		public String Firstname { get; set; }
		public String GroupName { get; set; }
		public String Type { get; set; }
		public string lastActivatedTime { get; set; }
		public string _idChat { get; set; }
		public Boolean IsDeleted { get; set; }
		public String Message { get; set; }
		public String ChatGroupID { get; set; }
        public String Image { get; set; }

		public String NameToShow
		{
			get
			{

				String name = "";

				if (this.Type == "oneToMany")
				{
					name = this.GroupName;
				}

				else
				{

					name = this.Firstname;
					if (name == null)
					{
						name = this.PhoneNumber;
					}
				}


				return name;
			}

		}


		public String TimeToShow
		{
			get
			{
				String time = "";
				if (this.lastActivatedTime == null)
				{
					time = "00:00";
				}
				else
				{
					DateTime parsedFixtureDate = DateTime.Parse(this.lastActivatedTime);
					TimeZoneInfo timeZoneInformation = TimeZoneInfo.Local;
					DateTimeOffset timeOffset = new DateTimeOffset(parsedFixtureDate, timeZoneInformation.GetUtcOffset(parsedFixtureDate));
					time = parsedFixtureDate.ToString("HH: mm");
				}

				return time;
			}

		}

	}
}
