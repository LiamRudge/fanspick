﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetUserPickDTO
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "accessToken")]
        public String AccessToken { get; set; }

        [JsonProperty(PropertyName = "fixtureId")]
        public String FixtureId { get; set; }

        [JsonProperty(PropertyName = "isLive")]
        public Boolean IsLive { get; set; }

        [JsonProperty(PropertyName = "teamId")]
        public String TeamId { get; set; }
    }

    public class RequestGetUserPickRefitDTO
	{
		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "isLive")]
		public Boolean IsLive { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }
	}


    public class ResponseGetUserPickDTO : ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public ResponsePickData Data { get; set; }
    }   

    public class ResponsePickData
    {
        [JsonProperty(PropertyName = "formation")]
        public Formation1 CurrentFormation { get; set; }

        [JsonProperty(PropertyName = "lineUpPlayers")]
        public List<LineUpPlayerData> LineUpPlayers { get; set; }

        [JsonProperty(PropertyName = "positions")]
        public List<Position> Positions { get; set; }
    }

    public class LineUpPlayerData
    {
        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "playerId")]
        public Player Player { get; set; }

        [JsonProperty(PropertyName = "positionId")]
        public Position Position { get; set; }

		[JsonProperty(PropertyName = "userActions")]
		public List<UserActions> userActions { get; set; } = new List<UserActions>();

    }

	public class UserActions
	{
		[JsonProperty(PropertyName = "action")]
		public string Action { get; set; }
	}
}
