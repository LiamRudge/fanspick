﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{   
    public class RequestSignUpDTO
    {
        [JsonProperty(PropertyName = "username")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "emailId")]
        public string EmailAddress { get; set; }

        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }

        [JsonProperty(PropertyName = "gender")]
        public String Gender { get; set; }

        [JsonProperty(PropertyName = "deviceToken")]
        public String DeviceToken { get; set; }

        [JsonProperty(PropertyName = "deviceType")]
        public String DeviceType { get; set; }

        [JsonProperty(PropertyName = "appVersion")]
        public String AppVersion { get; set; }

		[JsonProperty(PropertyName = "fcmId")]
		public string FcmId { get; set; }

    }
}
