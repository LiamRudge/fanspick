﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestFansPManagerPLiveDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

	}

	public class ResponseFansPManagerLivePDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseFansPManagerPLiveData Data { get; set; }
	}

	public class ResponseFansPManagerPLiveData
	{
		[JsonProperty(PropertyName = "percentage")]
		public String Percentage { get; set; }
	}


}