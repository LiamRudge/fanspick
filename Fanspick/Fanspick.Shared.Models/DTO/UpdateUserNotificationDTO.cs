﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestUpdateUserNotificationDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "notificationId")]
		public String NotificationId { get; set; }

		[JsonProperty(PropertyName = "readStatus")]
        public Boolean ReadStatus { get; set; }

		[JsonProperty(PropertyName = "deletedStatus")]
        public Boolean DeletedStatus { get; set; }

	}

	public class ResponseUpdateUserNotificationDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseUpdateUserNotificationData Data { get; set; }
	}

	public class ResponseUpdateUserNotificationData
	{


	}

}
