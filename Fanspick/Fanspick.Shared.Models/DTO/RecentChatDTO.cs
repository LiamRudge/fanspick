﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestRecentChatDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

	}

	public class ResponseRecentChatDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseAgeVerificationData Data { get; set; }
	}

	public class ResponseRecentChatData
	{
		// api returns null
	}
}
