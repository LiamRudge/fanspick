﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestRegisteredContactsDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }


	}

	public class ResponseRegisteredContactsDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseRegisteredContactsData Data { get; set; }
	}

	public class ResponseRegisteredContactsData
	{
		// api returns null
		[JsonProperty(PropertyName = "groups")]
		public List<Groups> Groups { get; set; }

		[JsonProperty(PropertyName = "registeredContacts")]
		public List<ContactsRegistered> RegisteredContacts { get; set; }

		[JsonProperty(PropertyName = "nonRegisteredContacts")]
		public List<ContactsunRegistered> NonRegisteredContacts { get; set; }

	}

	public class Groups
	{
	
	}

	public class ContactsRegistered
	{
		[JsonProperty(PropertyName = "status")]
		public String Status { get; set; }

		[JsonProperty(PropertyName = "groupId")]
		public String GroupId { get; set; }

		[JsonProperty(PropertyName = "name")]
		public String Name { get; set; }

		[JsonProperty(PropertyName = "contactNo")]
		public String ContactNo { get; set; }

		[JsonProperty(PropertyName = "userId")]
        public String ContactId { get; set; }

		[JsonProperty(PropertyName = "photo")]
		public String PhotoLink { get; set; }

		public String UserPhoto
		{
			get
			{
				String image = "sample_player_image";

				if (this.PhotoLink != null)
				{

					if (this.PhotoLink != "null")
					{
						image = "http://fanspick-admin-test.5sol.co.uk/Uploads/" + this.PhotoLink;
					}
					else
					{
						image = "sample_player_image";
					}


				}
				else
				{

					image = "sample_player_image";
				}

				return image;
			}

		}

        public Boolean isRegistered
		{
			get
			{
				return true;
			}
		}

	}

	public class ContactsunRegistered
	{
		[JsonProperty(PropertyName = "status")]
		public String Status { get; set; }

		[JsonProperty(PropertyName = "groupId")]
		public String GroupId { get; set; }

		[JsonProperty(PropertyName = "name")]
		public String Name { get; set; }

		[JsonProperty(PropertyName = "contactNo")]
		public String ContactNo { get; set; }

		[JsonProperty(PropertyName = "userId")]
		public String ContactId { get; set; }

		public Boolean isRegistered
		{
			get
			{
                return false;
			}
		}

		[JsonProperty(PropertyName = "photo")]
		public String PhotoLink { get; set; }

		public String UserPhoto
		{
			get
			{
				String image = "sample_player_image";

				if (this.PhotoLink != null)
				{

					if (this.PhotoLink != "null")
					{
						image = "http://fanspick-admin-test.5sol.co.uk/Uploads/" + this.PhotoLink;
					}
					else
					{
						image = "sample_player_image";
					}


				}
				else
				{

					image = "sample_player_image";
				}

				return image;
			}

		}

	}


	public class Contacts
	{
		[JsonProperty(PropertyName = "status")]
		public String Status { get; set; }

		[JsonProperty(PropertyName = "groupId")]
		public String GroupId { get; set; }

		[JsonProperty(PropertyName = "name")]
		public String Name { get; set; }

		[JsonProperty(PropertyName = "contactNo")]
		public String ContactNo { get; set; }

		[JsonProperty(PropertyName = "userId")]
		public String ContactId { get; set; }

        public Boolean  IsRegistered { get; set; }

        public String image
		{
			get
			{
                String source = "";
                
                if(this.IsRegistered){
                    source = "start_chat_icon";
                }else{
                    source = "Invite_Friend";
                }

				return source;
			}
		}

        public Thickness thick{
            get{

				var thickness = new Thickness(5);
				if (this.IsRegistered)
				{
					thickness = new Thickness(12);
				}

                return thickness;
            }
           
        }

		[JsonProperty(PropertyName = "photo")]
		public String PhotoLink { get; set; }

		public String UserPhoto
		{
			get
			{
				String image = "sample_player_image";

				if (this.PhotoLink != null)
				{

					if (this.PhotoLink != "null")
					{
						image = "http://fanspick-admin-test.5sol.co.uk/Uploads/" + this.PhotoLink;
					}
					else
					{
						image = "sample_player_image";
					}


				}
				else
				{

					image = "sample_player_image";
				}

				return image;
			}

		}

	}

}