﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestClearUserPickDTO
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "accessToken")]
        public String AccessToken { get; set; }

        [JsonProperty(PropertyName = "fixtureId")]
        public String FixtureId { get; set; }

        [JsonProperty(PropertyName = "isLive")]
        public Boolean IsLive { get; set; }

        [JsonProperty(PropertyName = "teamId")]
        public String TeamId { get; set; }
    }

    public class ResponseClearUserPickDTO : ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public ResponsePickData Data { get; set; }
    }   

}
