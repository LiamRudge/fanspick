﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class GetTeamDataDTO
	{

		public class RequestGetTeamDataDTO
		{
			[JsonIgnore]
			[JsonProperty(PropertyName = "accessToken")]
			public String AccessToken { get; set; }

			[JsonProperty(PropertyName = "teamId")]
			public String TeamId { get; set; }

		}

	}

    public class RequestGetTeamDataRefitDTO
	{
		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

	}

	public class ResponseGetTeamDataDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseGetTeamData Data { get; set; }
	}

	public class ResponseGetTeamData
	{
		[JsonProperty(PropertyName = "teamShirtURL")]
		public string TeamShirtURL { get; set; }

	}



}
