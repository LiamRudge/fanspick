﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetTeamsForCompetitionDTO
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "accessToken")]
        public String AccessToken { get; set; }

        [JsonProperty(PropertyName = "competitionId")]
        public String CompetitionId { get; set; }

    }

    public class RequestGetTeamsForCompetitionRefitDTO
	{
	
		[JsonProperty(PropertyName = "competitionId")]
		public String CompetitionId { get; set; }

	}


    public class ResponseGetTeamsForCompetitionDTO : ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public List<Team> Data { get; set; }
    }
}
