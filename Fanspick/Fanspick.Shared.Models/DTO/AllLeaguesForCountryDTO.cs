﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestAllLeaguesForCountryDTO
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "accessToken")]
        public String AccessToken { get; set; }

        [JsonProperty(PropertyName = "countryId")]
        public String CountryId { get; set; }

    }

    public class RequestAllLeaguesForCountryRefitDTO
	{

		[JsonProperty(PropertyName = "countryId")]
		public String CountryId { get; set; }

	}

    public class ResponseAllLeaguesForCountryDTO : ResponseBaseDTO
    {
        public ResponseAllLeaguesForCountryDTO()
        {
            this.Data = new List<Competition>();
        }

        [JsonProperty(PropertyName = "data")]
        public List<Competition> Data { get; set; }

    }
}