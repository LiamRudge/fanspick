﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestLoginStatusDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "onceLogin")]
        public string OnceLogin { get; set; }
	}

	public class ResponseLoginStatusDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseLoginStatusData Data { get; set; }
	}

	public class ResponseLoginStatusData
	{
		// api returns null
	}
}
