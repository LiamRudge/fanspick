﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestSetUserPickDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "isLive")]
		public Boolean IsLive { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

		[JsonProperty(PropertyName = "newPlayerId")]
		public String NewPlayerId { get; set; }

		[JsonProperty(PropertyName = "oldPlayerId")]
		public String OldPlayerId { get; set; }

		[JsonProperty(PropertyName = "positionId")]
		public String PositionId { get; set; }

		[JsonProperty(PropertyName = "formation")]
		public String Formation { get; set; }
	}

    public class RequestSetUserPickRefitDTO
	{
	
		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "isLive")]
		public Boolean IsLive { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

		[JsonProperty(PropertyName = "newPlayerId")]
		public String NewPlayerId { get; set; }

		[JsonProperty(PropertyName = "oldPlayerId")]
		public String OldPlayerId { get; set; }

		[JsonProperty(PropertyName = "positionId")]
		public String PositionId { get; set; }

		[JsonProperty(PropertyName = "formation")]
		public String Formation { get; set; }
	}

	public class ResponseSetUserPickDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseSetUserPickData Data { get; set; }
	}

	public class ResponseSetUserPickData
	{
		

	}


}