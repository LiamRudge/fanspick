﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestTickerBarDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

	}

	public class ResponseTickerBarDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseTickerBarData Data { get; set; }
	}

	public class ResponseTickerBarData
	{
		[JsonProperty(PropertyName = "newsText")]
        public String NewsText { get; set; }

		[JsonProperty(PropertyName = "bgcolour")]
		public String Bgcolour { get; set; }

		[JsonProperty(PropertyName = "count")]
		public int matchCount { get; set; }
        	
	}

}
