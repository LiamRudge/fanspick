﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestUnsetFavTeamDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public string TeamId { get; set; }


	}

	public class ResponseUnsetFavTeamDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseUnsetFavTeamData Data { get; set; }
	}

	public class ResponseUnsetFavTeamData
	{
		// api returns null
	}
}
