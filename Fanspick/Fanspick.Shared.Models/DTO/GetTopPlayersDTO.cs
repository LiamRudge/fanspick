﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class GetTopPlayersDTO
	{

		public class RequestGetTopPlayersDTO
		{
			[JsonIgnore]
			[JsonProperty(PropertyName = "accessToken")]
			public String AccessToken { get; set; }

			[JsonIgnore]
			[JsonProperty(PropertyName = "competitionId")]
			public String CompetitionId { get; set; }

		}


		public class ResponseGetTopPlayersDTO : ResponseBaseDTO
		{
            [JsonProperty(PropertyName = "data")]
            public List<ResponseGetTopPlayersData> Data { get; set; } = new List<ResponseGetTopPlayersData>();
		}

		public class ResponseGetTopPlayersData
		{
			//Top teams data to be populated

			[JsonProperty(PropertyName = "_id")]
			public string Id { get; set; }

			[JsonProperty(PropertyName = "players")]
            public Player PlayerDetails { get; set; }

		}

	

	}
}
