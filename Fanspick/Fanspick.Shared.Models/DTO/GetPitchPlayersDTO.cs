﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Xamarin.Forms;

namespace Fanspick.Shared.Models.DTO
{
    public class GetPitchPlayersDTO
	{
		public class RequestGetPitchPlayersDataDTO: RequestBaseDTO
		{
			[JsonIgnore]
			public override String APITarget { get { return "fanspick/getPitchPlayers"; } }


			[JsonProperty(PropertyName = "fixtureId")]
			public String FixtureId { get; set; }

			[JsonProperty(PropertyName = "teamId")]
			public String TeamId { get; set; }

			[JsonProperty(PropertyName = "selectedPlayerId")]
			public String SelPlayerId { get; set; }

			[JsonProperty(PropertyName = "isLive")]
			public Boolean IsLive { get; set; }

		}

		public class ResponsePitchPlayersDTO : ResponseBaseDTO
		{
		
			[JsonProperty(PropertyName = "data")]
			public List<ResponsePitchPlayersData> Data { get; set; } = new List<ResponsePitchPlayersData>();
		}

		public class ResponsePitchPlayersData
		{
			
			[JsonProperty(PropertyName = "playerId")]
			public pitchPlayersDetails pitchPlayersDetail { get; set; }
			[JsonProperty(PropertyName = "_id")]
			public string Id { get; set; }

			[JsonProperty(PropertyName = "positionId")]
			public positionValues positionValues { get; set; }
			
		}

		
		public class positionValues
		{
			[JsonProperty(PropertyName = "_id")]
			public string Id { get; set; }
			[JsonProperty(PropertyName = "PosX")]
			public string PosX { get; set; }
			[JsonProperty(PropertyName = "PosY")]
			public string PosY { get; set; }
			[JsonProperty(PropertyName = "Role")]
			public string Role { get; set; }
			[JsonProperty(PropertyName = "Key")]
			public string Key { get; set; }

		}
	}
}
