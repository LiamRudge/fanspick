﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.DeviceInfo;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestUserPManagerPLiveDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

	}

	public class ResponseUserPManagerPLiveDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseUserPManagerPLiveData Data { get; set; }
	}

	public class ResponseUserPManagerPLiveData
	{

        [JsonProperty(PropertyName = "userPickSub")]
        public List<ResponseSubstitute> userPickSub { get; set; } = new List<ResponseSubstitute>();

		[JsonProperty(PropertyName = "managerPickSub")]
		public List<ResponseSubstitute> managerPickSub { get; set; } = new List<ResponseSubstitute>();
	}


    public class ResponseSubstitute
    {

        [JsonProperty(PropertyName = "playerInId")]
        public PlayerResponse PlayerInId { get; set; }

		[JsonProperty(PropertyName = "playerOutId")]
		public PlayerResponse PlayerOutId { get; set; }


	public double SubTextSize
	{
		get
		{
			double size = 10;
			String device = CrossDeviceInfo.Current.Model;

			if (device == "iPad")
			{
				size = 15;
			}
			return size;
		}
	}


    public double SubImageSize
    {
        get
        {
            double size = 15;
            String device = CrossDeviceInfo.Current.Model;

            if (device == "iPad")
            {
                size = 17;
            }
            return size;
        }
    }

        public double SubBackImage
		{
			get
			{
				double size = 19;

				String device = CrossDeviceInfo.Current.Model;

				if (device == "iPad")
				{
					size = 25;
				}

				return size;
			}
		}
    }

	public class PlayerResponse
	{
		[JsonProperty(PropertyName = "_id")]
		public string _id { get; set; }

		[JsonProperty(PropertyName = "knownName")]
		public string KnownName
		{
			get
			{
				return WebUtility.HtmlDecode(decodedKnownName);
			}
			set
			{
				decodedKnownName = WebUtility.HtmlDecode(value);
			}
		}

		private string decodedKnownName;

		[JsonProperty(PropertyName = "lastname")]
		public string lastname { get; set; }

		[JsonProperty(PropertyName = "firstname")]
		public string firstname { get; set; }

	}


}