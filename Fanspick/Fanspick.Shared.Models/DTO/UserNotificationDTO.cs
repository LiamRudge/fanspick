﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace Fanspick.Shared.Models.DTO
{
    public class UserNotificationDTO
	{
		public class RequestUserNotificationDTO : RequestBaseDTO
		{
			[JsonIgnore]
			public override MethodType Method { get { return MethodType.Get; } }
        }

		public class ResponseUserNotificationDTO : ResponseBaseDTO
		{
			[JsonProperty(PropertyName = "data")]
			public List<ResponseUserNotificationData> Data { get; set; } = new List<ResponseUserNotificationData>();
		}

		public class ResponseUserNotificationData
		{
			[JsonProperty(PropertyName = "notificationId")]
			public NotificationDetails notification { get; set; }

			[JsonProperty(PropertyName = "isRead")]
			public bool isRead { get; set; }

			public string BackColor
			{
				get
				{
					string color = "";
					if (this.isRead)
					{
						color = "#5f6e8c";
					}
					else
					{
						color = "#22374f";
					}

					return color;
				}
			}


            public double TextsizeLine1{

                get{

                    double size = 11;
					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						size = 16;
					}

					return size;
                }


            }

			public double TextsizeLine2
			{

				get
				{

					double size = 10;
					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						size = 15;
					}

					return size;
				}


			}
			public double ImageSize
			{

				get
				{

					double size = 50;
					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						size = 80;
					}

					return size;
				}


			}

            public Thickness thick
			{

				get
				{

                    var size = new Thickness(12);
					String device = CrossDeviceInfo.Current.Model;

					if (device == "iPad")
					{
						size = new Thickness(28);
					}

					return size;
				}


			}

		}

		public class NotificationDetails
		{
			[JsonProperty(PropertyName = "_id")]
            public string _id { get; set; }

			[JsonProperty(PropertyName = "notificationType")]
			public string NotificationType { get; set; }

			[JsonProperty(PropertyName = "notificationTitle")]
			public string NotificationTitle { get; set; }

			[JsonProperty(PropertyName = "notificationMessage")]
			public string NotificationMessage { get; set; }

			[JsonProperty(PropertyName = "triggerAgeCheck")]
			public string TriggerAgeCheck { get; set; }

			[JsonProperty(PropertyName = "bannerImage")]
			public string BannerImage { get; set; }

			[JsonProperty(PropertyName = "bannerUrl")]
			public string BannerUrl { get; set; }

			[JsonProperty(PropertyName = "notificationIcon")]
			public string NotificationIcon { get; set; }



			public ImageSource NotiIcon
			{
				get
				{
                    return ConvertToImage(this.NotificationIcon);
				}
			}


			[JsonProperty(PropertyName = "questionnaires")]
			public List<Questionnaires> questionnaires { get; set; } = new List<Questionnaires>();
		}

		public class Questionnaires
		{
			[JsonProperty(PropertyName = "questions")]
            public List<Questions> questions { get; set; } = new List<Questions>();

		}

		public static ImageSource ConvertToImage(string imageBase64)
		{
			try
			{
				ImageSource image;

				if (string.IsNullOrEmpty(imageBase64))
				{
					image = Xamarin.Forms.ImageSource.FromFile("small_logo_no_border.png");
                    return image;
				}
				else
				{
					using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(imageBase64)))
					{
						return Xamarin.Forms.ImageSource.FromStream(() => ms);
					}
				}
				
			}
			catch (Exception ex)
			{
				return Xamarin.Forms.ImageSource.FromFile("small_logo_no_border.png");
			}

		}

		

		public class Questions
		{
			[JsonProperty(PropertyName = "question")]
            public string question { get; set; }

			[JsonProperty(PropertyName = "_id")]
			public string questionId { get; set; }
		}
	}
}


//BannerAlert

//"notificationType": "BasicAlert",