﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestUnSetUserPickDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "isLive")]
		public Boolean IsLive { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

		[JsonProperty(PropertyName = "playerId")]
		public String PlayerId { get; set; }
	}

    public class RequestUnSetUserPickRefitDTO
	{
		[JsonProperty(PropertyName = "fixtureId")]
		public String FixtureId { get; set; }

		[JsonProperty(PropertyName = "isLive")]
		public Boolean IsLive { get; set; }

		[JsonProperty(PropertyName = "teamId")]
		public String TeamId { get; set; }

		[JsonProperty(PropertyName = "playerId")]
		public String PlayerId { get; set; }
	}

	public class ResponseUnSetUserPickDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseUnSetUserPickData Data { get; set; }
	}

	public class ResponseUnSetUserPickData
	{
		
	}


}