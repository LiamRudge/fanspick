﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestGetGroupDetailDTO
    {
        
        [JsonProperty(PropertyName = "groupId")]
        public string GroupId { get; set; }

    }

    public class ResponseGetGroupDetailDTO : ResponseBaseDTO
    {
        [JsonProperty(PropertyName = "data")]
        public ResponseGetGroupDetailData Data { get; set; }
    }

    public class ResponseGetGroupDetailData
    {
        [JsonProperty(PropertyName = "groupId")]
        public String GroupId { get; set; }

        [JsonProperty(PropertyName = "type")]
        public String GroupType { get; set; }


		[JsonProperty(PropertyName = "createdBy")]
		public MemberDetail CreatedBy { get; set; }

		[JsonProperty(PropertyName = "groupMembers")]
		public List<GroupMembersDetails> groupMembers { get; set; } = new List<GroupMembersDetails>();

		[JsonProperty(PropertyName = "admin")]
		public List<AdminMembersDetails> admin { get; set; } = new List<AdminMembersDetails>();
    }

	public class GroupMembersDetails
	{

		[JsonProperty(PropertyName = "memberId")]
		public MemberDetail MemberId { get; set; }

	}

	public class AdminMembersDetails
	{
		[JsonProperty(PropertyName = "adminId")]
		public MemberDetail AdminId { get; set; }
	}


	public class MemberDetail
	{
		[JsonProperty(PropertyName = "_id")]
        public string _id { get; set; }

		[JsonProperty(PropertyName = "username")]
		public string username { get; set; }

		[JsonProperty(PropertyName = "lastname")]
		public string lastname { get; set; }

		[JsonProperty(PropertyName = "firstname")]
		public string firstname { get; set; }
	}
    	

}
