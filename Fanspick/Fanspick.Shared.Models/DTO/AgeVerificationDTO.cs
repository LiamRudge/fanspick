﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestAgeVerificationDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "dob")]
		public string Dob { get; set; }


	}

	public class ResponseAgeVerificationDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseAgeVerificationData Data { get; set; }
	}

	public class ResponseAgeVerificationData
	{
		// api returns null
	}
}
