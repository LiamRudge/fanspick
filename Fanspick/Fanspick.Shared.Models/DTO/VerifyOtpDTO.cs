﻿using System;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
    public class RequestVerifyOtpDTO
	{
        [JsonProperty(PropertyName = "otp")]
		public string OTP { get; set; }

    }

	public class ResponseVerifyOtpDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public ResponseAgeVerificationData Data { get; set; }
	}

	public class ResponseVerifyOtpData
	{
		// api returns null
	}

}
