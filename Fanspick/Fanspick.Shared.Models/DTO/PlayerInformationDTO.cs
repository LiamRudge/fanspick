﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Fanspick.Shared.Models.DTO
{
	public class RequestPlayerInfoDTO
	{
		[JsonIgnore]
		[JsonProperty(PropertyName = "accessToken")]
		public String AccessToken { get; set; }

		[JsonProperty(PropertyName = "playerId")]
		public string PlayerId { get; set; }


	}

    public class RequestPlayerInfoRefitDTO
	{
	
		[JsonProperty(PropertyName = "playerId")]
		public string PlayerId { get; set; }


	}

	public class ResponsePlayerInfoDTO : ResponseBaseDTO
	{
		[JsonProperty(PropertyName = "data")]
		public List<Player> Data { get; set; } = new List<Player>();

	}
}
