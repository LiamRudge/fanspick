﻿using System;
using Fanspick.Screens;
using Fanspick.Shared.Models;
//using Quobject.SocketIoClientDotNet.Client;
using Xamarin.Forms;

namespace Fanspick
{
    public partial class App : Application
    {
        public static NavigationPage nav;
		public static byte[] CroppedImage;
		public App()
        {
            InitializeComponent();
            Shared.Helper.FanspickSettings.RuntimePurpose = Shared.Models.AppModels.RuntimePurpose.Live;
            MainPage = new NavigationPage(new Screens.SplashScreen())
            { 
                 BarBackgroundColor = Color.FromHex(Shared.Helper.FanspickStrings.Color_FpDarkBlue),
			     BarTextColor = Color.White
			};

            RegisterDependencyServices();

        }

        private void RegisterDependencyServices()
        {
            //DependencyService.Register<IScreenCapture>();
        }

        protected override void OnStart()
        {
            // Handle when your app starts

        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes

        }

    }
}
