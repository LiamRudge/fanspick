﻿﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Refit;

namespace Fanspick.RestAPI
{
    public interface IGitHubApi
    {
        [Post("/fanspick/login")]
		Task<ResponseLoginDTO> LoginUser([Body] RequestLoginDTO user);

		[Post("/fanspick/login")]
		Task<ResponseFBLoginDTO> LoginFBUser([Body] RequestFacebookLoginDTO user);

        [Get("/fanspick/viewProfile")]
		Task<ResponseViewProfileDTO> GetProfile([Header("authorization")] string authorization);

        [Get("/?q={teamId}")]
        Task<ResponseGetSocialFeedDTO> GetSocial([AliasAs("teamId")] string TeamId);

		[Post("/fanspick/getTeamData")]
		Task<ResponseGetTeamDataDTO> GetTeamData([Body] RequestGetTeamDataRefitDTO user, [Header("authorization")] string authorization);

		[Get("/fanspick/getAllFavouriteTeams")]
		Task<ResponseMyTeamsDTO> GetAllFavouriteTeams([Header("authorization")] string authorization);

		[Post("/fanspick/getUpcomingFixtures")]
		Task<ResponseGetUpcomingFixturesDTO> GetUpcomingFixtures([Body] RequestGetUpcomingFixturesRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/getAllFormations")]
		Task<ResponseGetAllFormationsDTO> GetAllFormations([Header("authorization")] string authorization);

		[Post("/fanspick/getTeamsForCompetitionName")]
		Task<ResponseCupGamesDTO> GetTeamsForCompetitionName([Body] RequestCupGameRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/setFavouriteTeam")]
		Task<ResponseSetFavTeamDTO> SetFavouriteTeam([Body] RequestSetFavTeamRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/getPlayerData")]
		Task<ResponsePlayerInfoDTO> GetPlayerData([Body] RequestPlayerInfoRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/register")]
		Task<ResponseLoginDTO> Register([Body] RequestSignUpDTO user);

		[Post("/fanspick/editProfile")]
		Task<ResponseUpdateProfileDTO> EditProfile([Body] RequestUpdateProfileRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/editProfile")]
		Task<ResponseUpdateProfileDTO> EditProfilePro([Body] RequestUpdateProfileRefitPRoDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/editProfile")]
        Task<ResponseUpdateProfileDTO> EditProfileDictonary([Body] Dictionary<string, string> user, [Header("authorization")] string authorization);

        [Post("/fanspick/getCountriesForSport")]
        Task<ResponseGetCountriesForSportDTO> GetCountriesForSport([Body] RequestGetCountriesForSportRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/getLeaguesForCountry")]
		Task<ResponseAllLeaguesForCountryDTO> GetLeaguesForCountry([Body] RequestAllLeaguesForCountryRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/getTeamsForCompetition")]
		Task<ResponseGetTeamsForCompetitionDTO> GetTeamsForCompetition([Body] RequestGetTeamsForCompetitionRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/getMyCommunity")]
		Task<ResponseMyCommunityDTO> GetMyCommunity([Body] RequestMyCommunityRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/getTeamSquad")]
		Task<ResponseGetTeamSquadDTO> GetTeamSquad([Body] RequestGetTeamSquadRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/getFormationByType")]
		Task<ResponseGetFormationByTypeDTO> GetFormationByType([Body] RequestGetFormationByTypeRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/getUserPick")]
		Task<ResponseGetUserPickDTO> GetUserPick([Body] RequestGetUserPickRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/getNewFormationWithPlayers")]
		Task<ResponseChangeFormationDTO> GetNewFormationWithPlayers([Body] RequestChangeFormationRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/unsetUserPick")]
        Task<String> UnsetUserPick([Body] RequestUnSetUserPickRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/setUserPick")]
        Task<String> SetUserPick([Body] RequestSetUserPickRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/swapPlayers")]
		Task<ResponseSwapPitchPlayersDTO> SwapPlayers([Body] RequestSwapPitchPlayersDataRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/getManagerPick_V2")]
		Task<ResponseGetManagerPickDTO> GetManagerPick([Body] RequestGetManagerPickRefitDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/getFanspickFixture")]
		Task<ResponseGetFanspickDTO> GetFanspick([Body] RequestGetFanspickRefitDTO user, [Header("authorization")] string authorization);

        [Post("/fanspick/getSponsorBillboard")]
		Task<ResponseGetBillBoardDTO> GetSponsorBillboard([Body] RequestGetBillBoardDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/deleteChatsForGroup")]
		Task<ResponseGetBillBoardDTO> DeleteChatsForGroup([Body] RequestDeleteMessageDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/getGroupDetail")]
		Task<ResponseGetGroupDetailDTO> GetGroupDetail([Body] RequestGetGroupDetailDTO user, [Header("authorization")] string authorization);

		[Post("/fanspick/generateOTP")]
        Task<ResponseGenerateOtpDTO> GenerateOTP([Body] RequestGenerateOtpDTO user);

		[Post("/fanspick/verifyOTP")]
		Task<ResponseVerifyOtpDTO> VerifyOTP([Body] RequestVerifyOtpDTO user);

		[Put("/fanspick/resetForgotPassword")]
		Task<ResponseResetPasswordDTO> ResetForgotPassword([Body] RequestResetPasswordDTO user);

    }
}