﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Plugin.DeviceInfo;
using Plugin.Geolocator;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;

namespace Fanspick.PluginModels
{
    public class Walkthrough
    {
        public List<CarouselPage> CarouselPages { get; set; } = new List<CarouselPage>();
        public RequestEventLoggingDTO requestEventLoggingDTO { get; set; } = new RequestEventLoggingDTO();

        public Walkthrough()
        {

            CarouselPages.Add(new CarouselPage() { Image = "1.YourPick_pre.png", IsImage = true, IsText = false });
            CarouselPages.Add(new CarouselPage() { Image = "2.YourPick_Live.png", IsImage = true, IsText = false });
            CarouselPages.Add(new CarouselPage() { Image = "3.ManagerPick.png", IsImage = true, IsText = false });
            CarouselPages.Add(new CarouselPage() { Image = "4.Fanspick Pre.png", IsImage = true, IsText = false });
            CarouselPages.Add(new CarouselPage() { Image = "5.Fanspick Live.png", IsImage = true, IsText = false });
            Position = 0;

            ItemsSource = new ObservableCollection<int>() { 0, 1, 2, 3,4};

            TemplateSelector = new TemplateSelector();
			
        }

        public int Position { get; set; }

        public ObservableCollection<int> ItemsSource { get; set; }

        public TemplateSelector TemplateSelector { get; set; }

		public async Task<ServiceResponse> EventLog()
		{
            
            ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
			return serviceResponse;
		}


    }


}