﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Fanspick.PluginModels
{
    public class TemplateSelector : DataTemplateSelector
    {
        private readonly DataTemplate WalkthroughPageIntro;

        public TemplateSelector()
        {
            this.WalkthroughPageIntro = new DataTemplate(typeof(CarouselViews.WalkthroughPageInfographic));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            //if ((int)item % 2 == 0)
            //return templateTwo;
            return WalkthroughPageIntro;
        }
    }
}
