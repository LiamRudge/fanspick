﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Fanspick.PluginModels
{
    public class TemplateSelector_Pitches : DataTemplateSelector
    {
        private readonly DataTemplate YourPick_Runup;

        public TemplateSelector_Pitches()
        {
            this.YourPick_Runup = new DataTemplate(typeof(Templates.FanspickPitch));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            //if ((int)item % 2 == 0)
            //return templateTwo;
            return YourPick_Runup;
        }
    }
}
