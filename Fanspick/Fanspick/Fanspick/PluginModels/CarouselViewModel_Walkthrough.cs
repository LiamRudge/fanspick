﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Plugin.DeviceInfo;
using Plugin.Geolocator;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;

namespace Fanspick.PluginModels
{
    public class CarouselViewModel_Walkthrough
    {
        public List<CarouselPage> CarouselPages { get; set; } = new List<CarouselPage>();
        public RequestEventLoggingDTO requestEventLoggingDTO { get; set; } = new RequestEventLoggingDTO();

        public CarouselViewModel_Walkthrough()
        {

            CarouselPages.Add(new CarouselPage() { Image = "", IsImage = false, IsText = true , width=GetWidth()});
            CarouselPages.Add(new CarouselPage() { Image = "one.png", IsImage = true, IsText = false , width = GetWidth()});
            CarouselPages.Add(new CarouselPage() { Image = "two.png", IsImage = true, IsText = false , width = GetWidth()});
            CarouselPages.Add(new CarouselPage() { Image = "three.png", IsImage = true, IsText = false , width = GetWidth()});
            Position = 0;

            ItemsSource = new ObservableCollection<int>() { 0, 1, 2, 3 };

            TemplateSelector = new TemplateSelector();
			
        }

        public int Position { get; set; }

        public ObservableCollection<int> ItemsSource { get; set; }

        public TemplateSelector TemplateSelector { get; set; }

		public async Task<ServiceResponse> EventLog()
		{
            
            ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
			return serviceResponse;
		}

        public double GetWidth()
		{

            double width = 170;
			String device = CrossDeviceInfo.Current.Model;

            if (device == "iPad")
            {
                width = 250;
            }else{
                width = 170;
            }

			return width;
		}


    }

    public class CarouselPage{
        public bool IsText
        {
            get;
            set;
        }
        public bool IsImage
        {
            get;
            set;
        }
        public string Image
        {
            get;
            set;
        }

        public double width
		{
			get;
			set;
		}


   
    }


   


}