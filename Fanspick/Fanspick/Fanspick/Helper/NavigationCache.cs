﻿using Fanspick.PopUp;
using Fanspick.Screens;
using Fanspick.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Fanspick.Helper
{
    public static partial class CacheStorage
    {
        static List<NavigationPageDetails> navigationPages = null;
        public static List<NavigationPageDetails> NavigationPages
        {
            get
            {
                if (navigationPages == null)
                {
                    AddNavigationPages();
                }
                return navigationPages;
            }
            set
            {
                navigationPages = value;
            }
        }

        private static void AddNavigationPages()
        {
            
            navigationPages = new List<NavigationPageDetails>();
            navigationPages.Add(new NavigationPageDetails()
            {
                Title = "Home",
                Type = ScreenType.HomePitch,
                IsVisbleInNavigation = true,
                IsPopType = false,
                HasRemovePreviousPage = true,
                TargetType = typeof(HomePage),
                IconSource = "icon_home.png"
                    
            });

            navigationPages.Add(new NavigationPageDetails()
            {
                Title = "My Profile",
                Type = ScreenType.MyProfile,
                IsVisbleInNavigation = true,
                IsPopType = false,
                HasRemovePreviousPage = true,
                TargetType = typeof(UserProfilePage),
                IconSource = "slider_icon_my_teams.png",
            });


            navigationPages.Add(new NavigationPageDetails()
            {
                Title = "My Team(s)",
                Type = ScreenType.MyTeam,
                IsVisbleInNavigation = true,
                IsPopType = false,
                HasRemovePreviousPage = true,
                TargetType = typeof(MyTeamPage),
                IconSource = "slider_icon_my_teams.png",
            });


            navigationPages.Add(new NavigationPageDetails()
            {
                Title = "Teams",
                Type = ScreenType.Teams,
                IsVisbleInNavigation = true,
                IsPopType = false,
                HasRemovePreviousPage = true,
                TargetType = typeof(App_TeamSelector),
                IconSource = "slider_icon_team.png",
            });


			navigationPages.Add(new NavigationPageDetails()
			{
				Title = "Europa League",
				Type = ScreenType.Europa,
				IsVisbleInNavigation = true,
				IsPopType = false,
				HasRemovePreviousPage = true,
                TargetType = typeof(EuropaLeague),
				IconSource = "uefa-europa-league.png",
			});

			navigationPages.Add(new NavigationPageDetails()
			{
				Title = "Champions League",
				Type = ScreenType.Champions,
				IsVisbleInNavigation = true,
				IsPopType = false,
				HasRemovePreviousPage = true,
                TargetType = typeof(ChampionLeague),
				IconSource = "Champions-League-logo.png",
			});

            navigationPages.Add(new NavigationPageDetails()
            {
                Title = "Fixtures",
                Type = ScreenType.Fixtures,
                IsVisbleInNavigation = false,
                IsPopType = false,
                HasRemovePreviousPage = true,
                TargetType = typeof(FixturePage),
                IconSource = "icon_logo_vector.png",
            });


            navigationPages.Add(new NavigationPageDetails()
            {
                Title = "Standings",
                Type = ScreenType.Standings,
                IsVisbleInNavigation = true,
                IsPopType = false,
                HasRemovePreviousPage = true,
                TargetType = typeof(MenuStanding),
                IconSource = "menu_icon_standings.png",
            });

			navigationPages.Add(new NavigationPageDetails()
			{
				Title = "Chat with Friends",
                Type = ScreenType.ContactChat,
				IsVisbleInNavigation = true,
				IsPopType = false,
				HasRemovePreviousPage = true,
                TargetType = typeof(ChatContacts),
				IconSource = "chat_icon.png",
			});


            navigationPages.Add(new NavigationPageDetails()
            {
                Title = "Communities",
                Type = ScreenType.Community,
                IsVisbleInNavigation = false,
                IsPopType = false,
                TargetType = null,
                HasRemovePreviousPage = true,
                IconSource = "menu_icon_standings.png",
            });


            navigationPages.Add(new NavigationPageDetails()
            {
                Title = "Sports",
                Type = ScreenType.Sports,
                IsVisbleInNavigation = true,
                IsPopType = false,
                HasRemovePreviousPage = true,
                TargetType = typeof(SportsPage),
                IconSource = "slider_icon_sports.png",
            });

			navigationPages.Add(new NavigationPageDetails()
			{
				Title = "How it works",
                Type = ScreenType.help,
				IsVisbleInNavigation = true,
				IsPopType = false,
				HasRemovePreviousPage = true,
                TargetType = typeof(HelpPage),
				IconSource = "icon_help2.png",
			});

            navigationPages.Add(new NavigationPageDetails()
            {
                Title = "Logout",
                Type = ScreenType.Logout,
                IsVisbleInNavigation = true,
                IsPopType = false,
                HasRemovePreviousPage = true,
                TargetType = typeof(App_Logout),
                IconSource = "icon_logout.png",
            });

            navigationPages.Add(new NavigationPageDetails()
            {
                Title = "Team Squad",
                Type = ScreenType.TeamSquad,
                IsVisbleInNavigation = false,
                IsPopType = true,
                HasRemovePreviousPage = true,
                TargetType = typeof(TeamSquad)
            });

			navigationPages.Add(new NavigationPageDetails()
			{
				Title = "Player PopUp",
                Type = ScreenType.OptionPop,
				IsVisbleInNavigation = false,
				IsPopType = true,
				HasRemovePreviousPage = true,
                TargetType = typeof(OptionPop)
			});


			navigationPages.Add(new NavigationPageDetails()
			{
				Title = "Terms",
				Type = ScreenType.Terms,
				IsVisbleInNavigation = false,
				IsPopType = true,
				HasRemovePreviousPage = true,
                TargetType = typeof(TermsPopUp)
			});

			navigationPages.Add(new NavigationPageDetails()
            {
                Title = "FormationPop",
                Type = ScreenType.FormationPop,
                IsVisbleInNavigation = false,
                IsPopType = true,
                HasRemovePreviousPage = true,
                TargetType = typeof(FormationOption)
			});



			navigationPages.Add(new NavigationPageDetails()
			{
				Title = "Sample Profile",
                Type = ScreenType.Player,
				IsVisbleInNavigation = false,
				IsPopType = true,
				HasRemovePreviousPage = true,
				TargetType = typeof(PlayerInformationPage)
			});



			navigationPages.Add(new NavigationPageDetails()
			{
				Title = "BillBoard",
				Type = ScreenType.BillBoard,
				IsVisbleInNavigation = false,
				IsPopType = true,
				HasRemovePreviousPage = true,
				TargetType = typeof(BillBoard)
			});

			navigationPages.Add(new NavigationPageDetails()
			{
				Title = "OnBoarding",
                Type = ScreenType.onBoarding,
				IsVisbleInNavigation = false,
				IsPopType = true,
				HasRemovePreviousPage = true,
                TargetType = typeof(OnBoardingPopup)
			});


			navigationPages.Add(new NavigationPageDetails()
            {
                Title = "Social Web View",
                Type = ScreenType.SocialWebView,
                IsVisbleInNavigation = false,
                IsPopType = false,
                HasRemovePreviousPage = false,
                TargetType = typeof(SocialWebView)
            });

			navigationPages.Add(new NavigationPageDetails()
			{
				Title = "Fanspick",
                Type = ScreenType.ContextMenu,
				IsVisbleInNavigation = false,
                IsPopType = true,
				HasRemovePreviousPage = false,
                TargetType = typeof(ContextMenu)
			});

            navigationPages.Add(new NavigationPageDetails()
            {
                Title = "Fanspick",
                Type = ScreenType.AlertPopUp,
                IsVisbleInNavigation = false,
                IsPopType = true,
                HasRemovePreviousPage = false,
                TargetType = typeof(PopUp.NavigationPopUp)
            });

			navigationPages.Add(new NavigationPageDetails()
			{
				Title = "Fanspick",
                Type = ScreenType.InAppWebView,
				IsVisbleInNavigation = false,
				IsPopType = true,
				HasRemovePreviousPage = false,
                TargetType = typeof(InAppWebView)
			});
        }

        public static NavigationPageDetails GetNavigationPageDetails(ScreenType screenType)
        {
            return CacheStorage.NavigationPages.FirstOrDefault(x => x.Type == screenType);
        }

        public static NavigationPageDetails GetNavigationPageDetails(string screenType)
        {
            ScreenType type;
            Enum.TryParse(screenType, out type);
            return GetNavigationPageDetails(type);
        }
    }

    public class NavigationPageDetails
    {
        public ScreenType Type { get; set; }
        public Type TargetType { get; set; }
        public string Title { get; set; }
        public string IconSource { get; set; }
        public bool IsVisbleInNavigation { get; set; }
        public bool IsPopType { get; set; }
        public bool HasRemovePreviousPage { get; set; }
    }

    public enum ScreenType
    {
        MyProfile,
        MyTeam,
        Teams,
        Fixtures,
        Standings,
        Sports,
        Logout,
        HomePitch,
        TeamSquad,
        Community,
        SocialWebView,
        ContextMenu,
        OptionPop,
        BillBoard,
        FormationPop,
        Europa,
        Champions,
        Player,
        AlertPopUp,
        Terms,
        help,
        onBoarding,
        ContactChat,
        InAppWebView
    }
    

}
