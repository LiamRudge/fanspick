﻿using Fanspick.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Helper
{
    public static partial class CacheStorage
    {
        static AllViewModels viewModels = null;
        public static AllViewModels ViewModels
        {
            get
            {
                if (viewModels == null)
                    viewModels = new AllViewModels();
                return viewModels;
            }
            set {
                viewModels = value;
            }
        }
    }
}
