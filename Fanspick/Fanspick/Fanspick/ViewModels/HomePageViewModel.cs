﻿using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.AppModels;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.ViewModels
{
    public class HomePageViewModel : BasicViewModel, INotifyPropertyChanged
    {

       
        public HomePageViewModel()
        {
        }


        public List<Fanspick.Shared.Models.Fixture> Fixtures
        {
            get
            {
                return FanspickCache.CurrentFanspickScope.UpComingFixtures;
            }
        }

        public Fanspick.Shared.Models.Fixture NextFixture
        {
            get
            {
                List<Fanspick.Shared.Models.Fixture> fixtures = Fixtures;
                if (fixtures != null)
                    return Fixtures.First();
                return null;
            }
        }


        public Fanspick.Shared.Models.Fixture SelectedFixture
        {
            get
            {
                return FanspickCache.CurrentFanspickScope.SelectedFixture;
            }
        }

        public Fanspick.Shared.Models.Team SelectedTeam
        {
            get
            {
                return FanspickCache.CurrentFanspickScope.SelectedTeam;
            }
        }

        public FanspickUser UserData
        {
            get
            {
                return FanspickCache.UserData;
            }
        }



        public String TickerBarUrl
        {
            get
            {
                return $"http://fanspick-admin-test.5sol.co.uk/Marquee/Index/0";
                //if (SelectedFixture != null)
                //return $"http://fanspick-admin-test.5sol.co.uk/Marquee/Index/{this.SelectedFixture.Id}";
                //return $"http://fanspick-admin-test.5sol.co.uk/Marquee/Index/0";

            }
        }

        public bool NoMoreNextFixture
        {
            get
            {
                return (NextFixture == null);
            }
        }

        private PitchViewModel managerPickVM = null;
        public PitchViewModel ManagerPickVM
        {
            get
            {
                if (managerPickVM == null)
                {
                    managerPickVM = new PitchViewModel();
                }
                if (FanspickCache.CurrentFanspickScope.ManagerPickData != null && FanspickCache.CurrentFanspickScope.ManagerPickData.Positions != null)
                {
                    ResponsePickData pickData = FanspickCache.CurrentFanspickScope.ManagerPickData;
                    managerPickVM.ReInitialize(pickData.Positions, pickData.LineUpPlayers);
                }
                else
                {
                    managerPickVM.SetDefaultPitch();
                }
                return managerPickVM;
            }
        }


        private PitchViewModel yourPickVM = null;
        public PitchViewModel YourPickVM
        {
            get
            {
                ResponsePickData pickData;
                if (yourPickVM == null)
                {
                    yourPickVM = new PitchViewModel();
                }
                if (FanspickCache.CurrentFanspickScope.YourPickData != null)
                {
                    pickData = FanspickCache.CurrentFanspickScope.YourPickData;
                    yourPickVM.ReInitialize(pickData.Positions, pickData.LineUpPlayers);
                }
                else
                {
                    yourPickVM.SetDefaultPitch();
                }

                return yourPickVM;
            }
        }


        private PitchViewModel fansPickVM = null;
        public PitchViewModel FansPickVM
        {
            get
            {
                if (fansPickVM == null)
                {
                    fansPickVM = new PitchViewModel();
                }
                if (FanspickCache.CurrentFanspickScope.FanspickPickData != null)
                {
                    ResponsePickData pickData = FanspickCache.CurrentFanspickScope.FanspickPickData;
                    fansPickVM.ReInitialize(pickData.Positions, pickData.LineUpPlayers);
                }
                else
                {
                    fansPickVM.SetDefaultPitch();
                }
                return fansPickVM;
            }
        }

        private SocialViewModel socialVM = null;
        public SocialViewModel SocialVM
        {
            get
            {
                if (socialVM == null)
                {
                    socialVM = new SocialViewModel();
                }
                return socialVM;
            }
        }

        public async void RefreshSocialData()
        {
            await SocialVM.LoadData();
        }


        //public async Task RefreshPitchData()
        //{
        //    await LoadPitchData();
        //}

        private PitchPickViewModel yourPickViewModel;
        public PitchPickViewModel YourPickViewModel
        {
            get
            {
                if (yourPickViewModel==null)
                {
                    yourPickViewModel = new PitchPickViewModel();
                }

                RaisePropertyChanged("YourPickViewModel");

                return yourPickViewModel;
            }

            set{
                yourPickViewModel = value;
				RaisePropertyChanged("YourPickViewModel");
            }
        }

		private PitchPickViewModel yourPickViewModelPre;
		public PitchPickViewModel YourPickViewModelPre
		{
			get
			{
				if (yourPickViewModelPre == null)
				{
					yourPickViewModelPre = new PitchPickViewModel();
				}

				RaisePropertyChanged("YourPickViewModelPre");

				return yourPickViewModelPre;
			}
		}

        public PitchPickViewModel ManagersPickViewModel { get; set; } = new PitchPickViewModel();
        public PitchPickViewModel FansPickViewModel { get; set; } = new PitchPickViewModel();


        private string counterText = string.Empty;
        public string CounterText
        {
            get
            {
                return counterText;
            }
            set
            {
                counterText = value;
                RaisePropertyChanged("CounterText");
            }
        }

		private string counterTimeText = string.Empty;
		public string CounterTimeText
		{
			get
			{
				return counterTimeText;
			}
			set
			{
				counterTimeText = value;
				RaisePropertyChanged("CounterTimeText");
			}
		}
    }
}
