﻿using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using static Fanspick.Shared.Models.DTO.MyTeamsDTO;
using Fanspick.Shared.Models.DTO;
using static Fanspick.Shared.Models.DTO.CupGamesDTO;

namespace Fanspick.ViewModels
{
	public class MyTeamPageViewModel : BasicViewModel
	{
		public RequestMyTeamsDTO requestMyTeamsDTO { get; set; } = new RequestMyTeamsDTO();
        public RequestUnsetFavTeamDTO requestUnsetFavTeamDTO { get; set; } = new RequestUnsetFavTeamDTO();
		public RequestCupGameDTO requestCupGame { get; set; } = new RequestCupGameDTO();
		public RequestSetFavTeamDTO requestSetFavTeamDTO { get; set; } = new RequestSetFavTeamDTO();

        public ResponseMyTeamsDTO ResponseMyTeams { get; set; }

		public MyTeamPageViewModel()
		{
			requestMyTeamsDTO.AccessToken = FanspickCache.UserData.AccessToken;
            requestUnsetFavTeamDTO.AccessToken = FanspickCache.UserData.AccessToken;
            requestCupGame.AccessToken = FanspickCache.UserData.AccessToken;
            requestSetFavTeamDTO.AccessToken = FanspickCache.UserData.AccessToken;
		}

		public async Task<ServiceResponse> getAllFavoriteTeams()
		{
            ServiceResponse serviceResponse = await new FanspickServices<MyTeamsService>().Run(requestMyTeamsDTO);
			return serviceResponse;
		}

		public async Task<ServiceResponse> SetFavTeam()
		{
			ServiceResponse serviceResponse = await new FanspickServices<SetFavouriteTeamService>().Run(requestSetFavTeamDTO);
			return serviceResponse;
		}

		public async Task<ServiceResponse> getEuropaTeams()
		{
            ServiceResponse serviceResponse = await new FanspickServices<CupGamesService>().Run(requestCupGame);
			return serviceResponse;
		}

		public async Task<ServiceResponse> getChampionsTeams()
		{
			ServiceResponse serviceResponse = await new FanspickServices<CupGamesService>().Run(requestCupGame);
			return serviceResponse;
		}


		private List<ResponseMyTeamsData> favTeams;
		public List<ResponseMyTeamsData> FavTeams
		{
			get
			{
				return favTeams;
			}
			set
			{
				this.favTeams = value;
				RaisePropertyChanged("FavTeams");
			}
		}


		private List<ResponseCupGameData> europaTeams;
		public List<ResponseCupGameData> EuropaTeams
		{
			get
			{
				return europaTeams;
			}
			set
			{
				this.europaTeams = value;
				RaisePropertyChanged("EuropaTeams");
			}
		}


		public async Task<ServiceResponse> UnsetFavTeam()
		{
            ServiceResponse serviceResponse = await new FanspickServices<UnsetFavouriteTeamService>().Run(requestUnsetFavTeamDTO);
			return serviceResponse;
		}

	}
}
