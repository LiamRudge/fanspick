﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;
using static Fanspick.Shared.Models.DTO.MarkAllNotificationDeleteDTO;
using static Fanspick.Shared.Models.DTO.MarkAllNotificationReadDTO;
using static Fanspick.Shared.Models.DTO.UserNotificationDTO;

namespace Fanspick.ViewModels
{
	public class AlertScreenModal : BasicViewModel
	{
		public RequestUserNotificationDTO requestUserNotification { get; set; } = new RequestUserNotificationDTO();
        public RequestUpdateUserNotificationDTO requestUpdateUserNotification { get; set; } = new RequestUpdateUserNotificationDTO();
        public RequestNotificationResponseDTO requestNotificationResponse { get; set; } = new RequestNotificationResponseDTO();
        public RequestAgeVerificationDTO requestAgeVerification { get; set; } = new RequestAgeVerificationDTO();
        public RequestMarkAllNotificationReadDTO requestMarkAllNotificationRead { get; set; } = new RequestMarkAllNotificationReadDTO();
        public RequestMarkAllNotificationDeleteDTO requestMarkAllNotificationDelete { get; set; } = new RequestMarkAllNotificationDeleteDTO();
		public RequestEventLoggingDTO requestEventLoggingDTO { get; set; } = new RequestEventLoggingDTO();
        

		public AlertScreenModal()
		{
			requestUserNotification.AccessToken = FanspickCache.UserData.AccessToken;
            requestUpdateUserNotification.AccessToken = FanspickCache.UserData.AccessToken;
            requestNotificationResponse.AccessToken = FanspickCache.UserData.AccessToken;
            requestAgeVerification.AccessToken = FanspickCache.UserData.AccessToken;
            requestMarkAllNotificationRead.AccessToken = FanspickCache.UserData.AccessToken;
            requestMarkAllNotificationDelete.AccessToken = FanspickCache.UserData.AccessToken;

        }

		public async Task<ServiceResponse> GetAlerts()
		{
            ServiceResponse serviceResponse = await new FanspickServices<UserNotificationService>().Run(requestUserNotification);
			return serviceResponse;
		}

        public async Task<ServiceResponse> UpdateNotificationStatus(string notficationID, bool isRead, bool isDelete)
        {
            requestUpdateUserNotification.NotificationId = notficationID;
            requestUpdateUserNotification.ReadStatus = isRead;
            requestUpdateUserNotification.DeletedStatus = isDelete;

            ServiceResponse serviceResponse = await new FanspickServices<UpdateNotificationService>().Run(requestUpdateUserNotification);
			return serviceResponse;
		}

		public async Task<ServiceResponse> CreateNotificationResponse(string notficationID,string questionId, string questionText,string response)
		{
			requestNotificationResponse.NotificationId = notficationID;
            requestNotificationResponse.QuestionId = questionId;
            requestNotificationResponse.QuestionText = questionText;
            requestNotificationResponse.Response = response;
		
            ServiceResponse serviceResponse = await new FanspickServices<NotificationResponseService>().Run(requestNotificationResponse);
			return serviceResponse;
		}

		public async Task<ServiceResponse> AgeVerification(string dob)
		{
            requestAgeVerification.Dob = dob;

            ServiceResponse serviceResponse = await new FanspickServices<AgeVerificationService>().Run(requestAgeVerification);
			return serviceResponse;
		}

		public async Task<ServiceResponse> ReadAllNotification()
		{

            ServiceResponse serviceResponse = await new FanspickServices<ReadAllNotificationService>().Run(requestMarkAllNotificationRead);
			return serviceResponse;
		}

		public async Task<ServiceResponse> DeleteAllNotification()
		{
            ServiceResponse serviceResponse = await new FanspickServices<DeletAllNotificationService>().Run(requestMarkAllNotificationDelete);
			return serviceResponse;
		}

		public async Task<ServiceResponse> EventLog()
		{
			ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
			return serviceResponse;
		}

		private List<ResponseUserNotificationData> alerts;
		public List<ResponseUserNotificationData> Alerts
		{
			get
			{
				return alerts;
			}
			set
			{
				this.alerts = value;
				RaisePropertyChanged("Alerts");
                RaisePropertyChanged("NoAlerts");
			}
		}

        public bool NoAlerts
        {
            get{
                 if (alerts==null)
                {
                    return true;
                }
				else if (!alerts.Any())
				{
					return true;
				}

                return false;
            }
        }

	}
}