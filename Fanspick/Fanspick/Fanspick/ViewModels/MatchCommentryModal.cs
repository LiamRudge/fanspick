﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using static Fanspick.Shared.Models.DTO.MatchCommentriesDTO;

namespace Fanspick.ViewModels
{
	public class MatchCommentryModal : BasicViewModel
	{
		public RequestMatchCommentriesDTO requestMatchCommentriesDTO { get; set; } = new RequestMatchCommentriesDTO();

		public MatchCommentryModal()
		{
            IsLoading = false;
			requestMatchCommentriesDTO.AccessToken = FanspickCache.UserData.AccessToken;

		}

		public async Task<ServiceResponse> getMatchCommentry()
		{
			ServiceResponse serviceResponse = await new FanspickServices<MatchCommentryService>().Run(requestMatchCommentriesDTO);
			return serviceResponse;
		}

		private List<Commentry> matchCommentry;
		public List<Commentry> MatchCommentry
		{
			get
			{
                return matchCommentry;//.OrderBy(c => c.EventTimeAsDateTime).ToList();
			}
			set
			{
				this.matchCommentry = value;
				RaisePropertyChanged("MatchCommentry");
			}
		}

	}

}
