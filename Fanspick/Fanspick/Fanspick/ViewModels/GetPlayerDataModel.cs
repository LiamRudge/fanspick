﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;

namespace Fanspick.ViewModels
{
	public class GetPlayerDataModel : BasicViewModel
	{
		public RequestPlayerInfoDTO requestPlayerInfoDTO
        {
            get
            { 
                return new RequestPlayerInfoDTO() { 
                    AccessToken = FanspickCache.UserData.AccessToken,
                    PlayerId = FanspickCache.CurrentFanspickScope.SelectedPlayerId
                }; 
            }
        }


        public GetPlayerDataModel()
        {
        }

        private Player player;
		public  Player Player
		{
			get
			{
				return player;
			}
			set
			{
				this.player = value;
				RaisePropertyChanged("Player");
			}
		}


		public async Task<ServiceResponse> getPlayerData()
		{
            ServiceResponse serviceResponse = await new FanspickServices<GetPlayerDataService>().Run(requestPlayerInfoDTO);
			return serviceResponse;
		}

	}
}