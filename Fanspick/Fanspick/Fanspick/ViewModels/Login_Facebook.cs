﻿using System;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;

namespace Fanspick.ViewModels
{
    public class Login_Facebook : BasicViewModel
    {
        public RequestFacebookLoginDTO dto { get; set; } = new RequestFacebookLoginDTO();
        public RequestEventLoggingDTO requestEventLoggingDTO { get; set; } = new RequestEventLoggingDTO();

        public Login_Facebook()
        {
            
            dto = new RequestFacebookLoginDTO();
            dto.AppVersion = "1.0";
            dto.LoginType = "FACEBOOK";
            dto.DeviceType = "IOS";
            dto.DeviceToken = String.IsNullOrEmpty(FanspickCache.CurrentFanspickScope.FCMId) ? Guid.NewGuid().ToString() : FanspickCache.CurrentFanspickScope.FCMId;
        }

    


        public string AppVersion
        {
            get
            {
                return dto.AppVersion;
            }
            set
            {
                dto.AppVersion = value;
            }
        }

        public string LoginType
        {
            get
            {
                return dto.LoginType;
            }
            set
            {
                dto.LoginType = value;
            }
        }

        public string DeviceType
        {
            get
            {
                return dto.DeviceType;
            }
            set
            {
                dto.DeviceType = value;
            }
        }

        public string DeviceToken
        {
            get
            {
                return dto.DeviceToken;
            }
            set
            {
                dto.DeviceToken = value;
            }
        }

              public string Email
        {
            get
            {
                return dto.Email;
            }
            set
            {
                dto.Email = value;

            }
        }
           public string FacebookID
        {
            get
            {
                return dto.FacebookID;
            }
            set
            {
                dto.FacebookID = value;

            }
        }

        public async Task<ServiceResponse> Authenticate()
        {
            ServiceResponse serviceResponse = await new FanspickServices<AuthenticationService>().Run(dto);

            if (serviceResponse.OK)
            {
                StoreUserData(serviceResponse);
            }
            return serviceResponse;
        }

        public void StoreUserData(ServiceResponse serviceResponse)
        {
            ResponseFBLoginDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseFBLoginDTO>(serviceResponse.Data);
            response.Data.UserDetails.AccessToken = response.Data.AccessToken;
            FanspickCache.UserData = response.Data.UserDetails;
        }

        public async Task<ServiceResponse> EventLog()
        {

            ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
            return serviceResponse;
        }

    }
}
