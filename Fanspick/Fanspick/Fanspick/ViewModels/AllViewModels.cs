﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.ViewModels
{
    public class AllViewModels
    {
        public AllViewModels()
        {
            this.Basic = new BasicViewModel();
            this.HomePage = new HomePageViewModel();
            this.App_Main = new App_MainViewModel();
            this.App_TeamSelector = new App_TeamSelectorViewModel();
            this.FixturePage = new FixturePageViewModel();
            this.MyTeamPage = new MyTeamPageViewModel();
            this.Pitch = new PitchViewModel();
            this.UpdateProfile = new UpdateProfileModal();
            this.viewProfile = new ViewProfileModal();
            this.MyCommunity = new MyCommunityModal();
            this.MatchCommentry = new MatchCommentryModal();
            this.PlayerData = new GetPlayerDataModel();
            this.AlertModal = new AlertScreenModal();
            this.OnetoOneChat = new OnetoOneChatModal();
            this.groupChatModal = new GetChatGroupModal();

        }

        public BasicViewModel Basic { get; set; }
        public HomePageViewModel HomePage { get; set; }
        public App_MainViewModel App_Main { get; set; }
        public App_TeamSelectorViewModel App_TeamSelector { get; set; }
        public FixturePageViewModel FixturePage { get; set; }
        public MyTeamPageViewModel MyTeamPage { get; set; }
        public PitchViewModel Pitch { get; set; }
        public UpdateProfileModal UpdateProfile { get; set; }
        public ViewProfileModal viewProfile { get; set; }
        public MyCommunityModal MyCommunity { get; set; }
        public MatchCommentryModal MatchCommentry { get; set; }
        public OnetoOneChatModal OnetoOneChat { get; set; }
        public GetPlayerDataModel PlayerData { get; set; }
        public AlertScreenModal AlertModal { get; set; }
		public FacebookViewModel FacebookViewModel { get; set; }
        public GetChatGroupModal groupChatModal { get; set; }


    }
}
