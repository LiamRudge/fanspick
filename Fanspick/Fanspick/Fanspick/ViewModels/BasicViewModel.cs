﻿using Fanspick.Helper;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Events.Events;
using System.Runtime.CompilerServices;
using Refit;
using Fanspick.RestAPI;

namespace Fanspick.ViewModels
{
    public class BasicViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        public void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

        #region loading overlay
        private bool isLoading;
        public bool IsLoading
        {
            get
            {
                return this.isLoading;
            }

            set
            {
                this.isLoading = value;
                NotifyPropertyChanged();
            }
        }

        public void ShowLoader()
        {
            Task.Run(new Action(() => { IsLoading = true; }));
        }
        public void HideLoader()
        {
            Task.Run(new Action(() => { IsLoading = false; }));
        }
        #endregion


        #region Download Resources

        public void ClearResources()
        {
            FanspickCache.CountryData.Clear();
            FanspickCache.CompetitionData.Clear();
            //FanspickCache.CurrentFanspickScope.SelectedTeam = null;

            CacheStorage.ViewModels = null;
            CacheStorage.NavigationPages = null;

        }
        public async Task DownloadResources()
        {
            ClearResources();
            if (!FanspickCache.CountryData.Any())
            {
                await CacheAllCountries();
                await CacheAllCompetion();
                await CacheAllTeams();
            }
        }

        private async Task CacheAllCountries()
        {
            FanspickCache.CountryData.Clear();
			var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

            RequestGetCountriesForSportRefitDTO getTeamDataRequest = new RequestGetCountriesForSportRefitDTO();
            getTeamDataRequest.SportId = "58906bc577a25834fa86fed3";
			var header = "Bearer " + FanspickCache.UserData.AccessToken;
            var response = await gitHubApi.GetCountriesForSport(getTeamDataRequest, header);

            if (response.StatusCode == 200)
            {
				FanspickCache.CountryData.AddRange(response.Data);
            }

        }

        public async Task CacheAllCompetion()
        {
            FanspickCache.CompetitionData.Clear();
            foreach (Country country in FanspickCache.CountryData)
            {
                RequestAllLeaguesForCountryDTO requestDTO = new RequestAllLeaguesForCountryDTO();
                requestDTO.AccessToken = FanspickCache.UserData.AccessToken;
                requestDTO.CountryId = country.Id;

                ServiceResponse serviceResponse = await new FanspickServices<AllLeaguesForCountry>().Run(requestDTO);
                if (serviceResponse.OK)
                {
                    ResponseAllLeaguesForCountryDTO responseDTO = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseAllLeaguesForCountryDTO>(serviceResponse.Data);
                    FanspickCache.CompetitionData.AddRange(responseDTO.Data);
                }
            }
        }


        private async Task CacheAllTeams()
        {
            foreach (Competition competition in FanspickCache.CompetitionData)
            {
                RequestGetTeamsForCompetitionDTO requestDTO = new RequestGetTeamsForCompetitionDTO();
                requestDTO.AccessToken = FanspickCache.UserData.AccessToken;
                requestDTO.CompetitionId = competition.Id;

                ServiceResponse serviceResponse = await new FanspickServices<AllTeamsForCompetitionService>().Run(requestDTO);
                if (serviceResponse.OK)
                {
                    ResponseGetTeamsForCompetitionDTO responseDTO = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetTeamsForCompetitionDTO>(serviceResponse.Data);
                    competition.Teams.AddRange(responseDTO.Data);
                }
            }
        }


        #endregion

        public async Task DownloadUpComingFixtures()
        {
            if (FanspickCache.CurrentFanspickScope.SelectedTeam == null)
            {
                FanspickCache.CurrentFanspickScope.SelectedTeam = FanspickCache.CompetitionData.First().Teams.First();
                //DefaultTeam defaultTeam = FanspickCache.UserData.DefaultTeam.FirstOrDefault();
                //FanspickCache.CurrentFanspickScope.SelectedTeam = FanspickCache.CompetitionData.SelectMany(x => x.Teams).FirstOrDefault(y => y.Id == defaultTeam.FavouriteTeam);
            }

            RequestGetUpcomingFixturesDTO requestDTO = new RequestGetUpcomingFixturesDTO();
            requestDTO.AccessToken = FanspickCache.UserData.AccessToken;
            requestDTO.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;


            ServiceResponse serviceResponse = await new FanspickServices<GetUpcomingFixturesService>().Run(requestDTO);
            if (serviceResponse.OK)
            {

                ResponseGetUpcomingFixturesDTO responseGetUpcomingFixturesDTO = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetUpcomingFixturesDTO>(serviceResponse.Data);
                FanspickCache.CurrentFanspickScope.UpComingFixtures = responseGetUpcomingFixturesDTO.Data;
                SetSelectedFixture();

            }
        }

        private void SetSelectedFixture()
        {
            if (FanspickCache.CurrentFanspickScope.UpComingFixtures != null && FanspickCache.CurrentFanspickScope.UpComingFixtures.Any())
            {
                FanspickCache.CurrentFanspickScope.SelectedFixture = FanspickCache.CurrentFanspickScope.UpComingFixtures.First();
            }
        }


        #region Load Pitch Data

        Fanspick.Shared.Models.Fixture selectedFixture;
        Fanspick.Shared.Models.Team selectedTeam;
        FanspickUser userData;

        public void SetCacheData(){

			selectedFixture = FanspickCache.CurrentFanspickScope.SelectedFixture;
			selectedTeam = FanspickCache.CurrentFanspickScope.SelectedTeam;
			userData = FanspickCache.UserData;
		}

        public async Task GetUserPickData(bool isLive)
        {
			if (FanspickCache.CurrentFanspickScope?.SelectedFixture != null && FanspickCache.CurrentFanspickScope?.SelectedTeam != null)
			{
                ServiceResponse serviceResponse = await new FanspickServices<GetUserPickService>().Run(new RequestGetUserPickDTO
                {
                    AccessToken = userData.AccessToken,
                    FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id,
                    TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id,
                    IsLive = isLive
                });

                PitchViewModel pitch = new PitchViewModel();
                int index = Screens.HomePage.homePageVM.YourPickViewModel.Pitches.Count();
                pitch.PitchIndex = index;

                if (serviceResponse.Data != null)
                {
                    ResponseGetUserPickDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetUserPickDTO>(serviceResponse.Data);
                    ResponsePickData pickData = response.Data;
                    pitch.ReInitialize(pickData.Positions, pickData.LineUpPlayers);
                    pitch.IsLive = isLive; 
                    pitch.showShirts = true;
                } 
                else
                {
                    pitch.SetDefaultPitch();

                    if (!isLive)
                    {
                        if (FanspickPitchEventHandler.MatchStage >= MatchStage.PreMatch)
                        {
                            pitch.ShowYourPickPreMessage = true;
                            pitch.showShirts = false;
                        }
                    }
                    else
                    {
                        if (FanspickPitchEventHandler.MatchStage < MatchStage.Live)
                        {
                            pitch.ShowYourPickLiveMessage = true;
                            pitch.showShirts = false;
                        }
                    }
                }

                pitch.showShirts = true;
                pitch.ShowYourPickPreMessage = false;
                pitch.ShowYourPickLiveMessage = false;
                Screens.HomePage.homePageVM.YourPickViewModel.AddPitch(pitch);
            }
        }

		public async Task GetUserPickDataPre()
		{
			if (FanspickCache.CurrentFanspickScope?.SelectedFixture != null && FanspickCache.CurrentFanspickScope?.SelectedTeam != null)
			{
				ServiceResponse serviceResponse = await new FanspickServices<GetUserPickService>().Run(new RequestGetUserPickDTO
				{
					AccessToken = userData.AccessToken,
					FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id,
					TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id,
					IsLive = false
				});

				PitchViewModel pitch = new PitchViewModel();

				if (serviceResponse.Data != null)
				{
					ResponseGetUserPickDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetUserPickDTO>(serviceResponse.Data);
					ResponsePickData pickData = response.Data;
					pitch.ReInitialize(pickData.Positions, pickData.LineUpPlayers);
                    pitch.IsLive = false;
					pitch.showShirts = true;
				}
				else
				{
					pitch.SetDefaultPitch();
				}

				pitch.showShirts = true;
				pitch.ShowYourPickPreMessage = false;
				pitch.ShowYourPickLiveMessage = false;
				Screens.HomePage.homePageVM.YourPickViewModelPre.PitchData = pitch;
				RaisePropertyChanged("YourPickViewModelPre");
			}
		}

        public async Task GetManagerPickData()
        {
            if (FanspickCache.CurrentFanspickScope?.SelectedFixture != null && FanspickCache.CurrentFanspickScope?.SelectedTeam != null)
            {
                ServiceResponse serviceResponse = await new FanspickServices<GetManagerPickService>()
                     .Run(new RequestGetManagerPickDTO
                     {
                         AccessToken = userData.AccessToken,
                         FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id,
                         TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id
                     });

				

				PitchViewModel pitch = new PitchViewModel();
                int index = Screens.HomePage.homePageVM.ManagersPickViewModel.Pitches.Count();
				pitch.PitchIndex = index;

                if (serviceResponse.Data != null)
                {
                    ResponseGetManagerPickDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetManagerPickDTO>(serviceResponse.Data);
                    ResponsePickData pickData = response.Data;
                    pitch.ReInitialize(pickData.Positions, pickData.LineUpPlayers);

                    //FanspickCache.CurrentFanspickScope.ManagerPickData = pickData;
                    pitch.ShowManagersMessage = true;
                }
                else
                {
                    //FanspickCache.CurrentFanspickScope.ManagerPickData = null;
                }

                Screens.HomePage.homePageVM.ManagersPickViewModel.AddPitch(pitch);
            }
        }

        public async Task GetFansPickData(bool isLive)
        {
            if (FanspickCache.CurrentFanspickScope?.SelectedFixture != null && FanspickCache.CurrentFanspickScope?.SelectedTeam != null)
            {
                ServiceResponse serviceResponse = await new FanspickServices<GetFanspickService>()
                     .Run(new RequestGetFanspickDTO
                     {
                         AccessToken = userData.AccessToken,
                         FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id,
                         TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id,
                         IsLive = isLive
                     });

				PitchViewModel pitch = new PitchViewModel();
                int index = Screens.HomePage.homePageVM.FansPickViewModel.Pitches.Count();
				pitch.PitchIndex = index;

                if (serviceResponse.Data != null)
                {
                    ResponseGetFanspickDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetFanspickDTO>(serviceResponse.Data);
                    ResponsePickData pickData = response.Data;
                    pitch.ReInitialize(pickData.Positions, pickData.LineUpPlayers);
                    //FanspickCache.CurrentFanspickScope.FanspickPickData = pickData;
                    pitch.IsLive = isLive;
                    pitch.ShowFansPickMessage = true;
                }
                else
                {
                    //FanspickCache.CurrentFanspickScope.FanspickPickData = null;
                    pitch.ShowFansPickMessage = true;
                }

                Screens.HomePage.homePageVM.FansPickViewModel.AddPitch(pitch);
            }
        }

        public async Task GetTeamSquadData(bool isLive)
        {
			if (FanspickCache.CurrentFanspickScope?.SelectedFixture != null && FanspickCache.CurrentFanspickScope?.SelectedTeam != null)
			{
				ServiceResponse serviceResponse = await APIService.Run(new RequestGetTeamSquadDTO
				{
					AccessToken = userData.AccessToken,
					FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id,
					TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id,
					IsLive = isLive
				});
				if (serviceResponse.Data != null)
				{
					ResponseGetTeamSquadDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetTeamSquadDTO>(serviceResponse.Data);
					FanspickCache.CurrentFanspickScope.TeamSquadData = response.Data;
				}
				else
				{
					FanspickCache.CurrentFanspickScope.TeamSquadData = null;
				}
			}
        }

        #endregion



    }
}
