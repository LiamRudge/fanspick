﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Xamarin.Forms;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;
using static Fanspick.Shared.Models.DTO.MyCommunityDTO;

namespace Fanspick.ViewModels
{
	public class MyCommunityModal : BasicViewModel
	{
		public RequestMyCommunityDTO requestMyCommunityDTO { get; set; } = new RequestMyCommunityDTO();
		public RequestEventLoggingDTO requestEventLoggingDTO { get; set; } = new RequestEventLoggingDTO();
		public RequestChatHistoryDTO requestChatHistoryDTO { get; set; } = new RequestChatHistoryDTO();

		public MyCommunityModal()
		{
			requestMyCommunityDTO.AccessToken = FanspickCache.UserData.AccessToken;
			requestChatHistoryDTO.AccessToken = FanspickCache.UserData.AccessToken;

		}

		private List<ResponseMyCommunityData> myCommunity;
		public List<ResponseMyCommunityData> MyCommunity
		{
			get
			{
				return myCommunity;
			}
			set
			{
				this.myCommunity = value;
				RaisePropertyChanged("MyCommunity");
			}
		}

		private List<Topics> myTopics;
		public List<Topics> MyTopics
		{
			get
			{
				return myTopics;
			}
			set
			{
				this.myTopics = value;
				RaisePropertyChanged("MyTopics");
			}
		}


		ObservableCollection<CommunityPosts> communityPosts = null;
		public ObservableCollection<CommunityPosts> CommunityPosts
		{
			get
			{
				if (communityPosts == null)
					communityPosts = new ObservableCollection<CommunityPosts>();
				//socialFeedData.Shuffle();
				return communityPosts;
			}
			set
			{
				this.communityPosts = value;
				RaisePropertyChanged("CommunityPosts");
			}
		}

		public ImageSource TeamLogo
		{
			get
			{

				string ImageString = "";

				if (FanspickCache.CurrentFanspickScope.SelectedTeam.Id == FanspickCache.CurrentFanspickScope.SelectedFixture.LocalTeam.FanspickTeam.Id)
				{
					ImageString = FanspickCache.CurrentFanspickScope.SelectedFixture.LocalTeam.FanspickTeam.ImageURL;
				}
				else
				{
					ImageString = FanspickCache.CurrentFanspickScope.SelectedFixture.VisitorTeam.FanspickTeam.ImageURL;
				}

				//  Uri location = new Uri();
				// return Xamarin.Forms.ImageSource.FromUri(location);
				if (ImageString != null)
				{
					return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamLogos/" + ImageString;
				}
				else
				{
					return "no_image";
				}
			}
		}

		public async Task<ServiceResponse> GetMyCommunities()
		{
			ServiceResponse serviceResponse = await new FanspickServices<MyCommunityService>().Run(requestMyCommunityDTO);
			return serviceResponse;
		}

		public async Task<ServiceResponse> GetChatHistory()
		{
			ServiceResponse serviceResponse = await new FanspickServices<ChatHistoryService>().Run(requestChatHistoryDTO);
			return serviceResponse;
		}

		public async Task<ServiceResponse> EventLog()
		{

			ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
			return serviceResponse;
		}


	}
}