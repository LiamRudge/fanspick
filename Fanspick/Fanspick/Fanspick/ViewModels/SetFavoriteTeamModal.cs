﻿using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using System;
using System.Threading.Tasks;

namespace Fanspick.ViewModels
{
    public class SetFavoriteTeamModal : BasicViewModel
    {
    		public RequestSetFavTeamDTO requestSetFavTeamDTO { get; set; } = new RequestSetFavTeamDTO();

		public SetFavoriteTeamModal()
		{
			requestSetFavTeamDTO.AccessToken = FanspickCache.UserData.AccessToken;
		}

    }

	
}
