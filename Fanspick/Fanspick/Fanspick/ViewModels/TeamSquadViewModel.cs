﻿using Fanspick.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.ViewModels
{
    public class TeamSquadViewModel
    {
        public void Initialize(PitchViewModel pitchViewModel,  PositionDetailsViewModel positionDetails, List<Squad> teamSquadData)
        {
            this.PositionDetails = positionDetails;
            this.TeamSquadData = teamSquadData;
            this.Pitch = pitchViewModel;

        }

        public PositionDetailsViewModel PositionDetails { get; set; }

        public List<Squad> TeamSquadData { get; set; }

        public PitchViewModel Pitch { get; set; }

        public void SetTeamSquad(Squad teamSquad)
        {
            this.Pitch.SetTeamSquadData(this.PositionDetails.PositionId, teamSquad);
        }

        public void SetTeamSquad(Squad teamSquad, string fixtureid, string teamid)
		{
            this.Pitch.SetTeamSquadData(this.PositionDetails.PositionId, teamSquad, false, fixtureid, teamid);
		}
    }
}
