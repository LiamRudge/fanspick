﻿using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using System;
using System.Threading.Tasks;

namespace Fanspick.ViewModels
{
	public class UnsetFavoriteTeamModal : BasicViewModel
	{
		public RequestUnsetFavTeamDTO requestUnsetFavTeamDTO { get; set; } = new RequestUnsetFavTeamDTO();

		public UnsetFavoriteTeamModal()
		{
			requestUnsetFavTeamDTO.AccessToken = FanspickCache.UserData.AccessToken;
		}

	}


}
