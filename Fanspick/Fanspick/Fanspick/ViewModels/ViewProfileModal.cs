﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using static Fanspick.Shared.Models.DTO.ViewProfileDTO;

namespace Fanspick.ViewModels
{
	public class ViewProfileModal : BasicViewModel
	{
		public RequestViewProfileDTO requestViewProfileDTO { get; set; } = new RequestViewProfileDTO();
        public RequestGetUpcomingFixturesDTO requestGetUpcomingFixturesDTO { get; set; } = new RequestGetUpcomingFixturesDTO();


		public ViewProfileModal()
		{
            requestViewProfileDTO.AccessToken = FanspickCache.UserData.AccessToken;
            requestGetUpcomingFixturesDTO.AccessToken = FanspickCache.UserData.AccessToken;

        }

        public String FirstName { get
            {
                return FanspickCache.UserData.Firstname;
            }
            set{
                FanspickCache.UserData.Firstname = value;
            }}

		public String LastName
		{
			get
			{
                return FanspickCache.UserData.Lastname;
			}
			set
			{
				FanspickCache.UserData.Lastname = value;
			}
		}




		public String Address
		{
			get
			{
                return FanspickCache.UserData.Address;
			}
			//set
			//{
			//	FanspickCache.UserData.Address = value;
			//}
		}
		public String City
		{
			get
			{
				return FanspickCache.UserData.City;
			}
			set
			{
				FanspickCache.UserData.City = value;
			}
		}
		public String Zip
		{
			get
			{
				return FanspickCache.UserData.Zip;
			}
			set
			{
				FanspickCache.UserData.Zip = value;
			}
		}
        public String DateOfBirth
		{
			get
			{
				return FanspickCache.UserData.Dob.ToString();
			}
            set => FanspickCache.UserData.Dob =DateTime.Parse(value) ;
        }

		public async Task<ServiceResponse> getUserProfile()
		{
			ServiceResponse serviceResponse = await new FanspickServices<ViewProfileService>().Run(requestViewProfileDTO);
			return serviceResponse;
		}

	}
}