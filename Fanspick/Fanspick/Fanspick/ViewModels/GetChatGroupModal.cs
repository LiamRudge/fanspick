﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;

namespace Fanspick.ViewModels
{
	public class GetChatGroupModal : BasicViewModel
	{
		public RequestGetChatForGroupDTO requestGetChatForGroupDTO { get; set; } = new RequestGetChatForGroupDTO();

		public GetChatGroupModal()
		{
			requestGetChatForGroupDTO.AccessToken = FanspickCache.UserData.AccessToken;

        }

		public async Task<ServiceResponse> GetChatForGroup()
		{
            ServiceResponse serviceResponse = await new FanspickServices<GetChatForGroupService>().Run(requestGetChatForGroupDTO);
			return serviceResponse;
		}



		ObservableCollection<ChatMessages> friendsChat = null;
		public ObservableCollection<ChatMessages> FriendsChat
		{
			get
			{
				if (friendsChat == null)
					friendsChat = new ObservableCollection<ChatMessages>();
				//socialFeedData.Shuffle();
				return friendsChat;
			}
			set
			{
				this.friendsChat = value;
				RaisePropertyChanged("FriendsChat");
			}
		}

      
	}
}