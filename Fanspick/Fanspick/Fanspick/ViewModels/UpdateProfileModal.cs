﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Fanspick.RestAPI;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Refit;
using static Fanspick.Shared.Models.DTO.UpdateProfileDTO;

namespace Fanspick.ViewModels
{
    public class UpdateProfileModal : BasicViewModel
    {
        public RequestUpdateProfileRefitDTO requestUpdateProfileDTO { get; set; } = new RequestUpdateProfileRefitDTO();

		public UpdateProfileModal()
		{

        }

        public async Task<ServiceResponse> setUpdateProfile(Dictionary<string, string> dict)
		{
            //	ServiceResponse serviceResponse = await new FanspickServices<UpdateProfileService>().Run(requestUpdateProfileDTO);

            ServiceResponse serviceResponse = new ServiceResponse();

            try{
				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.EditProfileDictonary(dict, header);

				if (response.StatusCode == 200)
				{
                    serviceResponse.OK = true;
                    serviceResponse.ReasonPhrase = "Profile updated successfully";
                   //message = "Profile updated successfully";

				}
				else
				{
                    serviceResponse.OK = false;
                    serviceResponse.ReasonPhrase = response.Message;
				}



            }
			catch (ApiException ex)
			{
				var content = ex.GetContentAs<Dictionary<String, String>>();
				serviceResponse.OK = false;
				serviceResponse.ReasonPhrase = content.Values.Last().ToString();
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.ToString());
			}

			return serviceResponse;
		}

	}
	
}
