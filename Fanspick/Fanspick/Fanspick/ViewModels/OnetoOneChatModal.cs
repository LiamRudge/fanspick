﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using static Fanspick.Shared.Models.DTO.MatchCommentriesDTO;
using static Fanspick.Shared.Models.DTO.RecentChatContactDTO;

namespace Fanspick.ViewModels
{
	public class OnetoOneChatModal : BasicViewModel
	{

		public RequestRegisteredContactsDTO requestRegisteredContacts { get; set; } = new RequestRegisteredContactsDTO();
		public RequestRecentChatContactDTO RequestRecentChatContactDTO { get; set; } = new RequestRecentChatContactDTO();
        public RequestGetChatForGroupDTO requestGetChatForGroupDTO { get; set; } = new RequestGetChatForGroupDTO();


		public OnetoOneChatModal()
		{
			requestRegisteredContacts.AccessToken = FanspickCache.UserData.AccessToken;
			RequestRecentChatContactDTO.AccessToken = FanspickCache.UserData.AccessToken;
            requestGetChatForGroupDTO.AccessToken = FanspickCache.UserData.AccessToken;
		}

		public async Task<ServiceResponse> GetRegisteredContact()
		{
			ServiceResponse serviceResponse = await new FanspickServices<RegisteredContactService>().Run(requestRegisteredContacts);
			return serviceResponse;
		}

		public async Task<ServiceResponse> GetChatList()
		{
			ServiceResponse serviceResponse = await new FanspickServices<RecentChatContactService>().Run(RequestRecentChatContactDTO);
			return serviceResponse;
		}


		private List<ContactModal> phoneContacts;
		public List<ContactModal> PhoneContacts
		{
			get
			{
				return phoneContacts;
			}
			set
			{
				this.phoneContacts = value;
				RaisePropertyChanged("PhoneContacts");
			}
		}

        private ObservableCollection<AdminMembers> adminMembers;
		public ObservableCollection<AdminMembers> AdminMembers
		{
			get
			{
				if (adminMembers == null)
					adminMembers = new ObservableCollection<AdminMembers>();
				return adminMembers;
			}
			set
			{
				this.adminMembers = value;
				RaisePropertyChanged("AdminMembers");
			}
		}

		private ObservableCollection<MemberWithAdmin> overAllMembers;
		public ObservableCollection<MemberWithAdmin> OverAllMembers
		{
			get
			{
				if (overAllMembers == null)
					overAllMembers = new ObservableCollection<MemberWithAdmin>();
				return overAllMembers;
			}
			set
			{
				this.overAllMembers = value;
				RaisePropertyChanged("OverAllMembers");
			}
		}


		private ObservableCollection<GroupMembers> groupMembers;
		public ObservableCollection<GroupMembers> GroupMembers
		{
			get
			{
				if (groupMembers == null)
					groupMembers = new ObservableCollection<GroupMembers>();
				return groupMembers;
			}
			set
			{
				this.groupMembers = value;
				RaisePropertyChanged("GroupMembers");
			}
		}



		private ObservableCollection<ContactsRegistered> phoneContactsForGroup;
		public ObservableCollection<ContactsRegistered> PhoneContactsForGroup
		{
			get
			{
				if (phoneContactsForGroup == null)
					phoneContactsForGroup = new ObservableCollection<ContactsRegistered>();
				return phoneContactsForGroup;
			}
			set
			{
				this.phoneContactsForGroup = value;
				RaisePropertyChanged("PhoneContactsForGroup");
			}
		}

		private ObservableCollection<Contacts> overallContacts;
		public ObservableCollection<Contacts> OverallContacts
		{
			get
			{
				if (overallContacts == null)
					overallContacts = new ObservableCollection<Contacts>();
				return overallContacts;
			}
			set
			{
				this.overallContacts = value;
				RaisePropertyChanged("OverallContacts");
			}
		}

		public async Task<ServiceResponse> GetChatForGroup()
		{
			ServiceResponse serviceResponse = await new FanspickServices<GetChatForGroupService>().Run(requestGetChatForGroupDTO);
			return serviceResponse;
		}



		ObservableCollection<ChatMessages> friendsChat = null;
		public ObservableCollection<ChatMessages> FriendsChat
		{
			get
			{
				if (friendsChat == null)
					friendsChat = new ObservableCollection<ChatMessages>();
				//socialFeedData.Shuffle();
				return friendsChat;
			}
			set
			{
				this.friendsChat = value;
				RaisePropertyChanged("FriendsChat");
			}
		}

		private ObservableCollection<ContactsRegistered> registeredContacts;
		public ObservableCollection<ContactsRegistered> RegisteredContacts
		{
			get
			{
				if (registeredContacts == null)
					registeredContacts = new ObservableCollection<ContactsRegistered>();
				return registeredContacts;
			}
			set
			{
				this.registeredContacts = value;
				RaisePropertyChanged("RegisteredContacts");
			}
		}

		private ObservableCollection<RecentChatToShow> recentChat;
		public ObservableCollection<RecentChatToShow> RecentChat
		{
			get
			{
				if (recentChat == null)
					recentChat = new ObservableCollection<RecentChatToShow>();
				return recentChat;
			}
			set
			{
				this.recentChat = value;
				RaisePropertyChanged("RecentChat");
			}
		}



	}

}
