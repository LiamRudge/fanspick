﻿using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;
using Fanspick.RestAPI;
using Refit;
using System.Diagnostics;

namespace Fanspick.ViewModels
{
    public class SignUpViewModel : BasicViewModel
    {
        RequestSignUpDTO dto;
        public RequestEventLoggingDTO requestEventLoggingDTO { get; set; } = new RequestEventLoggingDTO();
        public SignUpViewModel()
        {
            IsLoading = false;
            dto = new RequestSignUpDTO();
            dto.AppVersion = "1.0";
            dto.DeviceType = "IOS";
            dto.Gender = "MALE";
            dto.DeviceToken = String.IsNullOrEmpty(FanspickCache.CurrentFanspickScope.FCMId) ? Guid.NewGuid().ToString() : FanspickCache.CurrentFanspickScope.FCMId;
            dto.FcmId = String.IsNullOrEmpty(FanspickCache.CurrentFanspickScope.FCMId) ? Guid.NewGuid().ToString() : FanspickCache.CurrentFanspickScope.FCMId;
        }

        public string UserName
        {
            get
            {
                return dto.UserName;
            }
            set
            {
                dto.UserName = value;

            }
        }

        public string EmailAddress
        {
            get
            {
                return dto.EmailAddress;
            }
            set
            { 
                dto.EmailAddress = value;

            }
        }

        public string Password
        {
            get
            {
                return dto.Password;
            }
            set
            {
                dto.Password = value;
            }
        }

        private string _confirmPassword = string.Empty;
        public string ConfirmPassword
        {
            get
            {
                return _confirmPassword;
            }
            set
            {
                _confirmPassword = value;
            }
        }


        //public string Gender
        //{
        //    get
        //    {
        //        return dto.Gender;
        //    }
        //    set
        //    {
        //        dto.Gender = value;

        //    }
        //}

        public string DeviceToken
        {
            get
            {
                return dto.DeviceToken;
            }
            set
            {
                dto.DeviceToken = value;

            }
        }

        public string DeviceType
        {
            get
            {
                return dto.DeviceType;
            }
            set
            {
                dto.DeviceType = value;

            }
        }

        public string AppVersion
        {
            get
            {
                return dto.AppVersion;
            }
            set
            {
                dto.AppVersion = value;

            }
        }


        public async Task<ServiceResponse> CreateAccount()
        {
            ServiceResponse validateServiceResponse = await Validate();
            if (validateServiceResponse.OK)
            {
                ServiceResponse createAccountServiceResponse = new ServiceResponse();
                try{

					
					var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
					var request = dto;
					var response = await gitHubApi.Register(request);

					if (response.StatusCode == 201)
					{
						response.Data.UserDetails.AccessToken = response.Data.AccessToken;
						FanspickCache.UserData = response.Data.UserDetails;

						createAccountServiceResponse.OK = true;
						createAccountServiceResponse.ReasonPhrase = response.Message;

					}
					else
					{
						createAccountServiceResponse.OK = false;
						createAccountServiceResponse.ReasonPhrase = response.Message;
					}


                }catch(ApiException ex){
					var content = ex.GetContentAs<Dictionary<String, String>>();
					createAccountServiceResponse.OK = false;
                    createAccountServiceResponse.ReasonPhrase = content.Values.Skip(2).First().ToString();

				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
				}
              
                return createAccountServiceResponse;
            }
            else
            {
                return validateServiceResponse;
            }
        }
      

        private async Task<ServiceResponse> Validate()
        {
            ServiceResponse serviceResponse = new ServiceResponse();
            if (string.IsNullOrEmpty(this.UserName))
            {
                serviceResponse.ReasonPhrase = "Username is empty";
                serviceResponse.MessageStack.Add("Username is empty");
            }
            else
            {
                ServiceResponse uniqueUsernameResponse = await new FanspickServices<UniqueUsernameService>()
                       .Run(this.UserName);

                if (!uniqueUsernameResponse.OK)
                {
                    serviceResponse.ReasonPhrase = "Username is already in use";
                    serviceResponse.MessageStack.Add("Username is already in use");
                }
            }

            //MISSING UNIQUE EMAIL ADDRESS VALIDATION

            if (string.IsNullOrEmpty(this.EmailAddress))
            {
                serviceResponse.ReasonPhrase = "Email address is empty";
                serviceResponse.MessageStack.Add("Email address is empty");
            }

            if (String.IsNullOrEmpty(this.Password))
            {
                serviceResponse.MessageStack.Add("Password is empty");
                serviceResponse.ReasonPhrase = "Password is empty";
            }
            else if (this.Password.Length < 6)
            {
                serviceResponse.ReasonPhrase = "Password must be longer than 5 characters";
                serviceResponse.MessageStack.Add("Password must be longer than 5 characters");
            }

            if (String.IsNullOrEmpty(this.ConfirmPassword))
            {
                serviceResponse.ReasonPhrase = "Password confirmation is empty";
                serviceResponse.MessageStack.Add("Password confirmation is empty");
            }
            else if (this.Password != this.ConfirmPassword)
            {
                serviceResponse.ReasonPhrase = "Password did not match";
                serviceResponse.MessageStack.Add("Password did not match");
            }

            serviceResponse.OK = (serviceResponse.MessageStack.Count() == 0);
            
            return serviceResponse;
        }

		public async Task<ServiceResponse> EventLog()
		{

			ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
			return serviceResponse;
		}

        
    }

}
