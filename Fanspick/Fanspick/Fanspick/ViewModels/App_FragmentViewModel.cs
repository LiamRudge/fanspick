﻿using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.AppModels;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.ViewModels
{
    public class App_FragmentViewModel : BasicViewModel
    {
        public App_FragmentViewModel()
        {
        }


        public List<Fanspick.Shared.Models.Fixture> Fixtures
        {
            get
            {
                return FanspickCache.CurrentFanspickScope.UpComingFixtures;
            }
        }

        public Fanspick.Shared.Models.Fixture NextFixture
        {
            get
            {
                List<Fanspick.Shared.Models.Fixture> fixtures = Fixtures;
                if (fixtures != null)
                    return Fixtures.First();
                return null;
            }
        }

        private Fanspick.Shared.Models.Fixture selectedFixture;
        public Fanspick.Shared.Models.Fixture SelectedFixture
        {
            get
            {
                if (selectedFixture == null)
                    return Fixtures.First();
                return selectedFixture;
            }
            set
            {
                selectedFixture = value;
                RaisePropertyChanged("SelectedFixture");
            }
        }

        public String TickerBarUrl
        {
            get
            {
                if (SelectedFixture != null)
                    return $"http://fanspick-admin-test.5sol.co.uk/Marquee/Index/{this.SelectedFixture.Id}";
                return $"http://fanspick-admin-test.5sol.co.uk/Marquee/Index/0";

            }
        }


        public bool NoMoreNextFixture
        {
            get
            {
                return (NextFixture == null);
            }
        }


        private Boolean isFixturePitchActive;
        public Boolean IsFixturePitchActive
        {
            set
            {
                if (isFixturePitchActive != value)
                {
                    isFixturePitchActive = value;

                    RaisePropertyChanged("IsFixturePitchActive");
                }
            }
            get
            {
                return isFixturePitchActive;
            }
        }

        private Boolean isFixtureListActive;
        public Boolean IsFixtureListActive
        {
            set
            {
                if (isFixtureListActive != value)
                {
                    isFixtureListActive = value;

                    RaisePropertyChanged("IsFixtureListActive");
                }
            }
            get
            {
                return isFixtureListActive;
            }
        }

        public void MakePitchActive()
        {
            IsFixturePitchActive = true;
            IsFixtureListActive = false;
            RaisePropertyChanged("TickerBarUrl");
            LoadPitchData();
        }

        private void LoadPitchData()
        {
           // GetUserPickData(false);
          //  GetManagerPickData();
          //  GetFansPickData(false);
        }



        public void MakeFixtureListActive()
        {
            IsFixturePitchActive = false;
            IsFixtureListActive = true;
            RaisePropertyChanged("Fixtures");
            RaisePropertyChanged("NextFixture");
            RaisePropertyChanged("NoMoreNextFixture");
        }




        private PitchViewModel managerPickVM = null;
        public PitchViewModel ManagerPickVM
        {
            get
            {
                if (managerPickVM == null)
                {
                    managerPickVM = new PitchViewModel();
                }
                return managerPickVM;
            }
        }


        private PitchViewModel yourPickVM = null;
        public PitchViewModel YourPickVM
        {
            get
            {
                if (yourPickVM == null)
                {
                    yourPickVM = new PitchViewModel();
                }
                return yourPickVM;
            }
        }


        private PitchViewModel fansPickVM = null;
        public PitchViewModel FansPickVM
        {
            get
            {
                if (fansPickVM == null)
                {
                    fansPickVM = new PitchViewModel();
                }
                return fansPickVM;
            }
        }


    }
}
