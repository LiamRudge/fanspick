﻿using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using System;
using System.Threading.Tasks;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;

namespace Fanspick.ViewModels
{

    public class Login_NormalViewModel : BasicViewModel
    {
        public RequestLoginDTO dto { get; set; } = new RequestLoginDTO();
		public RequestEventLoggingDTO requestEventLoggingDTO { get; set; } = new RequestEventLoggingDTO();

        public Login_NormalViewModel()
        {
            IsLoading = false;
            dto = new RequestLoginDTO();
            dto.AppVersion = "1.0";
            dto.LoginType = "SIMPLE";
            dto.DeviceType = "IOS";
            dto.DeviceToken = String.IsNullOrEmpty(FanspickCache.CurrentFanspickScope.FCMId) ? Guid.NewGuid().ToString() : FanspickCache.CurrentFanspickScope.FCMId;
            dto.FcmId = String.IsNullOrEmpty(FanspickCache.CurrentFanspickScope.FCMId) ? Guid.NewGuid().ToString() : FanspickCache.CurrentFanspickScope.FCMId;
        }

        public string Email
        {
            get
            {
                return dto.Email;
            }
            set
            {
                dto.Email = value;

            }
        }
        
        public string Password
        {
            get
            {
                return dto.Password;
            }
            set
            {
                dto.Password = value;

            }
        }

        public string AppVersion
        {
            get
            {
                return dto.AppVersion;
            }
            set
            {
                dto.AppVersion = value;
            }
        }

        public string LoginType
        {
            get
            {
                return dto.LoginType;
            }
            set
            {
                dto.LoginType = value;
            }
        }

        public string DeviceType
        {
            get
            {
                return dto.DeviceType;
            }
            set
            {
                dto.DeviceType = value;
            }
        }

        public string DeviceToken
        {
            get
            {
                return dto.DeviceToken;
            }
            set
            {
                dto.DeviceToken = value;
            }
        }



        public async Task<ServiceResponse> Authenticate()
        {
            ServiceResponse serviceResponse = await new FanspickServices<AuthenticationService>().Run(dto);

            if (serviceResponse.OK)
            {
                StoreUserData(serviceResponse);            
            }
            return serviceResponse;
        }

        public void StoreUserData(ServiceResponse serviceResponse)
        {
            ResponseLoginDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseLoginDTO>(serviceResponse.Data);
            response.Data.UserDetails.AccessToken = response.Data.AccessToken;
            FanspickCache.UserData = response.Data.UserDetails;
        }

		public async Task<ServiceResponse> EventLog()
		{

			ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
			return serviceResponse;
		}

    }

}