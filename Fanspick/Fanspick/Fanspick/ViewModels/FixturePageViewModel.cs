﻿using Fanspick.Shared.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;

namespace Fanspick.ViewModels
{
    public class FixturePageViewModel : BasicViewModel
    {

        public RequestEventLoggingDTO requestEventLoggingDTO { get; set; } = new RequestEventLoggingDTO();
        public FixturePageViewModel()
        {
        }

        private List<Fanspick.Shared.Models.Fixture> fixtures;
        public List<Fanspick.Shared.Models.Fixture> Fixtures
        {
            get
            {
                return FanspickCache.CurrentFanspickScope.UpComingFixtures;
            }set{
                this.fixtures = value;
				for (int i = 0; i < fixtures.Count(); i++)
				{
					fixtures[i].ItemIndex = i + 1;
				}
            }
        }

		private Fanspick.Shared.Models.Fixture nextFixture;
		public Fanspick.Shared.Models.Fixture NextFixture
		{
			get
			{
				return nextFixture;
			}
			set
			{
				this.nextFixture = value;
				RaisePropertyChanged("NextFixture");
			}
		}

        private Fanspick.Shared.Models.Fixture selectedFixture;
        public Fanspick.Shared.Models.Fixture SelectedFixture
        {
            get
            {
                if (selectedFixture == null)
                    return Fixtures.First();
                else
                {
                    selectedFixture= FanspickCache.CurrentFanspickScope.SelectedFixture;
                }
                return selectedFixture;
            }
            set
            {
                FanspickCache.CurrentFanspickScope.SelectedFixture = value;                
                RaisePropertyChanged("SelectedFixture");
            }
        }

        public bool NoMoreNextFixture
        {
            get
            {
                return (NextFixture == null);
            }
        }

		public async Task<ServiceResponse> EventLog()
		{
			ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
			return serviceResponse;
		}

    }
}
