﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;

namespace Fanspick.ViewModels
{
    public class PitchPickViewModel : BasicViewModel
    {
        public List<PitchViewModel> Pitches { get; set; } = new List<PitchViewModel>();

        private PitchViewModel pitchData;
        public PitchViewModel PitchData
        {
            get{
                return pitchData;
            }
            set{
                pitchData = value;
                NotifyPropertyChanged();
            }
        }

        public void AddPitch(PitchViewModel Pitch)
        {
            Pitches.Add(Pitch);
            RaisePropertyChanged("Pitches");
        }
    }
}
