﻿using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace Fanspick.ViewModels
{
	public class PitchViewModel : BasicViewModel
	{
		public bool showShirts { get; set; } = false;
		bool _isDefaultPitch = false;
        public string currentFormation { get; set; }



		public PitchViewModel()
		{
		}

		public void ReInitialize(List<Position> formationPositions, List<LineUpPlayerData> lineUpPlayers)
		{
			ClearAllPositionsData();
			VisibleFormationPostions(formationPositions);
			SetColumnWidth();
			SetRowHeight();
			SetPlayerInformation(lineUpPlayers);
			RaisePropertyChanged("Shirt");
		}


		public async void SetDefaultPitch()
		{
			ResponsePickData pickData = await GetDefaultPitch();
			if (pickData != null)
			{
				ReInitialize(pickData.Positions, pickData.LineUpPlayers);
			}
		}

		private async Task<ResponsePickData> GetDefaultPitch()
		{
			ServiceResponse serviceResponse = await new FanspickServices<GetFormationByTypeService>()
				   .Run(new RequestGetFormationByTypeDTO
				   {
					   AccessToken = FanspickCache.UserData.AccessToken,
					   Type = "4321"
				   });

			if (serviceResponse.Data != null)
			{
				ResponseGetFormationByTypeDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetFormationByTypeDTO>(serviceResponse.Data);
                currentFormation = response.Data.CurrentFormation.Id;
				return response.Data;
			}
			return null;
		}

		public async void ChangePitchFormation(string SelectedType)
		{

			ServiceResponse serviceResponse = await new FanspickServices<GetFormationByTypeService>().Run(new RequestGetFormationByTypeDTO
			{
				AccessToken = FanspickCache.UserData.AccessToken,
				Type = SelectedType
			});

			if (serviceResponse.Data != null)
			{
				ResponseGetFormationByTypeDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetFormationByTypeDTO>(serviceResponse.Data);
                currentFormation = dto.Data.CurrentFormation.Id;
                ReInitialize(dto.Data.Positions, dto.Data.LineUpPlayers);
			}
        } 

		public string Shirt
		{
			get
			{
				if (_isDefaultPitch || FanspickCache.CurrentFanspickScope.SelectedTeam == null)
					return "gk_grey_plus.png";
				return "Dundee.png";
				//return FanspickSettings.TeamShirtBaseURL + FanspickCache.CurrentFanspickScope.SelectedTeam.KnownName + ".png";
			}
		}


		#region All Rows Positions Properties

		#region row 1

		PositionDetailsViewModel position_1_1 = new PositionDetailsViewModel(false, 1, 1) { };
		public PositionDetailsViewModel Position_1_1
		{
			get
			{
				return position_1_1;
			}
		}

		PositionDetailsViewModel position_1_2 = new PositionDetailsViewModel(false, 1, 2);
		public PositionDetailsViewModel Position_1_2
		{
			get
			{
                RaisePropertyChanged("Position_1_2");
				return position_1_2;
			}
		}

		PositionDetailsViewModel position_1_3 = new PositionDetailsViewModel(true, 1, 3);
		public PositionDetailsViewModel Position_1_3
		{
			get
			{
                RaisePropertyChanged("Position_1_3");
				return position_1_3;
			}
		}

		PositionDetailsViewModel position_1_4 = new PositionDetailsViewModel(false, 1, 4);
		public PositionDetailsViewModel Position_1_4
		{
			get
			{
                RaisePropertyChanged("Position_1_4");
				return position_1_4;
			}
		}

		PositionDetailsViewModel position_1_5 = new PositionDetailsViewModel(false, 1, 5);
		public PositionDetailsViewModel Position_1_5
		{
			get
			{
                RaisePropertyChanged("Position_1_5");
				return position_1_5;
			}
		}
		#endregion

		#region row 2
		PositionDetailsViewModel position_2_1 = new PositionDetailsViewModel(true, 2, 1);
		public PositionDetailsViewModel Position_2_1
		{
			get
			{
                RaisePropertyChanged("Position_2_1");
				return position_2_1;
			}
		}

		PositionDetailsViewModel position_2_2 = new PositionDetailsViewModel(false, 2, 2);
		public PositionDetailsViewModel Position_2_2
		{
			get
			{
                RaisePropertyChanged("Position_2_2");
				return position_2_2;
			}
		}

		PositionDetailsViewModel position_2_3 = new PositionDetailsViewModel(true, 2, 3);
		public PositionDetailsViewModel Position_2_3
		{
			get
			{
                RaisePropertyChanged("Position_2_3");
				return position_2_3;
			}
		}

		PositionDetailsViewModel position_2_4 = new PositionDetailsViewModel(true, 2, 4);
		public PositionDetailsViewModel Position_2_4
		{
			get
			{
                RaisePropertyChanged("Position_2_4");
				return position_2_4;
			}
		}

		PositionDetailsViewModel position_2_5 = new PositionDetailsViewModel(false, 2, 5);
		public PositionDetailsViewModel Position_2_5
		{
			get
			{
                RaisePropertyChanged("Position_2_5");
				return position_2_5;
			}
		}

		#endregion

		#region row 3
		PositionDetailsViewModel position_3_1 = new PositionDetailsViewModel(true, 3, 1);
		public PositionDetailsViewModel Position_3_1
		{
			get
			{
                RaisePropertyChanged("Position_3_1");
				return position_3_1;
			}
		}

		PositionDetailsViewModel position_3_2 = new PositionDetailsViewModel(false, 3, 2);
		public PositionDetailsViewModel Position_3_2
		{
			get
			{
                RaisePropertyChanged("Position_3_2");
				return position_3_2;
			}
		}

		PositionDetailsViewModel position_3_3 = new PositionDetailsViewModel(false, 3, 3);
		public PositionDetailsViewModel Position_3_3
		{
			get
			{
                RaisePropertyChanged("Position_3_3");
				return position_3_3;
			}
		}

		PositionDetailsViewModel position_3_4 = new PositionDetailsViewModel(false, 3, 4);
		public PositionDetailsViewModel Position_3_4
		{
			get
			{
                RaisePropertyChanged("Position_3_4");
				return position_3_4;
			}
		}

		PositionDetailsViewModel position_3_5 = new PositionDetailsViewModel(false, 3, 5);
		public PositionDetailsViewModel Position_3_5
		{
			get
			{
                RaisePropertyChanged("Position_3_5");
				return position_3_5;
			}
		}

		#endregion

		#region row 4
		PositionDetailsViewModel position_4_1 = new PositionDetailsViewModel(true, 4, 1);
		public PositionDetailsViewModel Position_4_1
		{
			get
			{
                RaisePropertyChanged("Position_4_1");
				return position_4_1;
			}
		}

		PositionDetailsViewModel position_4_2 = new PositionDetailsViewModel(true, 4, 2);
		public PositionDetailsViewModel Position_4_2
		{
			get
			{
                RaisePropertyChanged("Position_4_2");
				return position_4_2;
			}
		}

		PositionDetailsViewModel position_4_3 = new PositionDetailsViewModel(false, 4, 3);
		public PositionDetailsViewModel Position_4_3
		{
			get
			{
                RaisePropertyChanged("Position_4_3");
				return position_4_3;
			}
		}

		PositionDetailsViewModel position_4_4 = new PositionDetailsViewModel(false, 4, 4);
		public PositionDetailsViewModel Position_4_4
		{
			get
			{
                RaisePropertyChanged("Position_4_4");
				return position_4_4;
			}
		}

		PositionDetailsViewModel position_4_5 = new PositionDetailsViewModel(false, 4, 5);
		public PositionDetailsViewModel Position_4_5
		{
			get
			{
                RaisePropertyChanged("Position_4_5");
				return position_4_5;
			}
		}

		#endregion

		#region row 5
		PositionDetailsViewModel position_5_1 = new PositionDetailsViewModel(true, 5, 1);
		public PositionDetailsViewModel Position_5_1
		{
			get
			{
                RaisePropertyChanged("Position_5_1");
				return position_5_1;
			}
		}

		PositionDetailsViewModel position_5_2 = new PositionDetailsViewModel(true, 5, 2);
		public PositionDetailsViewModel Position_5_2
		{
			get
			{
                RaisePropertyChanged("Position_5_2");
				return position_5_2;
			}
		}

		PositionDetailsViewModel position_5_3 = new PositionDetailsViewModel(true, 5, 3);
		public PositionDetailsViewModel Position_5_3
		{
			get
			{
                RaisePropertyChanged("Position_5_3");
				return position_5_3;
			}
		}

		PositionDetailsViewModel position_5_4 = new PositionDetailsViewModel(false, 5, 4);
		public PositionDetailsViewModel Position_5_4
		{
			get
			{
                RaisePropertyChanged("Position_5_4");
				return position_5_4;
			}
		}

		PositionDetailsViewModel position_5_5 = new PositionDetailsViewModel(false, 5, 5);
		public PositionDetailsViewModel Position_5_5
		{
			get
			{
                RaisePropertyChanged("Position_5_5");
				return position_5_5;
			}
		}

		#endregion

		#endregion

		private void ClearAllPositionsData()
		{
			foreach (PositionDetailsViewModel positionBasicDetails in GetAllPositionDetails())
			{
				positionBasicDetails.IsVisible = false;
				positionBasicDetails.PlayerId = string.Empty;
				positionBasicDetails.PlayerName = string.Empty;
				positionBasicDetails.PositionId = string.Empty;
				positionBasicDetails.Role = string.Empty;
			}
		}

		private void VisibleFormationPostions(List<Position> formationPositions)
		{
			PositionDetailsViewModel positionBasicDetails;
			foreach (Position position in formationPositions)
			{
				positionBasicDetails = GetPositionDetails(position.PosX, position.PosY);
				positionBasicDetails.IsVisible = true;
				positionBasicDetails.PlayerName = "Player";
				positionBasicDetails.PositionId = position.Id;
				positionBasicDetails.Role = position.Role;
			}
		}

		private void SetPlayerInformation(List<LineUpPlayerData> lineUpPlayers)
		{
			if (lineUpPlayers != null)
			{
				_isDefaultPitch = false;
				PositionDetailsViewModel positionBasicDetails;
				foreach (LineUpPlayerData lineUpPlayerData in lineUpPlayers)
				{
					positionBasicDetails = GetPositionDetails(lineUpPlayerData.Position.PosX, lineUpPlayerData.Position.PosY);
					positionBasicDetails.PlayerId = lineUpPlayerData.Player.Id;
					positionBasicDetails.PlayerName = lineUpPlayerData.Player.KnownName;
				}
			}
			else
			{
				_isDefaultPitch = true;
			}
		}

		TeamSquadViewModel teamSquadVM = null;
		public TeamSquadViewModel TeamSquadVM
		{
			get
			{
				if (teamSquadVM == null)
				{
					teamSquadVM = new TeamSquadViewModel();
				}
				return teamSquadVM;
			}
		}

		public void InitializeTeamSquadData(int row, int col)
		{
			if (FanspickCache.CurrentFanspickScope.TeamSquadData != null)
			{
				List<string> selectedPlayerIds = GetAllPositionDetails().Where(x => x.PlayerId != "").Select(x => x.PlayerId).ToList();
				PositionDetailsViewModel positionDetails = GetPositionDetails(row, col);
				List<Squad> teamSquadData = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(x => x.Role == positionDetails.Role && !selectedPlayerIds.Contains(x.PlayerId)).ToList();
				TeamSquadVM.Initialize(this, positionDetails, teamSquadData);
                RaisePropertyChanged("TeamSquadVM");
			}
		}

		public void SetTeamSquadData(string positionId, Squad teamSquad)
		{
			PositionDetailsViewModel position = GetPositionDetails(positionId);
			position.PlayerId = teamSquad.PlayerId;
			position.PlayerName = teamSquad.Name;
		}

        public async void SetTeamSquadData(string positionId, Squad teamSquad, bool isLive, string fixtureid, string teamid)
        {
            SetTeamSquadData(positionId, teamSquad);

			RequestSetUserPickDTO updateUserPick = new RequestSetUserPickDTO();
            updateUserPick.AccessToken = FanspickCache.UserData.AccessToken;
            updateUserPick.FixtureId = fixtureid;
            updateUserPick.Formation = currentFormation;
            updateUserPick.IsLive = isLive;
            updateUserPick.OldPlayerId = teamSquad.PlayerId;
            updateUserPick.NewPlayerId = teamSquad.PlayerId;
            updateUserPick.PositionId = positionId;
            updateUserPick.TeamId = teamid;
            ServiceResponse serviceResponse = await new FanspickServices<SetUserPickService>().Run(updateUserPick);
        }


		#region Calculate Row Width & Height
		private int VisibleCellsInRow(int rowNumber)
		{
			List<PositionDetailsViewModel> allPositons = GetAllPositionDetails();
			return allPositons.Count(x => x.Row == rowNumber && x.IsVisible);
		}

		private GridLength GetCellWidth(int rowNumber)
		{
			int visibleCells = VisibleCellsInRow(rowNumber);
			if (visibleCells == 0)
				return new GridLength(0);
			else
			{
				double cellWidth = 100 / visibleCells;
				return new GridLength(cellWidth, GridUnitType.Star);
			}
		}


		private int TotalVisibleRows()
		{
			int visibleRows = 0;
			for (int i = 1; i <= 5; i++)
			{
				visibleRows += VisibleCellsInRow(i) > 0 ? 1 : 0;
			}
			return visibleRows;
		}

		private GridLength GetRowHeight(int rowNumber, double rowHeight)
		{
			if (rowHeight == 0)
				return new GridLength(0);
			else
			{
				int visibleCells = VisibleCellsInRow(rowNumber);
				if (visibleCells == 0)
					return new GridLength(0);
				else
					return new GridLength(rowHeight, GridUnitType.Star);
			}
		}



		private void SetColumnWidth()
		{
			List<PositionDetailsViewModel> allPositons = GetAllPositionDetails();
			foreach (PositionDetailsViewModel position in allPositons)
			{
				if (position.IsVisible)
				{
					switch (position.Row)
					{
						case 1:
							position.Width = GetCellWidth(1);
							break;
						case 2:
							position.Width = GetCellWidth(2);
							break;
						case 3:
							position.Width = GetCellWidth(3); ;
							break;
						case 4:
							position.Width = GetCellWidth(4);
							break;
						case 5:
							position.Width = GetCellWidth(5);
							break;
						default:
							position.Width = new GridLength(0);
							break;
					}
				}
				else
				{
					position.Width = new GridLength(0);
				}
			}
		}

		private void SetRowHeight()
		{
			int visibleRows = TotalVisibleRows();
			double rowHeight = (visibleRows == 0) ? 0 : 100 / visibleRows;
			Row1_Height = GetRowHeight(1, rowHeight);
			Row2_Height = GetRowHeight(2, rowHeight);
			Row3_Height = GetRowHeight(3, rowHeight);
			Row4_Height = GetRowHeight(4, rowHeight);
			Row5_Height = GetRowHeight(5, rowHeight);
            RaisePropertyChanged("row1_Height");
            RaisePropertyChanged("row2_Height");
            RaisePropertyChanged("row3_Height");
            RaisePropertyChanged("row4_Height");
            RaisePropertyChanged("row5_Height");
		}


		#region Row Height Properties

		GridLength row1_Height = new GridLength(0);
		public GridLength Row1_Height
		{
			get
			{
				return row1_Height;
			}
			set
			{
				row1_Height = value;
				NotifyPropertyChanged();
			}
		}

		GridLength row2_Height = new GridLength(0);
		public GridLength Row2_Height
		{
			get
			{
				return row2_Height;
			}
			set
			{
				row2_Height = value;
				NotifyPropertyChanged();
			}
		}

		GridLength row3_Height = new GridLength(0);
		public GridLength Row3_Height
		{
			get
			{
				return row3_Height;
			}
			set
			{
				row3_Height = value;
                NotifyPropertyChanged();
			}
		}


		GridLength row4_Height = new GridLength(0);
		public GridLength Row4_Height
		{
			get
			{
				return row4_Height;
			}
			set
			{
				row4_Height = value;
                NotifyPropertyChanged();
			}
		}

		GridLength row5_Height = new GridLength(0);
		public GridLength Row5_Height
		{
			get
			{
				return row5_Height;
			}
			set
			{
				row5_Height = value;
                NotifyPropertyChanged();
			}
		}

		public int PitchIndex { get; set; }
		public bool pitchEmpty { get; set; }
		public bool IsLive { get; set; }
		public bool ShowYourPickPreMessage { get; set; } = false;
		public bool ShowYourPickLiveMessage { get; set; } = false;
		public bool AllowEditPitch { get; set; } = false;
		public bool ShowManagersMessage { get; set; } = false;
		public bool ShowFansPickMessage { get; set; } = false;


		#endregion

		#endregion

		#region Position Details

		private List<PositionDetailsViewModel> GetAllPositionDetails()
		{
			List<PositionDetailsViewModel> lstPositionDetails = new List<PositionDetailsViewModel>();

			lstPositionDetails.Add(Position_1_1);
			lstPositionDetails.Add(Position_1_2);
			lstPositionDetails.Add(Position_1_3);
			lstPositionDetails.Add(Position_1_4);
			lstPositionDetails.Add(Position_1_5);

			lstPositionDetails.Add(Position_2_1);
			lstPositionDetails.Add(Position_2_2);
			lstPositionDetails.Add(Position_2_3);
			lstPositionDetails.Add(Position_2_4);
			lstPositionDetails.Add(Position_2_5);

			lstPositionDetails.Add(Position_3_1);
			lstPositionDetails.Add(Position_3_2);
			lstPositionDetails.Add(Position_3_3);
			lstPositionDetails.Add(Position_3_4);
			lstPositionDetails.Add(Position_3_5);

			lstPositionDetails.Add(Position_4_1);
			lstPositionDetails.Add(Position_4_2);
			lstPositionDetails.Add(Position_4_3);
			lstPositionDetails.Add(Position_4_4);
			lstPositionDetails.Add(Position_4_5);

			lstPositionDetails.Add(Position_5_1);
			lstPositionDetails.Add(Position_5_2);
			lstPositionDetails.Add(Position_5_3);
			lstPositionDetails.Add(Position_5_4);
			lstPositionDetails.Add(Position_5_5);
			return lstPositionDetails;

		}

		private PositionDetailsViewModel GetPositionDetails(int row, int col)
		{
			return GetAllPositionDetails().First(x => x.Row == row && x.Column == col);
		}

		private PositionDetailsViewModel GetPositionDetails(string positionId)
		{
			return GetAllPositionDetails().First(x => x.PositionId == positionId);
		}

		private PositionDetailsViewModel GetPositionDetails(string x, string y)
		{
			if (x == "1" && y == "1")
			{
				return position_1_1;
			}
			else if (x == "1" && y == "2")
			{
				return position_1_2;
			}
			else if (x == "1" && y == "3")
			{
				return position_1_3;
			}
			else if (x == "1" && y == "4")
			{
				return position_1_4;
			}
			else if (x == "1" && y == "5")
			{
				return position_1_5;
			}

			else if (x == "2" && y == "1")
			{
				return position_2_1;
			}
			else if (x == "2" && y == "2")
			{
				return position_2_2;
			}
			else if (x == "2" && y == "3")
			{
				return position_2_3;
			}
			else if (x == "2" && y == "4")
			{
				return position_2_4;
			}
			else if (x == "2" && y == "5")
			{
				return position_2_5;
			}
			else if (x == "3" && y == "1")
			{
				return position_3_1;
			}
			else if (x == "3" && y == "2")
			{
				return position_3_2;
			}
			else if (x == "3" && y == "3")
			{
				return position_3_3;
			}
			else if (x == "3" && y == "4")
			{
				return position_3_4;
			}
			else if (x == "3" && y == "5")
			{
				return position_3_5;
			}
			else if (x == "4" && y == "1")
			{
				return position_4_1;
			}
			else if (x == "4" && y == "2")
			{
				return position_4_2;
			}
			else if (x == "4" && y == "3")
			{
				return position_4_3;
			}
			else if (x == "4" && y == "4")
			{
				return position_4_4;
			}
			else if (x == "4" && y == "5")
			{
				return position_4_5;
			}
			else if (x == "5" && y == "1")
			{
				return position_5_1;
			}
			else if (x == "5" && y == "2")
			{
				return position_5_2;
			}
			else if (x == "5" && y == "3")
			{
				return position_5_3;
			}
			else if (x == "5" && y == "4")
			{
				return position_5_4;
			}
			else if (x == "5" && y == "5")
			{
				return position_5_5;
			}
			return null;

		}

		#endregion

	}



	public class PositionDetailsViewModel : BasicViewModel
	{
		public PositionDetailsViewModel(bool makeItVisible, int rowNumber, int columnNumber)
		{
			this.Row = rowNumber;
			this.Column = columnNumber;
			this.isVisible = makeItVisible;
		}


		int row = 0;
		public int Row
		{
			get { return row; }
			set
			{
				row = value;
                NotifyPropertyChanged();
			}
		}

		int column = 0;
		public int Column
		{
			get { return column; }
			set
			{
				column = value;
                NotifyPropertyChanged();
			}
		}

		string playerId = string.Empty;
		public string PlayerId
		{
			get
			{
				return playerId;
			}
			set
			{
				playerId = value;
                NotifyPropertyChanged();
			}
		}

		string playerName = string.Empty;
		public string PlayerName
		{
			get
			{
				return playerName;
			}
			set
			{
				playerName = value;
                NotifyPropertyChanged();
			}
		}

		string positionId = string.Empty;
		public string PositionId
		{
			get
			{
				return positionId;
			}
			set
			{
				positionId = value;
                NotifyPropertyChanged();
			}
		}

		string role = string.Empty;
		public string Role
		{
			get
			{
				return role;
			}
			set
			{
				role = value;
                NotifyPropertyChanged();
			}
		}

		bool isVisible = false;
		public bool IsVisible
		{
			get { return isVisible; }
			set
			{
				if (IsVisible != value)
				{
					isVisible = value;
                    NotifyPropertyChanged();
					RaisePropertyChanged("Width");
				}
			}
		}

		GridLength width = new GridLength(0);
		public GridLength Width
		{
			get
			{
				return width;
			}
			set
			{
				width = value;
                NotifyPropertyChanged();
			}
		}
	}

}