﻿using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using static Fanspick.Shared.Models.DTO.GetTopTeamDTO;
using static Fanspick.Shared.Models.DTO.GetTopPlayersDTO;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;

namespace Fanspick.ViewModels
{
    public class App_TeamSelectorViewModel : BasicViewModel
    {

        public RequestSetFavTeamDTO requestSetFavTeamDTO { get; set; } = new RequestSetFavTeamDTO();
		public RequestGetTopTeamDTO requestGetTopTeamDTO { get; set; } = new RequestGetTopTeamDTO();
        public RequestGetTopPlayersDTO requestGetTopPlayersDTO { get; set; } = new RequestGetTopPlayersDTO();
        public RequestEventLoggingDTO requestEventLoggingDTO { get; set; } = new RequestEventLoggingDTO();

        public Shared.Models.DTO.GetTopPlayersDTO.ResponseGetTopPlayersDTO ResponseGetTopPlayers { get; set; }
        public Shared.Models.DTO.GetTopTeamDTO.ResponseGetTopTeamDTO ResponseGetTopTeam { get; set; }

        public App_TeamSelectorViewModel()
        {
            requestSetFavTeamDTO.AccessToken = FanspickCache.UserData.AccessToken;
            requestGetTopTeamDTO.AccessToken = FanspickCache.UserData.AccessToken;
            requestGetTopPlayersDTO.AccessToken = FanspickCache.UserData.AccessToken;
        }

        //public List<Country> Countries
        //{
        //    get
        //    {
        //        return FanspickCache.CountryData;
        //    }
        //}

		private List<Country> countries;
		public List<Country> Countries
		{
			get
			{
				return countries;//.OrderBy(c => c.EventTimeAsDateTime).ToList();
			}
			set
			{
				this.countries = value;
				RaisePropertyChanged("Countries");
			}
		}

		private List<Competition> competitions;
		public List<Competition> Competitions
		{
			get
			{
				return competitions;//.OrderBy(c => c.EventTimeAsDateTime).ToList();
			}
			set
			{
				this.competitions = value;
				RaisePropertyChanged("Competitions");
			}
		}

		private List<Team> teams;
		public List<Team> Teams
		{
			get
			{
				return teams;//.OrderBy(c => c.EventTimeAsDateTime).ToList();
			}
			set
			{
				this.teams = value;
				RaisePropertyChanged("Teams");
			}
		}


		//public List<Competition> Competitions
		//{
		//	get
		//	{
		//		return FanspickCache.CompetitionData.Where(x => x.CountryId == SelectedCountry.Id).ToList();
		//	}
		//}



		Country selectedCountry;
        public Country SelectedCountry
        {
            get
            {
                if (selectedCountry == null)
                {
                    return FanspickCache.CountryData.First();
                }
                return selectedCountry;
            }
            set
            {
                selectedCountry = value;
                RaisePropertyChanged("Competitions");
                RaisePropertyChanged("Teams");
            }
        }


       

        Competition selectedCompetition;
        public Competition SelectedCompetition
        {
            get
            {
                if (selectedCompetition == null)
                {
                    return FanspickCache.CompetitionData.FirstOrDefault(x => x.CountryId == SelectedCountry.Id);
                }
                return selectedCompetition;
            }
            set
            {
                selectedCompetition = value;
                RaisePropertyChanged("Teams");
                RaisePropertyChanged("SelectedCompetition");
            }
        }

        //public List<Team> Teams
        //{
        //    get
        //    {
        //        if (selectedCompetition!=null)
        //        {
        //            return SelectedCompetition.Teams;
        //        }
        //        else{
        //            return new List<Team>();
        //        }
        //    }
        //}

        private Fanspick.Shared.Models.Team selectedTeam;
        public Fanspick.Shared.Models.Team SelectedTeam
        {
            get
            {
                selectedTeam = FanspickCache.CurrentFanspickScope.SelectedTeam;
                if (selectedTeam == null)
                    return Teams.First();
                return selectedTeam;
            }
            set
            {
                FanspickCache.CurrentFanspickScope.SelectedTeam = value;
                RaisePropertyChanged("SelectedTeam");
            }
        }

        private List<ResponseGetTopPlayersData> topPlayers;
        public List<ResponseGetTopPlayersData> TopPlayers { 
            get{
                return topPlayers;
            } 
            set{
                this.topPlayers = value;

                for (int i = 0; i < topPlayers.Count(); i++)
                {
                    topPlayers[i].PlayerDetails.ItemIndex = i + 1;
                    if (i == 0)
                    {
                        topPlayers[i].PlayerDetails.color = "#72bf44";
                        topPlayers[i].PlayerDetails.PosColor = "#72bf44";
                    }
                    else
                    {
                        topPlayers[i].PlayerDetails.color = "#23374e";
                        topPlayers[i].PlayerDetails.PosColor = "#23374e";
                    }
                }
              
				RaisePropertyChanged("TopPlayers");
            }
        }

        private List<ResponseGetTopTeamData> topTeams;
		public List<ResponseGetTopTeamData> TopTeams
		{
			get
			{
				return topTeams;
			}
			set
			{
				this.topTeams = value;
                for (int i = 0; i < topTeams.Count(); i++)
                {
                    
                    topTeams[i].ItemIndex = i + 1;
					if (i == 0)
					{
						topTeams[i].color = "#72bf44";
					}
                    else if(i==topTeams.Count()-1 || i == topTeams.Count() - 2 || i == topTeams.Count() - 3)
                    {
                        topTeams[i].color = "Red";
                    }
					else
					{
                        topTeams[i].color = "#23374e";
					}
                }
                RaisePropertyChanged("TopTeams");
			}
		}

        public async Task<ServiceResponse> SetFavTeam()
		{
            ServiceResponse serviceResponse = await new FanspickServices<SetFavouriteTeamService>().Run(requestSetFavTeamDTO);
            return serviceResponse;
		}

		public async Task<ServiceResponse> getTopTeamStainding()
		{
			ServiceResponse serviceResponse = await new FanspickServices<GetTopTeamService>().Run(requestGetTopTeamDTO);
			return serviceResponse;
		}

		public async Task<ServiceResponse> getTopPlayersStainding()
		{
			ServiceResponse serviceResponse = await new FanspickServices<GetTopPlayersService>().Run(requestGetTopPlayersDTO);
			return serviceResponse;
		}

		public async Task<ServiceResponse> EventLog()
		{

			ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
			return serviceResponse;
		}




    }
}
