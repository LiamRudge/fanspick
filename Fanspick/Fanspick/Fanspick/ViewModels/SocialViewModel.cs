﻿using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Diagnostics;

namespace Fanspick.ViewModels
{
	public class SocialViewModel : BasicViewModel
	{

        int counter = 1;
		ObservableCollection<SocialFeed> socialFeedData = null;
		public ObservableCollection<SocialFeed> SocialFeedData
		{
			get
			{
				if (socialFeedData == null)
					socialFeedData = new ObservableCollection<SocialFeed>();
				//socialFeedData.Shuffle();
				return socialFeedData;
			}
		}

		public SocialFeed SelectedSocialFeed { get; set; }


		public async Task LoadData()
		{

            try
            {

                if (FanspickCache.CurrentFanspickScope.SelectedTeam != null)
                {
                    ServiceResponse serviceResponse = await APIService.Run(new RequestGetSocialFeedDTO
                    {
                        AccessToken = string.Empty,
                        TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id
                    });

                    ResponseGetSocialFeedDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetSocialFeedDTO>(serviceResponse.Data);

                    if (this.SocialFeedData != null)
                    {
                        this.SocialFeedData.Clear();
                    }


                    FanspickCache.CurrentFanspickScope.twMaxId = response.Data.twMaxId;
                    FanspickCache.CurrentFanspickScope.fbNextTS = response.Data.fbNextTS;

                    foreach (SocialFeed sf in response.Data.Data)
                    {
                        sf.HorizontalOption = (counter % 2 == 0) ? LayoutOptions.End : LayoutOptions.Start;
                        counter++;
                        SocialFeedData.Add(sf);
                    }


                }

            }
            catch(Exception ex){
                Debug.WriteLine(ex.ToString());
            }


		}


		public async Task LoadDataLastitem()
		{
            try
            {
                
                if (FanspickCache.CurrentFanspickScope.SelectedTeam != null)
                {
                    ServiceResponse serviceResponse = await APIService.Run(new RequestGetSocialFeedDTOReload
                    {
                        AccessToken = string.Empty,
                        TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id,
                        twMaxId = FanspickCache.CurrentFanspickScope.twMaxId,
                        fbNextTS = FanspickCache.CurrentFanspickScope.fbNextTS


                    });

                    ResponseGetSocialFeedDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetSocialFeedDTO>(serviceResponse.Data);

                    foreach (SocialFeed sf in response.Data.Data)
                    {
                        sf.HorizontalOption = (counter % 2 == 0) ? LayoutOptions.End : LayoutOptions.Start;
                        counter++;
                        SocialFeedData.Add(sf);
                    }


                }
            }
            catch(Exception e){
                Debug.WriteLine(e.ToString());
            }

		}

		private bool _isRefreshing = false;
		public bool IsRefreshing
		{
			get { return _isRefreshing; }
			set
			{
				_isRefreshing = value;
				RaisePropertyChanged("IsRefreshing");
			}
		}

		public ICommand RefreshCommand
		{
			get
			{
				return new Command(async () =>
				{
					IsRefreshing = true;

					await LoadData();

					IsRefreshing = false;
				});
			}
		}

	}
}

