﻿using Fanspick.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Fanspick.Screens;
using Rg.Plugins.Popup.Extensions;
using Fanspick.PopUp;
using Rg.Plugins.Popup.Pages;
using Fanspick.Shared.Helper;
using System.ComponentModel;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;

namespace Fanspick.ViewModels
{
    public class App_MainViewModel : BasicViewModel, INotifyPropertyChanged
    {
        Fanspick.Shared.Models.Fixture selectedFixture = FanspickCache.CurrentFanspickScope.SelectedFixture;
        public RequestEventLoggingDTO requestEventLoggingDTO { get; set; } = new RequestEventLoggingDTO();

        public void ActiveDetailsPage(ScreenType screenType)
        {
            NavigationPageDetails navigationPageDetails = CacheStorage.GetNavigationPageDetails(screenType);

            if (navigationPageDetails.TargetType != null)
            {
                if (navigationPageDetails.IsPopType)
                {
                    NavPage.PushPopupAsync((PopupPage)Activator.CreateInstance(navigationPageDetails.TargetType), false);
                }
                else
                {
                    NavPage.PushAsync((Page)Activator.CreateInstance(navigationPageDetails.TargetType));
                }
            }
            else
            {
                NavPage.PushAsync((Page)Activator.CreateInstance(typeof(HomePage)));
            }

            if (navigationPageDetails.HasRemovePreviousPage && NavPage.NavigationStack != null && NavPage.NavigationStack.Count > 1)
            {                                                      
                NavPage.RemovePage(NavPage.NavigationStack[0]);
            }

            IsPresented = false;
        }

        internal void ActiveDetailsPage(object alertPopUp)
        {
//            throw new NotImplementedException();
        }

        INavigation navPage;
        public INavigation NavPage
        {
            get { return navPage; } 
            set { navPage = value; }
        }

        bool isPresented = false;
        public bool IsPresented
        {
            get { return isPresented; }
            set
            {
                isPresented = value;
                RaisePropertyChanged("IsPresented");
            }
        }

        String selectedLocalTeamLogo;
        public String SelectedLocalTeamLogo
		{
			get { return selectedLocalTeamLogo; }
			set {
                selectedLocalTeamLogo = ""+selectedFixture.LocalTeamImage; 
                RaisePropertyChanged("SelectedLocalTeamLogo");
            }
		}

		String selectedVisitorTeamLogo;
		public String SelectedVisitorTeamLogo
		{
			get { return selectedVisitorTeamLogo; }
			set
			{
                selectedVisitorTeamLogo = ""+selectedFixture.VisitorTeamImage;
				RaisePropertyChanged("SelectedVisitorTeamLogo");
			}
		}


        public async Task<ServiceResponse> EventLog()
        {
            ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
            return serviceResponse;
        }
		public DateTime ApiFixtureDate;
		public string ApiMatchStatus;
		public DateTime ApiFirstFixtureDate;
		public DateTime ApiSecondFixtureDate;

    }
}
