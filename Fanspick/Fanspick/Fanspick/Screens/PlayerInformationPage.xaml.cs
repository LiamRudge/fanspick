﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Helper;
using Fanspick.RestAPI;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.DTO;
using Fanspick.ViewModels;
using Plugin.DeviceInfo;
using Refit;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlayerInformationPage : PopupPage
    {
        GetPlayerDataModel vm;
        String SelectedPlayerId;
        public PlayerInformationPage(String playerID)
        {
            InitializeComponent();
			vm = CacheStorage.ViewModels.PlayerData;
            BindingContext = vm;

            SelectedPlayerId = playerID;
            Fanspick.Screens.SplashScreen.isFixturePage = false;

           GetPlayerData();

           // ImageLogo.Source = TeamLogo;

			close.GestureRecognizers.Add(
			new TapGestureRecognizer()
			{
				Command = new Command(() =>
				{
                    /* Handle the click here */
                    PopupNavigation.PopAsync();

				})
			        }
				);

		}

		public ImageSource TeamLogo
		{
			get
			{

				string ImageString = "";

				if (FanspickCache.CurrentFanspickScope.SelectedTeam.Id == FanspickCache.CurrentFanspickScope.SelectedFixture.LocalTeam.FanspickTeam.Id)
				{
					ImageString = FanspickCache.CurrentFanspickScope.SelectedFixture.LocalTeam.FanspickTeam.ImageURL;
				}
				else
				{
					ImageString = FanspickCache.CurrentFanspickScope.SelectedFixture.VisitorTeam.FanspickTeam.ImageURL;
				}

				//  Uri location = new Uri();
				// return Xamarin.Forms.ImageSource.FromUri(location);
				if (ImageString != null)
				{
					return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamLogos/" + ImageString;
				}
				else
				{
					return "no_image";
				}
			}
		}


		private async void GetPlayerData()
		{
			try
			{
                vm.ShowLoader();


				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				RequestPlayerInfoRefitDTO getTeamDataRequest = new RequestPlayerInfoRefitDTO();
                getTeamDataRequest.PlayerId = SelectedPlayerId;
			
				var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.GetPlayerData(getTeamDataRequest, header);

				if (response.StatusCode == 200)
				{
					vm.Player = response.Data.ToArray()[0];
					vm.HideLoader();

				}
				else
				{
					vm.HideLoader();
				}


			}
			catch (Exception e)
			{
				Debug.WriteLine(e.Message);
                vm.HideLoader();

			}


		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
        }

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{
				close.Margin = new Thickness(15);
                ImageCircle.Margin = new Thickness(90);
                header.FontSize = 26;
                header.VerticalOptions = LayoutOptions.EndAndExpand;

				ImageCircle.WidthRequest = 200;
				ImageCircle.HeightRequest = 200;

				FirstName.FontSize = 22;
				LastName.FontSize = 25;
				DOB.FontSize = 22;
				Position.FontSize = 22;
				Age.FontSize = 22;
				Weight.FontSize = 22;
				Height.FontSize = 22;
				Place.FontSize = 22;
				Gap.FontSize = 22;

				Div1.HeightRequest = 50;
				Div2.HeightRequest = 50;
				Div3.HeightRequest = 50;
				Div4.HeightRequest = 50;
				Div5.HeightRequest = 50;
				Div6.HeightRequest = 50;
				Div7.HeightRequest = 50;
				Div8.HeightRequest = 50;
				Div9.HeightRequest = 50;
				Div10.HeightRequest = 50;
				Div11.HeightRequest = 140;


				Div1Text1.FontSize = 18;
				Div1Text2.FontSize = 18;

				Div2Text1.FontSize = 18;
				Div2Text2.FontSize = 18;

				Div3Text1.FontSize = 18;
				Div3Text2.FontSize = 18;

				Div4Text1.FontSize = 18;
				Div4Text2.FontSize = 18;

				Div5Text1.FontSize = 18;
				Div5Text2.FontSize = 18;

				Div6Text1.FontSize = 18;
				Div6Text2.FontSize = 18;

				Div7Text1.FontSize = 18;
				Div7Text2.FontSize = 18;

				Div8Text1.FontSize = 18;
				Div8Text2.FontSize = 18;

				Div9Text1.FontSize = 18;
				Div9Text2.FontSize = 18;

				Div10Text1.FontSize = 18;
				Div10Text2.FontSize = 18;

			}
			else
			{

                BlockHeader.HeightRequest = 70;

                ImageGapheader1.Width = new GridLength(10, GridUnitType.Star);
                ImageGapheader2.Width = new GridLength(80, GridUnitType.Star);
                ImageGapheader3.Width = new GridLength(10, GridUnitType.Star);

                ImageCircle.WidthRequest = 140;
                ImageCircle.HeightRequest = 140;

                header.FontSize = 16;

                FirstName.FontSize = 15;
                LastName.FontSize = 20;
                DOB.FontSize = 15;
                Position.FontSize = 15;
                Age.FontSize = 15;
                Weight.FontSize = 15;
                Height.FontSize = 15;
                Place.FontSize = 15;
                Gap.FontSize = 15;

                Div1.HeightRequest = 30;
                Div2.HeightRequest = 30;
                Div3.HeightRequest = 30;
                Div4.HeightRequest = 30;
                Div5.HeightRequest = 30;
                Div6.HeightRequest = 30;
                Div7.HeightRequest = 30;
                Div8.HeightRequest = 30;
                Div9.HeightRequest = 30;
                Div10.HeightRequest = 30;
                Div11.HeightRequest = 70;

                Div1Text1.FontSize = 13;
                Div1Text2.FontSize = 13;

                Div2Text1.FontSize = 13;
                Div2Text2.FontSize = 13;

				Div3Text1.FontSize = 13;
				Div3Text2.FontSize = 13;

				Div4Text1.FontSize = 13;
				Div4Text2.FontSize = 13;

				Div5Text1.FontSize = 13;
				Div5Text2.FontSize = 13;

				Div6Text1.FontSize = 13;
				Div6Text2.FontSize = 13;

				Div7Text1.FontSize = 13;
				Div7Text2.FontSize = 13;

				Div8Text1.FontSize = 13;
				Div8Text2.FontSize = 13;

				Div9Text1.FontSize = 13;
				Div9Text2.FontSize = 13;

				Div10Text1.FontSize = 13;
				Div10Text2.FontSize = 13;

                close.Margin = new Thickness(5);
                ImageCircle.Margin = new Thickness(15);
                header.VerticalOptions = LayoutOptions.CenterAndExpand;

            

            }


		}


    }


}
