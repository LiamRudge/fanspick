﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Fanspick.Helper;
using Fanspick.RestAPI;
using Fanspick.Shared.Events.Events;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.ViewModels;
using Refit;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Fanspick.Shared.Models.DTO.CupGamesDTO;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChampionLeague : ContentPage
	{
		MyTeamPageViewModel vm;
		App_MainViewModel vmApp_Main;

		public ChampionLeague()
		{
			vm = CacheStorage.ViewModels.MyTeamPage;
			BindingContext = vm;
			InitializeComponent();
			vmApp_Main = CacheStorage.ViewModels.App_Main;
			NavigationPage.SetHasNavigationBar(this, false);

			GetAllChampionTeams();
            Fanspick.Screens.SplashScreen.isFixturePage = false;
		}

		private async void GetAllChampionTeams()
		{

			try
			{
				vm.ShowLoader();
		
				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

				RequestCupGameRefitDTO getTeamDataRequest = new RequestCupGameRefitDTO();
				getTeamDataRequest.CompetitionName = "UEFA Champions League";
				var header = "Bearer " + FanspickCache.UserData.AccessToken;
				var response = await gitHubApi.GetTeamsForCompetitionName(getTeamDataRequest, header);

				if (response.StatusCode == 200)
				{
					vm.EuropaTeams = response.Data;
					vm.HideLoader();
				}
				else
				{

					vm.HideLoader();
				}

			}
			catch (Exception e)
			{
				Debug.WriteLine(e.Message);
                vm.HideLoader();
			}


		}

        private async void lvTeams_ItemSelected(object sender, ItemTappedEventArgs e)
		{

            ResponseCupGameData team = e.Item as ResponseCupGameData;


			var action = await DisplayActionSheet("" + team.KnownName, "Cancel", "Load Team", "Set as Favorite", "Follow team");
			switch (action)
			{

				case "Load Team":
                    actIndSamp.IsVisible = true;
					FanspickCache.CurrentFanspickScope.SelectedTeam.Id = team.Id;
					FanspickCache.CurrentFanspickScope.SelectedTeam.KnownName = team.KnownName;
					FanspickPitchEventHandler.ResetPitchEvents();

					FanspickCache.CurrentFanspickScope.SelectedFixture = new Fixture();
				//	Application.Current.MainPage = new SplashScreen();
                    await Navigation.PushAsync(new Screens.SplashScreen(), true);
                    actIndSamp.IsVisible = false;
					break;

				case "Set as Favorite":
					var answer = await DisplayAlert("Are you sure ?", "You want this as your number one team ?", "Continue", "Cancel");
					switch (answer)
					{
						case true:
                            OnclickSetFav(team.Id, "PRIMARY", team.KnownName);
							break;
					}

					break;

				case "Follow team":

					var answer2 = await DisplayAlert("Are you sure ?", " You want to follow this team ?", "Continue", "Cancel");
					switch (answer2)
					{
						case true:
                            OnclickSetFav(team.Id, "SECONDARY", team.KnownName);
							break;
					}

					break;


			}

			 ((ListView)sender).SelectedItem = null;


		}

		private async void OnclickSetFav(String TeamId, String FavType, String teamName)
		{
            vm.requestSetFavTeamDTO.TeamId = TeamId;
			vm.requestSetFavTeamDTO.Type = FavType;


			Shared.Models.Service.ServiceResponse response = await vm.SetFavTeam();
			if (response.OK)
			{
				//Application.Current.MainPage = new SplashScreen();
				await Navigation.PushAsync(new Screens.MyTeamPage(), true);
			}
			else
			{
				await DisplayAlert("Fanspick", "" + response.ReasonPhrase, "OK");

			}
		}

	}
}
