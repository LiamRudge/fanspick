﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SportsPage : ContentPage
    {
        public SportsPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}