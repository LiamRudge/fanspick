﻿﻿using Fanspick.Helper;
using Fanspick.ViewModels;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;
using Plugin.Geolocator;
using Fanspick.Shared.Helper;
using Plugin.DeviceInfo;
using Xamarin.Forms.Xaml;
using Refit;
using Fanspick.RestAPI;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FixturePage : ContentPage
    {

        FixturePageViewModel vmFixturePage;
        App_MainViewModel vmApp_Main;
        public static HtmlWebViewSource html;
        public FixturePage()
        {
            InitializeComponent();
            Fanspick.Screens.SplashScreen.isFixturePage = true;
            vmFixturePage = CacheStorage.ViewModels.FixturePage;
            vmApp_Main = CacheStorage.ViewModels.App_Main;
            vmFixturePage.NextFixture = Fanspick.Shared.Helper.FanspickCache.CurrentFanspickScope.NextFixture;
            BindingContext = vmFixturePage;
            lvFixtures.ItemSelected += LvFixtures_ItemSelected;
            NavigationPage.SetHasNavigationBar(this, false);

			 html = new HtmlWebViewSource();
			//iconArrow.Rotation = 0;
			
        }

		public void BackPressed(object sender, EventArgs e)
		{
			//  vm.ShowLoader();
			//  Application.Current.MainPage = new App_Main();
			// Navigation.PushModalAsync(new Screens.App_Main(), true);
			Navigation.PopAsync();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (FanspickCache.CurrentFanspickScope.HasUpcomingFixture)
            {
                UpcomingText.IsVisible = true;
                StackNoFixture.IsVisible = false;
                StackFixture.IsVisible = true;
            }
            else
            {
				UpcomingText.IsVisible = true;
                // LastAvailableText.IsVisible = true;
                StackNoFixture.IsVisible = true;
                StackFixture.IsVisible = false;
            }
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
        }
        
        private void LvFixtures_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
			vmFixturePage.SelectedFixture = e.SelectedItem as Fanspick.Shared.Models.Fixture;
			var selFixture = e.SelectedItem as Fanspick.Shared.Models.Fixture;
			if (selFixture.dateMsg != "" || selFixture.timeMsg != "")
			{
				DisplayAlert("", "This fixture will be accessible once Date and Time are announced.", "OK");
			}
			else
			{
				vmFixturePage.ShowLoader();
                GetBillBoardBillBoard(vmFixturePage.SelectedFixture);
				GetInfo(vmFixturePage.SelectedFixture.FixtureName, vmFixturePage.SelectedFixture.Id);
			}

		}

		public void loadUpcoming(object sender, EventArgs e)
		{
			vmFixturePage.ShowLoader();
			vmFixturePage.SelectedFixture = FanspickCache.CurrentFanspickScope.NextFixture;
            GetBillBoardBillBoard(vmFixturePage.SelectedFixture);
			GetInfo(vmFixturePage.SelectedFixture.FixtureName, vmFixturePage.SelectedFixture.Id);
		}

        public async void GetInfo(String fixtureName, String fixtureId)
		{
			try
			{
                var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;
                dynamic position = null;

				try
                {
                    position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
                }
                catch (Exception ex)
                {

                }

                if (position != null)
                {
					if (position.Latitude.ToString() != null)
					{
						vmFixturePage.requestEventLoggingDTO.Latitude = "" + position.Latitude;
					}
                    if (position.Longitude.ToString() != null)
					{
						vmFixturePage.requestEventLoggingDTO.Longitude = "" + position.Longitude;
					}
                }
                else{
					Debug.WriteLine("null gps", "null gps");
					//return;
				}

                vmFixturePage.requestEventLoggingDTO.EventType = "fixtureSelection";
                vmFixturePage.requestEventLoggingDTO.EventDescription = "fixtureSelection " + fixtureName;
                vmFixturePage.requestEventLoggingDTO.EventAdditionalInfoID = "" + fixtureId;
				vmFixturePage.requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
				vmFixturePage.requestEventLoggingDTO.DeviceType = "IOS";
				vmFixturePage.requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
				vmFixturePage.requestEventLoggingDTO.AppVersion = "1";

                Shared.Models.Service.ServiceResponse response = await vmFixturePage.EventLog();
				if (response.OK)
				{

				}
				Shared.Events.Events.FanspickPitchEventHandler.ResetPitchEvents();
                Application.Current.MainPage = new OptimisedFiles.MasterPage();
                vmFixturePage.HideLoader();
			}
			catch (Exception ex)
			{
				Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);
			}

		}

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
        }


		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{
				NavigationRow1.Height = new GridLength(6, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(94, GridUnitType.Star);

                UpperBlock.Height = new GridLength(180);
				btnback.WidthRequest = 12;

				Division1.Height = new GridLength(50);
				Division2.Height = new GridLength(80);
				Division3.Height = new GridLength(50);
                DivisionImage.HeightRequest = 180;
                HeaderGrid.HeightRequest = 180;

                HeaderDateTxt.FontSize = 16;
				label1.FontSize = 15;
				label2.FontSize = 15;
				label3.FontSize = 15;
                LeagueName.FontSize = 18;
			}
			else
			{
				NavigationRow1.Height = new GridLength(10, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(90, GridUnitType.Star);

                UpperBlock.Height = new GridLength(120);
				btnback.WidthRequest = 8;

                Division1.Height = new GridLength(40);
                Division2.Height = new GridLength(40);
                Division3.Height = new GridLength(40);
                DivisionImage.HeightRequest = 100;
                HeaderGrid.HeightRequest = 120;

                HeaderDateTxt.FontSize = 11;
				label1.FontSize = 10;
				label2.FontSize = 10;
				label3.FontSize = 10;
				LeagueName.FontSize = 13;

			}


		}

		private async void GetBillBoardBillBoard(Fixture fixture)
		{
			try
			{

				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				var request = "Bearer " + FanspickCache.UserData.AccessToken;

				RequestGetBillBoardDTO billboard = new RequestGetBillBoardDTO();
				billboard.FixtureId = fixture.Id;
				billboard.SponsorImageType = "Billboard";

				var responseProfile = await gitHubApi.GetSponsorBillboard(billboard, request);

				if (responseProfile.StatusCode == 200)
				{
					if (responseProfile.Data.Count == 0)
					{

						FanspickCache.BannerImage = "Banner_FP_with-Billboard.gif.gif";
						FanspickCache.BannerPopUp = "PopUp_Page_v4.png";
						FanspickCache.BannerUrl = "http://fanspick.com";

					}
					else
					{
                        FanspickCache.BannerImage = Fanspick.Constants.SponserUrl + responseProfile.Data[0].BillBoardImage;
						FanspickCache.BannerPopUp = Fanspick.Constants.SponserUrl + responseProfile.Data[0].BillBoardBanner;
						FanspickCache.BannerUrl = responseProfile.Data[0].BillBoardLink;

					}


					GetBillBoardPitch(fixture);
				}

			}
			catch (ApiException e)
			{

				FanspickCache.BannerImage = "Banner_FP_with-Billboard.gif.gif";
				FanspickCache.BannerPopUp = "PopUp_Page_v4.png";
				FanspickCache.BannerUrl = "http://fanspick.com";
				GetBillBoardPitch(fixture);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.ToString());
				FanspickCache.BannerImage = "Banner_FP_with-Billboard.gif.gif";
				FanspickCache.BannerPopUp = "PopUp_Page_v4.png";
				FanspickCache.BannerUrl = "http://fanspick.com";

				FanspickCache.PitchImage = "banner_FP_pitch.gif";
				FanspickCache.PitchPopUp = "PopUp_Page_v4.png";
				FanspickCache.PitchUrl = "http://fanspick.com";
			}

		}

		private async void GetBillBoardPitch(Fixture fixture)
		{
			try
			{

				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				var request = "Bearer " + FanspickCache.UserData.AccessToken;

				RequestGetBillBoardDTO billboard = new RequestGetBillBoardDTO();
				billboard.FixtureId = fixture.Id;
				billboard.SponsorImageType = "Pitch";

				var responseProfile = await gitHubApi.GetSponsorBillboard(billboard, request);

				if (responseProfile.StatusCode == 200)
				{

                    if (responseProfile.Data.Count == 0)
					{
						FanspickCache.PitchImage = "banner_FP_pitch.gif";
						FanspickCache.PitchPopUp = "PopUp_Page_v4.png";
						FanspickCache.PitchUrl = "http://fanspick.com";

					}
					else
					{
						FanspickCache.PitchImage = Fanspick.Constants.SponserUrl + responseProfile.Data[0].BillBoardImage;
						FanspickCache.PitchPopUp = Fanspick.Constants.SponserUrl + responseProfile.Data[0].BillBoardBanner;
						FanspickCache.PitchUrl = responseProfile.Data[0].BillBoardLink;
					}

				}

			}
			catch (ApiException e)
			{
				FanspickCache.PitchImage = "banner_FP_pitch.gif";
				FanspickCache.PitchPopUp = "PopUp_Page_v4.png";
				FanspickCache.PitchUrl = "http://fanspick.com";

			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.ToString());
			
				FanspickCache.PitchImage = "banner_FP_pitch.gif";
				FanspickCache.PitchPopUp = "PopUp_Page_v4.png";
				FanspickCache.PitchUrl = "http://fanspick.com";
			}

		}

    }
}
