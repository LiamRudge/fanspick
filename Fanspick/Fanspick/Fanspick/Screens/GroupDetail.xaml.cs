﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using DK.SlidingPanel.Interface;
using Fanspick.Helper;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.DTO;
using Fanspick.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Quobject.SocketIoClientDotNet.Client;
using Xamarin.Forms;

namespace Fanspick.Screens
{
    public partial class GroupDetail : ContentPage
    {
		Socket socket;
		ImageSource _imageSource;
		private IMedia _mediaPicker;
		String ImageSt, Filename;
		byte[] imageAsByte;
        OnetoOneChatModal vm;
		Guid uniqueId;
        public GroupDetail()
        {
           
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
			vm = CacheStorage.ViewModels.OnetoOneChat;
			BindingContext = vm;

            ShowGroupMembers();


            try
            {

                socket = IO.Socket(CreateUri());
                socket.Connect();


                socket.On(Socket.EVENT_CONNECT, (data) =>
                {

                });

                socket.On(Socket.EVENT_DISCONNECT, (data) =>
               {
               });

                socket.On(Socket.EVENT_MESSAGE, (data) =>
               {

               });

                socket.On("messageFromServer", (data) =>
               {

               });

                socket.On("removedFromGroup", (data) =>
               {


               });

                socket.On("removedSelfFromGroup", (data) =>
               {
                   BackPressed(this, EventArgs.Empty);

               });


                socket.On("addedAsAdminInGroup", (data) =>
               {
               //  DisplayAlert("New admin added", "A new admin has been added to the group", "ok");
               BackPressed(this, EventArgs.Empty);

               });
            }
            catch(Exception e){
                Debug.WriteLine(e.ToString());
            }


		}

        public void ShowGroupMembers(){

            vm.OverAllMembers.Clear();


                for (int j = 0; j < vm.GroupMembers.Count; j++)
                {
                    MemberWithAdmin member = new MemberWithAdmin();
                    member.MemberId = vm.GroupMembers[j].MemberId;
                    member.MemberName = vm.GroupMembers[j].MemberName;

                    member.isAdmin = false;

				for (int i = 0; i < vm.AdminMembers.Count; i++)
				{
					if (vm.AdminMembers[i].AdminId == vm.GroupMembers[j].MemberId)
					{
						member.isAdmin = true;
                        break;
                    }
					
				}

                    vm.OverAllMembers.Add(member);
                }




		}


		async void OnContactSelected(object sender, EventArgs e)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)e;

			ContactsRegistered contactSelected = (ContactsRegistered)eventArgs.Parameter;


				Boolean isanyAvailable = false;

            for (int i = 0; i < vm.OverAllMembers.Count; i++)
				{
                if (vm.OverAllMembers[i].MemberId == contactSelected.ContactId)
					{
						isanyAvailable = true;
						break;
					}

				}

				if (isanyAvailable)
				{
					await DisplayAlert("Contact Already added", "The contact has already been added for your group. Please select other contact", "ok");
				}
				else
				{

				JObject sendUser = new JObject();
				sendUser.Add("userId", FanspickCache.UserData.AccessToken);
				sendUser.Add("memberId", contactSelected.ContactId);
				sendUser.Add("groupId", FanspickCache.SelectedGroupId);
				socket.Emit("add member", sendUser);

				MemberWithAdmin member = new MemberWithAdmin();
				member.MemberId = contactSelected.ContactId;
                member.MemberName = contactSelected.Name;
				member.isAdmin = false;

				vm.OverAllMembers.Add(member);

                OnClosePanel(this, EventArgs.Empty);

				}

		}



         public async void OnAdmin(object sender, EventArgs e)
        {
			var mi = ((MenuItem)sender);

			MemberWithAdmin contactSelected = (MemberWithAdmin)mi.CommandParameter;

            if(!CheckIfUserAdmin()){

                await DisplayAlert("No Admin Rights Found", "You can add or remove others as admin, only if you have an admin right.", "ok");

            }else{

				if (!contactSelected.isAdmin)
				{
					var answer = await DisplayAlert("Add as admin", "Are you sure you want to add " + contactSelected.MemberId + " as Admin of the group", "Yes", "cancel");
					if (answer)
					{
						JObject sendUser = new JObject();
						sendUser.Add("userId", FanspickCache.UserData.AccessToken);
						sendUser.Add("memberId", contactSelected.MemberId);
						sendUser.Add("groupId", FanspickCache.SelectedGroupId);
						socket.Emit("add new Admin", sendUser);

					}

				}

            }


        }

        public async void OnDelete(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);

            MemberWithAdmin contactSelected = (MemberWithAdmin)mi.CommandParameter;

            string message = "Are you sure you want to remove " + contactSelected.MemberId + " from the group";
            bool isMe = false;

            if (contactSelected.MemberId == FanspickCache.UserData.Id)
            {
                isMe = true;
                if (vm.AdminMembers.Count == 1)
				{
                    message = "You are the only admin of the group. Are you sure you want to remove yourself from the group";
                }else{
                    message = "Are you sure you want to remove yourself from the group";
                }

            }

            var answer = await DisplayAlert("Remove user", message, "Remove", "cancel");
			if (answer)
			{
                if(isMe){
                await DisplayAlert("InProgress", "The functionility to remove user itself is in progress", "ok");


					JObject sendUser = new JObject();
					sendUser.Add("userId", FanspickCache.UserData.AccessToken);
					sendUser.Add("groupId", FanspickCache.SelectedGroupId);
					socket.Emit("rremove self from group", sendUser);

					vm.OverAllMembers.Remove(contactSelected);


                }else{

					if (!CheckIfUserAdmin())
					{

						await DisplayAlert("No Admin Rights Found", "You can add or remove others only if you have an admin right.", "ok");

                    }else{

						JObject sendUser = new JObject();
						sendUser.Add("userId", FanspickCache.UserData.AccessToken);
						sendUser.Add("memberId", contactSelected.MemberId);
						sendUser.Add("groupId", FanspickCache.SelectedGroupId);
						socket.Emit("remove member from group", sendUser);

						vm.OverAllMembers.Remove(contactSelected);

                    }

					

                }

			}


		

			//

			//		userId: accessToken, --who is removing member
			//memberId: userIdString, – who will be removed from group
			//groupId : stringId

		}

        public bool CheckIfUserAdmin(){

            bool send = false;

			for (int i = 0; i < vm.AdminMembers.Count; i++)
			{
				if (vm.AdminMembers[i].AdminId == FanspickCache.UserData.Id)
				{
					send = true;
                    break;
				}
				else
				{
                    send = false;
				}

			}

            return send;

        }

		async void GetImage(object sender, EventArgs e)

		{

			var action = await DisplayActionSheet("Choose Image From", "Cancel", "Camera", "Photo Roll");

			Debug.WriteLine("Action: " + action);

			switch (action)

			{

				case "Camera":
                    await TakePictureCrop();
                    break;


				case "Photo Roll":
                    await SelectPictureCrop();
					break;


			}

		}

		private async void Setup()
		{

			if (_mediaPicker != null)
			{
				return;

			}
			await CrossMedia.Current.Initialize();
			_mediaPicker = CrossMedia.Current;

		}

		private async Task SelectPictureCrop()

		{
			Setup();
			_imageSource = null;

			try
			{
				if (!CrossMedia.Current.IsPickPhotoSupported)
				{

					await DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
					return;

				}


				var mediaFile = await this._mediaPicker.PickPhotoAsync();
                _imageSource = ImageSource.FromStream(mediaFile.GetStream);



				var memoryStream = new MemoryStream();
				await mediaFile.GetStream().CopyToAsync(memoryStream);
				imageAsByte = memoryStream.ToArray();
				ImageSt = Convert.ToBase64String(imageAsByte);

				await Navigation.PushModalAsync(new CropView(imageAsByte, Refresh));



			}

			catch (System.Exception ex)

			{

				Debug.WriteLine(ex.Message);

			}

		}



		private async Task TakePictureCrop(){

			Setup();
            _imageSource = null;

			try

			{
				if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
				{
					await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
					return;
				}

				var mediaFile = await this._mediaPicker.TakePhotoAsync(new StoreCameraMediaOptions

				{
					DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Front
				});
				_imageSource = ImageSource.FromStream(mediaFile.GetStream);

				var memoryStream = new MemoryStream();
				await mediaFile.GetStream().CopyToAsync(memoryStream);
				imageAsByte = memoryStream.ToArray();
				ImageSt = Convert.ToBase64String(imageAsByte);


				await Navigation.PushModalAsync(new CropView(imageAsByte, Refresh));

			}

			catch (Exception ex)

			{

				Debug.WriteLine(ex.Message);

			}

		}

		private void Refresh()
		{
			try

			{
				if (App.CroppedImage != null)
				{

					Stream stream = new MemoryStream(App.CroppedImage);
					GroupPhoto.Source = ImageSource.FromStream(() => stream);
					
				}

			}

			catch (Exception ex)

			{

				Debug.WriteLine(ex.Message);

			}

		}

	
		public void BackPressed(object sender, EventArgs e)
		{
			Navigation.PopAsync();
		}

		public void EditGroupName(object sender, EventArgs e)
		{
            GroupName.IsEnabled = true;
            GroupName.Focus();
            SendNewName.IsVisible = true;
            iconEdit.IsVisible = false;

		}

		public void SendNewGroupNmae(object sender, EventArgs e)
		{
            
            GroupName.IsEnabled = false;
            iconEdit.Focus();
            SendNewName.IsVisible = false;
            iconEdit.IsVisible = true;

			//    userId: token , groupId: groupId, name: newNameOfGroup

			JObject addGroup = new JObject();
			addGroup.Add("userId", FanspickCache.UserData.AccessToken);
            addGroup.Add("groupId", FanspickCache.chatPersonDetails.ChatGroupID);
            addGroup.Add("name", GroupName.Text);
			socket.Emit("rename group", addGroup);
            FanspickCache.chatPersonDetails.GroupName = GroupName.Text;

		}


		private void OpenPanel(object sender, EventArgs e)
		{
			spTest.ShowExpandedPanel();
		}

		private void OnClosePanel(object sender, EventArgs e)
		{
			spTest.HidePanel();
		}


        protected override void OnAppearing()
        {
            base.OnAppearing();
            GroupName.Text = FanspickCache.chatPersonDetails.GroupName;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
			SlidingPanelConfig config = new SlidingPanelConfig();
			config.MainView = GetMainStackLayout();
			config.TitleBackgroundColor = Color.Green;
            spTest.ApplyConfig(config);
        }


		private StackLayout GetMainStackLayout()
		{
			StackLayout mainStackLayout = new StackLayout();
			mainStackLayout.VerticalOptions = LayoutOptions.FillAndExpand;
			mainStackLayout.HorizontalOptions = LayoutOptions.FillAndExpand;

			return (mainStackLayout);
		}

		public string CreateUri()
		{
			var options = CreateOptions();
			var uri = string.Format("{0}://{1}:{2}", options.Secure ? "https" : "http", options.Hostname, options.Port);
			return uri;
		}


		public IO.Options CreateOptions()
		{
			var config = ConfigBase.Load();
			var options = new IO.Options();
			options.Port = config.server.port;
			options.Hostname = config.server.hostname;
			options.ForceNew = true;

			return options;
		}


		public class ConfigBase
		{
			public ConfigServer server { get; set; }

			public static ConfigBase Load()
			{
				var result = new ConfigBase()
				{
					server = new ConfigServer()
				};
				result.server.hostname = "52.163.48.178";
				result.server.port = 8000;

				return result;
			}
		}


		// fplive.ukwest.cloudapp.azure.com
		// 52.163.48.178

		public class ConfigServer
		{
			public string hostname { get; set; }
			public int port { get; set; }
		}
	}
}
