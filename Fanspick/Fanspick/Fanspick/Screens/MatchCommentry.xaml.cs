﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Helper;
using Fanspick.Shared.Events.Events;
using Fanspick.Shared.Helper;
using Fanspick.ViewModels;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace Fanspick.Screens
{
    public partial class MatchCommentry : ContentPage
    {
        MatchCommentryModal vm;
        FanspickPitchEventHandler fanspickPitchEventHandler;
        public MatchCommentry()
        {
            InitializeComponent();


            Fanspick.Screens.SplashScreen.isFixturePage = false;
            vm = CacheStorage.ViewModels.MatchCommentry;
            BindingContext = vm;
            vm.ShowLoader();


            TimeSpan ts = new TimeSpan(0, 0, 5);
            //Device.StartTimer(ts, GetMatchCommentry);
            GetCommentry();

			NavigationPage.SetHasNavigationBar(this, false);
            fanspickPitchEventHandler = new FanspickPitchEventHandler();
			fanspickPitchEventHandler.UpdateCounterTimeText += FanspickPitchEventHandler_UpdateCounterTimeText;


            // BottomLabel
            if (FanspickPitchEventHandler.MatchStage == MatchStage.RunUp || FanspickPitchEventHandler.MatchStage == MatchStage.PreMatch){
                BottomLabel.Text = "Come back after kick off to see live match commentary";
            }else{
                BottomLabel.Text = "No live match commentary to display";
            }
              
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LocalLogo.Source = LocalTeamLogo;
            VisitorLogo.Source = VisitorTeamLogo;
            String device = CrossDeviceInfo.Current.Model;
            ajustDevice(device);
          
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
        }

		void FanspickPitchEventHandler_UpdateCounterTimeText(object sender, EventArgs e)
		{
           
			KickOffTime.Text = sender as string;
			if (KickOffTime.Text.Equals("Dont forget to make YourPick now on the upcoming fixture!"))
			{
				KickOffTime.Text = "";
			}
		}


		public void BackPressed(object sender, EventArgs e)
		{
			vm.ShowLoader();
			//Application.Current.MainPage = new App_Main();
            Navigation.PopAsync();
		}

        async void GetCommentry()
        {
            vm.requestMatchCommentriesDTO.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
			Shared.Models.Service.ServiceResponse response = await vm.getMatchCommentry();
			if (response.OK)
			{
				Shared.Models.DTO.MatchCommentriesDTO.ResponseMatchCommentries dto = Newtonsoft.Json.JsonConvert.DeserializeObject<Shared.Models.DTO.MatchCommentriesDTO.ResponseMatchCommentries>(response.Data);
				vm.MatchCommentry = dto.Data.Commentry;
                vm.HideLoader();

                if(vm.MatchCommentry.Count>0){
                BackImage.IsVisible = false;    
                }else{
                    BackImage.IsVisible = true;
                }


            }else{
                vm.HideLoader();
                BackImage.IsVisible = true;
            }
        }

		public ImageSource LocalTeamLogo
		{

			get
			{

                string ImageString = FanspickCache.CurrentFanspickScope.SelectedFixture.LocalTeam.FanspickTeam.ImageURL;
				if (ImageString != null)
				{
					return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamLogos/" + ImageString;
				}
				else
				{
					return "no_image";
				}

			}


		}

		public ImageSource VisitorTeamLogo
		{
		
			get
			{

				string ImageString = FanspickCache.CurrentFanspickScope.SelectedFixture.VisitorTeam.FanspickTeam.ImageURL;

				if (ImageString != null)
				{
					return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamLogos/" + ImageString;
				}
				else
				{
					return "no_image";
				}


			}
		}

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{
				NavigationRow1.Height = new GridLength(6, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(94, GridUnitType.Star);

				btnback.WidthRequest = 12;
			}
			else
			{
				NavigationRow1.Height = new GridLength(10, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(90, GridUnitType.Star);

				btnback.WidthRequest = 8;


			}


		}

    }
}
