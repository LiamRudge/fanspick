﻿using System;
using System.Collections.Generic;
using Fanspick.Helper;
using Plugin.DeviceInfo;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WebviewLink : ContentPage
    {
        public WebviewLink()
        {
            InitializeComponent();
          //  LocalWebView.Source = SplashScreen.openUrl;

			var source = new UrlWebViewSource();
			source.Url = SplashScreen.openUrl;
			LocalWebView.Source = source;

        }

		public void BackPressed(object sender, EventArgs e)
		{
            Navigation.PopModalAsync();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);


		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{
				NavigationRow1.Height = new GridLength(6, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(94, GridUnitType.Star);
				btnback.WidthRequest = 12;
			}
			else
			{
				NavigationRow1.Height = new GridLength(10, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(90, GridUnitType.Star);
                btnback.WidthRequest = 8;

			}


		}
    }
}
