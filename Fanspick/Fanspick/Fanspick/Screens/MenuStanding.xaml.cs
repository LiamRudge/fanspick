﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.ViewModels;
using System.ComponentModel;

using Xamarin.Forms;
using Fanspick.Helper;
using System.Diagnostics;
using static Fanspick.Shared.Models.DTO.GetTopPlayersDTO;
using static Fanspick.Shared.Models.DTO.GetTopTeamDTO;
using Fanspick.Shared.Events.Events;
using Plugin.DeviceInfo;
using Fanspick.PopUp;
using Rg.Plugins.Popup.Extensions;
using Fanspick.Shared.Models.DTO;
using Refit;
using Fanspick.RestAPI;
using Xamarin.Forms.Xaml;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuStanding : ContentPage, INotifyPropertyChanged
    {
        

        App_TeamSelectorViewModel vm;
        App_MainViewModel vmApp_Main;
        public MenuStanding()
        {
            InitializeComponent();
            Fanspick.Screens.SplashScreen.isFixturePage = false;
			vm = CacheStorage.ViewModels.App_TeamSelector;
            vmApp_Main = CacheStorage.ViewModels.App_Main;
			BindingContext = vm;
            NavigationPage.SetHasNavigationBar(this, false);
            GetCountryAsync();
           
        }

		private async void GetCountryAsync()
		{
			FanspickCache.CountryData.Clear();
			var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

			RequestGetCountriesForSportRefitDTO getTeamDataRequest = new RequestGetCountriesForSportRefitDTO();
			getTeamDataRequest.SportId = "58906bc577a25834fa86fed3";
			var header = "Bearer " + FanspickCache.UserData.AccessToken;
			var response = await gitHubApi.GetCountriesForSport(getTeamDataRequest, header);

			if (response.StatusCode == 200)
			{
				vm.Countries = response.Data;
				FanspickCache.CountryData.AddRange(response.Data);
				pkrCountries.SelectedIndex = 1;
			}

		}

		private void pkrCountries_SelectedIndexChanged(object sender, EventArgs e)
		{
			vm.SelectedCountry = pkrCountries.SelectedItem as Country;
			GetCompetition(vm.SelectedCountry.Id);

		}


		public async void GetCompetition(String countryID)
		{

			var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
			RequestAllLeaguesForCountryRefitDTO getTeamDataRequest = new RequestAllLeaguesForCountryRefitDTO();
			getTeamDataRequest.CountryId = countryID;
			var header = "Bearer " + FanspickCache.UserData.AccessToken;
			var response = await gitHubApi.GetLeaguesForCountry(getTeamDataRequest, header);

			if (response.StatusCode == 200)
			{
				vm.Competitions = response.Data;
			}
		}


		private void pkrLeaguesForSelectedCountry_SelectedIndexChanged(object sender, EventArgs e)
		{
			vm.SelectedCompetition = pkrLeaguesForSelectedCountry.SelectedItem as Competition;
            GetTopPlayersStanding();
			GetTopTeamStanding();
		}

		private void ListView_ItemSelectedPlayer(object sender, ItemTappedEventArgs e)
		{
			ResponseGetTopPlayersData playersData = (ResponseGetTopPlayersData)e.Item;
			FanspickCache.CurrentFanspickScope.SelectedPlayerId = playersData.Id;
			CacheStorage.ViewModels.PlayerData.Player = playersData.PlayerDetails;
            var _player = new PlayerInformationPage(playersData.Id);
             Navigation.PushPopupAsync(_player);
			((ListView)sender).SelectedItem = null;
		}


		private async void ListView_ItemSelectedTable(object sender, ItemTappedEventArgs e)
		{

			ResponseGetTopTeamData response = e.Item as ResponseGetTopTeamData;

			Team newTeam = new Team();

			newTeam.Id = response.Stats.TeamID;
			newTeam.KnownName = response.Stats.KnownName;
			newTeam.ImageURL = response.Stats.ImageURLteam;

			FanspickCache.CurrentFanspickScope.SelectedTeam = newTeam;
			await vm.DownloadUpComingFixtures();
			FanspickPitchEventHandler.ResetPitchEvents();

			FanspickCache.CurrentFanspickScope.SelectedFixture = new Fixture();

            await Navigation.PushAsync(new Screens.SplashScreen(), true);

			((ListView)sender).SelectedItem = null;
		}


		private async void GetTopTeamStanding()
		{

			try
			{

				vm.requestGetTopTeamDTO.CompetitionId = vm.SelectedCompetition.Id;

				Shared.Models.Service.ServiceResponse response = await vm.getTopTeamStainding();
				if (response.OK)
				{
                    
                    Shared.Models.DTO.GetTopTeamDTO.ResponseGetTopTeamDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<Shared.Models.DTO.GetTopTeamDTO.ResponseGetTopTeamDTO>(response.Data);

                    if(dto.Data == null){
                     
						vm.TopTeams = new Shared.Models.DTO.GetTopTeamDTO.ResponseGetTopTeamDTO().Data;
                    }else{
                    vm.TopTeams = dto.Data;    
                    }


                    
				}

			}
			catch (Exception e)
			{
				Debug.WriteLine(e.Message);
			}


		}

		private async void GetTopPlayersStanding()
		{

			try
			{

				vm.requestGetTopPlayersDTO.CompetitionId = vm.SelectedCompetition.Id;

				Shared.Models.Service.ServiceResponse response = await vm.getTopPlayersStainding();
				if (response.OK)
				{
                    Shared.Models.DTO.GetTopPlayersDTO.ResponseGetTopPlayersDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<Shared.Models.DTO.GetTopPlayersDTO.ResponseGetTopPlayersDTO>(response.Data);

					if (dto.Data == null)
					{
                        vm.TopPlayers = new Shared.Models.DTO.GetTopPlayersDTO.ResponseGetTopPlayersDTO().Data;
                    }else{
                    vm.TopPlayers = dto.Data;

                    }

				}

			}
			catch (Exception e)
			{
				Debug.WriteLine(e.Message);


			}


		}

		void Handle_Clicked_Players(object sender, System.EventArgs e)
		{
            btn_topPlayer.BorderWidth = 1;
            btn_topTeam.BorderWidth = 0;
            StackPlayers.IsVisible = true;
            StackTable.IsVisible = false;
		}

		void Handle_Clicked_Teams(object sender, System.EventArgs e)
		{
			btn_topPlayer.BorderWidth = 0;
			btn_topTeam.BorderWidth = 1;
            StackPlayers.IsVisible = false;
            StackTable.IsVisible = true;
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
        }

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{
				Block1.Height = new GridLength(45);
				Block2.Height = new GridLength(45);
				SeasonTxt.FontSize = 16;
				btn_topPlayer.FontSize = 16;
				btn_topTeam.FontSize = 16;

                GridTable.HeightRequest = 35;

                TableTxt1.FontSize = 15;
                TableTxt2.FontSize = 15;
                TableTxt3.FontSize = 15;
                TableTxt4.FontSize = 15;
                TableTxt5.FontSize = 15;
                TableTxt6.FontSize = 15;
                TableTxt7.FontSize = 15;

			}
			else
			{
				
                Block1.Height = new GridLength(45);
				Block2.Height = new GridLength(30);

                SeasonTxt.FontSize = 12;
                btn_topPlayer.FontSize = 12;
                btn_topTeam.FontSize = 12;
				
			}


		}

    }
}
