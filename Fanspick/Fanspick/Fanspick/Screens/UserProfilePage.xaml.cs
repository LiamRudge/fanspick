﻿﻿﻿using System;

using System.Collections.Generic;

using System.Diagnostics;

using System.IO;

using System.Linq;

using System.Text;

using System.Threading.Tasks;

using Fanspick.Helper;

using Fanspick.Shared.Helper;

using Fanspick.Shared.Models.Service;

using Fanspick.Shared.Services;

using Fanspick.ViewModels;

using Plugin.Media;

using Xamarin.Forms;

using XLabs.Ioc;

using XLabs.Platform.Device;

using XLabs.Platform.Services.Media;

using static Fanspick.Shared.Models.DTO.UpdateProfileDTO;
using Fanspick.Shared.Models;
using Plugin.DeviceInfo;
using Plugin.Media.Abstractions;
using static Fanspick.Shared.Models.DTO.ViewProfileDTO;
using System.Net.Http;
using ModernHttpClient;
using System.Threading;
using Newtonsoft.Json;
using Fanspick.Shared.Models.DTO;
using Refit;
using Fanspick.RestAPI;
using Xamarin.Forms.Xaml;
using DK.SlidingPanel.Interface;

namespace Fanspick.Screens

{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserProfilePage : ContentPage

    {

        void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
			DOB.IsVisible = false;
            DependencyService.Get<IForceKeyboardDismissalService>().DismissKeyboard();
            StackDob.IsVisible = true;
            StackDob.ForceLayout();
        }



        ImageSource _imageSource;
        private IMedia _mediaPicker;
        String ImageSt, Filename;
        byte[] imageAsByte;
        Boolean isImageSelected = false;
        Boolean isImageUploaded = false;
        Guid uniqueId;


		void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)

		{

			code = lstCountries.SelectedItem as CCode;



			if (code != null)

			{

				txtCountry.Text = code.CodeN;

				PopUpCountries.IsVisible = false;

				txtAddress.Text = "";

				txtAddress.IsVisible = true;

			}

			else

			{



				PopUpCountries.IsVisible = true;

				txtCountry.Text = "";

				txtAddress.IsVisible = false;

			}

		}
        void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)

        {

            cCodes = null;

            isCheck = false;

            GetCountriesCode();

        }

        Prediction selPrediction;


		void Handle_ItemSelected1(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)

		{

			selPrediction = pkCountries.SelectedItem as Prediction;


			if (selPrediction != null)

			{

				PopUpCities.IsVisible = false;

				cityName = selPrediction.Terms[0].Value;

				txtAddress.Text = selPrediction.Description;

				txtAddress.IsVisible = true;

				txtZip.IsVisible = true;



			}

			else

			{

				txtZip.IsVisible = false;

				isCheck = false;

				pkCountries.ItemsSource = null;

				PopUpCities.IsVisible = false;

				// PopUpZip.IsVisible = false;

			}



		}


		bool isCheck = false;

		async void Handle_TextChangedAsync(object sender, Xamarin.Forms.TextChangedEventArgs e)

		{
			if (selPrediction != null)
			{
				if (selPrediction.Description == txtAddress.Text)
				{
					pkCountries.ItemsSource = null;

					PopUpCities.IsVisible = false;
				}
				else
				{
					if (code != null && txtAddress.Text != "")

					{
					await GetCitiesListAsync();

					if (predictions.predictions.Count() > 0)

					{

						pkCountries.ItemsSource = predictions.predictions;

						PopUpCities.IsVisible = true;


					}
				}
					else

					{
						pkCountries.ItemsSource = null;
						PopUpCities.IsVisible = false;
					}

                }
			}
			else
			{
				if (code != null && txtAddress.Text != "")

				{
					await GetCitiesListAsync();
					if (predictions.predictions.Count() > 0)
					{
						pkCountries.ItemsSource = predictions.predictions;
						PopUpCities.IsVisible = true;
					}
					else
					{
						pkCountries.ItemsSource = null;
						PopUpCities.IsVisible = false;

					}

				}
				else
				{
					pkCountries.ItemsSource = null;
					PopUpCities.IsVisible = false;
				}

				if (isCheck)
				{
					pkCountries.ItemsSource = null;
					PopUpCities.IsVisible = false;

				}
			}



		}

		ViewProfileModal vm;
        Predictions predictions;
        CCode code;
        string cityName = "";
        List<CCode> cCodes;
        List<string> pCodes;
        public UserProfilePage()

        {
            InitializeComponent();
            vm = CacheStorage.ViewModels.viewProfile;
            predictions = null;
            code = null;
            isCheck = false;
            pkCountries.ItemsSource = null;

            BindingContext = vm;
            NavigationPage.SetHasNavigationBar(this, false);
            Fanspick.Screens.SplashScreen.isFixturePage = false;

            DateOfBirth.MaximumDate = DateTime.Now;
            DateOfBirth.MinimumDate = DateTime.Now.AddYears(-100);
        }

        private async Task<Predictions> GetCitiesListAsync()
        {
            predictions = await DependencyService.Get<IPlacesAutocomplete>().GetAutocomplete(txtAddress.Text, code.Code.ToLower());
            return predictions;
        }
		private void GetCountriesCode()

		{

			if (txtCountry.Text != "")

			{

				if (txtCountry.Text != FanspickCache.UserData.Address)

				{

					cCodes = DependencyService.Get<ICountryCodes>().DisplayCountryCodes(txtCountry.Text).Distinct().ToList();

				}

				if (cCodes != null)

				{

					if (cCodes.Count() > 0)

					{

						lstCountries.ItemsSource = cCodes;

						PopUpCountries.IsVisible = true;

					}

					else

					{

						lstCountries.ItemsSource = null;

						PopUpCountries.IsVisible = false;

					}

				}

				else

				{

					lstCountries.ItemsSource = null;

					PopUpCountries.IsVisible = false;

				}



			}

			else

			{

				cCodes = null;

				txtAddress.IsVisible = false;

				txtZip.IsVisible = false;

				lstCountries.ItemsSource = null;

				PopUpCountries.IsVisible = false;

			}



		}

        void OnClosePlayerPop1(object sender, EventArgs e)

        {
           PopUpCountries.IsVisible = false;
                  }

        void OnClosePlayerPop2(object sender, EventArgs e)

        {

            PopUpCities.IsVisible = false;
        }

        //void OnClosePlayerPop3(object sender, EventArgs e)

        //{
        //    PopUpZip.IsVisible = false;
        //}

        //private void GetPostalCode()

        //{

        //    if (code != null)

        //    {

        //        pCodes = DependencyService.Get<IPlacesAutocomplete>().PostalCodes(code.Code, cityName, txtZip.Text).Distinct().ToList();

        //        if (pCodes.Count() > 0)

        //        {
        //            lstZip.ItemsSource = pCodes;
        //        }

        //    }

        //    else

        //    {
        //        lstZip.ItemsSource = null;

        //    }


        //}

        protected override void OnAppearing()

        {

            base.OnAppearing();
            if(FanspickCache.UserData.Photo != null){

                if(FanspickCache.UserData.Photo == "null"){
                ProfileImage.Source = "profile_pick_sample";    
                }else{
					ProfileImage.Source = "http://fanspick-admin-test.5sol.co.uk/Uploads/" + FanspickCache.UserData.Photo;
                }
            }else{
                ProfileImage.Source = "profile_pick_sample";
            }


            if (FanspickCache.UserData.Dob.ToString() != "")

            {
                DateOfBirth.Date = (System.DateTime)FanspickCache.UserData.Dob;
                DateTime convertedFixtureDate = DateTime.Parse(FanspickCache.UserData.Dob.ToString());
                DOB.Text = convertedFixtureDate.ToString("MM-dd-yyyy");
            }

            else
            {
                DOB.IsVisible = true;
                StackDob.IsVisible = false;
                DateOfBirth.Date = DateTime.Now.AddYears(-18);
            }


			if (FanspickCache.UserData.Address != "")

			{
				txtCountry.Text = FanspickCache.UserData.Address;
				txtCountry.IsVisible = true;
			}

			else

			{
				txtCountry.IsVisible = false;
			}


			if (FanspickCache.UserData.City != "")

            {
                txtAddress.Text = FanspickCache.UserData.City;
                txtAddress.IsVisible = true;
            }

            else

            {
                txtAddress.IsVisible = false;
            }

            if (FanspickCache.UserData.Zip != "")

            {
                txtZip.Text = FanspickCache.UserData.Zip;
                txtZip.IsVisible = true;

            }

            else

            {
                txtZip.IsVisible = false;
            }

            pkCountries.ItemsSource = null;
         //   lstZip.ItemsSource = null;
            GetProfilePercent();
            String device = CrossDeviceInfo.Current.Model;
          //  ajustDevice(device);

        }

		
        protected override void OnBindingContextChanged()

        {
            base.OnBindingContextChanged();
      
            String device = CrossDeviceInfo.Current.Model;

      //      ajustDevice(device);

        }

	
        protected void txtFirstname_Completed(object sender, EventArgs e)
        {
            txtLastname.Focus();
        }

        protected void txtLastname_Completed(object sender, EventArgs e)
        {
            DateOfBirth.Focus();
        }

        protected void txtDateofbirth_Completed(object sender, EventArgs e)
        {
            txtCountry.Focus();

        }

		void txtAddress_Completed(object sender, System.EventArgs e)
		{
			txtZip.Focus();
		}

		void txtCountry_Completed(object sender, System.EventArgs e)
		{
			txtAddress.Focus();
		}

        async void GetImage(object sender, EventArgs e)

        {

            var action = await DisplayActionSheet("Choose Image From", "Cancel", "Camera", "Photo Roll");

            Debug.WriteLine("Action: " + action);

            switch (action)

            {

                case "Camera":

                    await TakePictureCrop();

                    break;



                case "Photo Roll":

                    await SelectPictureCrop();

                    break;



            }

        }



       
		private async Task SendFileToServer(byte[] image, string URI, string shipNum)
		{
			byte[] fileContents = image;
			Uri webService = new Uri(URI + shipNum);
			HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, webService);
            requestMessage.Headers.Add("authorization", "Bearer " + FanspickCache.UserData.AccessToken);
			requestMessage.Headers.ExpectContinue = false;

			MultipartFormDataContent multiPartContent = new MultipartFormDataContent("----MyGreatBoundary");
			ByteArrayContent byteArrayContent = new ByteArrayContent(fileContents);
			byteArrayContent.Headers.Add("Content-Type", "multipart/form-data");

			uniqueId = Guid.NewGuid();
			multiPartContent.Add(byteArrayContent, "file", "image" + uniqueId + ".jpg");
			requestMessage.Content = multiPartContent;
			HttpClient httpClient = new HttpClient(new NativeMessageHandler());
			try
			{
				HttpResponseMessage httpRequest = await httpClient.SendAsync(requestMessage, HttpCompletionOption.ResponseContentRead, CancellationToken.None);
				var responseContent = await httpRequest.Content.ReadAsStringAsync();

                ErrorMessageResponse dto = JsonConvert.DeserializeObject<ErrorMessageResponse>(responseContent);

                if(dto.StatusCode == 200){
                    isImageUploaded = true;
                    Filename = dto.Data.Filename;
                }else{
                    isImageUploaded = false;
                }


			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
                isImageUploaded = false;
			}
		}



		async void btnUpdate_Clicked(object sender, EventArgs e)

        {
            bool profileUpdated = false;
      try{
				actInd.IsVisible = true;
				RequestUpdateProfileRefitPRoDTO requestUpdateProfileDTO = new RequestUpdateProfileRefitPRoDTO();
				requestUpdateProfileDTO.FirstName = txtFirstname.Text;
				requestUpdateProfileDTO.LastName = txtLastname.Text;
				requestUpdateProfileDTO.DateOfBirth = DateOfBirth.Date.ToString("yyyy-MM-dd");
				requestUpdateProfileDTO.Address = txtCountry.Text;
				requestUpdateProfileDTO.City = txtAddress.Text;
                requestUpdateProfileDTO.Zip = txtZip.Text;

				if (requestUpdateProfileDTO.Zip == "Zip")
				{
					requestUpdateProfileDTO.Zip = "";
				}


				Dictionary<string, string> dict = new Dictionary<string, string>();

				if (txtFirstname.Text != null || txtFirstname.Text != "")
				{
					dict.Add("firstname", txtFirstname.Text);
				}
				if (txtLastname.Text != null || txtLastname.Text != "")
				{
					dict.Add("lastname", txtLastname.Text);
				}
				if (requestUpdateProfileDTO.DateOfBirth != null || requestUpdateProfileDTO.DateOfBirth != "")
				{
					dict.Add("dob", DateOfBirth.Date.ToString("yyyy-MM-dd"));
				}

				if (requestUpdateProfileDTO.Address != null || requestUpdateProfileDTO.Address != "")
				{
					dict.Add("address", requestUpdateProfileDTO.Address);
				}

				if (requestUpdateProfileDTO.Zip != null)
				{
					if (requestUpdateProfileDTO.Zip != "")
					{
						dict.Add("zipcode", requestUpdateProfileDTO.Zip);
					}

				}




				if (isImageSelected)
				{
					await SendFileToServer(imageAsByte, FanspickSettings.ApiUrl, "/fanspick/uploadFile");
				}
				else
				{
					requestUpdateProfileDTO.Photo = FanspickCache.UserData.Photo;

				}

				if (isImageUploaded)
				{
					requestUpdateProfileDTO.Photo = Filename;
					if (requestUpdateProfileDTO.Photo != null)
					{
						dict.Add("photo", Filename);
					}
				}

                Debug.WriteLine(dict);


				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.EditProfilePro(requestUpdateProfileDTO, header);

				if (response.StatusCode == 200)
				{
					string message;
					    if(isImageUploaded){
					        message = "Profile updated successfully";
					    }else {

                        if(isImageSelected){
                            message = "Image was not uploaded. The profile was updated successfully";
                        }else{
                            message = "Profile updated successfully";
                        }

					    }

					    await DisplayAlert("Fanspick", message, "OK");
					    getUserProfile();
					    actInd.IsVisible = false;

				}
				else
				{
					actInd.IsVisible = false;
                    await DisplayAlert("Fanspick", response.Message, "OK");
				}


			}

            catch (ApiException ex)

            { 
                actInd.IsVisible = false;
				var content = ex.GetContentAs<Dictionary<String, String>>();
				await DisplayAlert("Fanspick", content.Values.Last().ToString(), "ok");

			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.ToString());
			}



            GetProfilePercent();

        }

                  async void getUserProfile(){
            
                try
                {
                    var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
                    var request = "Bearer " + FanspickCache.UserData.AccessToken;
                    var responseProfile = await gitHubApi.GetProfile(request);

                    if (responseProfile.StatusCode == 200)
                    {
                        if (responseProfile.Data.First().TeamData.Count == 0)
                        {
                            Application.Current.MainPage = new SignUp_Skippable();
                        }
                        else
                        {
                            if (FanspickCache.CurrentFanspickScope.SelectedTeam == null)
                            {
                                Fanspick.Shared.Models.DefaultTeamFavorite defaultTeam = responseProfile.Data.First().TeamData.First();
                                FanspickCache.CurrentFanspickScope.SelectedTeam = responseProfile.Data.First().TeamData.First().FavouriteTeam;
                            }

                            FanspickCache.UserData.Firstname = responseProfile.Data.First().FirstName;
                            FanspickCache.UserData.Lastname = responseProfile.Data.First().LastName;
                            FanspickCache.UserData.Address = responseProfile.Data.First().Address;
                            FanspickCache.UserData.Photo = responseProfile.Data.First().UserPhoto;
                            FanspickCache.UserData.PhoneNumber = responseProfile.Data.First().PhoneNumber;

						if (FanspickCache.UserData.Photo != null || FanspickCache.UserData.Photo !="null")
						{
							ProfileImage.Source = "http://fanspick-admin-test.5sol.co.uk/Uploads/" + FanspickCache.UserData.Photo;
						}
						else
						{
							ProfileImage.Source = "profile_pick_sample";
						}

                            if (responseProfile.Data.First().DateOfBirth != null)
                            {
                                FanspickCache.UserData.Dob = DateTime.Parse(responseProfile.Data.First().DateOfBirth);
                            }
                            else
                            {
                                FanspickCache.UserData.Dob = DateTime.Now.AddYears(-18);
                            }

                            if (responseProfile.Data.First().PhoneNumber != null)
                            {
                                FanspickCache.UserData.PhoneNumber = responseProfile.Data.First().PhoneNumber;
                            }
                            else
                            {
                                FanspickCache.UserData.PhoneNumber = "";
                            }

                            if (responseProfile.Data.First().City != null)
                            {
                                FanspickCache.UserData.City = responseProfile.Data.First().City;
                            }
                            else
                            {
                                FanspickCache.UserData.City = "";
                            }
                            if (responseProfile.Data.First().Zip != null)
                            {
                                FanspickCache.UserData.Zip = responseProfile.Data.First().Zip;
                            }
                            else
                            {
                                FanspickCache.UserData.Zip = "";
                            }
                            FanspickCache.UserData.OnceLogin = responseProfile.Data.First().OnceLogin;

                        }


                    }


                }
            catch (ApiException ex)
			{

			}

                catch (Exception e)
                {

                }
            }
	
        async void GetProfilePercent()

        {

            try

            {

                int percentage = 0;



                ServiceResponse serviceResponse = await new FanspickServices<GetUserProfileGrade>().Run(string.Empty);



                RootObject response = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(serviceResponse.Data);



                percentage = response.data.gradeValue;



                lblPercentageProfile.Text = $"Your profile is {percentage}% complete";



                double percentageDouble = percentage;



                pbPercentageProfile.Progress = percentageDouble / 100;



                double progressToPercent = percentage == 0 ? 0 : Convert.ToDouble(percentage / 100);



                // progressControl.ProgressTo(progressToPercent, 1000, Easing.BounceIn);

            }

            catch (Exception ex)

            {



            }

        }



        private void Refresh()
        {
            try

            {
                if (App.CroppedImage != null)
                {

					Stream stream = new MemoryStream(App.CroppedImage);
					ProfileImage.Source = ImageSource.FromStream(() => stream);
                    //using (MemoryStream ms = new MemoryStream(App.CroppedImage))
                    //{
                    //    ProfileImage.Source = ImageSource.FromStream(() => ms);
                    //}

                }

            }

            catch (Exception ex)

            {

                Debug.WriteLine(ex.Message);

            }

        }



        private async void Setup()

        {

            if (_mediaPicker != null)

            {

                return;

            }

            await CrossMedia.Current.Initialize();

            _mediaPicker = CrossMedia.Current;

        }



        private async Task SelectPictureCrop()

        {

            Setup();

            _imageSource = null;

            try
            {


                if (!CrossMedia.Current.IsPickPhotoSupported)

                {

                    await DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");

                    return;

                }



                var mediaFile = await this._mediaPicker.PickPhotoAsync();



                _imageSource = ImageSource.FromStream(mediaFile.GetStream);



                var memoryStream = new MemoryStream();
                await mediaFile.GetStream().CopyToAsync(memoryStream);
                imageAsByte = memoryStream.ToArray();
                ImageSt = Convert.ToBase64String(imageAsByte);
                isImageSelected = true;

			

                 await Navigation.PushModalAsync(new CropView(imageAsByte, Refresh));



            }

            catch (System.Exception ex)

            {

                Debug.WriteLine(ex.Message);

            }

        }



        private async Task TakePictureCrop()

        {

            Setup();



            _imageSource = null;



            try

            {
                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                    return;
                }

                var mediaFile = await this._mediaPicker.TakePhotoAsync(new StoreCameraMediaOptions

                {
                    DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Front
                });
                _imageSource = ImageSource.FromStream(mediaFile.GetStream);

				//using (MemoryStream ms = new MemoryStream())
				//{
				//	await mediaFile.GetStream().CopyToAsync(ms);
				//	imageAsByte = ms.ToArray();
				//	ImageSt = Convert.ToBase64String(imageAsByte);
				//	isImageSelected = true;
				//}


				var memoryStream = new MemoryStream();
				await mediaFile.GetStream().CopyToAsync(memoryStream);
				imageAsByte = memoryStream.ToArray();
				ImageSt = Convert.ToBase64String(imageAsByte);
				isImageSelected = true;


                await Navigation.PushModalAsync(new CropView(imageAsByte, Refresh));

            }

            catch (Exception ex)

            {

                Debug.WriteLine(ex.Message);

            }

        }



        public void ajustDevice(string deviceType)

        {



            if (deviceType == "iPad")

            {





                //TotalRow1.Height = new GridLength(30);

                TotalRow2.Height = new GridLength(200);
                TotalRow3.Height = new GridLength(42);

                TotalRow4.Height = new GridLength(42);

                TotalRow5.Height = new GridLength(42);

                TotalRow6.Height = new GridLength(42);



                TotalRow8.Height = new GridLength(42);

                TotalRow9.Height = new GridLength(40);

                TotalRow10.Height = new GridLength(50);

                TotalRow11.Height = new GridLength(120);



                BoxDivision1.Width = new GridLength(20, GridUnitType.Star);

                BoxDivision2.Width = new GridLength(60, GridUnitType.Star);

                BoxDivision3.Width = new GridLength(20, GridUnitType.Star);



                // StackDob.HeightRequest = 42;

                DateOfBirth.HeightRequest = 42;

                txtAddress.HeightRequest = 42;



                ProfileImage.WidthRequest = 150;

                ProfileImage.HeightRequest = 150;
               
                ProfileImage.Margin = new Thickness(30);

            }

            else

            {
                
                //TotalRow1.Height = new GridLength(10);

                TotalRow2.Height = new GridLength(100);
                TotalRow3.Height = new GridLength(32);
                TotalRow4.Height = new GridLength(32);
                TotalRow5.Height = new GridLength(32);
                TotalRow6.Height = new GridLength(32);
                TotalRow8.Height = new GridLength(40);
                TotalRow9.Height = new GridLength(40);
                TotalRow10.Height = new GridLength(50);
                TotalRow11.Height = new GridLength(120);
                BoxDivision1.Width = new GridLength(10, GridUnitType.Star);
                BoxDivision2.Width = new GridLength(80, GridUnitType.Star);
                BoxDivision3.Width = new GridLength(10, GridUnitType.Star);
                txtAddress.HeightRequest = 32;
                ProfileImage.WidthRequest = 80;
                ProfileImage.HeightRequest = 80;
                ProfileImage.Margin = new Thickness(10);

            }


        }




    }

    public class Data

    {

        public string gradePoint { get; set; }

        public int gradeValue { get; set; }

    }



    public class RootObject

    {

        public int statusCode { get; set; }

        public string message { get; set; }

        public Data data { get; set; }

    }







    //BoxDivision1

}
