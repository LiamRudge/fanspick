﻿using System;
using System.Diagnostics;

namespace Fanspick.Screens
{
    internal class FanspickSocketManager
    {
        public void Open(string url, string port)
        {
            
        }
        
		private void RaiseOnMsgReceived(EventArgs e)
		{
			try
			{
				OnMsgReceived(this, e);
			}
			catch (Exception ex)
			{
                Debug.WriteLine(ex.ToString());
			}
		}

		public event EventHandler OnMsgReceived;
    }	
}