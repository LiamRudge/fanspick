﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Text;
using Fanspick.Shared.Helper;
using Fanspick.ViewModels;
using Fanspick.Helper;
using Fanspick.Shared.Models.DTO;
using Newtonsoft.Json;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json.Linq;
using Fanspick.Shared.Models;
using System.Linq;
using System.Globalization;
using Plugin.DeviceInfo;
using Plugin.Connectivity;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatSample : ContentPage
    {
        Socket socket;

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtMessage.Text))
            {
				DateTime current = DateTime.Now;
                DateTime convertedFixtureDate = TimeZoneInfo.ConvertTime(current, TimeZoneInfo.Utc);
				String nowTime = convertedFixtureDate.ToString("yyyy-MM-dd HH:MM");
                String utcTime = current.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");

                JObject o = new JObject();
                o.Add("topicId", FanspickCache.CurrentFanspickScope.SelectTopicID);
                o.Add("userId", FanspickCache.UserData.AccessToken);
                o.Add("type", "message");
                o.Add("chatType", "community");
                o.Add("message", txtMessage.Text);
                o.Add("time", nowTime);

                //SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());

                socket.Emit("new message", o);

                CommunityPosts getMessage = new CommunityPosts();
                User userid = new User();

                userid.Username = FanspickCache.UserData.Username;

                userid.Sender_id = FanspickCache.UserData.Id;

                getMessage.Id = "0";
                getMessage.Message = txtMessage.Text;
                getMessage.UserId = userid;
                getMessage.CurrentUserId = FanspickCache.UserData.Id;
                getMessage.Time = utcTime;

				if (CrossConnectivity.Current.IsConnected)
				{
                    getMessage.isConnect = true;
                }else{
                    getMessage.isConnect = false;
                }


                vm.CommunityPosts.Add(getMessage);

                txtMessage.Text = "";

                ScrollToLast();

            }
        }


        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
            // throw new NotImplementedException();
        }

        MyCommunityModal vm;
        public ChatSample()
        {
            InitializeComponent();

            vm = CacheStorage.ViewModels.MyCommunity;

            BindingContext = vm;

            NavigationPage.SetHasNavigationBar(this, false);
            Fanspick.Constants.raiseAbove = 0;


            try
            {

                socket = IO.Socket(CreateUri());

                socket.Connect();

                TopicName.Text = FanspickCache.CurrentFanspickScope.SelectTopicName;

                //    stackUser1.BackgroundImage.Source = Forms9Patch.ImageSource.FromMultiResource("bg_fr.9.png");

                GetChatHistory();



                socket.On(Socket.EVENT_CONNECT, (data) =>
                    {
                        JObject o = new JObject();
                        o.Add("topicId", FanspickCache.CurrentFanspickScope.SelectTopicID);
                        o.Add("userId", FanspickCache.UserData.AccessToken);
                        socket.Emit("createRoom", o);

                    });

                socket.On(Socket.EVENT_DISCONNECT, (data) =>
               {
               });

                socket.On(Socket.EVENT_MESSAGE, (data) =>
               {
                   GetChatHistory();
               });

                socket.On("messageFromServer", (data) =>
               {

                   MessageFromSocket dto = Newtonsoft.Json.JsonConvert.DeserializeObject<MessageFromSocket>(data.ToString());

               });

                socket.On("onOtherMessage", (data) =>
               {

                   MessageFromSocket dto = Newtonsoft.Json.JsonConvert.DeserializeObject<MessageFromSocket>(data.ToString());
                   DateTime current = DateTime.Now;
                   String utcTime = current.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");

                   CommunityPosts getMessage = new CommunityPosts();
                   User userid = new User();

                   userid.Username = dto.MsgSocket.userName;
                   userid.Sender_id = dto.MsgSocket.uID;

                   getMessage.Id = dto.MsgSocket.topicId;
                   getMessage.Message = dto.MsgSocket.message;
                   getMessage.UserId = userid;
                   getMessage.CurrentUserId = FanspickCache.UserData.Id;
                   getMessage.Time = utcTime;
                   getMessage.isConnect = true;

                   vm.CommunityPosts.Add(getMessage);

                   ScrollToLast();

               });

            }
            catch(Exception e){
                Debug.WriteLine(e.ToString());
            }


        }

        public void AddDefaultMessage(string message, ObservableCollection<CommunityPosts> list)
		{
			


		//	list.Add(getMessage);
		}
       

        public void ScrollToLast()
        {

            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    try
                    {
                        var last = CommunityList.ItemsSource.Cast<object>().LastOrDefault();
                        CommunityList.ScrollTo(last, ScrollToPosition.MakeVisible, false);
                    }
                    catch (Exception ex)
                    {

                    }

                });


            }
            catch (Exception ex)
            {

            }

        }

		public class MessageFromSocket
		{
			[JsonProperty(PropertyName = "message")]
			public Message MsgSocket { get; set; }

		}

        public class Message
        {
            [JsonProperty(PropertyName = "message")]
            public string message { get; set; }

            [JsonProperty(PropertyName = "topicId")]
            public string topicId { get; set; }

            [JsonProperty(PropertyName = "userName")]
            public string userName { get; set; }

            [JsonProperty(PropertyName = "userId")]
            public string uID { get; set; }

        }

        public void BackPressed(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private async void GetChatHistory()
        {

            vm.CommunityPosts.Clear();

            vm.requestChatHistoryDTO.TopicId = FanspickCache.CurrentFanspickScope.SelectTopicID;

            Shared.Models.Service.ServiceResponse response = await vm.GetChatHistory();

            if (response.OK)
            {
                ResponseChatHistoryDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseChatHistoryDTO>(response.Data);
                //  vm.CommunityPosts = dto.Data[0].CommunityPosts;

                foreach (CommunityPosts sf in dto.Data[0].CommunityPosts)
                {
                    sf.isConnect = true;
                    sf.CurrentUserId = FanspickCache.UserData.Id;
                    vm.CommunityPosts.Add(sf);
                }


            }

            ScrollToLast();


        }

        private void ScrollViewLast()
        {

            Device.BeginInvokeOnMainThread(() =>
            {

                try
                {
                    var v = CommunityList.ItemsSource.Cast<object>().LastOrDefault();
                    CommunityList.ScrollTo(v, ScrollToPosition.End, true);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }

            });


        }

        public string CreateUri()
        {
            var options = CreateOptions();
            var uri = string.Format("{0}://{1}:{2}", options.Secure ? "https" : "http", options.Hostname, options.Port);
            return uri;
        }


        public IO.Options CreateOptions()
        {


            var config = ConfigBase.Load();
            var options = new IO.Options();
            options.Port = config.server.port;
            options.Hostname = config.server.hostname;
            options.ForceNew = true;

            return options;
        }


        public class ConfigBase
        {
            public ConfigServer server { get; set; }

            public static ConfigBase Load()
            {
                var result = new ConfigBase()
                {
                    server = new ConfigServer()
                };
                result.server.hostname = "52.163.48.178";
                result.server.port = 8000;

                return result;
            }
        }

		// fplive.ukwest.cloudapp.azure.com
		// 52.163.48.178

		public class ConfigServer
        {
            public string hostname { get; set; }
            public int port { get; set; }
        }

		protected override void OnAppearing()
		{
			base.OnAppearing();

			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
			// TODO add functionality to automatically log back in
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
           
		}

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{

				NavigationBlock1.Height = new GridLength(6, GridUnitType.Star);
				NavigationBlock2.Height = new GridLength(30);
				NavigationBlock3.Height = new GridLength(1);
				NavigationBlock4.Height = new GridLength(94, GridUnitType.Star);
				NavigationBlock5.Height = new GridLength(55);

                TopicName.FontSize = 16;
				btnback.WidthRequest = 12;

                txtMessage.FontSize = 16;
                txtMessage.WidthRequest = 600;
			}
			else
			{
				NavigationBlock1.Height = new GridLength(10, GridUnitType.Star);
				NavigationBlock2.Height = new GridLength(30);
                NavigationBlock3.Height = new GridLength(1);
                NavigationBlock4.Height = new GridLength(90, GridUnitType.Star);
                NavigationBlock5.Height = new GridLength(45);

                TopicName.FontSize = 12;
				btnback.WidthRequest = 8;

                txtMessage.FontSize = 12;
                txtMessage.WidthRequest = 260;
			}


		}

    }

}
