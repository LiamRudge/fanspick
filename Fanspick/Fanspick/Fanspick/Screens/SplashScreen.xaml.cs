﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Badge.Plugin;
using Fanspick.Helper;
using Fanspick.RestAPI;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Fanspick.ViewModels;
using Plugin.Connectivity;
using Plugin.DeviceInfo;
using Refit;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Fanspick.Shared.Models.DTO.UserNotificationDTO;

namespace Fanspick.Screens
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SplashScreen : ContentPage
	{
        public static Boolean isFixturePage = false;
        public static string openUrl = "";
        public static Boolean isPopUpshown;
		public static HtmlWebViewSource html;
        public SplashScreen()
        {
            InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			Task.Delay(100);
            OpenPage();
        }

        private async void OpenPage()
		{
            html = new HtmlWebViewSource();
			if (!CrossConnectivity.Current.IsConnected)
			{
                actInd.IsVisible = false;
                sampleLabel.Text = " " + "\n" + "Sorry, no internet connectivity detected. Please reconnect and try again";
                sampleLabel.HorizontalOptions = LayoutOptions.Center;
                sampleLabel.VerticalOptions = LayoutOptions.Center;
                sampleLabel.HorizontalTextAlignment = TextAlignment.Center;
                sampleLabel.Margin = new Thickness(10, 100, 10, 0);
                sampleLabel.FontSize = 12;

            }else{

                actInd.IsVisible = true;
                sampleLabel.Text = "Loading...";

                await FetchAlerts();

				GetAllFormations();
				if (FanspickCache.UserData.AccessToken != null)
				{
					GetUserProfile();
				}
				else
				{
					await Task.Delay(500);
					Application.Current.MainPage = new Start();
				}

            }

		}

		private async void GetAllFormations()
		{
			try
			{

				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				var request = "Bearer " + FanspickCache.UserData.AccessToken;
				var responseProfile = await gitHubApi.GetAllFormations(request);

				if (responseProfile.StatusCode == 200)
				{
					FanspickCache.Formations = responseProfile.Data;
				}

			}
			catch (Exception e)
			{
                
			}

		}

		private async void GetUserProfile()
		{
			try
			{
				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				var request = "Bearer " + FanspickCache.UserData.AccessToken;
				var responseProfile = await gitHubApi.GetProfile(request);

				if (responseProfile.StatusCode == 200)
				{
					if (responseProfile.Data.First().TeamData.Count == 0)
					{
						Application.Current.MainPage = new SignUp_Skippable();
                      //  Application.Current.MainPage = new SignUpDefaultTeam();
					}
					else
					{
						if (FanspickCache.CurrentFanspickScope.SelectedTeam == null)
						{
							Fanspick.Shared.Models.DefaultTeamFavorite defaultTeam = responseProfile.Data.First().TeamData.First();
							FanspickCache.CurrentFanspickScope.SelectedTeam = responseProfile.Data.First().TeamData.First().FavouriteTeam;
                            SaveTeamIds(responseProfile.Data.First().TeamData);
						}

						FanspickCache.UserData.Firstname = responseProfile.Data.First().FirstName;
						FanspickCache.UserData.Lastname = responseProfile.Data.First().LastName;
						FanspickCache.UserData.Address = responseProfile.Data.First().Address;
						FanspickCache.UserData.Photo = responseProfile.Data.First().UserPhoto;
                        FanspickCache.UserData.PhoneNumber = responseProfile.Data.First().PhoneNumber;

						if (responseProfile.Data.First().DateOfBirth != null)
						{
							FanspickCache.UserData.Dob = DateTime.Parse(responseProfile.Data.First().DateOfBirth);
						}
						else
						{
							FanspickCache.UserData.Dob = DateTime.Now.AddYears(-18);
						}

						if (responseProfile.Data.First().PhoneNumber != null)
						{
							FanspickCache.UserData.PhoneNumber = responseProfile.Data.First().PhoneNumber;
						}
						else
						{
							FanspickCache.UserData.PhoneNumber = "";
						}

						if (responseProfile.Data.First().City != null)
						{
							FanspickCache.UserData.City = responseProfile.Data.First().City;
						}
						else
						{
							FanspickCache.UserData.City = "";
						}
						if (responseProfile.Data.First().Zip != null)
						{
							FanspickCache.UserData.Zip = responseProfile.Data.First().Zip;
						}
						else
						{
							FanspickCache.UserData.Zip = "";
						}
						FanspickCache.UserData.OnceLogin = responseProfile.Data.First().OnceLogin;
                        GetFixtureAsync();
					}


				}
				else
				{
					Fanspick.Shared.Helper.FanspickCache.UserData = null;
					Application.Current.MainPage = new Start();
				}

			}

            catch (ApiException e)
			{
				Fanspick.Shared.Helper.FanspickCache.UserData = null;
				Application.Current.MainPage = new Start();
			}

			catch (Exception e)
			{
				Fanspick.Shared.Helper.FanspickCache.UserData = null;
				Application.Current.MainPage = new Start();
			}

		}

        private void SaveTeamIds(List<DefaultTeamFavorite> teamData)
        {
            List<String> primeIds = new List<String>();
            for (int i = 0; i < teamData.Count(); i++)
            {
				primeIds.Add(teamData[i].FavouriteTeam.Id);
            }
            FanspickCache.CurrentFanspickScope.FavoritePrimaryTeamID = primeIds;

        }

        private async void GetFixtureAsync()
		{
            try{

				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				RequestGetUpcomingFixturesRefitDTO requestDTO = new RequestGetUpcomingFixturesRefitDTO();
				requestDTO.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
				var header = "Bearer " + FanspickCache.UserData.AccessToken;
				var responseProfile = await gitHubApi.GetUpcomingFixtures(requestDTO, header);

				if (responseProfile.StatusCode == 200)
				{
					FanspickCache.CurrentFanspickScope.UpComingFixtures = responseProfile.Data;
                    SetSelectedFixture();
				}

            }
            catch(Exception e){
                
            }

		}

		private void SetSelectedFixture()
		{
			if (FanspickCache.CurrentFanspickScope.UpComingFixtures != null && FanspickCache.CurrentFanspickScope.UpComingFixtures.Any())
			{
				FanspickCache.CurrentFanspickScope.SelectedFixture = FanspickCache.CurrentFanspickScope.UpComingFixtures.First();
			}
			LoadNextFixture();

		}

		private void LoadNextFixture()
		{
			Fixture upcomingFixture = FanspickCache.CurrentFanspickScope.UpComingFixtures
												   //get the fixtures after the current date (minus the 12 hours josh wants to show the fixture for)
												   .Where(f => DateTime.Parse(f.FixtureDate) >= DateTime.Now.AddHours(-12))
												   //order them by the date of the fixture (nearest first)
												   .OrderBy(f => DateTime.Parse(f.FixtureDate))
												   //take the nearest fixture
												   .FirstOrDefault();

			if (upcomingFixture == null)
			{
				// there are no more fixtures available so grab the last fixture
				upcomingFixture = FanspickCache.CurrentFanspickScope.UpComingFixtures.LastOrDefault();
				FanspickCache.CurrentFanspickScope.HasUpcomingFixture = false;
			}
			else
			{
				FanspickCache.CurrentFanspickScope.HasUpcomingFixture = true;
			}

            GetBillBoardBillBoard(upcomingFixture);
		}

        private async void GetBillBoardBillBoard(Fixture fixture)
		{
			try
			{

				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				var request = "Bearer " + FanspickCache.UserData.AccessToken;

				RequestGetBillBoardDTO billboard = new RequestGetBillBoardDTO();
                billboard.FixtureId = fixture.Id;
				billboard.SponsorImageType = "Billboard";

				var responseProfile = await gitHubApi.GetSponsorBillboard(billboard, request);

				if (responseProfile.StatusCode == 200)
				{

                    if(responseProfile.Data.Count() ==0){

						FanspickCache.BannerImage = "Banner_FP_with-Billboard.gif.gif";
						FanspickCache.BannerPopUp = "PopUp_Page_v4.png";
						FanspickCache.BannerUrl = "http://fanspick.com";

					}else{

                        FanspickCache.BannerImage = Fanspick.Constants.SponserUrl +responseProfile.Data.First().BillBoardImage;
                        FanspickCache.BannerPopUp = Fanspick.Constants.SponserUrl + responseProfile.Data.First().BillBoardBanner;
                        FanspickCache.BannerUrl = responseProfile.Data.First().BillBoardLink;

                    }


					

					GetBillBoardPitch(fixture);
				}

			}
			catch (ApiException e)
			{

				FanspickCache.BannerImage = "Banner_FP_with-Billboard.gif.gif";
				FanspickCache.BannerPopUp = "PopUp_Page_v4.png";
				FanspickCache.BannerUrl = "http://fanspick.com";
				GetBillBoardPitch(fixture);
			}catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
				FanspickCache.BannerImage = "Banner_FP_with-Billboard.gif.gif";
				FanspickCache.BannerPopUp = "PopUp_Page_v4.png";
				FanspickCache.BannerUrl = "http://fanspick.com";
                }

		}

        private async void GetBillBoardPitch(Fixture fixture)
		{
			try
			{

				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				var request = "Bearer " + FanspickCache.UserData.AccessToken;

				RequestGetBillBoardDTO billboard = new RequestGetBillBoardDTO();
                billboard.FixtureId = fixture.Id;
				billboard.SponsorImageType = "Pitch";

				var responseProfile = await gitHubApi.GetSponsorBillboard(billboard, request);

				if (responseProfile.StatusCode == 200)
				{
					if (responseProfile.Data.Count() == 0)
					{
                        FanspickCache.PitchImage = "banner_FP_pitch.gif";
                        FanspickCache.PitchPopUp = "PopUp_Page_v4.png";
                        FanspickCache.PitchUrl = "http://fanspick.com";

					}
					else
					{
                        FanspickCache.PitchImage = Fanspick.Constants.SponserUrl + responseProfile.Data.First().BillBoardImage;
						FanspickCache.PitchPopUp = Fanspick.Constants.SponserUrl + responseProfile.Data.First().BillBoardBanner;
						FanspickCache.PitchUrl = responseProfile.Data.First().BillBoardLink;
					}

                    LoadFixture(fixture);
				}

			}
			catch (ApiException e)
			{
				FanspickCache.PitchImage = "banner_FP_pitch.gif";
				FanspickCache.PitchPopUp = "PopUp_Page_v4.png";
				FanspickCache.PitchUrl = "http://fanspick.com";
                LoadFixture(fixture);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.ToString());
				FanspickCache.BannerImage = "Banner_FP_with-Billboard.gif.gif";
				FanspickCache.BannerPopUp = "PopUp_Page_v4.png";
				FanspickCache.BannerUrl = "http://fanspick.com";
			}

		}

		void LoadFixture(Fixture fixture)
		{
			FanspickCache.CurrentFanspickScope.NextFixture = fixture;
			FanspickCache.CurrentFanspickScope.SelectedFixture = fixture;
			Application.Current.MainPage = new OptimisedFiles.MasterPage();
		}

		public async Task<ServiceResponse> GetAlerts()
		{
            RequestUserNotificationDTO requestUserNotification = new RequestUserNotificationDTO();
            requestUserNotification.AccessToken = FanspickCache.UserData.AccessToken;

			ServiceResponse serviceResponse = await new FanspickServices<UserNotificationService>().Run(requestUserNotification);
			return serviceResponse;
		}


		private async Task FetchAlerts()
		{
			Shared.Models.Service.ServiceResponse response = await GetAlerts();

			if (response.OK)
			{
				Shared.Models.DTO.UserNotificationDTO.ResponseUserNotificationDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<Shared.Models.DTO.UserNotificationDTO.ResponseUserNotificationDTO>(response.Data);
				if (dto.Data.Count > 0)
				{
					

					int unread = 0;
					for (int i = 0; i < dto.Data.Count(); i++)
					{

						if (!dto.Data[i].isRead)
						{
							unread = unread + 1;
						}


					}

					CrossBadge.Current.SetBadge(unread);

				}
				else
				{
					CrossBadge.Current.ClearBadge();

				}

			}
			else
			{
				CrossBadge.Current.ClearBadge();
			}
		}

	
	}

}