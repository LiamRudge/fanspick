﻿using System;
using System.Collections.Generic;
using Fanspick.Helper;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.ViewModels;
using Plugin.DeviceInfo;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TopicCommunity : ContentPage
    {
        void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            
        }

        MyCommunityModal vm;
        public TopicCommunity()
        {
            InitializeComponent();
			vm = CacheStorage.ViewModels.MyCommunity;
			BindingContext = vm;
            NavigationPage.SetHasNavigationBar(this, false);
            ResponseMyCommunityData dto = FanspickCache.CurrentFanspickScope.SelectedCommunity;
            vm.MyTopics = dto.Data;
            SplashScreen.isFixturePage = false;
            NavigationPage.SetHasNavigationBar(this, false);

        }

		public void BackPressed(object sender, EventArgs e)
		{
            Navigation.PopAsync();
		}

        async void Handle_ItemSelected(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
            Topics topics = e.Item as Topics;
            FanspickCache.CurrentFanspickScope.SelectTopicID = topics.Id;
            FanspickCache.CurrentFanspickScope.SelectTopicName = topics.Name;
            GetInfo(topics.Name, topics.Id);

			((ListView)sender).SelectedItem = null;
            await Navigation.PushAsync(new Screens.ChatSample(), true);


		}

        public async void GetInfo(String topic_name, String topic_id)
		{
			try
			{

				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

				if (position == null)
				{
					//Debug.WriteLine("null gps", "null gps");
					return;
				}
				if (position.Latitude.ToString() != null)
				{
					vm.requestEventLoggingDTO.Latitude = "" + position.Latitude;
				}
				else
				{
					vm.requestEventLoggingDTO.Latitude = "0";
				}

				if (position.Longitude.ToString() != null)
				{
					vm.requestEventLoggingDTO.Longitude = "" + position.Longitude;
				}
				else
				{
					vm.requestEventLoggingDTO.Longitude = "0";
				}
				vm.requestEventLoggingDTO.EventType = "topicSelected";
				vm.requestEventLoggingDTO.EventDescription = "topicSelected " + topic_name;
				vm.requestEventLoggingDTO.EventAdditionalInfoID = "" + topic_id;
				vm.requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
				vm.requestEventLoggingDTO.DeviceType = "IOS";
				vm.requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
				vm.requestEventLoggingDTO.AppVersion = "1";

				Shared.Models.Service.ServiceResponse response = await vm.EventLog();

			}
			catch (Exception ex)
			{
				//Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);
			}

		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{
				NavigationRow1.Height = new GridLength(6, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(94, GridUnitType.Star);
				btnback.WidthRequest = 12;
			}
			else
			{
				NavigationRow1.Height = new GridLength(10, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(90, GridUnitType.Star);
				btnback.WidthRequest = 8;

			}


		}


    }
}
