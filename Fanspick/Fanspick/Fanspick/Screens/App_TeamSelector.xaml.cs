﻿using Fanspick.Helper;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Fanspick.Shared.Events.Events;
using System.Diagnostics;
using Plugin.Geolocator;
using Plugin.DeviceInfo;
using Refit;
using Fanspick.RestAPI;
using Fanspick.Shared.Models.DTO;
using Xamarin.Forms.Xaml;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class App_TeamSelector : ContentPage, INotifyPropertyChanged
	{
		App_TeamSelectorViewModel vm;
		public App_TeamSelector()
		{
			InitializeComponent();
			vm = CacheStorage.ViewModels.App_TeamSelector;
			BindingContext = vm;

			NavigationPage.SetHasNavigationBar(this, false);
			GetCountryAsync();


		}

		private async void GetCountryAsync()
		{
			FanspickCache.CountryData.Clear();
			var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

			RequestGetCountriesForSportRefitDTO getTeamDataRequest = new RequestGetCountriesForSportRefitDTO();
			getTeamDataRequest.SportId = "58906bc577a25834fa86fed3";
			var header = "Bearer " + FanspickCache.UserData.AccessToken;
			var response = await gitHubApi.GetCountriesForSport(getTeamDataRequest, header);

			if (response.StatusCode == 200)
			{
				vm.Countries = response.Data;
				FanspickCache.CountryData.AddRange(response.Data);
				pkrCountries.SelectedIndex = 0;
			}

		}

		private void pkrCountries_SelectedIndexChanged(object sender, EventArgs e)
		{
			vm.SelectedCountry = pkrCountries.SelectedItem as Country;
			GetCompetition(vm.SelectedCountry.Id);

		}


		public async void GetCompetition(String countryID)
		{

			var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
			RequestAllLeaguesForCountryRefitDTO getTeamDataRequest = new RequestAllLeaguesForCountryRefitDTO();
			getTeamDataRequest.CountryId = countryID;
			var header = "Bearer " + FanspickCache.UserData.AccessToken;
			var response = await gitHubApi.GetLeaguesForCountry(getTeamDataRequest, header);

			if (response.StatusCode == 200)
			{
				vm.Competitions = response.Data;
                pkrLeaguesForSelectedCountry.SelectedIndex = 0;
			}
		}


		private void pkrLeaguesForSelectedCountry_SelectedIndexChanged(object sender, EventArgs e)
		{
			vm.SelectedCompetition = pkrLeaguesForSelectedCountry.SelectedItem as Competition;
            if(vm.SelectedCompetition != null){
                GetTeam(vm.SelectedCompetition.Id);
            }

			
		}

		public async void GetTeam(String compId)
		{

			var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
			RequestGetTeamsForCompetitionRefitDTO getTeamDataRequest = new RequestGetTeamsForCompetitionRefitDTO();
			getTeamDataRequest.CompetitionId = compId;
			var header = "Bearer " + FanspickCache.UserData.AccessToken;
			var response = await gitHubApi.GetTeamsForCompetition(getTeamDataRequest, header);

			if (response.StatusCode == 200)
			{
				vm.Teams = response.Data;
			}
		}



        public async void lvTeams_ItemSelected(object sender, ItemTappedEventArgs e)
		{
			Team team = e.Item as Team;
			if ((vm.SelectedTeam == null) || (vm.SelectedTeam != null && vm.SelectedTeam.Id != team.Id))
			{
				vm.SelectedTeam = team;
			
			}

			var action = await DisplayActionSheet("", "Cancel", "Load Team", "Set as Favorite", "Follow team");
			switch (action)
			{
				case "Load Team":
					actIndSamp.IsVisible = true;
					FanspickCache.CurrentFanspickScope.SelectedFixture = new Fixture();
					await Navigation.PushAsync(new Screens.SplashScreen(), true);
					actIndSamp.IsVisible = false;
					break;

				case "Set as Favorite":
					var answer = await DisplayAlert("Are you sure ?", "You want this as your number one team ?", "Continue", "Cancel");
					switch (answer)
					{
						case true:
							OnclickSetFav(vm.SelectedTeam.Id, "PRIMARY", vm.SelectedTeam.KnownName);
							break;
					}

					break;

				case "Follow team":

					var answer2 = await DisplayAlert("Are you sure ?", " You want to follow this team ?", "Continue", "Cancel");
					switch (answer2)
					{
						case true:
							OnclickSetFav(vm.SelectedTeam.Id, "SECONDARY", vm.SelectedTeam.KnownName);
							break;
					}

					break;

			}

			 ((ListView)sender).SelectedItem = null;


		}

		private async void OnclickSetFav(String TeamId, String FavType, String teamName)
		{
			vm.requestSetFavTeamDTO.TeamId = TeamId;
			vm.requestSetFavTeamDTO.Type = FavType;

			GetInfo(teamName, TeamId);


			Shared.Models.Service.ServiceResponse response = await vm.SetFavTeam();
			if (response.OK)
			{
				//Application.Current.MainPage = new SplashScreen();
				await Navigation.PushAsync(new Screens.MyTeamPage(), true);
			}
			else
			{
				await DisplayAlert("Fanspick", "" + response.ReasonPhrase.Replace("favourite", "favorite"), "OK");

			}
		}

		public async void GetInfo(String team_name, String team_id)
		{
			try
			{

				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

				if (position == null)
				{
					//Debug.WriteLine("null gps", "null gps");
					return;
				}

				if (position.Latitude.ToString() != null)
				{
					vm.requestEventLoggingDTO.Latitude = "" + position.Latitude;
				}
				else
				{
					vm.requestEventLoggingDTO.Latitude = "0";
				}

				if (position.Longitude.ToString() != null)
				{
					vm.requestEventLoggingDTO.Longitude = "" + position.Longitude;
				}
				else
				{
					vm.requestEventLoggingDTO.Longitude = "0";
				}
				vm.requestEventLoggingDTO.EventType = "teamSelection";
				vm.requestEventLoggingDTO.EventDescription = "teamSelection " + team_name;
				vm.requestEventLoggingDTO.EventAdditionalInfoID = "" + team_id;
				vm.requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
				vm.requestEventLoggingDTO.DeviceType = "IOS";
				vm.requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
				vm.requestEventLoggingDTO.AppVersion = "1";

				Shared.Models.Service.ServiceResponse response = await vm.EventLog();


			}
			catch (Exception ex)
			{
                Debug.WriteLine(ex.ToString());
				//Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);
			}

		}

	}
}