﻿using Fanspick.Helper;
using Fanspick.ViewModels;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;
using Fanspick.Shared.Models;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Events.Events;
using Fanspick.Shared.Models.DTO;
using Refit;
using Fanspick.RestAPI;
using Xamarin.Forms.Xaml;

namespace Fanspick.Screens
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MyTeamPage : ContentPage
	{
		MyTeamPageViewModel vm;
		public MyTeamPage()
		{
			vm = CacheStorage.ViewModels.MyTeamPage;
			BindingContext = vm;
			InitializeComponent();
			SplashScreen.isFixturePage = false;
			NavigationPage.SetHasNavigationBar(this, false);

			getAllFavoriteTeams();

		}

		private async void getAllFavoriteTeams()
		{

			try
			{

				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				var request = "Bearer " + FanspickCache.UserData.AccessToken;
				var responseProfile = await gitHubApi.GetAllFavouriteTeams(request);

				if (responseProfile.StatusCode == 200)
				{
					vm.FavTeams = responseProfile.Data;
					SaveTeamIds(vm.FavTeams);
				}

			}
			catch (Exception e)
			{
				Debug.WriteLine(e.Message);
			}


		}

		private void SaveTeamIds(List<ResponseMyTeamsData> teamData)
		{
			List<String> primeIds = new List<String>();
			for (int i = 0; i < teamData.Count; i++)
			{
				if (teamData[i].IsPrimaryFavouriteTeam)
				{
					primeIds.Add(teamData[i].Id);
				}

			}
			FanspickCache.CurrentFanspickScope.FavoritePrimaryTeamID = primeIds;

		}

		private async void lvTeams_ItemSelected(object sender, ItemTappedEventArgs e)
		{

			ResponseMyTeamsData team = e.Item as ResponseMyTeamsData;


			var action = await DisplayActionSheet("" + team.KnownName, "Cancel", "Load Team", "Remove team");
			switch (action)
			{

				case "Load Team":
					actIndSamp.IsVisible = true;

					FanspickCache.CurrentFanspickScope.SelectedTeam.Id = team.Id;
					FanspickCache.CurrentFanspickScope.SelectedTeam.KnownName = team.KnownName;

					FanspickCache.CurrentFanspickScope.SelectedFixture = new Fixture();
					await Navigation.PushAsync(new Screens.SplashScreen(), true);
					//  Application.Current.MainPage = new SplashScreen();
					actIndSamp.IsVisible = false;

					break;

				case "Remove team":
					OnclickUnsetFav(team.Id);
					break;

			}

		   ((ListView)sender).SelectedItem = null;

		}

		private async void OnclickUnsetFav(String TeamId)
		{
			vm.requestUnsetFavTeamDTO.TeamId = TeamId;

			Shared.Models.Service.ServiceResponse response = await vm.UnsetFavTeam();
			if (response.OK)
			{
				await Navigation.PushAsync(new Screens.MyTeamPage(), true);
			}
			else
			{
				await DisplayAlert("Fanspick", "" + response.ReasonPhrase, "OK");

			}
		}
	}
}
