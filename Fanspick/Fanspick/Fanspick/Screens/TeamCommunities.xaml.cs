﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Fanspick.Helper;
using Fanspick.RestAPI;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.ViewModels;
using Plugin.DeviceInfo;
using Plugin.Geolocator;
using Refit;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Fanspick.Shared.Models.DTO.MyCommunityDTO;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TeamCommunities : ContentPage
    {
        
        MyCommunityModal vm;
        public TeamCommunities()
        {
            InitializeComponent();
			vm = CacheStorage.ViewModels.MyCommunity;
			BindingContext = vm;
            GetMyCommunities();
            Fanspick.Screens.SplashScreen.isFixturePage = false;
            NavigationPage.SetHasNavigationBar(this, false);
        }

		public void BackPressed(object sender, EventArgs e)
		{
          //  vm.ShowLoader();
            actIndSamp.IsVisible = true;
            	Navigation.PopAsync();
            actIndSamp.IsVisible = false;
		}

		private async void GetMyCommunities()
		{
            
            try{
                var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
                RequestMyCommunityRefitDTO requestMyCommunityDTO = new RequestMyCommunityRefitDTO();
                requestMyCommunityDTO.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
				var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.GetMyCommunity(requestMyCommunityDTO, header);

				if (response.StatusCode == 200)
				{
                    vm.MyCommunity = response.Data;
					if (vm.MyCommunity.Count > 0)
                    {
			        Nocomm.IsVisible = false;
                    }
			    else{
			        Nocomm.IsVisible = true;
			    
                    }

                }

                }catch(Exception e){
                Nocomm.IsVisible = true;
            }

		}

        void Handle_ItemSelected(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            ResponseMyCommunityData selectedCommunity = e.Item as ResponseMyCommunityData;
			Fanspick.Shared.Helper.FanspickCache.CurrentFanspickScope.SelectedCommunity = selectedCommunity;
			Navigation.PushAsync(new Screens.TopicCommunity(), true);
			// Application.Current.MainPage = new NavigationPage(new TopicCommunity());
			GetInfo(selectedCommunity.CommunityName, selectedCommunity.Id, sender);
			((ListView)sender).SelectedItem = null;

		}

        public async void GetInfo (String communitity_name, String CommunityId, object sender)
		{
			try
			{
                var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

				if (position == null)
				{
					Debug.WriteLine("null gps", "null gps");
					return;
				}
				if (position.Latitude.ToString() != null)
				{
					vm.requestEventLoggingDTO.Latitude = "" + position.Latitude;
				}
				else
				{
					vm.requestEventLoggingDTO.Latitude = "0";
				}

				if (position.Longitude.ToString() != null)
				{
					vm.requestEventLoggingDTO.Longitude = "" + position.Longitude;
				}
				else
				{
					vm.requestEventLoggingDTO.Longitude = "0";
				}
				vm.requestEventLoggingDTO.EventType = "communitySelected";
				vm.requestEventLoggingDTO.EventDescription = "communitySelected "+ communitity_name;
                vm.requestEventLoggingDTO.EventAdditionalInfoID = "" + CommunityId;
				vm.requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
				vm.requestEventLoggingDTO.DeviceType = "IOS";
				vm.requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
				vm.requestEventLoggingDTO.AppVersion = "1";

				Shared.Models.Service.ServiceResponse response = await vm.EventLog();
              //  Application.Current.MainPage = new NavigationPage(new TopicCommunity());
               // await Navigation.PushModalAsync(new Screens.TopicCommunity(), true);
           
			}
			catch (Exception ex)
			{
				Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);
			}

		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
        }

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{
				NavigationRow1.Height = new GridLength(6, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(94, GridUnitType.Star);
				btnback.WidthRequest = 12;
			}
			else
			{
				NavigationRow1.Height = new GridLength(10, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(90, GridUnitType.Star);
                btnback.WidthRequest = 8;

			}

		}


	}


}
