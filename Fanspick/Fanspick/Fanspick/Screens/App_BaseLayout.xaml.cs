﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Fanspick.Screens
{
    public partial class App_BaseLayout : ContentPage
    {
        public App_BaseLayout()
        {
            InitializeComponent();

            // hide the navigation as we are using a custom one for Fanspick
            //NavigationPage.SetHasNavigationBar(this, false);


        }
    }
}
