﻿using CarouselView.FormsPlugin.Abstractions;
using Fanspick.PluginModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using System.Diagnostics;
using Plugin.Geolocator;
using Plugin.DeviceInfo;
using Fanspick.Shared.Helper;
using Xamarin.Forms.Xaml;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Start : ContentPage
    {
        public CarouselViewModel_Walkthrough _vm;

        public Start()
        {
            InitializeComponent();

            BindingContext = _vm = new CarouselViewModel_Walkthrough();

            // hide the navigation not required on the start page
            NavigationPage.SetHasNavigationBar(this, false);

            GetInfo();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            String device = CrossDeviceInfo.Current.Model;
            ajustDevice(device);

            // TODO add functionality to automatically log back in

        }

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}


		private async void btnLogin_Clicked(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new Screens.Login_Normal(), true);
            await Navigation.PushModalAsync(new Screens.Login_Normal(), true);
        }

        private async void btnSignUp_Clicked(object sender, EventArgs e)
        {
			//  await Navigation.PushModalAsync(new Screens.SignUp(), true);
			await Navigation.PushModalAsync(new Screens.SignUp(), true);
		}

		
		public async void GetInfo()
		{
			try
			{
                
				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

				if (position == null)
				{
					Debug.WriteLine("null gps", "null gps");
					return;
				}

                if (position.Latitude.ToString() != null)
				{
					_vm.requestEventLoggingDTO.Latitude = "" + position.Latitude;
                }else {
                    _vm.requestEventLoggingDTO.Latitude = "0";
                }

                if (position.Longitude.ToString() != null)
				{
					_vm.requestEventLoggingDTO.Longitude = "" + position.Longitude;
				}else {
				_vm.requestEventLoggingDTO.Longitude = "0";
			}
				_vm.requestEventLoggingDTO.EventType = "applicationStarted";
				_vm.requestEventLoggingDTO.EventDescription = "applicationStarted";
				_vm.requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
 				_vm.requestEventLoggingDTO.DeviceType = "IOS";
				_vm.requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
				_vm.requestEventLoggingDTO.AppVersion = "1";

				EventLog();

			}
			catch (Exception ex)
			{
				Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);
			}

		}

		private async void EventLog()
		{

			try
			{


				Shared.Models.Service.ServiceResponse response = await _vm.EventLog();
				if (response.OK)
				{

				}
				else
				{
					Debug.WriteLine("error", "" + response.ReasonPhrase);
				}

			}
			catch (Exception e)
			{
				Debug.WriteLine(e.Message);
			}


		}

        public void ajustDevice(string deviceType){

            if(deviceType=="iPad"){

				btnSignUp.FontSize = 22;
				btnLogin.FontSize = 22;


				Col1.Width = new GridLength(10, GridUnitType.Star);
				Col2.Width = new GridLength(50, GridUnitType.Star);
				Col3.Width = new GridLength(50, GridUnitType.Star);
				Col4.Width = new GridLength(10, GridUnitType.Star);


            }else{
                btnSignUp.FontSize = 15;
                btnLogin.FontSize = 15;

			
                Col1.Width = new GridLength(1, GridUnitType.Star);
                Col2.Width = new GridLength(50, GridUnitType.Star);
                Col3.Width = new GridLength(50, GridUnitType.Star);
                Col4.Width = new GridLength(1, GridUnitType.Star);
			}


        }


	}
}
