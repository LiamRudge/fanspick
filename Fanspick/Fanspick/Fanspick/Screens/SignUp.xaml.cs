﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Fanspick.ViewModels;
using Plugin.Connectivity;
using Plugin.DeviceInfo;
using Plugin.Geolocator;
using SQLite;
using Xamarin.Forms;

namespace Fanspick.Screens
{
	public partial class SignUp : ContentPage
	{

		public List<String> Countries { get; set; } = new List<String>();
		public List<String> Leagues { get; set; } = new List<String>();
		public List<String> Teams { get; set; } = new List<String>();

		SignUpViewModel vm;
		public SignUp()
		{
			InitializeComponent();
            Fanspick.Constants.raiseAbove = 0;
			if (vm == null)
			{
				vm = new SignUpViewModel();
				BindingContext = vm;
			}
			GetInfo("accountCreationStart");

			if (!CrossConnectivity.Current.IsConnected)
			{
				DisplayAlert("No Internet Connection", "Sorry, no internet connectivity detected. Please reconnect and try again", "ok");
			}

		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);

			// TODO add functionality to automatically log back in

		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}

        protected void txtUserName_Completed(object sender, EventArgs e)
        {
            txtEmailAddress.Focus();
        }

        protected void txtEmailAddress_Completed(object sender, EventArgs e)
        {
            txtPassword.Focus();
        }

        protected void txtPassword_Completed(object sender, EventArgs e)
        {
            txtPasswordConfirmation.Focus();
        }
 
        #region favourite team
        private void pkrCountries_SelectedIndex(object sender, EventArgs e)
		{
			try
			{
				vm.ShowLoader();
			}
			catch (Exception error)
			{
				// TODO record error
				Debug.WriteLine(error.ToString());
			}
			finally
			{
				vm.HideLoader();
			}
		}

		private void pkrLeagues_SelectedIndex(object sender, EventArgs e)
		{
			try
			{
				vm.ShowLoader();
			}
			catch (Exception error)
			{
				// TODO record error
				Debug.WriteLine(error.ToString());
			}
			finally
			{
				vm.HideLoader();
			}
		}

		private void pkrTeams_SelectedIndex(object sender, EventArgs e)
		{
			try
			{
				vm.ShowLoader();
			}
			catch (Exception error)
			{
				// TODO record error
				Debug.WriteLine(error.ToString());
			}
			finally
			{
				vm.HideLoader();
			}
		}
		#endregion
		public void ShowAlert(String message)
		{
			DisplayAlert("Fanspick", message, "ok");
		}
		private async void btnCreateAccount_Clicked(object sender, EventArgs e)
		{

            if (!CrossConnectivity.Current.IsConnected)
            {
                await  DisplayAlert("No Internet Connection", "Sorry, no internet connectivity detected. Please reconnect and try again", "ok");
            }
            else
            {
                try
                {
                    if (txtUsername.Text == "" || txtUsername.Text == null)
                    {
                        ShowAlert("Please enter your screen name to proceed");
                    }
                    else if (txtEmailAddress.Text == "" || txtEmailAddress.Text == null)
                    {
                        ShowAlert("Please enter your email to proceed");
                    }
                    else if (txtPassword.Text == "" || txtPassword.Text == null)
                    {
                        ShowAlert("Please enter password to proceed");
                    }
                    else if (txtPasswordConfirmation.Text == "" || txtPasswordConfirmation.Text == null)
                    {
                        ShowAlert("Please confirm password to proceed");
                    }
                    else if (txtPasswordConfirmation.Text != txtPassword.Text)
                    {
                        ShowAlert("Password mismatch");
                    }
                    else
                    {
                        vm.ShowLoader();
                        ServiceResponse serviceResponse = await vm.CreateAccount();

                        Debug.WriteLine(serviceResponse.ToString());

                        if (serviceResponse.OK)
                        {
                            vm.HideLoader();
                            Application.Current.MainPage = new SignUp_Skippable();

                        }
                        else
                        {
                            if(serviceResponse.ReasonPhrase == "USERNAME_EXIT" || serviceResponse.ReasonPhrase.Contains("USERNAME")){
                                serviceResponse.ReasonPhrase = "Username already exists";
                            }

                            vm.HideLoader();
                            await DisplayAlert(FanspickStrings.GenericDialogTitle, "" + serviceResponse.ReasonPhrase, FanspickStrings.Alert_OK);
                        }
                    }

                }
                catch (Exception error)
                {
                    // TODO record error
                    Debug.WriteLine(error.ToString());
                }

            }

		}

		public async void GetInfo(String EventType)
		{
			try
			{

				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

				if (position == null)
				{
					Debug.WriteLine("null gps", "null gps");
					return;
				}

				if (position.Latitude.ToString() != null)
				{
					vm.requestEventLoggingDTO.Latitude = "" + position.Latitude;
				}
				else
				{
					vm.requestEventLoggingDTO.Latitude = "0";
				}

				if (position.Longitude.ToString() != null)
				{
					vm.requestEventLoggingDTO.Longitude = "" + position.Longitude;
				}
				else
				{
					vm.requestEventLoggingDTO.Longitude = "0";
				}
				vm.requestEventLoggingDTO.EventType = EventType;
				vm.requestEventLoggingDTO.EventDescription = EventType;

				if (FanspickCache.UserData.AccessToken != null)
				{
					vm.requestEventLoggingDTO.EventAdditionalInfoID = "" + FanspickCache.UserData.AccessToken;
				}

				vm.requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
				vm.requestEventLoggingDTO.DeviceType = "IOS";
				vm.requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
				vm.requestEventLoggingDTO.AppVersion = "1";

				Shared.Models.Service.ServiceResponse response = await vm.EventLog();
				if (response.OK)
				{

				}

				if (EventType == "accountCreationComplete")
				{
                    
				}

			}
			catch (Exception ex)
			{
				Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);
			}

		}

		public void InitDatabase(string AccessToken, string SelectedTeamId, string FavoriteTeamId, string SelectedFixtureId)
		{
			RandomThought randomThought = Shared.Repository.SQLite.FanspickDatabase.Get<RandomThought>().FirstOrDefault();

			if (randomThought == null)
			{
				randomThought = new RandomThought();
				randomThought.AccessToken = AccessToken;
				randomThought.SelectedTeamId = SelectedTeamId;
				randomThought.FavoriteTeamId = FavoriteTeamId;
				randomThought.SelectedFixtureId = SelectedFixtureId;
				Shared.Repository.SQLite.FanspickDatabase.InsertEntity(randomThought);
			}
			else
			{
				randomThought.AccessToken = AccessToken;
				randomThought.SelectedTeamId = SelectedTeamId;
				randomThought.FavoriteTeamId = FavoriteTeamId;
				randomThought.SelectedFixtureId = SelectedFixtureId;
				Shared.Repository.SQLite.FanspickDatabase.UpdateEntity(randomThought);
			}
		}

		public async void BackPressed(object sender, EventArgs e)
		{
			//await Navigation.PushModalAsync(new Screens.App_Main(), true);
			await Navigation.PushModalAsync(new Screens.Start(), true);
		}

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{
				NavigationRow1.Height = new GridLength(6, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(94, GridUnitType.Star);

				txtUsername.FontSize = 16;
				txtEmailAddress.FontSize = 16;
				txtPassword.FontSize = 16;
				txtPasswordConfirmation.FontSize = 16;

				TotalRow1.Height = new GridLength(75);
				TotalRow2.Height = new GridLength(45);
				TotalRow3.Height = new GridLength(20);
				TotalRow4.Height = new GridLength(25);
				TotalRow5.Height = new GridLength(50);
				TotalRow6.Height = new GridLength(350);

				BoxDivision1.Width = new GridLength(20, GridUnitType.Star);
				BoxDivision2.Width = new GridLength(60, GridUnitType.Star);
				BoxDivision3.Width = new GridLength(20, GridUnitType.Star);

				btnback.WidthRequest = 12;
			}
			else
			{
				NavigationRow1.Height = new GridLength(10, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(90, GridUnitType.Star);

                txtUsername.FontSize = 13;
                txtEmailAddress.FontSize = 13;
                txtPassword.FontSize = 13;
                txtPasswordConfirmation.FontSize = 13;

				TotalRow1.Height = new GridLength(75);
				TotalRow2.Height = new GridLength(40);
				TotalRow3.Height = new GridLength(5);
				TotalRow4.Height = new GridLength(19);
				TotalRow5.Height = new GridLength(10);
                TotalRow6.Height = new GridLength(280);

				BoxDivision1.Width = new GridLength(10, GridUnitType.Star);
				BoxDivision2.Width = new GridLength(80, GridUnitType.Star);
				BoxDivision3.Width = new GridLength(10, GridUnitType.Star);

				btnback.WidthRequest = 8;


			}


		}

		private async void SignupFacebook_Clicked(object sender, EventArgs e)
       {
          await Navigation.PushModalAsync(new FacebookProfilePage());
      }

	}
}
