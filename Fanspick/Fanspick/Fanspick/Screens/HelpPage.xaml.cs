﻿using System;
using System.Collections.Generic;
using Fanspick.PluginModels;
using Xamarin.Forms;

namespace Fanspick.Screens
{
    public partial class HelpPage : ContentPage
    {

		public Walkthrough _vm;
        public HelpPage()
        {
            InitializeComponent();
			BindingContext = _vm = new Walkthrough();

            NavigationPage.SetHasNavigationBar(this, false);
        }



		public void hideswipe(object sender, EventArgs e)
		{
            if(swipe.IsVisible){
                swipe.IsVisible = false;
            }

		}
    }
}
