﻿using Fanspick.Helper;
using Fanspick.Shared.Helper;
using Fanspick.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Fanspick.Screens
{
    public partial class App_Main : MasterDetailPage
    {
        App_MainViewModel vm;
        public App_Main()
        {
            InitializeComponent();

 //           Task.Run(() => new BasicViewModel().DownloadResources()).Wait();
  //          vm = CacheStorage.ViewModels.App_Main;
            vm.NavPage = NavPage.Navigation;
            vm.ActiveDetailsPage(ScreenType.HomePitch);
            BindingContext = vm;


            if (Device.OS == TargetPlatform.Windows)
            {
                Master.Icon = String.Empty;
            }
        }
    }
}
