﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.ViewModels;
using Xamarin.Forms;

using Fanspick.Helper;
using System.ComponentModel;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Plugin.DeviceInfo;
using System.Text.RegularExpressions;
using DK.SlidingPanel.Interface;
using Refit;
using Fanspick.RestAPI;
using Fanspick.PopUp;
using Rg.Plugins.Popup.Extensions;
using System.Diagnostics;

namespace Fanspick.Screens
{
	public partial class SignUp_Skippable : ContentPage
	{
	
		void OnClosePlayerPop1(object sender, EventArgs e)
		{

			PopUpCountries.IsVisible = false;
			txtCountry.Text = "";
			txtAddress.IsVisible = false;
		}
		void OnClosePlayerPop2(object sender, EventArgs e)
		{
			PopUpCities.IsVisible = false;
		}
		
		void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
		{
			code = lstCountries.SelectedItem as CCode;

			if (code != null)
			{
				txtCountry.Text = code.CodeN;
				PopUpCountries.IsVisible = false;
				txtAddress.Text = "";
				txtAddress.IsVisible = true;
			}
			else
			{

				PopUpCountries.IsVisible = true;
				txtCountry.Text = "";
				txtAddress.IsVisible = false;
			}
		}
		void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			cCodes = null;
			isCheck = false;
			GetCountriesCodeAuto();
			PopUpCountriesText.Text = txtCountry.Text;
		}

		void Handle_TextChangedPop(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			cCodes = null;
			isCheck = false;
			GetCountriesCodeAuto();
			txtCountry.Text = PopUpCountriesText.Text;
		}

		Prediction selPrediction;
		void Handle_ItemSelected1(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
		{
			selPrediction = pkCountries.SelectedItem as Prediction;
			if (selPrediction != null)
			{
				PopUpCities.IsVisible = false;
				cityName = selPrediction.Terms[0].Value;
				txtAddress.Text = selPrediction.Description;
				txtAddress.IsVisible = true;
				txtZip.IsVisible = true;
			}
			else
			{
				txtZip.IsVisible = false;
				isCheck = false;
				pkCountries.ItemsSource = null;
				PopUpCities.IsVisible = false;
				//PopUpZip.IsVisible = false;
			}

		}

		void Handle_Focused_Code(object sender, Xamarin.Forms.FocusEventArgs e)
		{
			spTest.ShowExpandedPanel();
		}

		private void OnClosePanel(object sender, EventArgs e)
		{
			spTest.HidePanel();
		}

		private AbsoluteLayout GetMainStackLayout()
		{
			AbsoluteLayout mainStackLayout = new AbsoluteLayout();
			mainStackLayout.VerticalOptions = LayoutOptions.FillAndExpand;
			mainStackLayout.HorizontalOptions = LayoutOptions.FillAndExpand;

			return (mainStackLayout);
		}

		async void Tapped_Code(object sender, EventArgs e)
		{
			spTest.ShowExpandedPanel();
		}

		async void OnCodeSelected(object sender, EventArgs e)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)e;
			CCode selectedItem = (CCode)eventArgs.Parameter;
			Code.Text = selectedItem.CodeP;
			Phone.Focus();
			OnClosePanel(this, EventArgs.Empty);
		}

		bool isCheck = false;

		async void Handle_TextChangedAsync(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			if (selPrediction != null)
			{
				if (selPrediction.Description == txtAddress.Text)
				{
					pkCountries.ItemsSource = null;

					PopUpCities.IsVisible = false;
				}
				else
				{
					await GetCitiesListAsync();

					if (predictions.predictions.Count() > 0)
					{
						pkCountries.ItemsSource = predictions.predictions;

						PopUpCities.IsVisible = true;


					}
				}
			}
			else
			{
				if (code != null && txtAddress.Text != "")

				{

					await GetCitiesListAsync();

					if (predictions.predictions.Count() > 0)

					{

						pkCountries.ItemsSource = predictions.predictions;

						PopUpCities.IsVisible = true;


					}

					else

					{
						pkCountries.ItemsSource = null;

						PopUpCities.IsVisible = false;

						// lstZip.ItemsSource = null;

					}

				}

				else

				{

					pkCountries.ItemsSource = null;

					PopUpCities.IsVisible = false;

					// lstZip.ItemsSource = null;

					txtZip.IsVisible = false;

					// PopUpZip.IsVisible = false;

				}

				if (isCheck)

				{

					pkCountries.ItemsSource = null;

					PopUpCities.IsVisible = false;

				}
			}

			// PopUpCitiesText.Text = txtAddress.Text;

		}

		async void Handle_TextChangedAsyncPop(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			if (code != null && txtAddress.Text != "")
			{
				await GetCitiesListAsync();

				if (predictions.predictions.Count() > 0)
				{
					pkCountries.ItemsSource = predictions.predictions;
					PopUpCities.IsVisible = true;

				}
				else
				{
					pkCountries.ItemsSource = null;
					PopUpCities.IsVisible = false;
					//lstZip.ItemsSource = null;
				}

			}
			else
			{
				pkCountries.ItemsSource = null;
				PopUpCities.IsVisible = false;
				//lstZip.ItemsSource = null;
				txtZip.IsVisible = false;
				//PopUpZip.IsVisible = false;
			}
			if (isCheck)
			{
				pkCountries.ItemsSource = null;
				PopUpCities.IsVisible = false;
			}

			txtAddress.Text = PopUpCitiesText.Text;
		}



		UpdateProfileModal vm;
		Predictions predictions;
		bool isDOBSelected = false;
		string cityName = "";
		CCode code;
		List<CCode> cCodes;
		List<CCode> cCodes1;
		List<string> pCodes;

		public SignUp_Skippable()
		{
			InitializeComponent();
			Fanspick.Constants.raiseAbove = 0;
			if (CacheStorage.ViewModels.FacebookViewModel != null)
			{
				FacebookViewModel vm;
				vm = CacheStorage.ViewModels.FacebookViewModel;
				FirstName.Text = vm.FacebookProfile.FirstName;
				LastName.Text = vm.FacebookProfile.LastName;
				//txtCountry.Text = vm.FacebookProfile.Locale;
				BindingContext = vm;
			}
			else
			{

				vm = CacheStorage.ViewModels.UpdateProfile;

				BindingContext = vm;
				FirstName.Text = vm.requestUpdateProfileDTO.FirstName;
				LastName.Text = vm.requestUpdateProfileDTO.LastName;
				txtAddress.Text = vm.requestUpdateProfileDTO.Address;
			}
			predictions = null;
			code = null;


			isCheck = false;
			pkCountries.ItemsSource = null;

			//lstZip.ItemsSource = null;
			BindingContext = vm;

			DateOfBirth.MaximumDate = DateTime.Now;
			DateOfBirth.MinimumDate = DateTime.Now.AddYears(-100);
			DateOfBirth.Date = DateTime.Now.AddYears(-18);
		}
		private void GetCountriesCode()
		{
			cCodes = DependencyService.Get<ICountryCodes>().DisplayCountryCodes("").Distinct().ToList();
			lstCountriesP.ItemsSource = cCodes;

		}
		private async Task<Predictions> GetCitiesListAsync()
		{

			try
			{

				predictions = await DependencyService.Get<IPlacesAutocomplete>().GetAutocomplete(txtAddress.Text, code.Code.ToLower());
				return predictions;

			}
			catch (Exception e)
			{
				return null;
			}

		}
		protected override void OnAppearing()
		{
			base.OnAppearing();


			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
			if (cCodes == null)
			{

				GetCountriesCode();

			}
		}
		private void GetCountriesCodeAuto()
		{
			if (txtCountry.Text != "")
			{
				cCodes1 = DependencyService.Get<ICountryCodes>().DisplayCountryCodes(txtCountry.Text).Distinct().ToList();
				lstCountries.ItemsSource = cCodes1;
				PopUpCountries.IsVisible = true;
			}
			else
			{
				cCodes1 = null;
				txtAddress.IsVisible = false;
				txtZip.IsVisible = false;
				lstCountries.ItemsSource = null;
				PopUpCountries.IsVisible = false;

			}

		}
		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();

			SlidingPanelConfig config = new SlidingPanelConfig();
			config.MainView = GetMainStackLayout();
			config.TitleBackgroundColor = Color.Green;

			spTest.ApplyConfig(config);

			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}
		protected void FirstName_Completed(object sender, EventArgs e)
		{
			LastName.Focus();
		}

		protected void LastName_Completed(object sender, EventArgs e)
		{
			txtAddress.Focus();
		}

		private void OnTapGestureRecognizerTapped(object sender, EventArgs e)
		{
			Application.Current.MainPage = new SignUpDefaultTeam();
		}

		void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
		{

			DOB.IsVisible = false;
			StackDob.IsVisible = true;
			isDOBSelected = true;


		}

		void Handle_Focused_Dob(object sender, Xamarin.Forms.FocusEventArgs e)
		{

		}

		public void ShowAlert(String message)
		{
			DisplayAlert("Fanspick", message, "ok");
		}

		private async void onclickSetFav(object sender, EventArgs e)
		{

			string phoneNo = Phone.Text;
			string MatchPhoneNumberPattern = "^[0-9]*$";
			if (FirstName.Text == "" || FirstName.Text == null)
			{
				ShowAlert("Please enter your first name to proceed");
			}
			else if (LastName.Text == "" || LastName.Text == null)
			{
				ShowAlert("Please enter your last name to proceed");
			}
			else if (!isDOBSelected)
			{
				ShowAlert("Please enter your date of birth to proceed");
			}
			else if (txtAddress.Text == "" || txtAddress.Text == null)
			{
				ShowAlert("Fanspick needs to know your current location. Please enter your address or choose from suggestions to proceed.");
			}

			else if (Phone.Text != null)
			{
				if (Phone.Text == "")
				{
					HitAPI(phoneNo);
				}
				else if (phoneNo.Length < 10 || phoneNo.Length > 10)
				{
					ShowAlert("Phone number should be of 10 characters");
				}
				else if (Code.Text == null)
				{
					ShowAlert("Please select a Country Code");
				}
				else if (Code.Text == "")
				{
					ShowAlert("Please select a Country Code");
				}
                else if (!FanspickCache.isphoneValid)
				{

                    var verify = new OTPpopup(Code.Text + Phone.Text);
					await Navigation.PushPopupAsync(verify);

				}
				else
				{
					HitAPI(phoneNo);
				}

			}



			else
			{

				HitAPI(phoneNo);

			}


		}

		public async void HitAPI(String phoneNo)
		{

			try
			{

				if (Phone.Text == null)
				{
					Phone.Text = "";
				}
				if (Code.Text != null && Phone.Text == "")
				{
					Code.Text = "";
				}

				phoneNo = Code.Text + Phone.Text;
				//   phoneNo = phoneNo.Replace("+", "");
				vm.ShowLoader();
				string sendNumber = "";

				if (Phone.Text == "" || Phone.Text == null)
				{
					sendNumber = "";
				}
				else
				{
                    sendNumber = phoneNo;
				}

				if (txtZip.Text == null)
				{
					txtZip.Text = "";
				}


				vm.requestUpdateProfileDTO.FirstName = FirstName.Text;
				vm.requestUpdateProfileDTO.LastName = LastName.Text;
				vm.requestUpdateProfileDTO.DateOfBirth = DateOfBirth.ToString();
				vm.requestUpdateProfileDTO.Address = txtCountry.Text;
				vm.requestUpdateProfileDTO.Zip = txtZip.Text;
				vm.requestUpdateProfileDTO.City = txtAddress.Text;
				vm.requestUpdateProfileDTO.PhoneNumber = sendNumber;
				vm.requestUpdateProfileDTO.Latitude = "0";
				vm.requestUpdateProfileDTO.Longitude = "0";
				vm.requestUpdateProfileDTO.Photo = "null";


				Dictionary<string, string> dict = new Dictionary<string, string>();

				if (FirstName.Text != null || FirstName.Text != "")
				{
					dict.Add("firstname", FirstName.Text);
				}
				if (LastName.Text != null || LastName.Text != "")
				{
					dict.Add("lastname", LastName.Text);
				}
				if (vm.requestUpdateProfileDTO.DateOfBirth != null || vm.requestUpdateProfileDTO.DateOfBirth != "")
				{
					dict.Add("dob", DateOfBirth.Date.ToString("yyyy-MM-dd"));
				}

				if (vm.requestUpdateProfileDTO.Address != null || vm.requestUpdateProfileDTO.Address != "")
				{
					dict.Add("address", vm.requestUpdateProfileDTO.Address);
				}

				if (vm.requestUpdateProfileDTO.Zip != null)
				{
					if (vm.requestUpdateProfileDTO.Zip != "")
					{
						dict.Add("zipcode", vm.requestUpdateProfileDTO.Zip);
					}

				}

				if (vm.requestUpdateProfileDTO.City != null || vm.requestUpdateProfileDTO.City != "")
				{
					dict.Add("city", vm.requestUpdateProfileDTO.City);
				}

				if (vm.requestUpdateProfileDTO.PhoneNumber != null)
				{
					if (vm.requestUpdateProfileDTO.PhoneNumber != "")
					{
						dict.Add("phoneNumber", vm.requestUpdateProfileDTO.PhoneNumber);
					}

				}

				if (vm.requestUpdateProfileDTO.Latitude != null || vm.requestUpdateProfileDTO.Latitude != "")
				{
					dict.Add("lat", vm.requestUpdateProfileDTO.Latitude);
				}

				if (vm.requestUpdateProfileDTO.Longitude != null || vm.requestUpdateProfileDTO.Longitude != "")
				{
					dict.Add("lon", vm.requestUpdateProfileDTO.Longitude);
				}



				Debug.WriteLine(dict);

				Shared.Models.Service.ServiceResponse response = await vm.setUpdateProfile(dict);
				if (response.OK)
				{
					vm.HideLoader();
					Application.Current.MainPage = new SignUpDefaultTeam();

				}
				else
				{
					if (response.ReasonPhrase.Contains("firstname")) { response.ReasonPhrase.Replace("firstname", "Firstname"); }
					if (response.ReasonPhrase.Contains("lastname")) { response.ReasonPhrase.Replace("last", "Lastname"); }
					await DisplayAlert(FanspickStrings.GenericDialogTitle, response.ReasonPhrase.ToString(), FanspickStrings.Alert_OK);
					vm.HideLoader();
				}

			}
			catch (Exception ex)
			{
				await DisplayAlert(FanspickStrings.GenericDialogTitle, "There seems to be an error in your connection. Please try again later", FanspickStrings.Alert_OK);
			}
		}

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{

				TotalRow1.Height = new GridLength(250);
				TotalRow2.Height = new GridLength(42);
				TotalRow3.Height = new GridLength(42);
				TotalRow4.Height = new GridLength(42);
				TotalRow5.Height = new GridLength(42);
				TotalRow7.Height = new GridLength(0);
				TotalRow8.Height = new GridLength(55);
				TotalRow9.Height = new GridLength(42);

				DateOfBirth.HeightRequest = 42;

				BoxDivision1.Width = new GridLength(20, GridUnitType.Star);
				BoxDivision2.Width = new GridLength(60, GridUnitType.Star);
				BoxDivision3.Width = new GridLength(20, GridUnitType.Star);

				skipable_Image.Margin = new Thickness(20);
			}
			else
			{
                TotalRow1.Height = new GridLength(170);
				TotalRow2.Height = new GridLength(32);
				TotalRow3.Height = new GridLength(32);
				TotalRow4.Height = new GridLength(32);
				TotalRow5.Height = new GridLength(32);
				TotalRow7.Height = new GridLength(0);
				TotalRow8.Height = new GridLength(40);
				TotalRow9.Height = new GridLength(40);

				BoxDivision1.Width = new GridLength(10, GridUnitType.Star);
				BoxDivision2.Width = new GridLength(80, GridUnitType.Star);
				BoxDivision3.Width = new GridLength(10, GridUnitType.Star);
                DateOfBirth.HeightRequest = 32;
                skipable_Image.Margin = new Thickness(10);

			}

		}

	}
}
