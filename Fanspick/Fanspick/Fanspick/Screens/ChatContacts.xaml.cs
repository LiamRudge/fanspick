﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DK.SlidingPanel.Interface;
using Fanspick.Helper;
using Fanspick.PopUp;
using Fanspick.RestAPI;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Plugin.DeviceInfo;
using Quobject.SocketIoClientDotNet.Client;
using Refit;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Fanspick.Shared.Models.DTO.RecentChatContactDTO;

namespace Fanspick.Screens
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChatContacts : ContentPage
	{
		OnetoOneChatModal vm;
		List<ContactModal> contacts;
        Boolean isSearchSelected;
		Socket socket;
		public ChatContacts()
		{

			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			vm = CacheStorage.ViewModels.OnetoOneChat;

			BindingContext = vm;

			if (vm.PhoneContactsForGroup != null)
			{
				vm.PhoneContactsForGroup.Clear();
			}

			if (vm.RegisteredContacts != null)
			{
				vm.RegisteredContacts.Clear();
			}

			contacts = new List<ContactModal>();

			if (contacts.Count != 0)
			{
				contacts.Clear();
			}

			contacts = DependencyService.Get<IPhoneService>().GetContacts();
			vm.PhoneContacts = contacts;

            try
            {

                socket = IO.Socket(CreateUri());
                socket.Connect();

                var minutes = TimeSpan.FromSeconds(30);
                Device.StartTimer(minutes, () =>
                {
                    JObject checkstatus = new JObject();

                    if (FanspickCache.UserData != null)
                    {
                        checkstatus.Add("userId", FanspickCache.UserData.AccessToken);
                        socket.Emit("userStatus", checkstatus);
                    }

                    return true;
                });


                socket.On(Socket.EVENT_CONNECT, (data) =>
              {

                  JObject sendUser = new JObject();
                  sendUser.Add("userId", FanspickCache.UserData.AccessToken);
                  socket.Emit("add user", sendUser);

                  JObject addContacts = new JObject();
                  addContacts.Add("userId", FanspickCache.UserData.AccessToken);

                  JArray arr = new JArray();

                  for (int i = 0; i < vm.PhoneContacts.Count; i++)
                  {

                      JObject contact_array = new JObject();
                      contact_array.Add("name", vm.PhoneContacts[i].Name);
                      contact_array.Add("contact", vm.PhoneContacts[i].PhoneNumber.ToString().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", ""));

                      arr.Add(contact_array);
                  //arr.Add(vm.PhoneContacts[i].ToString().Replace("[()\\\\s-]+", ""));

              }
                  addContacts.Add("contacts", arr);

                  socket.Emit("add contacts", addContacts);

              });

                socket.On(Socket.EVENT_DISCONNECT, (data) =>
               {

               });



                socket.On(Socket.EVENT_ERROR, (data) =>
               {

               });


                socket.On("messageFromServer", (data) =>
               {

               });

                socket.On("userStatusReply", (data) =>
               {

               });


                socket.On("messageReplyAddContacts", (data) =>
               {
                   GetResgiteredCont();

               });

                socket.On("newMessage", (data) =>
             {

                 GetChatList();

                 MessageFromSocketUser dto = Newtonsoft.Json.JsonConvert.DeserializeObject<MessageFromSocketUser>(data.ToString());

                 ChatMessages getMessage = new ChatMessages();
                 SenderDetails userid = new SenderDetails();

                 userid.SenderUsername = "OtherUser";
                 userid.Sender_id = dto.MsgSocket.userID;
                 userid.SenderPhoneNumber = "OtherUser";

             //getMessage.Id = "0";
             getMessage.Message = dto.MsgSocket.message;
                 getMessage.CurrentUserId = FanspickCache.UserData.Id;
                 getMessage.Time = dto.MsgSocket.Time;
                 getMessage.Sender = userid;

                 if (CrossConnectivity.Current.IsConnected)
                 {
                     getMessage.isConnect = true;
                 }
                 else
                 {
                     getMessage.isConnect = false;
                 }

                 vm.FriendsChat.Add(getMessage);
                 FanspickCache.isNewMessageRecived = true;

             //read message

             //   { userId: accessToken, groupId: stringId, messageIds: array of strings}

             JObject sendRead = new JObject();
                 sendRead.Add("userId", FanspickCache.UserData.AccessToken);
                 sendRead.Add("groupId", FanspickCache.SelectedGroupId);


                 JArray arr = new JArray();
                 for (int i = 0; i < 1; i++)
                 {
                     arr.Add(dto.MsgSocket.messageId);
                 }
                 sendRead.Add("messageIds", arr);

             //messageId

             socket.Emit("read message", sendRead);


             });




                socket.On("upateMessageStatusResponse", (data) =>
       {
       });


                socket.On("addedInGroup", (data) =>
            {

                MessageFromSocket dto = Newtonsoft.Json.JsonConvert.DeserializeObject<MessageFromSocket>(data.ToString());

                DateTime current = DateTime.Now;
                DateTime convertedFixtureDate = TimeZoneInfo.ConvertTime(current, TimeZoneInfo.Utc);
                String nowTime = convertedFixtureDate.ToString("yyyy-MM-dd HH:MM");
                String utcTime = current.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");

                RecentChatToShow cont = new RecentChatToShow();
                cont.lastActivatedTime = utcTime;
                cont.ChatGroupID = dto.MsgSocket._id;
                cont.IsDeleted = dto.MsgSocket.isDeleted;
                cont.Message = dto.MsgSocket.MessageShow;
                cont.GroupName = dto.MsgSocket.Name;
                cont.Type = dto.MsgSocket.Type;
                cont.Image = "sample_player_image";
                cont._idChat = "0";


                vm.RecentChat.Insert(0, cont);

            });

            }
            catch(Exception e){
                Debug.WriteLine(e.ToString());
            }


		}


		public class MessageFromSocket
		{
			[JsonProperty(PropertyName = "message")]
			public Message MsgSocket { get; set; }

		}

		public class Message
		{
			[JsonProperty(PropertyName = "name")]
			public string Name { get; set; }

			[JsonProperty(PropertyName = "type")]
			public string Type { get; set; }

			[JsonProperty(PropertyName = "message")]
			public string MessageShow { get; set; }

			[JsonProperty(PropertyName = "_id")]
			public string _id { get; set; }

			[JsonProperty(PropertyName = "isDeleted")]
			public bool isDeleted { get; set; }



		}

		public class MessageFromSocketUser
		{
			[JsonProperty(PropertyName = "message")]
			public MessageUser MsgSocket { get; set; }

		}

		public class MessageUser
		{
			[JsonProperty(PropertyName = "msg")]
			public string message { get; set; }

			[JsonProperty(PropertyName = "time")]
			public string Time { get; set; }

			[JsonProperty(PropertyName = "messageId")]
			public string messageId { get; set; }

			[JsonProperty(PropertyName = "userId")]
			public string userID { get; set; }


		}


		void Handle_ItemTapped(object sender, EventArgs e)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)e;

			RecentChatToShow selectedContact = (RecentChatToShow)eventArgs.Parameter;

			FanspickCache.chatPersonDetails = selectedContact;

			FanspickCache.SelectedGroupId = selectedContact.ChatGroupID;

		
			Navigation.PushAsync(new Screens.ChatOneToOne(), true);
		}

		private async void ShareImage_OnTapped(object sender, EventArgs e)
		{

			TappedEventArgs eventArgs = (TappedEventArgs)e;

			Contacts selectedContact = (Contacts)eventArgs.Parameter;


			if (selectedContact.IsRegistered)
			{
				FanspickCache.SelectedGroupId = selectedContact.GroupId;
                FanspickCache.SelectedImage = selectedContact.UserPhoto;
				FanspickCache.SelectedName = selectedContact.Name;

				await Navigation.PushAsync(new Screens.ChatOneToOne(), true);

			}
			else
			{
				await Plugin.Share.CrossShare.Current.Share(new Plugin.Share.Abstractions.ShareMessage
				{

					Url = "https://itunes.apple.com/us/app/fanspick/id1061898805?mt=8",
					Title = "Fanspick",
					Text = "Get this app to have your say on fanspick"
				}, new Plugin.Share.Abstractions.ShareOptions { ChooserTitle = "Fanspick" });
			}

		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();

			SlidingPanelConfig config = new SlidingPanelConfig();
			config.MainView = GetMainStackLayout();
			config.TitleBackgroundColor = Color.Green;

			spTest.ApplyConfig(config);

			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}

		private StackLayout GetMainStackLayout()
		{
			StackLayout mainStackLayout = new StackLayout();
			mainStackLayout.VerticalOptions = LayoutOptions.FillAndExpand;
			mainStackLayout.HorizontalOptions = LayoutOptions.FillAndExpand;

			return (mainStackLayout);
		}

        private void SearchChat(object sender, EventArgs e)
		{
            if(isSearchSelected){
                isSearchSelected = false;
                entrySearch.IsVisible = false;
				icon_search.Source = "search";

            }else{
                isSearchSelected = true;
				entrySearch.IsVisible = true;
				icon_search.Source = "close_button";

            }


		}

		private void OnTapGestureCommentry(object sender, EventArgs e)
		{
			spTest.ShowExpandedPanel();
		}

		private void OnClosePanel(object sender, EventArgs e)
		{
			spTest.HidePanel();
		}


		protected override void OnAppearing()
		{
			base.OnAppearing();

			contacts.AddRange(DependencyService.Get<IPhoneService>().GetContacts());
			vm.PhoneContacts = contacts;
			ClickRecent(this, EventArgs.Empty);

			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);

            if(FanspickCache.UserData.PhoneNumber == null){
				var _number = new AddPhoneNumber();
				Navigation.PushPopupAsync(_number);
            }

			GetChatList();


		}

		async void OnContactSelected(object sender, EventArgs e)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)e;
			ContactsRegistered contactSelected = (ContactsRegistered)eventArgs.Parameter;

			if (vm.PhoneContactsForGroup.Count != 0)
			{
				Boolean isanyAvailable = false;
				for (int i = 0; i < vm.PhoneContactsForGroup.Count; i++)
				{
					if (vm.PhoneContactsForGroup[i].ContactNo == contactSelected.ContactNo)
					{
						isanyAvailable = true;
						break;
					}

				}

				if (isanyAvailable)
				{
					await DisplayAlert("Contact Already added", "The contact has already been added for your group. Please select other contact", "ok");
				}
				else
				{
					vm.PhoneContactsForGroup.Add(contactSelected);
				}

			}
			else
			{
				vm.PhoneContactsForGroup.Add(contactSelected);
			}

		}


		void NewGroupClick(object sender, EventArgs e)
		{
			if (vm.PhoneContactsForGroup.Count == 0)
			{
				DisplayAlert("No Contact Found", "Please select a contact to add in the group", "ok");
			}
			else
			{
				JObject addGroup = new JObject();
				addGroup.Add("userId", FanspickCache.UserData.AccessToken);
				addGroup.Add("name", txtNewgroup.Text.ToString());

				//  List<string> arr = new List<string>();
				JArray arr = new JArray();
				for (int i = 0; i < vm.PhoneContactsForGroup.Count; i++)
				{
					arr.Add(vm.PhoneContactsForGroup[i].ContactId);
				}
				addGroup.Add("members", arr);

				socket.Emit("create group", addGroup);

				if (vm.PhoneContactsForGroup.Count != 0)
				{
					vm.PhoneContactsForGroup.Clear();
				}

				txtNewgroup.Text = "New Group";

				OnClosePanel(this, EventArgs.Empty);

			}

		}


		async void ContactRemoveFromGroup(object sender, EventArgs e)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)e;
			ContactsRegistered contactSelected = (ContactsRegistered)eventArgs.Parameter;
			vm.PhoneContactsForGroup.Remove(contactSelected);

		}


		void ClickRecent(object sender, EventArgs e)
		{
			txtContacts.TextColor = Color.FromHex("#000000");
			txtContacts.BackgroundColor = Color.FromHex("#c7c8ca");
			txtRecent.TextColor = Color.FromHex("#ffffff");
			txtRecent.BackgroundColor = Color.FromHex("#6a778c");
			lvRecent.IsVisible = true;
			lvContacts.IsVisible = false;

		}

		void ClickContacts(object sender, EventArgs e)
		{
			txtRecent.TextColor = Color.FromHex("#000000");
			txtRecent.BackgroundColor = Color.FromHex("#c7c8ca");
			txtContacts.TextColor = Color.FromHex("#ffffff");
			txtContacts.BackgroundColor = Color.FromHex("#6a778c");
			lvRecent.IsVisible = false;
			lvContacts.IsVisible = true;

		}



		public void OnDelete(object sender, EventArgs e)
		{
			var mi = ((MenuItem)sender);

			RecentChatToShow contactSelected = (RecentChatToShow)mi.CommandParameter;

			DeleteMsg(contactSelected);

		}

		public async void DeleteMsg(RecentChatToShow contSelected)
		{


			var answer = await DisplayAlert("Delete Chat", "Are you sure you want to delete chat ?" + contSelected.PhoneNumber, "Delete", "Cancel");

			if (answer)
			{

				try
				{

					var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
					RequestDeleteMessageDTO getTeamDataRequest = new RequestDeleteMessageDTO();

					getTeamDataRequest.GroupId = contSelected.ChatGroupID;
					getTeamDataRequest.Type = "All";

					var header = "Bearer " + FanspickCache.UserData.AccessToken;
					var response = await gitHubApi.DeleteChatsForGroup(getTeamDataRequest, header);

					if (response.StatusCode == 200)
					{
						vm.RecentChat.Remove(contSelected);
					}
					else
					{

                    }

				}
				catch (ApiException e)
				{

				}

				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
				}

			}

		}

		async void GetChatList()
		{
            try
            {
                
                ServiceResponse response = await vm.GetChatList();
                if (response.OK)
                {
                    ResponseRecentChatContactDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseRecentChatContactDTO>(response.Data);
                    ObservableCollection<RecentChatToShow> recentChatToshow = new ObservableCollection<RecentChatToShow>();

                    for (int i = 0; i < dto.Data.Recentchat.Count; i++)
                    {
                        RecentChatToShow cont = new RecentChatToShow();
                        cont.lastActivatedTime = dto.Data.Recentchat[i].lastActivatedTime;
                        cont.ChatGroupID = dto.Data.Recentchat[i]._idChat;
                        cont.IsDeleted = dto.Data.Recentchat[i].IsDeleted;
                        cont.Message = dto.Data.Recentchat[i].chat.Message;
                        cont._idChat = dto.Data.Recentchat[i].chat.Sender;
                        cont.GroupName = dto.Data.Recentchat[i].GroupName;
                        cont.Type = dto.Data.Recentchat[i].Type;

                      //  cont.Image = dto.Data.Recentchat[i]

                        for (int j = 0; j < dto.Data.Recentchat[i].GroupMembers.Count; j++)
                        {
                            if (FanspickCache.UserData.Id != dto.Data.Recentchat[i].GroupMembers[j].memberDetails._id)
                            {
                                cont._idUser = dto.Data.Recentchat[i].GroupMembers[j].memberDetails._id;
                                cont.PhoneNumber = dto.Data.Recentchat[i].GroupMembers[j].memberDetails.PhoneNumber;
                                cont.Firstname = checkPhoneServerToContacts(cont.PhoneNumber);
                                cont.Image = dto.Data.Recentchat[i].GroupMembers[j].memberDetails.UserPhoto;

                               
                            }
                        }
						if (cont.Image == null)
						{
							cont.Image = "sample_player_image";
						}
						else if (cont.Image == "null")
						{
							cont.Image = "sample_player_image";
						}

                        if(cont.Type =="oneToMany"){
                            cont.Image = "sample_player_image";
                        }


						recentChatToshow.Add(cont);
                    }
                    vm.RecentChat = recentChatToshow;

                }
                else
                {

                }
            }
            catch(Exception e){
                Debug.WriteLine(e.ToString());
            }

		}




		async void GetResgiteredCont()
		{
			ServiceResponse response = await vm.GetRegisteredContact();
			if (response.OK)
			{
                if(vm.RegisteredContacts !=null){
                vm.RegisteredContacts.Clear();    
                }

				ResponseRegisteredContactsDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseRegisteredContactsDTO>(response.Data);
				ObservableCollection<Contacts> all = new ObservableCollection<Contacts>();

				for (int i = 0; i < dto.Data.RegisteredContacts.Count; i++)
				{
					Contacts cont = new Contacts();
					cont.Status = dto.Data.RegisteredContacts[i].Status;
					cont.GroupId = dto.Data.RegisteredContacts[i].GroupId;
					cont.Name = dto.Data.RegisteredContacts[i].Name;
					cont.ContactNo = dto.Data.RegisteredContacts[i].ContactNo;
					cont.ContactId = dto.Data.RegisteredContacts[i].ContactId;
					cont.IsRegistered = dto.Data.RegisteredContacts[i].isRegistered;
                    cont.PhotoLink = dto.Data.RegisteredContacts[i].PhotoLink;


					Debug.WriteLine(checkPhoneServerToContacts(cont.ContactNo));

					all.Add(cont);

				}

				if (vm.RegisteredContacts != null && vm.RegisteredContacts.Count != 0)
				{
					vm.RegisteredContacts.Clear();
				}

				for (int i = 0; i < dto.Data.NonRegisteredContacts.Count; i++)
				{


					Contacts cont = new Contacts();
					cont.Status = dto.Data.NonRegisteredContacts[i].Status;
					cont.GroupId = dto.Data.NonRegisteredContacts[i].GroupId;
					cont.Name = dto.Data.NonRegisteredContacts[i].Name;
					cont.ContactNo = dto.Data.NonRegisteredContacts[i].ContactNo;
					cont.ContactId = dto.Data.NonRegisteredContacts[i].ContactId;
					cont.IsRegistered = dto.Data.NonRegisteredContacts[i].isRegistered;
                    cont.PhotoLink = dto.Data.NonRegisteredContacts[i].PhotoLink;

					all.Add(cont);

				}

				vm.OverallContacts = all;


				foreach (ContactsRegistered sf in dto.Data.RegisteredContacts)
				{
					vm.RegisteredContacts.Add(sf);
				}


				GetChatList();

			}
			else
			{

			}
		}


		public string CreateUri()
		{
			var options = CreateOptions();
			var uri = string.Format("{0}://{1}:{2}", options.Secure ? "https" : "http", options.Hostname, options.Port);
			return uri;
		}


		public IO.Options CreateOptions()
		{
			var config = ConfigBase.Load();
			var options = new IO.Options();
			options.Port = config.server.port;
			options.Hostname = config.server.hostname;
			options.ForceNew = true;

			return options;
		}


		public class ConfigBase
		{
			public ConfigServer server { get; set; }

			public static ConfigBase Load()
			{
				var result = new ConfigBase()
				{
					server = new ConfigServer()
				};
				result.server.hostname = "52.163.48.178";
				result.server.port = 8000;

				return result;
			}
		}


		// fplive.ukwest.cloudapp.azure.com
		// 52.163.48.178

		public class ConfigServer
		{
			public string hostname { get; set; }
			public int port { get; set; }
		}

	

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{
				NavigationBox1.Height = new GridLength(6, GridUnitType.Star);
				NavigationBox2.Height = new GridLength(94, GridUnitType.Star);

				txtContacts.FontSize = 17;
				txtRecent.FontSize = 17;
				txtNewgroup.FontSize = 14;

				icon_add.WidthRequest = 45;
				icon_add.HeightRequest = 30;

                icon_search.WidthRequest = 45;
                icon_search.HeightRequest = 30;

				txtNewgroup.WidthRequest = 450;

				NewgroupSend.WidthRequest = 40;
				NewgroupSend.HeightRequest = 20;

			}
			else
			{
				NavigationBox1.Height = new GridLength(8, GridUnitType.Star);
				NavigationBox2.Height = new GridLength(92, GridUnitType.Star);

				txtContacts.FontSize = 14;
				txtRecent.FontSize = 14;
				txtNewgroup.FontSize = 10;

				icon_add.WidthRequest = 30;
				icon_add.HeightRequest = 15;

				icon_search.WidthRequest = 30;
				icon_search.HeightRequest = 15;

				txtNewgroup.WidthRequest = 150;
				NewgroupSend.WidthRequest = 25;
				NewgroupSend.HeightRequest = 15;

			}


		}

		public void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
		{
			RcChatList.BeginRefresh();

			if (string.IsNullOrWhiteSpace(e.NewTextValue))
			{
                RcChatList.ItemsSource = vm.RecentChat;
			}
			else
			{
                RcChatList.ItemsSource = vm.RecentChat.Where(i => i.Firstname.Contains(e.NewTextValue) || i.GroupName.Contains(e.NewTextValue));
			}

			RcChatList.EndRefresh();
		}

		public string checkPhoneServerToContacts(String phonenumber)
		{

			String name = phonenumber;
			if (vm.RegisteredContacts != null && vm.RegisteredContacts.Count != 0)
			{

				for (int i = 0; i < vm.RegisteredContacts.Count; i++)
				{
					if (vm.RegisteredContacts[i].ContactNo.Contains(phonenumber))
					{
						name = vm.RegisteredContacts[i].Name;
						break;

					}
				}

				return name;

			}
			else
			{

				return name;
			}



		}

	}
}