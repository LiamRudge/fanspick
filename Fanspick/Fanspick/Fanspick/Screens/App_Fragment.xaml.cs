﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.AppModels;
using Fanspick.Shared.Models.FanspickAPI.ResponseModels.YourPick;
using Xamarin.Forms;
using Fanspick.ViewModels;
using Fanspick.Shared.Helper;

namespace Fanspick.Screens
{
    public partial class App_Fragment : App_BaseLayout
    {
        App_FragmentViewModel vm;
        public App_Fragment()
        {
            InitializeComponent();

            if (vm == null)
            {
                vm = new App_FragmentViewModel();
                BindingContext = vm;
            }
            vm.HideLoader();            
            Position = 0;

            TemplateSelector = new PluginModels.TemplateSelector_Pitches();

            // responsible for collecting and caching new data from feed, and updating the properties bound to UI.
            RoutineClock(null);
        }

        
        private void RoutineClock(TimeSpan? WaitFor)
        {
            Device.StartTimer(WaitFor.HasValue ? WaitFor.Value : TimeSpan.FromSeconds(1), () =>
              {
                  Task.Factory.StartNew(async () =>
                  {
                      if (!WaitFor.HasValue)
                      {
                          vm.ShowLoader();

                          await RoutineLogic();
                          vm.HideLoader();
                          vm.MakeFixtureListActive();
                      }
                      else
                      {
                          //await RoutineLogic();
                      }
                      Device.BeginInvokeOnMainThread(() =>
                        {
                            RoutineClock(TimeSpan.FromMinutes(1));
                        });
                  });

                  // Don't repeat the timer (we will start a new timer when the request is finished)
                  return false;
              });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        private async Task<Boolean> RoutineLogic()
        {
            await FanspickEventLoop();

            return true;
        }

        private async Task FanspickEventLoop()
        {
            await vm.DownloadResources();
        }

        #region event handlers
        private void btnFixtures_Clicked(object sender, EventArgs e)
        {
            vm.SelectedFixture = vm.NextFixture;
            vm.MakePitchActive();
        }

        private void LvFixtures_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            vm.SelectedFixture = lvFixtures.SelectedItem as Fanspick.Shared.Models.Fixture;
            vm.MakePitchActive();
        }

        #endregion 

        #region Properties

        public int Position { get; set; }

        public PluginModels.TemplateSelector_Pitches TemplateSelector { get; set; }

       
        #endregion

    }
}
