﻿﻿using Fanspick.Helper;
using Fanspick.Shared.Helper;
using Fanspick.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Fanspick.Shared.Events.Events;
using Xamarin.Forms.Internals;
using Refit;
using Fanspick.Shared.Models.DTO;
using Fanspick.RestAPI;
using Fanspick.Shared.Services;
using Fanspick.Shared.Models.Service;
using System.Diagnostics;

namespace Fanspick.Screens
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : TabbedPage
	{
		public static HomePageViewModel homePageVM;
		public HomePage()
		{
			InitializeComponent();
			homePageVM = CacheStorage.ViewModels.HomePage;
			BindingContext = homePageVM;
			NavigationPage.SetHasNavigationBar(this, false);

            this.CurrentPageChanged += HomePage_CurrentPageChanged;
            GetData();

 
		}

        public void GetData()
		{
			GetPlayerShirt();

		}

        public void HomePage_CurrentPageChanged(object sender, EventArgs e)
		{
			int pageIdx = this.Children.IndexOf(this.CurrentPage);
            FanspickCache.PageIndex = pageIdx;

			if (pageIdx == (int)PageIndex.Social)
			{
				homePageVM.RefreshSocialData();
			}
		}

		private enum PageIndex
		{
			Social = 3
		}



		public async void GetPlayerShirt()
		{
			try
			{

				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

				RequestGetTeamDataRefitDTO getTeamDataRequest = new RequestGetTeamDataRefitDTO();
				getTeamDataRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
				var header = "Bearer " + FanspickCache.UserData.AccessToken;
				var response = await gitHubApi.GetTeamData(getTeamDataRequest, header);

				if (response.StatusCode == 200)
				{
					if (response.Data == null || response == null)
					{
						FanspickCache.CurrentFanspickScope.TeamShirtImage = "no_shirt";
					}
					else
					{
						if (response.Data.TeamShirtURL == null)
						{
							FanspickCache.CurrentFanspickScope.TeamShirtImage = "no_shirt";
						}
						else
						{
							FanspickCache.CurrentFanspickScope.TeamShirtImage = "http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamShirts/" + response.Data.TeamShirtURL.Replace(" ", "%20") + ".png";
						}
					}

				}
				else
				{

				}

			}
			catch (Exception e)
			{
                Debug.WriteLine(e.ToString());
			}

		}

		

	}
}