﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Fanspick.Helper;
using Fanspick.RestAPI;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Quobject.SocketIoClientDotNet.Client;
using Refit;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fanspick.Screens
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChatOneToOne : ContentPage
	{
		Socket socket;
		OnetoOneChatModal vm;
        ChatMessages sentMSGcheck;
		string senderID, reciverID;
		public ChatOneToOne()
		{
			InitializeComponent();
            vm = CacheStorage.ViewModels.OnetoOneChat;
			BindingContext = vm;
			Fanspick.Constants.raiseAbove = 0;
			NavigationPage.SetHasNavigationBar(this, false);

            sentMSGcheck = new ChatMessages();

			socket = IO.Socket(CreateUri());
			socket.Connect();

            try
            {



                socket.On(Socket.EVENT_CONNECT, (data) =>
                {

                    JObject o = new JObject();
                    o.Add("userId", FanspickCache.UserData.AccessToken);
                    o.Add("groupId", FanspickCache.SelectedGroupId);
                    socket.Emit("register group", o);



                });

                socket.On(Socket.EVENT_DISCONNECT, (data) =>
               {
               });

                socket.On(Socket.EVENT_MESSAGE, (data) =>
               {

               });

                socket.On("messageFromServer", (data) =>
               {

               });

                socket.On("msgDelivered", (data) =>
              {

              });

                socket.On("updatedGroupInfo", (data) =>
                {
                });

                socket.On("messageReplyRegisterGroup", (data) =>
                {

                });

                socket.On("messageDelivered", (data) =>
                {
                    ChatMessages updatemsg = new ChatMessages();
                    updatemsg = sentMSGcheck;
                    updatemsg.imageType = 2;
                    vm.FriendsChat[vm.FriendsChat.IndexOf(sentMSGcheck)] = updatemsg;

                });





                UpdateTaskStatus();

                socket.On("receive message", (data) =>
              {

                  MessageFromSocket dto = Newtonsoft.Json.JsonConvert.DeserializeObject<MessageFromSocket>(data.ToString());

                  ChatMessages getMessage = new ChatMessages();
                  SenderDetails userid = new SenderDetails();

                  userid.SenderUsername = "OtherUser";
                  userid.Sender_id = dto.MsgSocket.userID;
                  userid.SenderPhoneNumber = "OtherUser";

              //getMessage.Id = "0";
              getMessage.Message = dto.MsgSocket.message;
                  getMessage.CurrentUserId = FanspickCache.UserData.Id;
                  getMessage.Time = dto.MsgSocket.Time; ;
                  getMessage.Sender = userid;

                  if (CrossConnectivity.Current.IsConnected)
                  {
                      getMessage.isConnect = true;
                  }
                  else
                  {
                      getMessage.isConnect = false;
                  }

                  vm.FriendsChat.Add(getMessage);
                  ScrollToLast();

              });



                socket.On("upateMessageStatusResponse", (data) =>
              {


              });

            }
            catch(Exception e){
                Debug.WriteLine(e.ToString());
            }


            Device.StartTimer(TimeSpan.FromSeconds(1), () =>

            {
                Task.Run(() =>
                {
                    if(FanspickCache.isNewMessageRecived){
                        FanspickCache.isNewMessageRecived = false;
                        ScrollToLast();
                    }

                    // do something with time...
                });
                return true;
            });




        }

		
		private void UpdateTaskStatus()
		{
			Device.StartTimer(TimeSpan.FromSeconds(30), () =>

			{
				Task.Run(() =>
				{
					socket.On("onIsActive", (data) =>
					{


					});

					JObject o = new JObject();
					o.Add("userId", reciverID);
					socket.Emit("isActive", o);

				});
				return true;
			});
		}

		public class MessageFromSocket
		{
			[JsonProperty(PropertyName = "message")]
			public Message MsgSocket { get; set; }

		}

		public class Message
		{
			[JsonProperty(PropertyName = "msg")]
			public string message { get; set; }

			[JsonProperty(PropertyName = "time")]
			public string Time { get; set; }

			[JsonProperty(PropertyName = "messageId")]
			public string messageId { get; set; }

			[JsonProperty(PropertyName = "userId")]
			public string userID { get; set; }


		}

		public void BackPressed(object sender, EventArgs e)
		{
			JObject o = new JObject();
			o.Add("userId", FanspickCache.UserData.AccessToken);
			o.Add("groupId", FanspickCache.SelectedGroupId);
			socket.Emit("unregister group", o);
			Navigation.PopAsync();

		}

		public void ChatDetail(object sender, EventArgs e)
		{

            if(FanspickCache.chatPersonDetails.Type == "oneToMany"){
            Navigation.PushAsync(new Screens.GroupDetail(), true);    
            }else{
                
            }

		}

        public async void DeleteChat(object sender, EventArgs e)
		{
            var answer = await DisplayAlert("Delete Chat!", "Are you sure you want to delete the chat ?", "Delete", "cancel");
            if(answer){

				try
				{

					var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
					RequestDeleteMessageDTO getTeamDataRequest = new RequestDeleteMessageDTO();

					getTeamDataRequest.GroupId = FanspickCache.SelectedGroupId;
					getTeamDataRequest.Type = "All";

					var header = "Bearer " + FanspickCache.UserData.AccessToken;
					var response = await gitHubApi.DeleteChatsForGroup(getTeamDataRequest, header);

					if (response.StatusCode == 200)
					{
                        BackPressed(this, EventArgs.Empty);
					}
					else
					{

					}

				}
				catch (ApiException exp)
				{
                    Debug.WriteLine(exp.ToString());
				}

				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
				}

            }
		}


		protected override void OnAppearing()
		{
			base.OnAppearing();

			JObject registerGroup = new JObject();
			registerGroup.Add("userId", FanspickCache.UserData.AccessToken);
			registerGroup.Add("groupId", FanspickCache.SelectedGroupId);
			socket.Emit("registerGroup", registerGroup);


			GetGroupDetail();
            GetGroupChat();
         
            if (FanspickCache.chatPersonDetails == null)
			{
                
				ChatWith.Text = FanspickCache.SelectedName;
                GroupImage.Source = FanspickCache.SelectedImage;
				lastseen.Text = "";
			}
			else
			{
                if (FanspickCache.chatPersonDetails.Type == "oneToMany")
				{
                    ChatWith.Text = FanspickCache.chatPersonDetails.GroupName;
					lastseen.Text = "";
                    Dots.IsVisible = true;
					
				}
				else
				{
					ChatWith.Text = FanspickCache.chatPersonDetails.Firstname;
					lastseen.Text = "last seen :" + TimeToShow(FanspickCache.chatPersonDetails.lastActivatedTime);
                    Dots.IsVisible = false;
				}

                GroupImage.Source = FanspickCache.chatPersonDetails.Image;

			}

           

		}

		async void GetGroupChat()
		{
			vm.requestGetChatForGroupDTO.GroupId = FanspickCache.SelectedGroupId;
			ServiceResponse response = await vm.GetChatForGroup();
			if (response.OK)
			{
				vm.FriendsChat.Clear();
                vm.AdminMembers.Clear();
                vm.GroupMembers.Clear();
				ResponseGetChatForGroupDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetChatForGroupDTO>(response.Data);

				foreach (ChatMessages sf in dto.Data.Chats)
				{
					sf.isConnect = true;
					sf.CurrentUserId = FanspickCache.UserData.Id;
					sf.imageType = 3;
					senderID = sf.Sender.Sender_id;
					vm.FriendsChat.Add(sf);
					ScrollToLast();
				}

                if(dto.Data.Chats.Count == 0){
					AddDefaultMessage("Start conversation here.", vm.FriendsChat);
                }


			}
			else
			{
                AddDefaultMessage("Start conversation here.",vm.FriendsChat);
			}

		}

        public void AddDefaultMessage(string message, ObservableCollection<ChatMessages> list){
			DateTime current = DateTime.Now;
			DateTime convertedFixtureDate = TimeZoneInfo.ConvertTime(current, TimeZoneInfo.Utc);
			String nowTime = convertedFixtureDate.ToString("yyyy-MM-dd HH:MM");
			String utcTime = current.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");


			ChatMessages getMessage = new ChatMessages();
			SenderDetails userid = new SenderDetails();

			userid.SenderUsername = FanspickCache.UserData.Username;
			userid.Sender_id = FanspickCache.UserData.Id;
			userid.SenderPhoneNumber = FanspickCache.UserData.PhoneNumber;

			//getMessage.Id = "0";
            getMessage.Message = message;
			getMessage.CurrentUserId = FanspickCache.UserData.Id;
			getMessage.Time = utcTime;
			getMessage.Sender = userid;
            getMessage.type = "Notification";


			if (CrossConnectivity.Current.IsConnected)
			{
				getMessage.isConnect = true;
				getMessage.imageType = 1;
			}
			else
			{
				getMessage.isConnect = false;
				getMessage.imageType = 0;
			}


            list.Add(getMessage);
        }

		private async void GetGroupDetail()
		{
			try
			{
				vm.ShowLoader();


				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				RequestGetGroupDetailDTO getTeamDataRequest = new RequestGetGroupDetailDTO();
                getTeamDataRequest.GroupId = FanspickCache.SelectedGroupId;

				var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.GetGroupDetail(getTeamDataRequest, header);

				if (response.StatusCode == 200)
				{

                    foreach (AdminMembersDetails sf in response.Data.admin)
					{
                        AdminMembers admin = new AdminMembers();
                        admin.AdminId = sf.AdminId._id;
                        admin.AdminName = sf.AdminId.firstname;
                        vm.AdminMembers.Add(admin);
					}

					for (int i = 0; i < response.Data.groupMembers.Count(); i++)
					{
                        if (response.Data.groupMembers[i].MemberId != null)
						{
							GroupMembers admin = new GroupMembers();
							admin.MemberId = response.Data.groupMembers[i].MemberId._id;

                            if(response.Data.groupMembers[i].MemberId.firstname == null){
                                admin.MemberName = response.Data.groupMembers[i].MemberId.username;
                            }else{
                            admin.MemberName = response.Data.groupMembers[i].MemberId.firstname;    
                            }
							
							vm.GroupMembers.Add(admin);
						}

					}

					vm.HideLoader();
				}
				else
				{
					vm.HideLoader();
				}


			}
            catch(ApiException ex){
				Debug.WriteLine(ex.Message);
				vm.HideLoader();
            }
			catch (Exception e)
			{
				Debug.WriteLine(e.Message);
				vm.HideLoader();

            }


		}

		public String TimeToShow(String Time)
		{
			String time = "";
			if (Time == null)
			{
				time = "00:00";
			}
			else
			{
				DateTime parsedFixtureDate = DateTime.Parse(Time);
				TimeZoneInfo timeZoneInformation = TimeZoneInfo.Local;
				DateTimeOffset timeOffset = new DateTimeOffset(parsedFixtureDate, timeZoneInformation.GetUtcOffset(parsedFixtureDate));
				time = parsedFixtureDate.ToString("d MMM yyyy, HH: mm");
			}
			return time;

		}

		void Handle_Clicked(object sender, System.EventArgs e)
		{
			if (!String.IsNullOrEmpty(txtMessage.Text))
			{
				DateTime current = DateTime.Now;
				DateTime convertedFixtureDate = TimeZoneInfo.ConvertTime(current, TimeZoneInfo.Utc);
				String nowTime = convertedFixtureDate.ToString("yyyy-MM-dd HH:MM");
				String utcTime = current.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");

				JObject o = new JObject();
				o.Add("userId", FanspickCache.UserData.AccessToken);
				o.Add("groupId", FanspickCache.SelectedGroupId);
				o.Add("msg", txtMessage.Text);
				o.Add("time", nowTime);

				socket.Emit("send message", o);

				ChatMessages getMessage = new ChatMessages();
				SenderDetails userid = new SenderDetails();

				userid.SenderUsername = FanspickCache.UserData.Username;
				userid.Sender_id = FanspickCache.UserData.Id;
				userid.SenderPhoneNumber = FanspickCache.UserData.PhoneNumber;

				//getMessage.Id = "0";
				getMessage.Message = txtMessage.Text;
				getMessage.CurrentUserId = FanspickCache.UserData.Id;
				getMessage.Time = utcTime;
				getMessage.Sender = userid;




				if (CrossConnectivity.Current.IsConnected)
				{
					getMessage.isConnect = true;
					getMessage.imageType = 1;
				}
				else
				{
					getMessage.isConnect = false;
					getMessage.imageType = 0;
				}


                sentMSGcheck = getMessage;
				vm.FriendsChat.Add(getMessage);

				txtMessage.Text = "";

				ScrollToLast();

			}
		}

		public void ScrollToLast()
		{

			try
			{
				Device.BeginInvokeOnMainThread(() =>
				{
					try
					{
						var last = chatlist.ItemsSource.Cast<object>().LastOrDefault();
						chatlist.ScrollTo(last, ScrollToPosition.MakeVisible, false);
					}
					catch (Exception ex)
					{
                        Debug.WriteLine(ex.ToString());
					}

				});


			}
			catch (Exception ex)
			{
                Debug.WriteLine(ex.ToString());
			}

		}

		public string CreateUri()
		{
			var options = CreateOptions();
			var uri = string.Format("{0}://{1}:{2}", options.Secure ? "https" : "http", options.Hostname, options.Port);
			return uri;
		}


		public IO.Options CreateOptions()
		{


			var config = ConfigBase.Load();
			var options = new IO.Options();
			options.Port = config.server.port;
			options.Hostname = config.server.hostname;
			options.ForceNew = true;

			return options;
		}


		public class ConfigBase
		{
			public ConfigServer server { get; set; }

			public static ConfigBase Load()
			{
				var result = new ConfigBase()
				{
					server = new ConfigServer()
				};
				result.server.hostname = "52.163.48.178";
				result.server.port = 8000;

				return result;
			}
		}

		// fplive.ukwest.cloudapp.azure.com
		// 52.163.48.178

		public class ConfigServer
		{
			public string hostname { get; set; }
			public int port { get; set; }
		}

		
    }



}
