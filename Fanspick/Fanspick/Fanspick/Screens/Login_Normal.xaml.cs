﻿﻿﻿using Fanspick.Shared.Models.Service;
using Fanspick.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using System.Diagnostics;
using Plugin.DeviceInfo;
using Plugin.Geolocator;
using Fanspick.Shared.Helper;
using Refit;
using Fanspick.Shared.Models.DTO;
using Fanspick.RestAPI;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Xamarin.Forms.Xaml;
using Fanspick.PopUp;
using Rg.Plugins.Popup.Extensions;
using Fanspick.Shared.Models;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Login_Normal : ContentPage
	{
		Login_NormalViewModel vm;
		public Login_Normal()
		{
			InitializeComponent();
			Fanspick.Constants.raiseAbove = 0;
			if (vm == null)
			{
				vm = new Login_NormalViewModel();
				BindingContext = vm;
			}

			if (!CrossConnectivity.Current.IsConnected)
			{
				DisplayAlert("No Internet Connection", "Sorry, no internet connectivity detected. Please reconnect and try again", "ok");
			}

		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
			// TODO add functionality to automatically log back in
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}


		protected void txtUsername_Completed(object sender, EventArgs e)
		{
			txtPassword.Focus();
		}

		protected void ForgotPassword(object sender, EventArgs e)
		{
            var _forgot = new ForgotPassword();
            Navigation.PushPopupAsync(_forgot);
		}

        public async void LoginByGoogle(object sender, EventArgs e){
			var result = await DependencyService.Get<IGoogleLogin>().SignIn();
			if (result.IsSuccess)
			{
				var name = result.Name;
				await DisplayAlert("", "Account Name -" + name, "Ok");
			}
        }


		private async void btnLogin_Clicked(object sender, EventArgs e)
		{

            if (!CrossConnectivity.Current.IsConnected)
            {
                await DisplayAlert("No Internet Connection", "Sorry, no internet connectivity detected. Please reconnect and try again", "ok");
            }

            else
            {
                try
                {
                    vm.ShowLoader();

                    var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
                    var request = vm.dto;
                    var response = await gitHubApi.LoginUser(request);

                    if (response.StatusCode == 200)
                    {
                        response.Data.UserDetails.AccessToken = response.Data.AccessToken;
                        FanspickCache.UserData = response.Data.UserDetails;
                        Application.Current.MainPage = new SplashScreen();

                    }
                    else
                    {
                        vm.HideLoader();
                        await DisplayAlert("Fanspick", response.Message, "ok");
                    }

                }
                catch (ApiException ex)
                {
                    vm.HideLoader();

                    var content = ex.GetContentAs<Dictionary<String, String>>();
                    await DisplayAlert("Fanspick", content.Values.Skip(2).First(), "ok");

				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
				}

            }
		}

		public async void GetInfo()
		{
			try
			{
                var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

				if (position == null)
				{
					Debug.WriteLine("null gps", "null gps");
					return;
				}

				if (position.Latitude.ToString() != null)
				{
					vm.requestEventLoggingDTO.Latitude = "" + position.Latitude;
				}
				else
				{
					vm.requestEventLoggingDTO.Latitude = "0";
				}

				if (position.Longitude.ToString() != null)
				{
					vm.requestEventLoggingDTO.Longitude = "" + position.Longitude;
				}
				else
				{
					vm.requestEventLoggingDTO.Longitude = "0";
				}
				vm.requestEventLoggingDTO.EventType = "userSignIn";
				vm.requestEventLoggingDTO.EventDescription = "userSignIn";
				vm.requestEventLoggingDTO.EventAdditionalInfoID = "" + FanspickCache.UserData.AccessToken;
				vm.requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
				vm.requestEventLoggingDTO.DeviceType = "IOS";
				vm.requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
				vm.requestEventLoggingDTO.AppVersion = "1";

				Shared.Models.Service.ServiceResponse response = await vm.EventLog();
				//  Application.Current.MainPage = new SplashScreen();


			}
			catch (Exception ex)
			{
				Debug.WriteLine(" " + ex);

			}

		}

		public async void BackPressed(object sender, EventArgs e)
		{
			//await Navigation.PushModalAsync(new Screens.App_Main(), true);
			await Navigation.PushModalAsync(new Screens.Start(), true);
		}


		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{
				txtUsername.FontSize = 16;
				txtPassword.FontSize = 16;
				btnLogin.FontSize = 16;

				NavigationRow1.Height = new GridLength(6, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(94, GridUnitType.Star);

				TotalRow1.Height = new GridLength(60);
				TotalRow2.Height = new GridLength(110);
				TotalRow3.Height = new GridLength(50);
				TotalRow4.Height = new GridLength(120);
				TotalRow5.Height = new GridLength(200);

				BoxDivision1.Width = new GridLength(20, GridUnitType.Star);
				BoxDivision2.Width = new GridLength(60, GridUnitType.Star);
				BoxDivision3.Width = new GridLength(20, GridUnitType.Star);

				btnback.WidthRequest = 12;

			}
			else
			{
				txtUsername.FontSize = 13;
				txtPassword.FontSize = 13;
				btnLogin.FontSize = 13;

				NavigationRow1.Height = new GridLength(10, GridUnitType.Star);
				NavigationRow2.Height = new GridLength(90, GridUnitType.Star);

				TotalRow1.Height = new GridLength(30);
				TotalRow2.Height = new GridLength(90);
				TotalRow3.Height = new GridLength(35);
				TotalRow4.Height = new GridLength(50);
				TotalRow5.Height = new GridLength(160);

				BoxDivision1.Width = new GridLength(10, GridUnitType.Star);
				BoxDivision2.Width = new GridLength(80, GridUnitType.Star);
				BoxDivision3.Width = new GridLength(10, GridUnitType.Star);

				btnback.WidthRequest = 8;
			}


		}
		private async void LoginFacebook_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushModalAsync(new FacebookLogin());
		}


	}
}