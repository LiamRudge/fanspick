﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fanspick.Screens
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class App_Logout : ContentPage
    {
        public App_Logout()
        {
            InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
			FanspickCache.UserData = null;
			Application.Current.MainPage = new Start();
        }

    }
}