﻿using System;
using System.Collections.Generic;
using Fanspick.RestAPI;
using Fanspick.ViewModels;
using Refit;
using Xamarin.Forms;
using Fanspick.Shared.Helper;
using System.Threading.Tasks;
using Fanspick.Services;
using Fanspick.Models;
using System.Diagnostics;

namespace Fanspick.Screens
{
    public partial class FacebookLogin : ContentPage
    {
       private string ClientId = "165942640479284";
        private FacebookProfile _facebookProfile;
        Login_Facebook vm;
        public FacebookLogin()
        {
            InitializeComponent();
            if (vm == null)
            {
                vm = new Login_Facebook();
                BindingContext = vm;
            }
            var apiRequest =
                "https://www.facebook.com/dialog/oauth?client_id="
                + ClientId
                + "&display=popup&response_type=token&redirect_uri=http://www.facebook.com/connect/login_success.html";

            var webView = new WebView
            {
                Source = apiRequest,
                HeightRequest = 1
            };

            webView.Navigated += WebViewOnNavigated;

            Content = webView;
            //Application.Current.MainPage = new SplashScreen();
        }
        public async Task SetFacebookUserProfileAsync(string accessToken)
        {
            var facebookServices = new FacebookServices();

            _facebookProfile = await facebookServices.GetFacebookProfileAsync(accessToken);
        }
        private async void WebViewOnNavigated(object sender, WebNavigatedEventArgs e)
        {
            try
            {
				var accessToken = ExtractAccessTokenFromUrl(e.Url);

				if (accessToken != "")
				{
					await SetFacebookUserProfileAsync(accessToken);
					var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
					var request = vm.dto;
					request.Email = _facebookProfile.Id.ToString() + "@facebook.com";
					request.FacebookID = _facebookProfile.Id.ToString();

					var response = await gitHubApi.LoginFBUser(request);


					if (response.StatusCode == 200)
					{
						response.Data.UserDetails.AccessToken = response.Data.AccessToken;
						FanspickCache.UserData = response.Data.UserDetails;
						Application.Current.MainPage = new SplashScreen();

					}
					
					else
					{
						vm.HideLoader();
						await DisplayAlert("Fanspick", response.Message, "ok");
					}
				}

			}
			catch (ApiException ex)
			{
				var content = ex.GetContentAs<Dictionary<String, String>>();
                if(content!=null)
                {
                    vm.HideLoader();
                    string[] values = ex.Content.Split(',');
                    if(values[2]!=null)
                    {
                        string[] msg = values[2].ToString().Split(':');
						await DisplayAlert("Fanspick", msg[1], "ok");
						Application.Current.MainPage = new Login_Normal();

					}
				}

			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.ToString());
			}
        }

        private string ExtractAccessTokenFromUrl(string url)
        {
            if (url.Contains("access_token") && url.Contains("&expires_in="))
            {
                var at = url.Replace("https://www.facebook.com/connect/login_success.html#access_token=", "");

                if (Device.OS == TargetPlatform.WinPhone || Device.OS == TargetPlatform.Windows)
                {
                    at = url.Replace("http://www.facebook.com/connect/login_success.html#access_token=", "");
                }

                var accessToken = at.Remove(at.IndexOf("&expires_in="));

                return accessToken;
            }

            return string.Empty;
        }
    }
}