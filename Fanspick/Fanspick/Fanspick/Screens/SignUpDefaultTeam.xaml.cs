﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Fanspick.Shared.Models;
using Fanspick.ViewModels;
using Fanspick.Helper;
using System.ComponentModel;
using Fanspick.Shared.Helper;
using System.Diagnostics;
using Plugin.DeviceInfo;
using Refit;
using Fanspick.RestAPI;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Services;
using Fanspick.Shared.Models.Service;
using System.Linq;

namespace Fanspick.Screens
{
    public partial class SignUpDefaultTeam : ContentPage, INotifyPropertyChanged
    {

        App_TeamSelectorViewModel vm;
        Boolean isCountrySelected = false;
        Boolean isCompetetionSelected = false;
        Boolean isTeamSeleted = false;

        public SignUpDefaultTeam()
        {
            InitializeComponent();
            vm = CacheStorage.ViewModels.App_TeamSelector;
            BindingContext = vm;
            //vm.DownloadResources();
            GetCountryAsync();
        }

        private async void GetCountryAsync()
        {
            FanspickCache.CountryData.Clear();
            var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

            RequestGetCountriesForSportRefitDTO getTeamDataRequest = new RequestGetCountriesForSportRefitDTO();
            getTeamDataRequest.SportId = "58906bc577a25834fa86fed3";
            var header = "Bearer " + FanspickCache.UserData.AccessToken;
            var response = await gitHubApi.GetCountriesForSport(getTeamDataRequest, header);

            if (response.StatusCode == 200)
            {
                vm.Countries = response.Data;
                FanspickCache.CountryData.AddRange(response.Data);
                pkrCountries.SelectedIndex = 1;
            }

        }

        private void pkrCountries_SelectedIndexChanged(object sender, EventArgs e)
		{
			vm.SelectedCountry = pkrCountries.SelectedItem as Country;
            isCountrySelected = true;
			isCompetetionSelected = false;
			isTeamSeleted = false;
            GetCompetition(vm.SelectedCountry.Id);

		}


        public async void GetCompetition(String countryID)
		{

			var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
			RequestAllLeaguesForCountryRefitDTO getTeamDataRequest = new RequestAllLeaguesForCountryRefitDTO();
            getTeamDataRequest.CountryId = countryID;
			var header = "Bearer " + FanspickCache.UserData.AccessToken;
            var response = await gitHubApi.GetLeaguesForCountry(getTeamDataRequest, header);

			if (response.StatusCode == 200)
			{
                vm.Competitions = response.Data;
			}
		}


		private void pkrLeaguesForSelectedCountry_SelectedIndexChanged(object sender, EventArgs e)
		{
			vm.SelectedCompetition = pkrLeaguesForSelectedCountry.SelectedItem as Competition;
            isCompetetionSelected = true;
			isTeamSeleted = false;
			if (vm.SelectedCompetition != null)
			{
				GetTeam(vm.SelectedCompetition.Id);
			}
			
		}

		public async void GetTeam(String compId)
		{

			var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
			RequestGetTeamsForCompetitionRefitDTO getTeamDataRequest = new RequestGetTeamsForCompetitionRefitDTO();
            getTeamDataRequest.CompetitionId = compId;
			var header = "Bearer " + FanspickCache.UserData.AccessToken;
            var response = await gitHubApi.GetTeamsForCompetition(getTeamDataRequest, header);

			if (response.StatusCode == 200)
			{
                vm.Teams = response.Data;
			}
		}


		private void pkrTeamForSelectedLeague_SelectedIndexChanged(object sender, EventArgs e)
		{
            vm.SelectedTeam = pkrTeamForSelectedLeague.SelectedItem as Team;
            isTeamSeleted = true;
		}

		void OnView(object sender, EventArgs e)
		{
			var source = new UrlWebViewSource();
			source.Url = "http://fanspick-admin-test.5sol.co.uk/resources/terms.html";
			browser.Source = source;
			viewTerms.IsVisible = true;
		}

		void OnClose(object sender, EventArgs e)
		{
			viewTerms.IsVisible = false;
		}


        private async void onclickSetFav(object sender, EventArgs e){
            
            if(!isCountrySelected || !isCompetetionSelected || !isTeamSeleted){
				await DisplayAlert(FanspickStrings.GenericDialogTitle, "You need to select a favorite team to proceed", FanspickStrings.Alert_OK);
            }else{
                
				var answer = await DisplayAlert("Are you sure ?", "You want this as your number one team ?", "Continue", "Cancel");
				switch (answer)
				{
					case true:
						vm.ShowLoader();


						try
						{
				            var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
							RequestSetFavTeamRefitDTO getTeamDataRequest = new RequestSetFavTeamRefitDTO();
							getTeamDataRequest.TeamId = vm.SelectedTeam.Id; ;
							getTeamDataRequest.Type = "PRIMARY";

							var header = "Bearer " + FanspickCache.UserData.AccessToken;
							var response = await gitHubApi.SetFavouriteTeam(getTeamDataRequest, header);

							if (response.StatusCode == 200)
							{
								Application.Current.MainPage = new SplashScreen();
                                vm.HideLoader();
							}
							else
							{
								await DisplayAlert("Fanspick", "" + response.Message, "OK");
                                vm.HideLoader();
							}

						}
						catch (ApiException ex)
						{
                            vm.HideLoader();
							var content = ex.GetContentAs<Dictionary<String, String>>();
							await DisplayAlert("Fanspick", content.Values.Last().ToString(), "ok");
						}
						catch (Exception ex)
						{
							Debug.WriteLine(ex.ToString());
						}


						break;
				}
            
            }


        }



		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
            String device = CrossDeviceInfo.Current.Model;
            ajustDevice(device);
        }

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{
		
				BoxDivision1.Width = new GridLength(20, GridUnitType.Star);
				BoxDivision2.Width = new GridLength(60, GridUnitType.Star);
				BoxDivision3.Width = new GridLength(20, GridUnitType.Star);

				BoxTab1.Height = new GridLength(20, GridUnitType.Star);
				BoxTab2.Height = new GridLength(25, GridUnitType.Star);
				BoxTab3.Height = new GridLength(6, GridUnitType.Star);
				BoxTab4.Height = new GridLength(6, GridUnitType.Star);
				BoxTab5.Height = new GridLength(7, GridUnitType.Star);
				BoxTab6.Height = new GridLength(34, GridUnitType.Star);

                browser.WidthRequest = 750;

			}
			else
			{
				BoxDivision1.Width = new GridLength(10, GridUnitType.Star);
				BoxDivision2.Width = new GridLength(80, GridUnitType.Star);
				BoxDivision3.Width = new GridLength(10, GridUnitType.Star);

                BoxTab1.Height = new GridLength(30, GridUnitType.Star);
                BoxTab2.Height = new GridLength(30, GridUnitType.Star);
                BoxTab3.Height = new GridLength(8, GridUnitType.Star);
                BoxTab4.Height = new GridLength(8, GridUnitType.Star);
                BoxTab5.Height = new GridLength(12, GridUnitType.Star);
                BoxTab6.Height = new GridLength(10, GridUnitType.Star);

                browser.WidthRequest = 300;
			}


		}
    }
}
