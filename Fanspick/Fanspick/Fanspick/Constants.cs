﻿using System;

namespace Fanspick
{
    public static class Constants
    {
		// OAuth
		// For Google login, configure at https://console.developers.google.com/
		public static string iOSClientId = "587723330313-vb0t736or41mi8ran9grcn3sndkhb83i.apps.googleusercontent.com";
        public static string SecretKey = "-6Hry-WiurgkNzR2bLgqYBP6";
		// These values do not need changing
		public static string Scope = "https://www.googleapis.com/auth/userinfo.email";
		public static string AuthorizeUrl = "https://accounts.google.com/o/oauth2/auth";
		public static string AccessTokenUrl = "https://www.googleapis.com/oauth2/v4/token";
		public static string UserInfoUrl = "https://www.googleapis.com/oauth2/v2/userinfo";

		// Set these to reversed iOS/Android client ids, with :/oauth2redirect appended
		public static string iOSRedirectUrl = "https://www.youtube.com/c/HoussemDellai/oauth2redirect";
        public static string AndroidRedirectUrl = "<insert Android redirect URL here>:/oauth2redirect";

		public static Uri AuthorizationEndpoint = new Uri("http://xamuath.azurewebsites.net/oauth/authorize");
		public static Uri TokenEndpoint = new Uri("http://xamuath.azurewebsites.net/oauth/token");
		public static Uri ApiEndpoint = new Uri("http://xamuath.azurewebsites.net/api/whoami");
		public static Uri RedirectionEndpoint = new Uri("http://www.xamarin.com");

        public static string SponserUrl = "http://fanspick-admin-test.5sol.co.uk/SponsorUploads/";

        public static double raiseAbove = 0;
    }
}
