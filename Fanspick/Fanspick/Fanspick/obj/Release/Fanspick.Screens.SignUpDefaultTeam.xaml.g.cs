// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.42000
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace Fanspick.Screens {
    
    
    [global::Xamarin.Forms.Xaml.XamlFilePathAttribute("/Users/manbir/Desktop/6 dec/fanspickxamarin/Fanspick/Fanspick/Fanspick/Screens/SignUpDefaultTeam.xaml")]
    public partial class SignUpDefaultTeam : global::Xamarin.Forms.ContentPage {
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition BoxTab1;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition BoxTab2;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition BoxTab3;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition BoxTab4;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition BoxTab5;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition BoxTab6;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ColumnDefinition BoxDivision1;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ColumnDefinition BoxDivision2;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ColumnDefinition BoxDivision3;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Picker pkrCountries;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Picker pkrLeaguesForSelectedCountry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Picker pkrTeamForSelectedLeague;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.AbsoluteLayout viewTerms;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.WebView browser;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ContentView actIndBackground;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ActivityIndicator actInd;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private void InitializeComponent() {
            global::Xamarin.Forms.Xaml.Extensions.LoadFromXaml(this, typeof(SignUpDefaultTeam));
            BoxTab1 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "BoxTab1");
            BoxTab2 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "BoxTab2");
            BoxTab3 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "BoxTab3");
            BoxTab4 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "BoxTab4");
            BoxTab5 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "BoxTab5");
            BoxTab6 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "BoxTab6");
            BoxDivision1 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.ColumnDefinition>(this, "BoxDivision1");
            BoxDivision2 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.ColumnDefinition>(this, "BoxDivision2");
            BoxDivision3 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.ColumnDefinition>(this, "BoxDivision3");
            pkrCountries = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Picker>(this, "pkrCountries");
            pkrLeaguesForSelectedCountry = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Picker>(this, "pkrLeaguesForSelectedCountry");
            pkrTeamForSelectedLeague = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Picker>(this, "pkrTeamForSelectedLeague");
            viewTerms = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.AbsoluteLayout>(this, "viewTerms");
            browser = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.WebView>(this, "browser");
            actIndBackground = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.ContentView>(this, "actIndBackground");
            actInd = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.ActivityIndicator>(this, "actInd");
        }
    }
}
