﻿﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Refit;

/* ******** Hey You! *********
 *
 * This is a generated file, and gets rewritten every time you build the
 * project. If you want to edit it, you need to edit the mustache template
 * in the Refit package */

namespace RefitInternalGenerated
{
    [AttributeUsage (AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event | AttributeTargets.Interface | AttributeTargets.Delegate)]
    sealed class PreserveAttribute : Attribute
    {
#pragma warning disable 0649
        //
        // Fields
        //
        public bool AllMembers;

        public bool Conditional;
#pragma warning restore 0649
    }
}

namespace Fanspick.RestAPI
{
    using RefitInternalGenerated;

    [Preserve]
    public partial class AutoGeneratedIGitHubApi : IGitHubApi
    {
        public HttpClient Client { get; protected set; }
        readonly Dictionary<string, Func<HttpClient, object[], object>> methodImpls;

        public AutoGeneratedIGitHubApi(HttpClient client, IRequestBuilder requestBuilder)
        {
            methodImpls = requestBuilder.InterfaceHttpMethods.ToDictionary(k => k, v => requestBuilder.BuildRestResultFuncForMethod(v));
            Client = client;
        }

        public virtual Task<ResponseLoginDTO> LoginUser(RequestLoginDTO user)
        {
            var arguments = new object[] { user };
            return (Task<ResponseLoginDTO>) methodImpls["LoginUser"](Client, arguments);
        }

        public virtual Task<ResponseFBLoginDTO> LoginFBUser(RequestFacebookLoginDTO user)
        {
            var arguments = new object[] { user };
            return (Task<ResponseFBLoginDTO>) methodImpls["LoginFBUser"](Client, arguments);
        }

        public virtual Task<ResponseViewProfileDTO> GetProfile(string authorization)
        {
            var arguments = new object[] { authorization };
            return (Task<ResponseViewProfileDTO>) methodImpls["GetProfile"](Client, arguments);
        }

        public virtual Task<ResponseGetSocialFeedDTO> GetSocial(string TeamId)
        {
            var arguments = new object[] { TeamId };
            return (Task<ResponseGetSocialFeedDTO>) methodImpls["GetSocial"](Client, arguments);
        }

        public virtual Task<ResponseGetTeamDataDTO> GetTeamData(RequestGetTeamDataRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseGetTeamDataDTO>) methodImpls["GetTeamData"](Client, arguments);
        }

        public virtual Task<ResponseMyTeamsDTO> GetAllFavouriteTeams(string authorization)
        {
            var arguments = new object[] { authorization };
            return (Task<ResponseMyTeamsDTO>) methodImpls["GetAllFavouriteTeams"](Client, arguments);
        }

        public virtual Task<ResponseGetUpcomingFixturesDTO> GetUpcomingFixtures(RequestGetUpcomingFixturesRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseGetUpcomingFixturesDTO>) methodImpls["GetUpcomingFixtures"](Client, arguments);
        }

        public virtual Task<ResponseGetAllFormationsDTO> GetAllFormations(string authorization)
        {
            var arguments = new object[] { authorization };
            return (Task<ResponseGetAllFormationsDTO>) methodImpls["GetAllFormations"](Client, arguments);
        }

        public virtual Task<ResponseCupGamesDTO> GetTeamsForCompetitionName(RequestCupGameRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseCupGamesDTO>) methodImpls["GetTeamsForCompetitionName"](Client, arguments);
        }

        public virtual Task<ResponseSetFavTeamDTO> SetFavouriteTeam(RequestSetFavTeamRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseSetFavTeamDTO>) methodImpls["SetFavouriteTeam"](Client, arguments);
        }

        public virtual Task<ResponsePlayerInfoDTO> GetPlayerData(RequestPlayerInfoRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponsePlayerInfoDTO>) methodImpls["GetPlayerData"](Client, arguments);
        }

        public virtual Task<ResponseLoginDTO> Register(RequestSignUpDTO user)
        {
            var arguments = new object[] { user };
            return (Task<ResponseLoginDTO>) methodImpls["Register"](Client, arguments);
        }

        public virtual Task<ResponseUpdateProfileDTO> EditProfile(RequestUpdateProfileRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseUpdateProfileDTO>) methodImpls["EditProfile"](Client, arguments);
        }

        public virtual Task<ResponseUpdateProfileDTO> EditProfilePro(RequestUpdateProfileRefitPRoDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseUpdateProfileDTO>) methodImpls["EditProfilePro"](Client, arguments);
        }

        public virtual Task<ResponseUpdateProfileDTO> EditProfileDictonary(Dictionary<string, string> user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseUpdateProfileDTO>) methodImpls["EditProfileDictonary"](Client, arguments);
        }

        public virtual Task<ResponseGetCountriesForSportDTO> GetCountriesForSport(RequestGetCountriesForSportRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseGetCountriesForSportDTO>) methodImpls["GetCountriesForSport"](Client, arguments);
        }

        public virtual Task<ResponseAllLeaguesForCountryDTO> GetLeaguesForCountry(RequestAllLeaguesForCountryRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseAllLeaguesForCountryDTO>) methodImpls["GetLeaguesForCountry"](Client, arguments);
        }

        public virtual Task<ResponseGetTeamsForCompetitionDTO> GetTeamsForCompetition(RequestGetTeamsForCompetitionRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseGetTeamsForCompetitionDTO>) methodImpls["GetTeamsForCompetition"](Client, arguments);
        }

        public virtual Task<ResponseMyCommunityDTO> GetMyCommunity(RequestMyCommunityRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseMyCommunityDTO>) methodImpls["GetMyCommunity"](Client, arguments);
        }

        public virtual Task<ResponseGetTeamSquadDTO> GetTeamSquad(RequestGetTeamSquadRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseGetTeamSquadDTO>) methodImpls["GetTeamSquad"](Client, arguments);
        }

        public virtual Task<ResponseGetFormationByTypeDTO> GetFormationByType(RequestGetFormationByTypeRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseGetFormationByTypeDTO>) methodImpls["GetFormationByType"](Client, arguments);
        }

        public virtual Task<ResponseGetUserPickDTO> GetUserPick(RequestGetUserPickRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseGetUserPickDTO>) methodImpls["GetUserPick"](Client, arguments);
        }

        public virtual Task<ResponseChangeFormationDTO> GetNewFormationWithPlayers(RequestChangeFormationRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseChangeFormationDTO>) methodImpls["GetNewFormationWithPlayers"](Client, arguments);
        }

        public virtual Task<String> UnsetUserPick(RequestUnSetUserPickRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<String>) methodImpls["UnsetUserPick"](Client, arguments);
        }

        public virtual Task<String> SetUserPick(RequestSetUserPickRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<String>) methodImpls["SetUserPick"](Client, arguments);
        }

        public virtual Task<ResponseSwapPitchPlayersDTO> SwapPlayers(RequestSwapPitchPlayersDataRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseSwapPitchPlayersDTO>) methodImpls["SwapPlayers"](Client, arguments);
        }

        public virtual Task<ResponseGetManagerPickDTO> GetManagerPick(RequestGetManagerPickRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseGetManagerPickDTO>) methodImpls["GetManagerPick"](Client, arguments);
        }

        public virtual Task<ResponseGetFanspickDTO> GetFanspick(RequestGetFanspickRefitDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseGetFanspickDTO>) methodImpls["GetFanspick"](Client, arguments);
        }

        public virtual Task<ResponseGetBillBoardDTO> GetSponsorBillboard(RequestGetBillBoardDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseGetBillBoardDTO>) methodImpls["GetSponsorBillboard"](Client, arguments);
        }

        public virtual Task<ResponseGetBillBoardDTO> DeleteChatsForGroup(RequestDeleteMessageDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseGetBillBoardDTO>) methodImpls["DeleteChatsForGroup"](Client, arguments);
        }

        public virtual Task<ResponseGetGroupDetailDTO> GetGroupDetail(RequestGetGroupDetailDTO user,string authorization)
        {
            var arguments = new object[] { user,authorization };
            return (Task<ResponseGetGroupDetailDTO>) methodImpls["GetGroupDetail"](Client, arguments);
        }

        public virtual Task<ResponseGenerateOtpDTO> GenerateOTP(RequestGenerateOtpDTO user)
        {
            var arguments = new object[] { user };
            return (Task<ResponseGenerateOtpDTO>) methodImpls["GenerateOTP"](Client, arguments);
        }

        public virtual Task<ResponseVerifyOtpDTO> VerifyOTP(RequestVerifyOtpDTO user)
        {
            var arguments = new object[] { user };
            return (Task<ResponseVerifyOtpDTO>) methodImpls["VerifyOTP"](Client, arguments);
        }

        public virtual Task<ResponseResetPasswordDTO> ResetForgotPassword(RequestResetPasswordDTO user)
        {
            var arguments = new object[] { user };
            return (Task<ResponseResetPasswordDTO>) methodImpls["ResetForgotPassword"](Client, arguments);
        }

    }
}
