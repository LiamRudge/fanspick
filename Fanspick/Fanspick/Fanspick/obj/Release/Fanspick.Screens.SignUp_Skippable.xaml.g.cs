// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.42000
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace Fanspick.Screens {
    
    
    [global::Xamarin.Forms.Xaml.XamlFilePathAttribute("/Users/manbir/Desktop/6 dec/fanspickxamarin/Fanspick/Fanspick/Fanspick/Screens/SignUp_Skippable.xaml")]
    public partial class SignUp_Skippable : global::Xamarin.Forms.ContentPage {
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::DK.SlidingPanel.Interface.SlidingUpPanel spTest;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ColumnDefinition BoxDivision1;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ColumnDefinition BoxDivision2;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ColumnDefinition BoxDivision3;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition TotalRow1;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition TotalRow2;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition TotalRow3;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition TotalRow4;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition TotalRow5;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition TotalRow6;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition TotalRow10;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition TotalRow11;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition TotalRow7;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition TotalRow8;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RowDefinition TotalRow9;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Label skipable_Image;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Entry FirstName;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Entry LastName;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Entry DOB;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.StackLayout StackDob;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.DatePicker DateOfBirth;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Entry Code;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Entry Phone;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Entry txtCountry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Entry txtAddress;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Entry txtZip;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.AbsoluteLayout PopUpCountries;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Entry PopUpCountriesText;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ListView lstCountries;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.AbsoluteLayout PopUpCities;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Entry PopUpCitiesText;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ListView pkCountries;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ListView lstCountriesP;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private void InitializeComponent() {
            global::Xamarin.Forms.Xaml.Extensions.LoadFromXaml(this, typeof(SignUp_Skippable));
            spTest = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::DK.SlidingPanel.Interface.SlidingUpPanel>(this, "spTest");
            BoxDivision1 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.ColumnDefinition>(this, "BoxDivision1");
            BoxDivision2 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.ColumnDefinition>(this, "BoxDivision2");
            BoxDivision3 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.ColumnDefinition>(this, "BoxDivision3");
            TotalRow1 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "TotalRow1");
            TotalRow2 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "TotalRow2");
            TotalRow3 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "TotalRow3");
            TotalRow4 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "TotalRow4");
            TotalRow5 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "TotalRow5");
            TotalRow6 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "TotalRow6");
            TotalRow10 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "TotalRow10");
            TotalRow11 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "TotalRow11");
            TotalRow7 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "TotalRow7");
            TotalRow8 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "TotalRow8");
            TotalRow9 = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.RowDefinition>(this, "TotalRow9");
            skipable_Image = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Label>(this, "skipable_Image");
            FirstName = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Entry>(this, "FirstName");
            LastName = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Entry>(this, "LastName");
            DOB = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Entry>(this, "DOB");
            StackDob = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.StackLayout>(this, "StackDob");
            DateOfBirth = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.DatePicker>(this, "DateOfBirth");
            Code = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Entry>(this, "Code");
            Phone = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Entry>(this, "Phone");
            txtCountry = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Entry>(this, "txtCountry");
            txtAddress = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Entry>(this, "txtAddress");
            txtZip = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Entry>(this, "txtZip");
            PopUpCountries = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.AbsoluteLayout>(this, "PopUpCountries");
            PopUpCountriesText = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Entry>(this, "PopUpCountriesText");
            lstCountries = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.ListView>(this, "lstCountries");
            PopUpCities = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.AbsoluteLayout>(this, "PopUpCities");
            PopUpCitiesText = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.Entry>(this, "PopUpCitiesText");
            pkCountries = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.ListView>(this, "pkCountries");
            lstCountriesP = global::Xamarin.Forms.NameScopeExtensions.FindByName <global::Xamarin.Forms.ListView>(this, "lstCountriesP");
        }
    }
}
