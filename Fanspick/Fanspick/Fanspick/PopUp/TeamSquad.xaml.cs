﻿using Fanspick.Helper;
using Fanspick.Shared.Models;
using Fanspick.ViewModels;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Fanspick.Shared.Helper;

namespace Fanspick.PopUp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TeamSquad : PopupPage
    {
		App_MainViewModel vmApp_Main;
        TeamSquadViewModel vm;
        public TeamSquad()
        {
            InitializeComponent();
            vm = CacheStorage.ViewModels.HomePage.YourPickViewModelPre.PitchData.TeamSquadVM;
            lvTeams.ItemsSource = vm.TeamSquadData;
            lvTeams.ItemSelected += LvTeams_ItemSelected;
            NavigationPage.SetHasNavigationBar(this, true);
            BindingContext = vm;
            vmApp_Main = CacheStorage.ViewModels.App_Main;
        }

        private void LvTeams_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Squad item = e.SelectedItem as Squad;
            vm.SetTeamSquad(item, FanspickCache.CurrentFanspickScope.SelectedFixture.Id, FanspickCache.CurrentFanspickScope.SelectedTeam.Id);
            PopupNavigation.PopAsync();
            vmApp_Main.ActiveDetailsPage(ScreenType.HomePitch);
        }

        private void OnClose(object sender, EventArgs e)
        {
            PopupNavigation.PopAsync();
        }

        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.5);
        }

        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(1);
        }
    }
}
