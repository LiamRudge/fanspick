﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Fanspick.RestAPI;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.DTO;
using Refit;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Fanspick.PopUp
{
    public partial class ForgotPassword : PopupPage
    {
        bool isOTP = false;
        bool isReset = false;
        string phoneNumber;

        public ForgotPassword()
        {
            InitializeComponent();
            entry2.IsVisible = false;
			entry.IsPassword = false;
			entry2.IsPassword = false;
        }

		void ClosePop(object sender, EventArgs e)
		{
			PopupNavigation.PopAsync();
		}


		void AlreadyHaveOTP(object sender, EventArgs e)
		{
          
		}


        async void BtnSubmit(object sender, EventArgs e)
		{

            if(!isOTP){


                if (entry.Text != null){

                   try{

                            RequestGenerateOtpDTO request = new RequestGenerateOtpDTO();
                            request.PhoneNumber = entry.Text;
                            request.signUp = false;

							var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
                            var response = await gitHubApi.GenerateOTP(request);

							if (response.StatusCode == 200)
							{
                                phoneNumber = entry.Text;
								HeaderText.Text = "Insert OTP";
								HeaderMessage.Text = "Please enter the OTP passcode sent to the number provided";
								entry.Text = "";
								entry.Placeholder = "Enter OTP";
								isOTP = true;
							
							}
							else
							{
								await DisplayAlert("Error", "Please make sure that the number has been registered with Fanspick. We wont be able to generate the number if you have not verified", "ok");
							}

                        }
                        catch (ApiException exp){
                        Debug.WriteLine(exp.ToString());
                            await DisplayAlert("Error", "Please make sure that the number has been registered with Fanspick. We wont be able to generate the number if you have not verified", "ok");
                        }
                        catch(Exception ex){
                        Debug.WriteLine(ex.ToString());
                            await DisplayAlert("Error", "Please make sure that the number has been registered with Fanspick. We wont be able to generate the number if you have not verified", "ok");
                        }

                }
                else{
                   await DisplayAlert("Error", "Phone number must not be null", "ok");
                }

            }else if(isOTP && !isReset){

                if(entry.Text==""){
				await	DisplayAlert("Error", "Please enter a valid OTP to continue", "ok");
                }else{

					try
					{

                        RequestVerifyOtpDTO request = new RequestVerifyOtpDTO();
                        request.OTP = entry.Text;

						var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
                        var response = await gitHubApi.VerifyOTP(request);

						if (response.StatusCode == 200)
						{
							HeaderText.Text = "Reset Password";
							HeaderMessage.Text = "Please enter the new Password of your choice";
							entry.Text = "";
							entry.Placeholder = "Enter Password";
							entry2.IsVisible = true;
							entry.IsPassword = true;
							entry2.IsPassword = true;
							isOTP = true;
							isReset = true;

						}
						else
						{
							await DisplayAlert("InCorrect OTP", "OTP does not match", "ok");
						}

					}
					catch (ApiException exp)
					{
                        Debug.WriteLine(exp.ToString());
						await DisplayAlert("InCorrect OTP", "OTP does not match", "ok");
					}
					catch (Exception ex)
					{
                        Debug.WriteLine(ex.ToString());
						await DisplayAlert("InCorrect OTP", "OTP does not match", "ok");
					}


                }

            }
            else if(isOTP && isReset){

                if(entry.Text == ""){
                  await  DisplayAlert("Error", "Please enter a valid password to continue", "ok");
                }else if(entry.Text != entry2.Text){
                  await  DisplayAlert("Error", "Password Mismatch", "ok");
                }else{


					try
					{

						RequestResetPasswordDTO request = new RequestResetPasswordDTO();
                        request.PhoneNumber = phoneNumber;
                        request.NewPassword = entry2.Text;

						var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
                        var response = await gitHubApi.ResetForgotPassword(request);

                        Debug.WriteLine(response.ToString());

						if (response.StatusCode == 200)
						{
							ClosePop(this, EventArgs.Empty);
                            await DisplayAlert("Success", "Password has been successfully changed", "ok");
						}
						else
						{
							await DisplayAlert("InCorrect OTP", "OTP does not match", "ok");
						}

					}
					catch (ApiException ex)
					{
						var content = ex.GetContentAs<Dictionary<String, String>>();
						await DisplayAlert("Fanspick", content.Values.Skip(2).First(), "ok");
					}
					catch (Exception ex)
					{
                        await DisplayAlert("Error", ex.ToString(), "ok");
					}



                 
                }

            }

		}



    }
}
