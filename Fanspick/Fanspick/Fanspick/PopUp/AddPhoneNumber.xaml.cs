﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DK.SlidingPanel.Interface;
using Fanspick.RestAPI;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Refit;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Fanspick.PopUp
{
    public partial class AddPhoneNumber : PopupPage
    {
		CCode code;
		List<CCode> cCodes;
        public AddPhoneNumber()
        {
            InitializeComponent();
        }

		void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
		{
            lstCountriesP.IsVisible = true;
		}

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            string phone = PhoneNumber.Text;

            if (phone == null){
                await DisplayAlert("Error", "Please enter a valid phone number", "ok");
            }else if(phone.Length < 10 || phone.Length > 10){
                await DisplayAlert("Error", "Phone number should be of 10 characters", "ok");
            }else if(CountryCode.Text == null){
                await DisplayAlert("Error", "Please select a valid country code", "ok");
			}
			else if (CountryCode.Text == "")
			{
				await DisplayAlert("Error", "Please select a valid country code", "ok");
			}

            else{

                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("phoneNumber", CountryCode.Text+PhoneNumber.Text);

				try
				{
					var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
					var header = "Bearer " + FanspickCache.UserData.AccessToken;
					var response = await gitHubApi.EditProfileDictonary(dict, header);

					if (response.StatusCode == 200)
					{
                        SideMenu(this, EventArgs.Empty);
                        var _number = new OTPpopup(phone);
                        await Navigation.PushPopupAsync(_number);
					}
					

				}
				catch (ApiException ex)
				{
					var content = ex.GetContentAs<Dictionary<String, String>>();
					string msg = content.Values.Last().ToString();
                    await DisplayAlert("Error", msg, "ok");
                    SideMenu(this, EventArgs.Empty);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
                    SideMenu(this, EventArgs.Empty);
				}

            }

            //
        }

        private void OnClosePanel(object sender, EventArgs e)
		{
            lstCountriesP.IsVisible = false;
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
			
			if (cCodes == null)
			{
                GetCountriesCode();
            }
        }

		private void GetCountriesCode()
		{
			cCodes = DependencyService.Get<ICountryCodes>().DisplayCountryCodes("").Distinct().ToList();
			lstCountriesP.ItemsSource = cCodes;

		}

		async void OnCodeSelected(object sender, EventArgs e)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)e;
			CCode selectedItem = (CCode)eventArgs.Parameter;
            CountryCode.Text = selectedItem.CodeP;
            PhoneNumber.Focus();
			OnClosePanel(this, EventArgs.Empty);
		}

        public void SideMenu(object sender, EventArgs e)
		{
			PopupNavigation.PopAsync();
		}

		
    }
}
