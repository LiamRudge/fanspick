﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Fanspick.Helper;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Fanspick.ViewModels;
using Plugin.DeviceInfo;
using Plugin.Geolocator;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;
using static Fanspick.Shared.Models.DTO.UserNotificationDTO;

namespace Fanspick.PopUp
{
    public partial class NavigationPopUp : PopupPage
    {

		RequestEventLoggingDTO requestEventLoggingDTO = new RequestEventLoggingDTO();
        void Positive_Clicked(object sender, System.EventArgs e)
        {

             updateNotificationResponse(notification.notification._id, notification.notification.questionnaires[0].questions[0].questionId, notification.notification.questionnaires[0].questions[0].question, "yes");
            PopupNavigation.PopAsync();
        }

        void Negative_Clicked(object sender, System.EventArgs e)
        {
            updateNotificationResponse(notification.notification._id, notification.notification.questionnaires[0].questions[0].questionId, notification.notification.questionnaires[0].questions[0].question, "no");
           PopupNavigation.PopAsync();
        }

        ResponseUserNotificationData notification;
        private bool isValidAge;

        AlertScreenModal vm;
        public NavigationPopUp()
        {
            InitializeComponent();
			vm = CacheStorage.ViewModels.AlertModal;
            notification = FanspickCache.CurrentFanspickScope.SelectedAlert;
            BindingContext = notification;
        }

		void ClosePop(object sender, EventArgs e)
		{
			PopupNavigation.PopAsync();
		}

        void bannerClicked(object sender, EventArgs e)
        {
           // if (!String.IsNullOrEmpty(notification.notification.BannerUrl))
         //   {
				//FanspickCache.CurrentFanspickScope.CurrentlyUrlToVisit = notification.notification.BannerUrl;

				//CacheStorage.ViewModels.App_Main.ActiveDetailsPage(ScreenType.InAppWebView);
          //  }
        }

        protected override void OnAppearing()
        {

			isValidAge = false;
			SimpleAlertTypeQuestion.IsVisible = false;
			SimpleAlertTypeAgeCheck.IsVisible = false;
			bannerAlert.IsVisible = false;

            base.OnAppearing();
            SetStage();
        }

        private void SetStage()
        {

            if(notification != null){


            if (notification.notification.TriggerAgeCheck == "True")
            {
                SimpleAlertTypeAgeCheck.IsVisible = true;
            }
            else{
                isValidAge = true;
            }
            }

            else{
                isValidAge = true;
            }
            


            if (isValidAge)
            {
                ShowStage();
            }
        }

        private async void ShowStage()
        {
            if (notification != null)
            {


                switch (notification.notification.NotificationType)
                {
                    case "Questionnaire":

                        if (notification.notification.questionnaires != null && notification.notification.questionnaires.Any())
                        {

                            if (notification.notification.TriggerAgeCheck == "True")
                            {
                                SimpleAlertTypeAgeCheck.IsVisible = true;
                            }
                            else
                            {

                                SimpleAlertTypeQuestion.IsVisible = true;

                                Questions question = notification.notification.questionnaires.FirstOrDefault().questions.FirstOrDefault();
                                if (question != null)
                                {
                                    Question.Text = question.question;
                                }

                            }


                        }
                        else
                        {
                            ClosePop(this, EventArgs.Empty);
							await ShowMessage("Sorry", "No questions Available", "Ok", async () =>

						{
							await PopupNavigation.PopAsync();
						});
                        }
                        break;
                    case "BannerAlert":
                        bannerAlert.IsVisible = true;

                        if (!String.IsNullOrEmpty(notification.notification.BannerImage))
                        {
                            bannerImage.Source = ConvertToImage(notification.notification.BannerImage);
                        }
                        GetInfo("alertBannerView", FanspickCache.CurrentFanspickScope.SelectedAlert.notification.NotificationMessage, FanspickCache.CurrentFanspickScope.SelectedAlert.notification._id);
                        break;

                    case "BasicAlert":

						await ShowMessage("Sorry", "No questions Available", "Ok", async () =>
			
                        {
                            await PopupNavigation.PopAsync();
                        });



                        break;


                    default:
                        bannerAlert.IsVisible = true;
                        bannerImage.Source = "Fanspick_alert_banner";

                        break;

                }

			}

			else
			{
				bannerAlert.IsVisible = true;
				bannerImage.Source = "Fanspick_alert_banner";
			}
        }



		public async Task ShowMessage(string message,
			string title,
			string buttonText,
			Action afterHideCallback)
		{
			await DisplayAlert(
				title,
				message,
				buttonText);

			afterHideCallback?.Invoke();
		}



		public static ImageSource ConvertToImage(string imageBase64)
		{
			try
			{
				ImageSource image;

				if (string.IsNullOrEmpty(imageBase64))
				{
					image = Xamarin.Forms.ImageSource.FromFile("no_shirt.png");
                    return image;
				}
				else
				{
                    using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(imageBase64)))
					{
						return Xamarin.Forms.ImageSource.FromStream(() => ms);
					}
				}
				
			}
			catch (Exception ex)
			{
                Debug.WriteLine(ex.ToString());
				return Xamarin.Forms.ImageSource.FromFile("no_shirt.png");
			}

		}

        async void ConfirmAge_Clicked(object sender, EventArgs e)
        {

            DateTime StartDate = Convert.ToDateTime(dpAgeCheck.Date.ToString());
			string Date = StartDate.ToString("yyyy-MM-dd");


            Shared.Models.Service.ServiceResponse responseserver = await vm.AgeVerification(Date);

			if (responseserver.OK)
			{
				SimpleAlertTypeAgeCheck.IsVisible = false;
				SimpleAlertTypeQuestion.IsVisible = true;

				Questions question = notification.notification.questionnaires.FirstOrDefault().questions.FirstOrDefault();
				if (question != null)
				{
				  Question.Text = question.question;
				}

			}
			else
			{
                await PopupNavigation.PopAsync();
                await DisplayAlert("Fanspick", "You need to be older than 18 years to view this alert.", "OK");

			}

        }

		private async void updateNotificationResponse(string notficationID, string questionId, string questionText, string response)
		{
            Shared.Models.Service.ServiceResponse responseserver = await vm.CreateNotificationResponse(notficationID, questionId, questionText, response);

			if (responseserver.OK)
			{
                await vm.GetAlerts();
			}
			else
			{

			}
		}

		public async void GetInfo(String eventtype, String description, String additionalId)
		{
			try
			{

				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

				if (position == null)
				{
					Debug.WriteLine("null gps", "null gps");
					return;
				}

				if (position.Latitude.ToString() != null)
				{
					requestEventLoggingDTO.Latitude = "" + position.Latitude;
				}
				else
				{
					requestEventLoggingDTO.Latitude = "0";
				}

				if (position.Longitude.ToString() != null)
				{
					requestEventLoggingDTO.Longitude = "" + position.Longitude;
				}
				else
				{
					requestEventLoggingDTO.Longitude = "0";
				}
				requestEventLoggingDTO.EventType = eventtype;
				requestEventLoggingDTO.EventDescription = description;
				requestEventLoggingDTO.EventAdditionalInfoID = additionalId;
				requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
				requestEventLoggingDTO.DeviceType = "IOS";
				requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
				requestEventLoggingDTO.AppVersion = "1";

				EventLog();
				//  Application.Current.MainPage = new SplashScreen();


			}
			catch (Exception ex)
			{
				Debug.WriteLine(" " + ex);

			}

		}

		public async Task<ServiceResponse> EventLog()
		{
			ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
			return serviceResponse;
		}

	}
}
