﻿using System;
using System.Collections.Generic;
using Fanspick.Helper;
using Fanspick.Shared.Models.DTO;
using Fanspick.ViewModels;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Fanspick.PopUp
{
    public partial class FormationOption : PopupPage
    {
        public static bool IsLive { get; set; }
		App_MainViewModel vmApp_Main;
        public FormationOption()
        {
            InitializeComponent();

            BindingContext = lstFormations;

			vmApp_Main = CacheStorage.ViewModels.App_Main;
        }

		public List<Formation> lstFormations
		{
			get
			{
				return Fanspick.Shared.Helper.FanspickCache.Formations;
			}
		}

		private void OnClose(object sender, EventArgs e)
		{
			PopupNavigation.PopAsync();
		}

        private void lstSelectFormation_OnSelectedItem(object sender, SelectedItemChangedEventArgs e)
        {
            Formation selectedFormation = (Formation)e.SelectedItem;

            Screens.HomePage.homePageVM.YourPickViewModelPre.PitchData.ChangePitchFormation(selectedFormation.type);

            OnClose(this, EventArgs.Empty);
        }
    }
}
