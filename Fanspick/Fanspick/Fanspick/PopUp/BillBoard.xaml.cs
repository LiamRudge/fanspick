﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Fanspick.Helper;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Plugin.DeviceInfo;
using Plugin.Geolocator;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;

namespace Fanspick.PopUp
{
    public partial class BillBoard : PopupPage
    {
		 RequestEventLoggingDTO requestEventLoggingDTO  = new RequestEventLoggingDTO();
        public BillBoard()
        {
            InitializeComponent();

            if (FanspickCache.isPitch){
                PopImage.Source = FanspickCache.PitchPopUp;
            }else{
                PopImage.Source = FanspickCache.BannerPopUp;
            }

             //PopImage.Source = "PopUp_Page_v4.png";

        }

        void ClosePop(object sender, EventArgs e)
        {
			PopupNavigation.PopAsync();
        }

		void OpenBanner(object sender, EventArgs e)
		{

			if (FanspickCache.isPitch)
			{
                Screens.SplashScreen.openUrl = FanspickCache.PitchUrl;
			}
			else
			{
                Screens.SplashScreen.openUrl = FanspickCache.BannerUrl;
			}

            Navigation.PushModalAsync(new Screens.WebviewLink(), true);

		}


		public async void GetInfo(String eventtype, String description)
		{
			try
			{

				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

				if (position == null)
				{
					Debug.WriteLine("null gps", "null gps");
					return;
				}

				if (position.Latitude.ToString() != null)
				{
					requestEventLoggingDTO.Latitude = "" + position.Latitude;
				}
				else
				{
					requestEventLoggingDTO.Latitude = "0";
				}

				if (position.Longitude.ToString() != null)
				{
					requestEventLoggingDTO.Longitude = "" + position.Longitude;
				}
				else
				{
					requestEventLoggingDTO.Longitude = "0";
				}
				requestEventLoggingDTO.EventType = eventtype;
				requestEventLoggingDTO.EventDescription = description;
				requestEventLoggingDTO.EventAdditionalInfoID = "" + FanspickCache.UserData.AccessToken;
				requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
				requestEventLoggingDTO.DeviceType = "IOS";
				requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
				requestEventLoggingDTO.AppVersion = "1";

                await EventLog();
				//  Application.Current.MainPage = new SplashScreen();


			}
			catch (Exception ex)
			{
				Debug.WriteLine(" " + ex);

			}

		}

		public async Task<ServiceResponse> EventLog()
		{
			ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
			return serviceResponse;
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
        }

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{
                CloseBtn.WidthRequest = 50;
                CloseBtn.Margin = new Thickness(10, 10, 120, 10);
				
			}
			else
			{
				CloseBtn.WidthRequest = 30;
				CloseBtn.Margin = new Thickness(10, 10, 10, 10);

			}


		}

        	
    
    }
}
