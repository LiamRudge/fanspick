﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Fanspick.PluginModels;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Fanspick.PopUp
{
    public partial class OnBoardingPopup : PopupPage
    {
        public Walkthrough _vm;
        public OnBoardingPopup()
        {
            InitializeComponent();
            BindingContext = _vm = new Walkthrough();
        }


        async void ClosePop(object sender, EventArgs e)
        {

            try{

				RequestLoginStatusDTO msg = new RequestLoginStatusDTO();

				msg.AccessToken = FanspickCache.UserData.AccessToken;
				msg.OnceLogin = "true";

				ServiceResponse serviceMResponse = await new FanspickServices<LoginStatusService>().Run(msg);

				FanspickCache.UserData.OnceLogin = "true";

				if (serviceMResponse.OK)
				{

				}

                
            }catch(Exception ex){
                Debug.WriteLine(ex.ToString());
				FanspickCache.UserData.OnceLogin = "true";
            }

            await PopupNavigation.PopAsync();
        }

		public void hideswipe(object sender, EventArgs e)
		{
			if (swipe.IsVisible)
			{
				swipe.IsVisible = false;
			}

		}
    }
}
