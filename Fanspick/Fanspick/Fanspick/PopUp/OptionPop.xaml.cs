﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Fanspick.Helper;
using Fanspick.ViewModels;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Fanspick.PopUp
{
    public partial class OptionPop : PopupPage
    {
        App_MainViewModel vmApp_Main;
        public OptionPop()
        {
            InitializeComponent();
			vmApp_Main = CacheStorage.ViewModels.App_Main;
        }

		private void OnClose(object sender, EventArgs e)
		{
			PopupNavigation.PopAsync();
		}

		protected override Task OnAppearingAnimationEnd()
		{
			return Content.FadeTo(0.5);
		}

		protected override Task OnDisappearingAnimationBegin()
		{
			return Content.FadeTo(1);
		}

		void OpenPlayer(object sender, EventArgs e)
		{
            PopupNavigation.PopAsync();
            //App.nav.PushAsync(new Screens.PlayerInformationPage(), true);
		}
    }
}
