﻿﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fanspick.PopUp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContextMenu : PopupPage
	{
        public ContextMenu(){
            InitializeComponent();
        }

		private void OnClose(object sender, EventArgs e)
		{
			PopupNavigation.PopAsync();
		}

		protected override Task OnAppearingAnimationEnd()
		{
			return Content.FadeTo(0.5);
		}

		protected override Task OnDisappearingAnimationBegin()
		{
			return Content.FadeTo(1);
		}
	}
}
