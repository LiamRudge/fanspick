﻿using System;
using System.Collections.Generic;
using Fanspick.Shared.Models.DTO;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Fanspick.PopUp
{
    public partial class FormationPopUp : PopupPage
    {
        public FormationPopUp()
        {
            InitializeComponent();
        }


        public List<Formation> lstFormations		
        {
            get{
                return Fanspick.Shared.Helper.FanspickCache.Formations;
            }
        }

		private void OnClose(object sender, EventArgs e)
		{
			PopupNavigation.PopAsync();
		}
    }
}
