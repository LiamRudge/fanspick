﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fanspick.RestAPI;
using Fanspick.Screens;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.DTO;
using Refit;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Fanspick.PopUp
{
    public partial class OTPpopup : PopupPage
    {
        string number;
        public OTPpopup(string phonenumber)
        {
            InitializeComponent();
            number = phonenumber;
            sendOTP(phonenumber);
        }

      async  void Handle_Clicked(object sender, System.EventArgs e)
        {
         
            try
            {
                RequestVerifyOtpDTO request = new RequestVerifyOtpDTO();
                request.OTP = labelOTP.Text;

                var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
                var response = await gitHubApi.VerifyOTP(request);

                if (response.StatusCode == 200)
                {
                    // Application.Current.MainPage = new SignUpDefaultTeam();
                    FanspickCache.isphoneValid = true;
					OnClose(this, EventArgs.Empty);
                    await DisplayAlert("Verifed", "Your phone number has been verifed please proceed to create your account", "ok");
                   
                }
                else
                {
                    await DisplayAlert("InCorrect OTP", "OTP does not match", "ok");
                }

            }
            catch (ApiException exp)
            {
				var content = exp.GetContentAs<Dictionary<String, String>>();
				await DisplayAlert("Fanspick", content.Values.Skip(2).First(), "ok");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                await DisplayAlert("InCorrect OTP", "OTP does not match", "ok");
            }
        }

        public async void sendOTP(string phone)
        {
            try
            {

                RequestGenerateOtpDTO request = new RequestGenerateOtpDTO();
                request.PhoneNumber = phone;
                request.signUp = true;

                var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
                var response = await gitHubApi.GenerateOTP(request);

                if (response.StatusCode == 200)
                {
                    await DisplayAlert("Sucess", "OTP has been send you phone number "+ phone +" for verification", "ok");
                }
                else
                {
                    await DisplayAlert("Error", "Please make sure that the number has been registered with Fanspick. We wont be able to generate the number if you have not verified", "ok");
                    OnClose(this, EventArgs.Empty);
                }

            }
            catch (ApiException exp)
            {
				OnClose(this, EventArgs.Empty);
				var content = exp.GetContentAs<Dictionary<String, String>>();
				await DisplayAlert("Fanspick", content.Values.Skip(2).First(), "ok");
              
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
				OnClose(this, EventArgs.Empty);
                await DisplayAlert("Error", "Please make sure that the number has been registered with Fanspick. We wont be able to generate the number if you have not verified", "ok");
               
            }
        }

        public void resend(object sender, EventArgs e){
            sendOTP(number);

        }

 

		private void OnClose(object sender, EventArgs e)
		{
			PopupNavigation.PopAsync();
		}

    
    }


}
