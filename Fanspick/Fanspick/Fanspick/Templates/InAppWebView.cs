﻿using Fanspick.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Fanspick.Shared.Helper;

namespace Fanspick.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InAppWebView : ContentPage
    {
        public InAppWebView()
        {
            InitializeComponent();

            LocalWebView.Source = FanspickCache.CurrentFanspickScope.CurrentlyUrlToVisit;
        }
    }
}