﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Helper;
using Fanspick.ViewModels;
using Xamarin.Forms;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using System.IO;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using static Fanspick.Shared.Models.DTO.GetTeamDataDTO;
using System.Diagnostics;
using Fanspick.Shared.Events.Events;
using System.Collections.ObjectModel;
using Plugin.Geolocator;
using Plugin.DeviceInfo;
using static Fanspick.Shared.Models.DTO.MinuteStatsDTO;
using Rg.Plugins.Popup.Extensions;
using DK.SlidingPanel.Interface;
using Newtonsoft.Json.Linq;
using Quobject.SocketIoClientDotNet.Client;
using static Fanspick.Shared.Models.DTO.MatchCommentriesDTO;
using Newtonsoft.Json;
using Fanspick.PopUp;
using Fanspick.Screens;
using Refit;
using Fanspick.RestAPI;
using Plugin.Connectivity;

namespace Fanspick.Templates
{
	public partial class FanspickPageMain : ContentPage
	{
		public async void SharePick(object sender, EventArgs e)
		{
			if (totalPlayersOnPitch == 11)
			{
				await DependencyService.Get<IScreenCapture>().Share("Fanspick!", "Check out what the fans think with Fanspick.", null);
			}
			else
			{
				await DisplayAlert("Warning!", "Pitch does not have all players to share the pitch", "ok");
			}
		}

        void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            isScrollAvailable = true; 
        }

        void Handle_FocusedList(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            isScrollAvailable = false;
        }

        Boolean isScrollAvailable;
        int totalPlayersOnPitch;

		void keyBoardShown(object sender, EventArgs e)
		{
            Debug.WriteLine("keyboardVisible");
            CommunityList.Margin = new Thickness(0, 50, 0, 0);
            labelGap.IsVisible = false;
            Fanspick.Constants.raiseAbove = 0;
		}

		void keyBoardHide(object sender, EventArgs e)
		{
			Debug.WriteLine("keyboardhidden");
            CommunityList.Margin = new Thickness(0, 200, 0, 0);
            labelGap.IsVisible = false;
            Fanspick.Constants.raiseAbove = 0;
		}



		public FanspickPageMain()
		{
			InitializeComponent();
			vmApp_Main = CacheStorage.ViewModels.App_Main;
			BindingContext = this;
			LocalTeamImage.Source = FanspickCache.CurrentFanspickScope.SelectedFixture.LocalTeamImage;
            VisitorTeamImage.Source = FanspickCache.CurrentFanspickScope.SelectedFixture.VisitorTeamImage;


			var keyboardService = Xamarin.Forms.DependencyService.Get<IKeyboardService>();

            keyboardService.KeyboardIsShown += keyBoardShown;
            keyboardService.KeyboardIsHidden += keyBoardHide;

			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{

				if (CrossConnectivity.Current.IsConnected)
				{
					InternetScreen.IsVisible = false;
				}
				else
				{
					InternetScreen.IsVisible = true;
				}


			};

			socket = IO.Socket(CreateUri());
			socket.Connect();

			socket.On(Socket.EVENT_CONNECT, (data) =>
		{


		});

			socket.On(Socket.EVENT_DISCONNECT, (data) =>
		   {
		   });

			socket.On(Socket.EVENT_MESSAGE, (data) =>
		   {
			   GetChatHistory();
		   });

			socket.On("messageFromServer", (data) =>
		   {

		   });

			socket.On("onOtherMessage", (data) =>
		   {

			   MessageFromSocket dto = Newtonsoft.Json.JsonConvert.DeserializeObject<MessageFromSocket>(data.ToString());
			   DateTime current = DateTime.Now;
			   String utcTime = current.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");

			   CommunityPosts getMessage = new CommunityPosts();
			   User userid = new User();

			   userid.Username = dto.MsgSocket.userName;
			   userid.Sender_id = dto.MsgSocket.uID;

			   getMessage.Id = dto.MsgSocket.topicId;
			   getMessage.Message = dto.MsgSocket.message;
			   getMessage.UserId = userid;
			   getMessage.CurrentUserId = FanspickCache.UserData.Id;
			   getMessage.Time = utcTime;
               getMessage.isConnect = true;
			   CommunityPosts.Add(getMessage);

                if(!isScrollAvailable){

				   //Task.Delay(3000);
				   //newMsg.IsVisible = false;

                }

			   

			   ScrollToLast();

            });


		}


		double youVsFans = 0;
		double FansVsManager = 0;
		double youVsManager = 0;
		public ResponseMinuteStatsDTO userManagerPercentageResponse1 = null;
		String selectedTopicId;
		private string matchHalves = "";
		DateTime fixtureDate;
		FanspickPitchEventHandler fanspickPitchEventHandler;
		List<String> playerListRow1 = new List<String>();
		List<String> playerListRow2 = new List<String>();
		List<String> playerListRow3 = new List<String>();
		List<String> playerListRow4 = new List<String>();
		private List<Squad> selectablePlayers;
		public List<Squad> SelectablePlayers
		{
			get
			{
				return selectablePlayers;
			}
			set
			{
				selectablePlayers = value;
				foreach (var item in selectablePlayers)
				{
					if (item.Role == "G")
					{
						item.shirt = "goalkeeper.png";
					}
					else
					{
						item.shirt = RenderedShirt;
					}
				}
			}
		}

		ObservableCollection<CommunityPosts> communityPosts = null;
		public ObservableCollection<CommunityPosts> CommunityPosts
		{
			get
			{
				if (communityPosts == null)
					communityPosts = new ObservableCollection<CommunityPosts>();
				//socialFeedData.Shuffle();
				return communityPosts;
			}
			set
			{
				this.communityPosts = value;
			}
		}

		public async Task<ServiceResponse> GetChatHistory()
		{
			ServiceResponse serviceResponse = await new FanspickServices<ChatHistoryService>().Run(requestChatHistoryDTO);
			return serviceResponse;
		}


		public async Task<ServiceResponse> GetTopicByFixture()
		{
			ServiceResponse serviceResponse = await new FanspickServices<TopicByFixtureService>().Run(requestTopicByFixtureDTO);
			return serviceResponse;
		}

		public RequestChatHistoryDTO requestChatHistoryDTO { get; set; } = new RequestChatHistoryDTO();
		public RequestTopicByFixtureDTO requestTopicByFixtureDTO { get; set; } = new RequestTopicByFixtureDTO();


		private async void GetTopicIdService()
		{

			requestTopicByFixtureDTO.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
			requestTopicByFixtureDTO.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
			requestTopicByFixtureDTO.AccessToken = FanspickCache.UserData.AccessToken;

			Shared.Models.Service.ServiceResponse response = await GetTopicByFixture();

			if (response.OK)
			{
				ResponseTopicByFixtureDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseTopicByFixtureDTO>(response.Data);

				if (dto.Data.Count != 0)
				{
					GetChatHistoryService(dto.Data.First()._id);
					selectedTopicId = dto.Data.First()._id;
				}

			}

			JObject o = new JObject();
			o.Add("topicId", selectedTopicId);
			o.Add("userId", FanspickCache.UserData.AccessToken);

			socket.Emit("createRoom", o);


		}


		private async void GetChatHistoryService(String topicID)
		{

			CommunityPosts.Clear();

			requestChatHistoryDTO.TopicId = topicID;
			requestChatHistoryDTO.AccessToken = FanspickCache.UserData.AccessToken;

			Shared.Models.Service.ServiceResponse response = await GetChatHistory();

			if (response.OK)
			{
				ResponseChatHistoryDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseChatHistoryDTO>(response.Data);

				foreach (CommunityPosts sf in dto.Data[0].FanspickPosts)
				{

                    sf.isConnect = true;
					sf.CurrentUserId = FanspickCache.UserData.Id;

                    if (sf.UserId.Sender_id == sf.CurrentUserId){
                        sf.UserPic = "http://fanspick-admin-test.5sol.co.uk/Uploads/" + FanspickCache.UserData.Photo;
                    }

					CommunityPosts.Add(sf);

					var v = CommunityList.ItemsSource.Cast<object>().LastOrDefault();
					CommunityList.ScrollTo(v, ScrollToPosition.End, true);

				}

			}


		}



		Socket socket;


		void Handle_Clicked(object sender, System.EventArgs e)
		{
			if (!String.IsNullOrEmpty(txtMessage.Text))
			{

				DateTime current = DateTime.Now;
				DateTime convertedFixtureDate = TimeZoneInfo.ConvertTime(current, TimeZoneInfo.Utc);
				String nowTime = convertedFixtureDate.ToString("yyyy-MM-dd HH:MM");
				String utcTime = current.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");

				JObject o = new JObject();
				o.Add("topicId", selectedTopicId);
				o.Add("userId", FanspickCache.UserData.AccessToken);
				o.Add("type", "message");
				o.Add("chatType", "fanspick");
				o.Add("message", txtMessage.Text);
                o.Add("time", nowTime);

				socket.Emit("new message", o);

				CommunityPosts getMessage = new CommunityPosts();
				User userid = new User();

				userid.Username = FanspickCache.UserData.Username;
				userid.Sender_id = FanspickCache.UserData.Id;

				getMessage.Id = "0";
				getMessage.Message = txtMessage.Text;
				getMessage.UserId = userid;
				getMessage.CurrentUserId = FanspickCache.UserData.Id;
                getMessage.Time = utcTime;
                getMessage.UserPic = "http://fanspick-admin-test.5sol.co.uk/Uploads/"+FanspickCache.UserData.Photo;


				if (CrossConnectivity.Current.IsConnected)
				{
					getMessage.isConnect = true;
				}
				else
				{
					getMessage.isConnect = false;
				}


				CommunityPosts.Add(getMessage);
				txtMessage.Text = "";
				ScrollToLast();

			}
		}

		public class MessageFromSocket
		{
			[JsonProperty(PropertyName = "message")]
			public Message MsgSocket { get; set; }

		}

		public void ScrollToLast()
		{

			try
			{
				Device.BeginInvokeOnMainThread(() =>
				{
					try
					{

                        if(isScrollAvailable){
                            
							var last = CommunityList.ItemsSource.Cast<object>().LastOrDefault();
							CommunityList.ScrollTo(last, ScrollToPosition.MakeVisible, false);    
                        }else{
                           
                        }
					
					}
					catch (Exception ex)
					{
                        Debug.WriteLine(ex.ToString());
					}

				});


			}
			catch (Exception ex)
			{
                Debug.WriteLine(ex.ToString());
			}

		}

		public class Message
		{
			[JsonProperty(PropertyName = "message")]
			public string message { get; set; }

			[JsonProperty(PropertyName = "topicId")]
			public string topicId { get; set; }

			[JsonProperty(PropertyName = "userName")]
			public string userName { get; set; }

			[JsonProperty(PropertyName = "userId")]
			public string uID { get; set; }

		}

		private void ScrollViewLast()
		{

			Device.BeginInvokeOnMainThread(() =>
			{

				try
				{
					var v = CommunityList.ItemsSource.Cast<object>().LastOrDefault();
					CommunityList.ScrollTo(v, ScrollToPosition.End, true);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
				}



			});


		}

		public string CreateUri()
		{
			var options = CreateOptions();
			var uri = string.Format("{0}://{1}:{2}", options.Secure ? "https" : "http", options.Hostname, options.Port);
			return uri;
		}


		public IO.Options CreateOptions()
		{


			var config = ConfigBase.Load();
			var options = new IO.Options();
			options.Port = config.server.port;
			options.Hostname = config.server.hostname;
			options.ForceNew = true;

			return options;
		}


		public class ConfigBase
		{
			public ConfigServer server { get; set; }

			public static ConfigBase Load()
			{
				var result = new ConfigBase()
				{
					server = new ConfigServer()
				};
				result.server.hostname = "52.163.48.178";
				result.server.port = 8000;

				return result;
			}
		}

		// fplive.ukwest.cloudapp.azure.com
		// 52.163.48.178

		public class ConfigServer
		{
			public string hostname { get; set; }
			public int port { get; set; }
		}





		private ImageSource renderedShirt;
		public ImageSource RenderedShirt
		{
			get
			{
				if (renderedShirt == null)
				{
					renderedShirt = ConvertToImage(TeamShirtImage);
				}
				return renderedShirt;
			}
		}

		public string TeamShirtImage { get; private set; }
		public static ImageSource ConvertToImage(string imageBase64)
		{
			try
			{
				ImageSource image;

				if (string.IsNullOrEmpty(imageBase64))
				{
					image = Xamarin.Forms.ImageSource.FromFile("no_shirt.png");
                    return image;
				}
				else
				{
					using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(imageBase64)))
					{
						return Xamarin.Forms.ImageSource.FromStream(() => ms);
					}
				}
				
			}
			catch (Exception ex)
			{
                Debug.WriteLine(ex.ToString());
				return Xamarin.Forms.ImageSource.FromFile("no_shirt.png");
			}

		}

		Boolean isLive = false;
		App_MainViewModel vmApp_Main;
		int tapCount = 0;
		int tapCountSw = 0;

		void Openbanner(object sender, EventArgs e)
		{
            FanspickCache.isPitch = false;
            var _billBoardPopup = new BillBoard();
			Navigation.PushPopupAsync(_billBoardPopup);
		}
		void OpenbannerPitch(object sender, EventArgs e)
		{
			FanspickCache.isPitch = true;
			var _billBoardPopup = new BillBoard();
			Navigation.PushPopupAsync(_billBoardPopup);
		}

		protected async override void OnAppearing()
		{
			base.OnAppearing();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);

            await Navigation.PopAllPopupAsync(true);
			hideAllPositions();
			getPlayerShirt();
			RemoveSpacing();
			LoadGif();
			GetTopicIdService();
			fanspickPitchEventHandler = new FanspickPitchEventHandler();
            fanspickPitchEventHandler.OnPreMatchEnd += FanspickPitchEventHandler_OnPreMatchEnd;

			ListOut.ItemTapped += DeselectItem;
			ListIn.ItemTapped += DeselectItem;
			ListOut.ItemSelected += DeselectItem;
			ListIn.ItemSelected += DeselectItem;
			Action15.ItemTapped += DeselectItem;
			Action15.ItemSelected += DeselectItem;
			Action30.ItemTapped += DeselectItem;
			Action30.ItemSelected += DeselectItem;
			Action45.ItemTapped += DeselectItem;
			Action45.ItemSelected += DeselectItem;
			Action60.ItemTapped += DeselectItem;
			Action60.ItemSelected += DeselectItem;
			Action75.ItemTapped += DeselectItem;
			Action75.ItemSelected += DeselectItem;
			Action90.ItemTapped += DeselectItem;
			Action90.ItemSelected += DeselectItem;




		}
		public void DeselectItem(object sender, EventArgs e)
		{
			((ListView)sender).SelectedItem = null;
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();

			SlidingPanelConfig config = new SlidingPanelConfig();
			config.MainView = GetMainStackLayout();

			spTest.ApplyConfig(config);

			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}

		private AbsoluteLayout GetMainStackLayout()
		{
			AbsoluteLayout mainStackLayout = new AbsoluteLayout();
			mainStackLayout.VerticalOptions = LayoutOptions.FillAndExpand;
			mainStackLayout.HorizontalOptions = LayoutOptions.FillAndExpand;

			return (mainStackLayout);
		}

		public async void getPlayerShirt()
		{
			try
			{
				actInd.IsVisible = true;
				RequestGetTeamDataDTO getTeamDataRequest = new RequestGetTeamDataDTO();
				getTeamDataRequest.AccessToken = FanspickCache.UserData.AccessToken;
				getTeamDataRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
				ServiceResponse getTeamDataResponse = await new FanspickServices<GetTeamDataService>().Run(getTeamDataRequest);
				ResponseGetTeamDataDTO teamDataResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetTeamDataDTO>(getTeamDataResponse.Data);

				if (teamDataResponse.Data.TeamShirtURL == null)
				{
					FanspickCache.CurrentFanspickScope.TeamShirtImage = "no_shirt";
					actInd.IsVisible = false;

				}
				else
				{
					FanspickCache.CurrentFanspickScope.TeamShirtImage = "http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamShirts/" + teamDataResponse.Data.TeamShirtURL.Replace(" ", "%20") + ".png";
					actInd.IsVisible = false;
				}
				if (!FanspickCache.showLIve)
				{
					SwIcon.Source = "swtich_off";

					isLive = false;

					hideAllPositions();

					GetFanspick(isLive);
				}
				else
				{
					SwIcon.Source = "swtich_on";

					isLive = true;

					hideAllPositions();

					GetFanspick(isLive);

				}
			}
			catch (Exception ex)
			{
                Debug.WriteLine(ex.ToString());
				FanspickCache.CurrentFanspickScope.TeamShirtImage = "no_shirt";
				GetFanspick(isLive);
			}

		}

		private async void GetFanspick(Boolean isLive)
		{
            totalPlayersOnPitch = 0;
			if (isLive)
			{
                if (FanspickPitchEventHandler.MatchStage != MatchStage.PreMatch || FanspickPitchEventHandler.MatchStage != MatchStage.RunUp)
				{

					GetInfo("fansPick", "fanspick access for fixture " + FanspickCache.CurrentFanspickScope.SelectedFixture.Id);

					actInd.IsVisible = true;
					playerListRow1.Clear();
					playerListRow2.Clear();
					playerListRow3.Clear();
					playerListRow4.Clear();


                    try
                    {
                        var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

                        RequestGetFanspickRefitDTO getUserPickRequest = new RequestGetFanspickRefitDTO();
						getUserPickRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
						getUserPickRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
						getUserPickRequest.IsLive = isLive;

                        var header = "Bearer " + FanspickCache.UserData.AccessToken;
                        var response = await gitHubApi.GetFanspick(getUserPickRequest, header);

                        if (response.StatusCode == 200)
                        {

                            ShowPlayers(response.Data.LineUpPlayers);

							actInd.IsVisible = false;
                            FanspickNullImage.IsVisible = false;
                        
                        }

                    }catch(ApiException ex){
                        Debug.WriteLine(ex.ToString());
						FanspickNullImage.IsVisible = true;
						actInd.IsVisible = false;

					}
					catch (Exception ex)
					{
						FanspickNullImage.IsVisible = true;
						actInd.IsVisible = false;
						Debug.WriteLine(ex.ToString());
					}

				}
                else
                    {
                    FanspickNullImage.IsVisible = true;
                    actInd.IsVisible = false;
                }

			}
			else
			{
				GetInfo("fansPick", "fanspick access for fixture " + FanspickCache.CurrentFanspickScope.SelectedFixture.Id);

				actInd.IsVisible = true;
				playerListRow1.Clear();
				playerListRow2.Clear();
				playerListRow3.Clear();
				playerListRow4.Clear();

				try
				{
					var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

					RequestGetFanspickRefitDTO getUserPickRequest = new RequestGetFanspickRefitDTO();
					getUserPickRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
					getUserPickRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
					getUserPickRequest.IsLive = isLive;

                    String header = "Bearer " + FanspickCache.UserData.AccessToken;
					var response = await gitHubApi.GetFanspick(getUserPickRequest, header);

                    if(response == null){
                        FanspickNullImage.IsVisible = true;
						actInd.IsVisible = false;
                    }else{

						if (response.StatusCode == 200)
						{
                            
							ShowPlayers(response.Data.LineUpPlayers);

							//if (response.Data.LineUpPlayers.Count == 0)
							//{
							//  FanspickNullImage.IsVisible = true;
							//}
							//else if (response.Data.LineUpPlayers.Count < 11)
							//{
							//  FanspickNullImage.IsVisible = true;
							//}

							//else
							//{
							//  FanspickNullImage.IsVisible = false;
							//  ShowPlayers(response.Data.LineUpPlayers);
							//}
                            actInd.IsVisible = false;
                            FanspickNullImage.IsVisible = false;

						}



					}

                    actInd.IsVisible = false;

					
				}
				catch (ApiException ex)
				{
                    Debug.WriteLine(ex.ToString());
					FanspickNullImage.IsVisible = true;
					actInd.IsVisible = false;

				}
				catch (Exception ex)
				{
                    FanspickNullImage.IsVisible = false;
                    actInd.IsVisible = false;
					Debug.WriteLine(ex.ToString());
				}

			}
		}



		protected override void OnDisappearing()
		{

			//FanspickNullImage.Opacity = 0;
			base.OnDisappearing();
		}

		private async void ShowPlayers(List<LineUpPlayerData> showPlayer)
		{

			List<SelectUserPickPlayer> playerList = new List<SelectUserPickPlayer>();
			for (int i = 0; i < showPlayer.Count(); i++)
			{
                if (showPlayer[i] != null && showPlayer[i].Position != null )
				{

					SelectUserPickPlayer selectedPlayer = new SelectUserPickPlayer();
					selectedPlayer.PosX = showPlayer[i].Position.PosX;
					selectedPlayer.PosY = showPlayer[i].Position.PosY;
					selectedPlayer.PositionId = showPlayer[i].Position.Id;


                    if(showPlayer[i].Player == null){	
						selectedPlayer.PlayerName = "Player N/A";
						selectedPlayer.PLayerId = "0";
                    }else{
						selectedPlayer.PlayerName = showPlayer[i].Player.KnownName;
						selectedPlayer.PLayerId = showPlayer[i].Player.Id;
					
                    }


					if (showPlayer[i].userActions.Count > 0)
					{

						if (showPlayer[i].userActions.Count == 1)
						{

							selectedPlayer.Action = showPlayer[i].userActions[0].Action;

						}
						else
						{
							selectedPlayer.Action = showPlayer[i].userActions[0].Action + "-" + showPlayer[i].userActions[1].Action;
						}


					}
					else
					{
						selectedPlayer.Action = "0";
					}

					ImageSource shirtimage = "";

					enablePlayers(selectedPlayer.PosX, selectedPlayer.PosY, selectedPlayer.PlayerName, FanspickCache.CurrentFanspickScope.TeamShirtImage, selectedPlayer.Action, "goalkeeper");
					playerList.Add(selectedPlayer);

					ArrangePlayerLine(showPlayer[i].Position.PosX, showPlayer[i].Position.PosY);
				}

				FanspickCache.SelectFansPick = playerList;


				ArrangementOfPlayers(playerListRow1, playerListRow2, playerListRow3, playerListRow4);

			}



			 totalPlayersOnPitch = playerListRow1.Count + playerListRow2.Count + playerListRow3.Count + playerListRow4.Count + 1;


			//if (totalPlayersOnPitch == 0)
			//{
			//	hideAllPositions();
			//	FanspickNullImage.IsVisible = true;
			//}
			//else if (totalPlayersOnPitch < 11)
			//{
			//	hideAllPositions();
			//	FanspickNullImage.IsVisible = true;
			//}
			//else
			//{
			//	FanspickNullImage.IsVisible = false;
			//}

		}




		public void enablePlayers(String Posx, String Posy, String PlayerName, ImageSource playerImage, String action, String ImageGoalKeeper)
		{

			if (Posx == "1" && Posy == "3")
			{
				Stack_1_3.IsVisible = true;
				PlayerName_1_3.Text = PlayerName;
				PlayerImage_1_3.Source = ImageGoalKeeper;
				if (action != "0")
				{
					PlayerImagePop_1_3.IsVisible = true;
					if (action == "star") { PlayerImagePop_1_3.Source = "shirt_st"; }
					else if (action == "hairdryer") { PlayerImagePop_1_3.Source = "shirt_hd"; }
					else if (action == "manofmatch") { PlayerImagePop_1_3.Source = "shirt_mom"; }
					else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_1_3.Source = "shirt_mom+s"; }
					else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_1_3.Source = "shirt_mom+h"; }
				}
            }

			if (Posx == "2")
			{
				if (Posy == "1")
				{
					Stack_2_1.IsVisible = true;
					PlayerImage_2_1.Source = playerImage;
					PlayerName_2_1.Text = PlayerName;
					PlayerImage_2_1.Source = playerImage;

					if (action != "0")
					{
						PlayerImagePop_2_1.IsVisible = true;
						if (action == "star") { PlayerImagePop_2_1.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_2_1.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_2_1.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_1.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_1.Source = "shirt_mom+h"; }
					}

				}
				if (Posy == "2")
				{
					Stack_2_2.IsVisible = true;
					PlayerImage_2_2.Source = playerImage;
					PlayerName_2_2.Text = PlayerName;
					PlayerImage_2_2.Source = playerImage;

					if (action != "0")
					{
						PlayerImagePop_2_2.IsVisible = true;
						if (action == "star") { PlayerImagePop_2_2.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_2_2.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_2_2.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_2.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_2.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "3")
				{
					Stack_2_3.IsVisible = true;
					PlayerImage_2_3.Source = playerImage;
					PlayerName_2_3.Text = PlayerName;
					PlayerImage_2_3.Source = playerImage;

					if (action != "0")
					{
						PlayerImagePop_2_3.IsVisible = true;
						if (action == "star") { PlayerImagePop_2_3.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_2_3.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_2_3.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_3.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_3.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "4")
				{
					Stack_2_4.IsVisible = true;
					PlayerImage_2_4.Source = playerImage;
					PlayerName_2_4.Text = PlayerName;
					PlayerImage_2_4.Source = playerImage;

					if (action != "0")
					{
						PlayerImagePop_2_4.IsVisible = true;
						if (action == "star") { PlayerImagePop_2_4.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_2_4.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_2_4.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_4.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_4.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "5")
				{
					Stack_2_5.IsVisible = true;
					PlayerImage_2_5.Source = playerImage;
					PlayerName_2_5.Text = PlayerName;
					PlayerImage_2_5.Source = playerImage;

					if (action != "0")
					{
						PlayerImagePop_2_5.IsVisible = true;
						if (action == "star") { PlayerImagePop_2_5.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_2_5.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_2_5.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_5.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_5.Source = "shirt_mom+h"; }
					}
				}

			}

			if (Posx == "3")
			{
				if (Posy == "1")
				{
					Stack_3_1.IsVisible = true;
					PlayerImage_3_1.Source = playerImage;
					PlayerName_3_1.Text = PlayerName;
					PlayerImage_3_1.Source = playerImage;

					if (action != "0")
					{
						PlayerImagePop_3_1.IsVisible = true;
						if (action == "star") { PlayerImagePop_3_1.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_3_1.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_3_1.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_1.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_1.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "2")
				{
					Stack_3_2.IsVisible = true;
					PlayerImage_3_2.Source = playerImage;
					PlayerName_3_2.Text = PlayerName;
					PlayerImage_3_2.Source = playerImage;

					if (action != "0")
					{
						PlayerImagePop_3_2.IsVisible = true;
						if (action == "star") { PlayerImagePop_3_2.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_3_2.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_3_2.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_2.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_2.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "3")
				{
					Stack_3_3.IsVisible = true;
					PlayerImage_3_3.Source = playerImage;
					PlayerName_3_3.Text = PlayerName;
					PlayerImage_3_3.Source = playerImage;

					if (action != "0")
					{
						PlayerImagePop_3_3.IsVisible = true;
						if (action == "star") { PlayerImagePop_3_3.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_3_3.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_3_3.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_3.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_3.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "4")
				{
					Stack_3_4.IsVisible = true;
					PlayerImage_3_4.Source = playerImage;
					PlayerName_3_4.Text = PlayerName;
					PlayerImage_3_4.Source = playerImage;
					if (action != "0")
					{
						PlayerImagePop_3_4.IsVisible = true;
						if (action == "star") { PlayerImagePop_3_4.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_3_4.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_3_4.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_4.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_4.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "5")
				{
					Stack_3_5.IsVisible = true;
					PlayerImage_3_5.Source = playerImage;
					PlayerName_3_5.Text = PlayerName;
					PlayerImage_3_5.Source = playerImage;
					if (action != "0")
					{
						PlayerImagePop_3_5.IsVisible = true;
						if (action == "star") { PlayerImagePop_3_5.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_3_5.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_3_5.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_5.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_5.Source = "shirt_mom+h"; }
					}
				}

			}

			if (Posx == "4")
			{
				if (Posy == "1")
				{
					Stack_4_1.IsVisible = true;
					PlayerImage_4_1.Source = playerImage;
					PlayerName_4_1.Text = PlayerName;
					PlayerImage_4_1.Source = playerImage;
					if (action != "0")
					{
						PlayerImagePop_4_1.IsVisible = true;
						if (action == "star") { PlayerImagePop_4_1.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_4_1.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_4_1.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_1.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_1.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "2")
				{
					Stack_4_2.IsVisible = true;
					PlayerImage_4_2.Source = playerImage;
					PlayerName_4_2.Text = PlayerName;
					PlayerImage_4_2.Source = playerImage;
					if (action != "0")
					{
						PlayerImagePop_4_2.IsVisible = true;
						if (action == "star") { PlayerImagePop_4_2.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_4_2.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_4_2.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_2.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_2.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "3")
				{
					Stack_4_3.IsVisible = true;
					PlayerImage_4_3.Source = playerImage;
					PlayerName_4_3.Text = PlayerName;
					PlayerImage_4_3.Source = playerImage;
					if (action != "0")
					{
						PlayerImagePop_4_3.IsVisible = true;
						if (action == "star") { PlayerImagePop_4_3.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_4_3.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_4_3.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_3.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_3.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "4")
				{
					Stack_4_4.IsVisible = true;
					PlayerImage_4_4.Source = playerImage;
					PlayerName_4_4.Text = PlayerName;
					PlayerImage_4_4.Source = playerImage;

					if (action != "0")
					{
						PlayerImagePop_4_4.IsVisible = true;
						if (action == "star") { PlayerImagePop_4_4.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_4_4.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_4_4.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_4.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_4.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "5")
				{
					Stack_4_5.IsVisible = true;
					PlayerImage_4_5.Source = playerImage;
					PlayerName_4_5.Text = PlayerName;
					PlayerImage_4_5.Source = playerImage;

					if (action != "0")
					{
						PlayerImagePop_4_5.IsVisible = true;
						if (action == "star") { PlayerImagePop_4_5.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_4_5.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_4_5.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_5.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_5.Source = "shirt_mom+h"; }
					}
				}

			}

			if (Posx == "5")
			{
				if (Posy == "1")
				{
					Stack_5_1.IsVisible = true;
					PlayerImage_5_2.Source = playerImage;
					PlayerName_5_1.Text = PlayerName;
					PlayerImage_5_1.Source = playerImage;
					if (action != "0")
					{
						PlayerImagePop_5_1.IsVisible = true;
						if (action == "star") { PlayerImagePop_5_1.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_5_1.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_5_1.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_1.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_1.Source = "shirt_mom+h"; }
					}

				}
				if (Posy == "2")
				{
					Stack_5_2.IsVisible = true;
					PlayerImage_5_2.Source = playerImage;
					PlayerName_5_2.Text = PlayerName;
					PlayerImage_5_2.Source = playerImage;

					if (action != "0")
					{
						PlayerImagePop_5_2.IsVisible = true;
						if (action == "star") { PlayerImagePop_5_2.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_5_2.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_5_2.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_2.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_2.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "3")
				{
					Stack_5_3.IsVisible = true;
					PlayerImage_5_3.Source = playerImage;
					PlayerName_5_3.Text = PlayerName;
					PlayerImage_5_3.Source = playerImage;
					if (action != "0")
					{
						PlayerImagePop_5_3.IsVisible = true;
						if (action == "star") { PlayerImagePop_5_3.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_5_3.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_5_3.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_3.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_3.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "4")
				{
					Stack_5_4.IsVisible = true;
					PlayerImage_5_4.Source = playerImage;
					PlayerName_5_4.Text = PlayerName;
					PlayerImage_5_4.Source = playerImage;
					if (action != "0")
					{
						PlayerImagePop_5_4.IsVisible = true;
						if (action == "star") { PlayerImagePop_5_4.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_5_4.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_5_4.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_4.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_4.Source = "shirt_mom+h"; }
					}
				}
				if (Posy == "5")
				{
					Stack_5_5.IsVisible = true;
					PlayerImage_5_5.Source = playerImage;
					PlayerName_5_5.Text = PlayerName;
					PlayerImage_5_5.Source = playerImage;

					if (action != "0")
					{
						PlayerImagePop_5_5.IsVisible = true;
						if (action == "star") { PlayerImagePop_5_5.Source = "shirt_st"; }
						else if (action == "hairdryer") { PlayerImagePop_5_5.Source = "shirt_hd"; }
						else if (action == "manofmatch") { PlayerImagePop_5_5.Source = "shirt_mom"; }
						else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_5.Source = "shirt_mom+s"; }
						else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_5.Source = "shirt_mom+h"; }
					}
				}

			}
		}
		#region On Tapped


		public void OnTapped_Position_1_3(object sender, EventArgs e)
		{

			MakeActivePosition(1, 3);
		}

		void OnTapped_Position_2_1(object sender, EventArgs e)
		{

			MakeActivePosition(2, 1);
		}

		void OnTapped_Position_2_2(object sender, EventArgs e)
		{

			MakeActivePosition(2, 2);
		}
		void OnTapped_Position_2_3(object sender, EventArgs e)
		{

			MakeActivePosition(2, 3);
		}

		void OnTapped_Position_2_4(object sender, EventArgs e)
		{

			MakeActivePosition(2, 4);
		}

		void OnTapped_Position_2_5(object sender, EventArgs e)
		{

			MakeActivePosition(2, 5);
		}


		void OnTapped_Position_3_1(object sender, EventArgs e)
		{

			MakeActivePosition(3, 1);
		}

		void OnTapped_Position_3_2(object sender, EventArgs e)
		{

			MakeActivePosition(3, 2);
		}

		void OnTapped_Position_3_3(object sender, EventArgs e)
		{

			MakeActivePosition(3, 3);
		}

		void OnTapped_Position_3_4(object sender, EventArgs e)
		{

			MakeActivePosition(3, 4);
		}

		void OnTapped_Position_3_5(object sender, EventArgs e)
		{

			MakeActivePosition(3, 5);
		}

		void OnTapped_Position_4_1(object sender, EventArgs e)
		{

			MakeActivePosition(4, 1);
		}

		void OnTapped_Position_4_2(object sender, EventArgs e)
		{

			MakeActivePosition(4, 2);
		}

		void OnTapped_Position_4_3(object sender, EventArgs e)
		{

			MakeActivePosition(4, 3);
		}

		void OnTapped_Position_4_4(object sender, EventArgs e)
		{

			MakeActivePosition(4, 4);
		}

		void OnTapped_Position_4_5(object sender, EventArgs e)
		{

			MakeActivePosition(4, 5);
		}

		void OnTapped_Position_5_1(object sender, EventArgs e)
		{

			MakeActivePosition(5, 1);
		}

		void OnTapped_Position_5_2(object sender, EventArgs e)
		{

			MakeActivePosition(5, 2);
		}

		void OnTapped_Position_5_3(object sender, EventArgs e)
		{

			MakeActivePosition(5, 3);
		}

		void OnTapped_Position_5_4(object sender, EventArgs e)
		{

			MakeActivePosition(5, 4);
		}

		void OnTapped_Position_5_5(object sender, EventArgs e)
		{

			MakeActivePosition(5, 5);
		}

		#endregion

		async void MakeActivePosition(int row, int col)
		{
			int positionNumber = 0;

			for (int i = 0; i < FanspickCache.SelectFansPick.Count(); i++)
			{

				if (row.ToString() == FanspickCache.SelectFansPick[i].PosX && col.ToString() == FanspickCache.SelectFansPick[i].PosY)
				{
					positionNumber = i;
					break;
				}

			}

			Debug.WriteLine("", positionNumber.ToString());

			selectedPlayer = FanspickCache.SelectFansPick[positionNumber];

            if(selectedPlayer.PLayerId == "0"){

                await DisplayAlert("Fanspick", "Player Infomation Not available", "ok");

            }else{
				FanspickCache.CurrentFanspickScope.SelectedPlayerId = selectedPlayer.PLayerId.ToString();
				var _player = new PlayerInformationPage(selectedPlayer.PLayerId.ToString());
				await Navigation.PushPopupAsync(_player);
            }

			

		}

		public SelectUserPickPlayer selectedPlayer { get; private set; }

		public void hideAllPositions()
		{
			Stack_1_3.IsVisible = false;

			Stack_2_1.IsVisible = false;
			Stack_2_2.IsVisible = false;
			Stack_2_3.IsVisible = false;
			Stack_2_4.IsVisible = false;
			Stack_2_5.IsVisible = false;

			Stack_3_1.IsVisible = false;
			Stack_3_2.IsVisible = false;
			Stack_3_3.IsVisible = false;
			Stack_3_4.IsVisible = false;
			Stack_3_5.IsVisible = false;

			Stack_4_1.IsVisible = false;
			Stack_4_2.IsVisible = false;
			Stack_4_3.IsVisible = false;
			Stack_4_4.IsVisible = false;
			Stack_4_5.IsVisible = false;

			Stack_5_1.IsVisible = false;
			Stack_5_2.IsVisible = false;
			Stack_5_3.IsVisible = false;
			Stack_5_4.IsVisible = false;
			Stack_5_5.IsVisible = false;

			PlayerImagePop_1_3.IsVisible = false;

			PlayerImagePop_2_1.IsVisible = false;
			PlayerImagePop_2_2.IsVisible = false;
			PlayerImagePop_2_3.IsVisible = false;
			PlayerImagePop_2_4.IsVisible = false;
			PlayerImagePop_2_5.IsVisible = false;

			PlayerImagePop_3_1.IsVisible = false;
			PlayerImagePop_3_2.IsVisible = false;
			PlayerImagePop_3_3.IsVisible = false;
			PlayerImagePop_3_4.IsVisible = false;
			PlayerImagePop_3_5.IsVisible = false;

			PlayerImagePop_4_1.IsVisible = false;
			PlayerImagePop_4_2.IsVisible = false;
			PlayerImagePop_4_3.IsVisible = false;
			PlayerImagePop_4_4.IsVisible = false;
			PlayerImagePop_4_5.IsVisible = false;

			PlayerImagePop_5_1.IsVisible = false;
			PlayerImagePop_5_2.IsVisible = false;
			PlayerImagePop_5_3.IsVisible = false;
			PlayerImagePop_5_4.IsVisible = false;
			PlayerImagePop_5_5.IsVisible = false;


		}

		public void ArrangementOfPlayers(List<String> line1, List<String> line2, List<String> line3, List<String> line4)
		{

			setBakgroundPlayer();

			if (line2.Count == 0)
			{
				UpdateRowHeight(25, 25, 1, 25, 25);
				set3Line();
			}
			else if (line3.Count == 0)
			{
				UpdateRowHeight(25, 25, 25, 1, 25);
				set3Line();
			}
			else
			{
				UpdateRowHeight(20, 20, 20, 20, 20);
				set4Line();
			}

			Debug.WriteLine("playerListRow1", "" + line1);
			Debug.WriteLine("playerListRow1", "" + line2);
			Debug.WriteLine("playerListRow1", "" + line3);
			Debug.WriteLine("playerListRow1", "" + line4);


			int denominator1 = line1.Where(l => l != "0").Count();
			denominator1 = denominator1 == 0 ? 1 : denominator1;
			int percentForEachLine1 = (100 / denominator1);
			percentForEachLine1 = percentForEachLine1 == 0 ? 1 : percentForEachLine1;
			List<int> methodValues1 = new List<int>();
			for (int i = 1; i <= 5; i++)
			{
				string columnPos = line1.FirstOrDefault(c => Convert.ToInt32(c) == i);
				if (!string.IsNullOrEmpty(columnPos))
				{
					methodValues1.Add(percentForEachLine1);
				}
				else
				{
					methodValues1.Add(1);
				}
			}
			UpdateRowLine1(methodValues1[0], methodValues1[1], methodValues1[2], methodValues1[3], methodValues1[4]);

			int denominator2 = line2.Where(l => l != "0").Count();
			denominator2 = denominator2 == 0 ? 1 : denominator2;
			int percentForEachLine2 = (100 / denominator2);
			percentForEachLine2 = percentForEachLine2 == 0 ? 1 : percentForEachLine2;
			List<int> methodValues2 = new List<int>();
			for (int i = 1; i <= 5; i++)
			{
				string columnPos = line2.FirstOrDefault(c => Convert.ToInt32(c) == i);
				if (!string.IsNullOrEmpty(columnPos))
				{
					methodValues2.Add(percentForEachLine2);
				}
				else
				{
					methodValues2.Add(1);
				}
			}
			UpdateRowLine2(methodValues2[0], methodValues2[1], methodValues2[2], methodValues2[3], methodValues2[4]);

			int denominator3 = line3.Where(l => l != "0").Count();
			denominator3 = denominator3 == 0 ? 1 : denominator3;
			int percentForEachLine3 = (100 / denominator3);
			percentForEachLine3 = percentForEachLine3 == 0 ? 1 : percentForEachLine3;
			List<int> methodValues3 = new List<int>();
			for (int i = 1; i <= 5; i++)
			{
				string columnPos = line3.FirstOrDefault(c => Convert.ToInt32(c) == i);
				if (!string.IsNullOrEmpty(columnPos))
				{
					methodValues3.Add(percentForEachLine3);
				}
				else
				{
					methodValues3.Add(1);
				}
			}
			UpdateRowLine3(methodValues3[0], methodValues3[1], methodValues3[2], methodValues3[3], methodValues3[4]);

			int denominator4 = line4.Where(l => l != "0").Count();
			denominator4 = denominator4 == 0 ? 1 : denominator4;
			int percentForEachLine4 = (100 / denominator4);
			percentForEachLine4 = percentForEachLine4 == 0 ? 1 : percentForEachLine4;
			List<int> methodValues4 = new List<int>();
			for (int i = 1; i <= 5; i++)
			{
				string columnPos = line4.FirstOrDefault(c => Convert.ToInt32(c) == i);
				if (!string.IsNullOrEmpty(columnPos))
				{
					methodValues4.Add(percentForEachLine4);
				}
				else
				{
					methodValues4.Add(1);
				}
			}
			UpdateRowLine4(methodValues4[0], methodValues4[1], methodValues4[2], methodValues4[3], methodValues4[4]);


		}



		public void UpdateRowHeight(int a, int b, int c, int d, int e)
		{
			Row1.Height = new GridLength(a, GridUnitType.Star);
			Row2.Height = new GridLength(b, GridUnitType.Star);
			Row3.Height = new GridLength(c, GridUnitType.Star);
			Row4.Height = new GridLength(d, GridUnitType.Star);
			Row5.Height = new GridLength(e, GridUnitType.Star);
		}

		public void UpdateRowLine1(int a, int b, int c, int d, int e)
		{
			Row2_Column1.Width = new GridLength(a, GridUnitType.Star);
			Row2_Column2.Width = new GridLength(b, GridUnitType.Star);
			Row2_Column3.Width = new GridLength(c, GridUnitType.Star);
			Row2_Column4.Width = new GridLength(d, GridUnitType.Star);
			Row2_Column5.Width = new GridLength(e, GridUnitType.Star);
		}

		public void UpdateRowLine2(int a, int b, int c, int d, int e)
		{
			Row3_Column1.Width = new GridLength(a, GridUnitType.Star);
			Row3_Column2.Width = new GridLength(b, GridUnitType.Star);
			Row3_Column3.Width = new GridLength(c, GridUnitType.Star);
			Row3_Column4.Width = new GridLength(d, GridUnitType.Star);
			Row3_Column5.Width = new GridLength(e, GridUnitType.Star);
		}

		public void UpdateRowLine3(int a, int b, int c, int d, int e)
		{
			Row4_Column1.Width = new GridLength(a, GridUnitType.Star);
			Row4_Column2.Width = new GridLength(b, GridUnitType.Star);
			Row4_Column3.Width = new GridLength(c, GridUnitType.Star);
			Row4_Column4.Width = new GridLength(d, GridUnitType.Star);
			Row4_Column5.Width = new GridLength(e, GridUnitType.Star);
		}

		public void UpdateRowLine4(int a, int b, int c, int d, int e)
		{
			Row5_Column2.Width = new GridLength(a, GridUnitType.Star);
			Row5_Column2.Width = new GridLength(b, GridUnitType.Star);
			Row5_Column3.Width = new GridLength(c, GridUnitType.Star);
			Row5_Column4.Width = new GridLength(d, GridUnitType.Star);
			Row5_Column5.Width = new GridLength(e, GridUnitType.Star);
		}

		public void ArrangePlayerLine(String x, String y)
		{

			//var c = new ColumnDefinition(); c.Width = new GridLength(,GridUnitType.Star);

			if (x == "2")
			{
				playerListRow1.Add(y);
			}

			if (x == "3")
			{
				playerListRow2.Add(y);
			}
			if (x == "4")
			{
				playerListRow3.Add(y);
			}

			if (x == "5")
			{
				playerListRow4.Add(y);
			}


		}

		private void OpenStats(object sender, EventArgs e)
		{
            
			List<String> primeIds = new List<string>();
			primeIds = FanspickCache.CurrentFanspickScope.FavoritePrimaryTeamID;
			bool isPrime = false;
            if(primeIds !=null)
            {
                if(primeIds.Count()>0)
                {
					for (int i = 0; i < primeIds.Count(); i++)
					{

						if (primeIds[i] == FanspickCache.CurrentFanspickScope.SelectedTeam.Id)
						{
							isPrime = true;
							break;
						}
						else
						{
							isPrime = false;
						}

					}
                }
            }
			


			if (isLive)
			{
				Type.Text = "Live Match Stats";
				youVsManager = 0 / 100;
				pbYouVManager.Progress = youVsManager / 100;
				pbYouVManager.IsVisible = false;
				YouVManagerText.IsVisible = false;
				LiveData.IsVisible = true;
				StatsMin.IsVisible = true;
				liveNotBox.IsVisible = false;

				GetUserPickVsManagerPickLive(isPrime);

			}
			else
			{
				Type.Text = "Pre-Match Stats";
				pbYouVManager.IsVisible = true;
				YouVManagerText.IsVisible = true;
				LiveData.IsVisible = false;
				StatsMin.IsVisible = false;
				liveNotBox.IsVisible = true;
                GetUserPickVsManagerPickPreMatch(isPrime);

			}

			StatsOptions.IsVisible = true;



		}

       

		public bool RaiseEvents()

		{
			Shared.Models.Fixture currentFixture = Fanspick.Shared.Helper.FanspickCache.CurrentFanspickScope.SelectedFixture;
			if (currentFixture != null)
			{

				fixtureDate = vmApp_Main.ApiFixtureDate;




				if (fixtureDate != null)
				{

					if (DateTime.Now >= fixtureDate && DateTime.Now <= fixtureDate.AddMinutes(15) && vmApp_Main.ApiMatchStatus.Contains("First"))
					{
						matchHalves = "0to15";

					}
					else if (DateTime.Now >= fixtureDate && DateTime.Now >= fixtureDate.AddMinutes(15) && DateTime.Now <= fixtureDate.AddMinutes(30) && vmApp_Main.ApiMatchStatus.Contains("First"))
					{
						matchHalves = "15to30";
					}
					else if (DateTime.Now >= fixtureDate && DateTime.Now >= fixtureDate.AddMinutes(30) && DateTime.Now <= fixtureDate.AddMinutes(45) && vmApp_Main.ApiMatchStatus.Contains("First"))
					{
						matchHalves = "30to45";
					}
					else if (DateTime.Now >= fixtureDate && DateTime.Now >= fixtureDate.AddMinutes(45) && DateTime.Now <= fixtureDate.AddMinutes(60) && vmApp_Main.ApiMatchStatus.Contains("Halftime"))
					{
						matchHalves = "45to60";
					}
					else if (DateTime.Now >= fixtureDate && DateTime.Now <= fixtureDate.AddMinutes(15) && vmApp_Main.ApiMatchStatus.Contains("Second"))
					{
						matchHalves = "60to75";
					}
					else if (DateTime.Now >= fixtureDate && DateTime.Now >= fixtureDate.AddMinutes(15) && DateTime.Now <= fixtureDate.AddMinutes(30) && vmApp_Main.ApiMatchStatus.Contains("Second"))
					{
						matchHalves = "75to90";
					}
					else if (DateTime.Now >= fixtureDate && DateTime.Now >= fixtureDate.AddMinutes(30) && DateTime.Now <= fixtureDate.AddMinutes(45) && vmApp_Main.ApiMatchStatus.Contains("Second"))
					{
						matchHalves = "90to105";
					}
					else if (DateTime.Now >= fixtureDate && DateTime.Now >= fixtureDate.AddMinutes(45) && DateTime.Now <= fixtureDate.AddMinutes(60) && vmApp_Main.ApiMatchStatus.Contains("Full"))
					{
						matchHalves = "105to120";
					}


				}
			}
			return true;
		}


		private void OnTapGestureCommentry(object sender, EventArgs e)
		{
			//Application.Current.MainPage = new MatchCommentry();
			//  Navigation.PushAsync(new Screens.MatchCommentry(), true);
			Fanspick.Constants.raiseAbove = 55;
			  spTest.ShowExpandedPanel();
		}


		private void OnClosePanel(object sender, EventArgs e)
		{
			Fanspick.Constants.raiseAbove = 0;
			  spTest.HidePanel();
		}


		private void OnListClicked(object sender, EventArgs e)
		{
            isScrollAvailable = false;
		}


		public void OnCloseStatsPop(object sender, EventArgs e)
		{
			StatsOptions.IsVisible = false;
		}

		public async void GetInfo(String eventtype, String description)
		{
			try
			{


				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

				if (position == null)
				{
					Debug.WriteLine("null gps", "null gps");
					return;
				}

				if (position.Latitude.ToString() != null)
				{
					vmApp_Main.requestEventLoggingDTO.Latitude = "" + position.Latitude;
				}
				else
				{
					vmApp_Main.requestEventLoggingDTO.Latitude = "0";
				}

				if (position.Longitude.ToString() != null)
				{
					vmApp_Main.requestEventLoggingDTO.Longitude = "" + position.Longitude;
				}
				else
				{
					vmApp_Main.requestEventLoggingDTO.Longitude = "0";
				}
				vmApp_Main.requestEventLoggingDTO.EventType = eventtype;
				vmApp_Main.requestEventLoggingDTO.EventDescription = description;
				vmApp_Main.requestEventLoggingDTO.EventAdditionalInfoID = "" + FanspickCache.UserData.AccessToken;
				vmApp_Main.requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
				vmApp_Main.requestEventLoggingDTO.DeviceType = "IOS";
				vmApp_Main.requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
				vmApp_Main.requestEventLoggingDTO.AppVersion = "1";

				Shared.Models.Service.ServiceResponse response = await vmApp_Main.EventLog();
				//  Application.Current.MainPage = new SplashScreen();


			}
			catch (Exception ex)
			{
				Debug.WriteLine(" " + ex);

			}

		}

		void FanspickPitchEventHandler_OnPreMatchEnd(object sender, EventArgs e)
		{

			SwIcon.Source = "swtich_on";
            isLive = true;
			hideAllPositions();
            GetFanspick(isLive);

			DependencyService.Get<IFanspickLocalNotification>().TriggerNotification("Fanspick", "We've now kicked-off! You are being taken to YourPick Live where you give your opinion on the live match.");

		}

		public void setBakgroundPlayer()
		{
			var semiTransparentColor = new Color(0, 0, 0, 0.7);

			List<Label> allPlayerName = new List<Label>();
			allPlayerName.Add(PlayerName_1_3);

			allPlayerName.Add(PlayerName_2_1);
			allPlayerName.Add(PlayerName_2_2);
			allPlayerName.Add(PlayerName_2_3);
			allPlayerName.Add(PlayerName_2_4);
			allPlayerName.Add(PlayerName_2_5);

			allPlayerName.Add(PlayerName_3_1);
			allPlayerName.Add(PlayerName_3_2);
			allPlayerName.Add(PlayerName_3_3);
			allPlayerName.Add(PlayerName_3_4);
			allPlayerName.Add(PlayerName_3_5);

			allPlayerName.Add(PlayerName_4_1);
			allPlayerName.Add(PlayerName_4_2);
			allPlayerName.Add(PlayerName_4_3);
			allPlayerName.Add(PlayerName_4_4);
			allPlayerName.Add(PlayerName_4_5);

			allPlayerName.Add(PlayerName_5_1);
			allPlayerName.Add(PlayerName_5_2);
			allPlayerName.Add(PlayerName_5_3);
			allPlayerName.Add(PlayerName_5_4);
			allPlayerName.Add(PlayerName_5_5);

			List<Image> allPlayerBack = new List<Image>();
			allPlayerBack.Add(ImageBack_1_3);

			allPlayerBack.Add(ImageBack_2_1);
			allPlayerBack.Add(ImageBack_2_2);
			allPlayerBack.Add(ImageBack_2_3);
			allPlayerBack.Add(ImageBack_2_4);
			allPlayerBack.Add(ImageBack_2_5);

			allPlayerBack.Add(ImageBack_3_1);
			allPlayerBack.Add(ImageBack_3_2);
			allPlayerBack.Add(ImageBack_3_3);
			allPlayerBack.Add(ImageBack_3_4);
			allPlayerBack.Add(ImageBack_3_5);

			allPlayerBack.Add(ImageBack_4_1);
			allPlayerBack.Add(ImageBack_4_2);
			allPlayerBack.Add(ImageBack_4_3);
			allPlayerBack.Add(ImageBack_4_4);
			allPlayerBack.Add(ImageBack_4_5);

			allPlayerBack.Add(ImageBack_5_1);
			allPlayerBack.Add(ImageBack_5_2);
			allPlayerBack.Add(ImageBack_5_3);
			allPlayerBack.Add(ImageBack_5_4);
			allPlayerBack.Add(ImageBack_5_5);


			for (int i = 0; i < allPlayerName.Count(); i++)
			{

				if (allPlayerName[i].Text != null || allPlayerName[i].Text == "")
				{
					var midTransparentColor = new Color(0, 0, 0, 0.7);
					//allPlayerName[i].BackgroundColor = midTransparentColor;
					allPlayerName[i].TextColor = Color.White;
					allPlayerName[i].WidthRequest = 60;
					allPlayerName[i].VerticalTextAlignment = TextAlignment.Center;
					allPlayerName[i].HorizontalTextAlignment = TextAlignment.Center;
					allPlayerName[i].VerticalOptions = LayoutOptions.FillAndExpand;
					allPlayerName[i].HorizontalOptions = LayoutOptions.FillAndExpand;
					allPlayerName[i].FontSize = 9;


					allPlayerBack[i].Source = "back_player_diffused";
					allPlayerBack[i].VerticalOptions = LayoutOptions.FillAndExpand;
					allPlayerBack[i].HorizontalOptions = LayoutOptions.FillAndExpand;
					allPlayerBack[i].Aspect = Aspect.Fill;
					//allPlayerBack[i].WidthRequest = 58;
					//allPlayerBack[i].HeightRequest = 20;

				}

			}

			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);


		}

		public void RemoveSpacing()
		{

			List<StackLayout> allPlayerLayout = new List<StackLayout>();
			allPlayerLayout.Add(Stack_1_3);

			allPlayerLayout.Add(Stack_2_1);
			allPlayerLayout.Add(Stack_2_2);
			allPlayerLayout.Add(Stack_2_3);
			allPlayerLayout.Add(Stack_2_4);
			allPlayerLayout.Add(Stack_2_5);

			allPlayerLayout.Add(Stack_3_1);
			allPlayerLayout.Add(Stack_3_2);
			allPlayerLayout.Add(Stack_3_3);
			allPlayerLayout.Add(Stack_3_4);
			allPlayerLayout.Add(Stack_3_5);

			allPlayerLayout.Add(Stack_4_1);
			allPlayerLayout.Add(Stack_4_2);
			allPlayerLayout.Add(Stack_4_3);
			allPlayerLayout.Add(Stack_4_4);
			allPlayerLayout.Add(Stack_4_5);

			allPlayerLayout.Add(Stack_5_1);
			allPlayerLayout.Add(Stack_5_2);
			allPlayerLayout.Add(Stack_5_3);
			allPlayerLayout.Add(Stack_5_4);
			allPlayerLayout.Add(Stack_5_5);

			for (int i = 0; i < allPlayerLayout.Count(); i++)
			{
				allPlayerLayout[i].Spacing = 0;

			}

		}

		public void LoadGif()
		{

			String htmlsource = FanspickCache.PitchImage;
			String htmlMain = FanspickCache.BannerImage.Replace(".gif.gif", ".gif");
			
			if (FanspickCache.CurrentFanspickScope.SelectedFixture.CompetitionName == "UEFA Champions League")
			{
				skyBack.IsVisible = true;
				skyBack.Source = "start_bg_night";

			}
			else if (FanspickCache.CurrentFanspickScope.SelectedFixture.CompetitionName == "UEFA Europa League")
			{
				skyBack.IsVisible = true;
				skyBack.Source = "start_bg_europa copy";
			}
			else
			{
				skyBack.IsVisible = false;

			}


			HtmlWebViewSource html = new HtmlWebViewSource();
			html.Html = String.Format
				(@"<html style='width:" + BannerWidthSize() + ";height=" + BannerheightSize() + "; margin-left:-6px;'><body style='width:100%;height=100%; background: #ffffff;'><img src='{0}' style='width:100%;height=100%;margin-top:-8px;'  /></body></html>",
				 htmlsource);
			Banner1.Source = html;
			Banner2.Source = html;
			FanspickbannerImg.Source = htmlMain;       

		}

		public void SwIconOption(object sender, EventArgs e)
		{
			if (tapCountSw % 2 == 0)
			{
				tapCountSw++;
				SwIcon.Source = "swtich_on";
				isLive = true;
				hideAllPositions();
				GetFanspick(isLive);
			}
			else
			{
				tapCountSw++;
				SwIcon.Source = "swtich_off";
				isLive = false;
				hideAllPositions();
				GetFanspick(isLive);
			}
		}
		async void JoinConversation(object sender, EventArgs e)
		{
			Navigation.PushAsync(new Screens.TeamCommunities(), true);
		}


		public void set3Line()
		{
			LoadAllImagePop();

			String device = CrossDeviceInfo.Current.Model;

			double playerShirtSize = 37;

			if (device == "iPad")
			{
				playerShirtSize = 85;
			}

			for (int i = 0; i < allImagePopUp.Count(); i++)
			{
				allImagePopUp[i].WidthRequest = playerShirtSize;
				allImage[i].WidthRequest = playerShirtSize;

				allImagePopUp[i].HeightRequest = playerShirtSize;
				allImage[i].HeightRequest = playerShirtSize;
			}

		}

		public void set4Line()
		{
			LoadAllImagePop();


			String device = CrossDeviceInfo.Current.Model;

			double playerShirtSize = 32;

			if (device == "iPad")
			{
				playerShirtSize = 85;
			}

			for (int i = 0; i < allImagePopUp.Count(); i++)
			{
				allImagePopUp[i].WidthRequest = playerShirtSize;
				allImage[i].WidthRequest = playerShirtSize;

				allImagePopUp[i].HeightRequest = playerShirtSize;
				allImage[i].HeightRequest = playerShirtSize;
			}

		}

		List<Image> allImagePopUp = new List<Image>();
		List<Image> allImage = new List<Image>();


		
        public void LoadAllImagePop()
		{

			if (allImagePopUp.Count > 0)
			{
				allImagePopUp.Clear();
			}

			if (allImage.Count > 0)
			{
				allImage.Clear();
			}


			allImagePopUp.Add(PlayerImagePop_1_3);

			allImagePopUp.Add(PlayerImagePop_2_1);
			allImagePopUp.Add(PlayerImagePop_2_2);
			allImagePopUp.Add(PlayerImagePop_2_3);
			allImagePopUp.Add(PlayerImagePop_2_4);
			allImagePopUp.Add(PlayerImagePop_2_5);

			allImagePopUp.Add(PlayerImagePop_3_1);
			allImagePopUp.Add(PlayerImagePop_3_2);
			allImagePopUp.Add(PlayerImagePop_3_3);
			allImagePopUp.Add(PlayerImagePop_3_4);
			allImagePopUp.Add(PlayerImagePop_3_5);

			allImagePopUp.Add(PlayerImagePop_4_1);
			allImagePopUp.Add(PlayerImagePop_4_2);
			allImagePopUp.Add(PlayerImagePop_4_3);
			allImagePopUp.Add(PlayerImagePop_4_4);
			allImagePopUp.Add(PlayerImagePop_4_5);

			allImagePopUp.Add(PlayerImagePop_5_1);
			allImagePopUp.Add(PlayerImagePop_5_2);
			allImagePopUp.Add(PlayerImagePop_5_3);
			allImagePopUp.Add(PlayerImagePop_5_4);
			allImagePopUp.Add(PlayerImagePop_5_5);


			allImage.Add(PlayerImage_1_3);

			allImage.Add(PlayerImage_2_1);
			allImage.Add(PlayerImage_2_2);
			allImage.Add(PlayerImage_2_3);
			allImage.Add(PlayerImage_2_4);
			allImage.Add(PlayerImage_2_5);

			allImage.Add(PlayerImage_3_1);
			allImage.Add(PlayerImage_3_2);
			allImage.Add(PlayerImage_3_3);
			allImage.Add(PlayerImage_3_4);
			allImage.Add(PlayerImage_3_5);

			allImage.Add(PlayerImage_4_1);
			allImage.Add(PlayerImage_4_2);
			allImage.Add(PlayerImage_4_3);
			allImage.Add(PlayerImage_4_4);
			allImage.Add(PlayerImage_4_5);

			allImage.Add(PlayerImage_5_1);
			allImage.Add(PlayerImage_5_2);
			allImage.Add(PlayerImage_5_3);
			allImage.Add(PlayerImage_5_4);
			allImage.Add(PlayerImage_5_5);

		}


		public String BannerheightSize()
		{

			String size = "50px";
			String device = CrossDeviceInfo.Current.Model;
			if (device == "iPad")
			{
				size = "100px";
			}

			return size;
		}


		public String BannerWidthSize()
		{

			String size = "100px";
			String device = CrossDeviceInfo.Current.Model;
			if (device == "iPad")
			{
				size = "200px";
			}

			return size;
		}


		public void ajustDevice(string deviceType)
		{
			List<Label> allPlayerName = new List<Label>();
			allPlayerName.Add(PlayerName_1_3);

			allPlayerName.Add(PlayerName_2_1);
			allPlayerName.Add(PlayerName_2_2);
			allPlayerName.Add(PlayerName_2_3);
			allPlayerName.Add(PlayerName_2_4);
			allPlayerName.Add(PlayerName_2_5);

			allPlayerName.Add(PlayerName_3_1);
			allPlayerName.Add(PlayerName_3_2);
			allPlayerName.Add(PlayerName_3_3);
			allPlayerName.Add(PlayerName_3_4);
			allPlayerName.Add(PlayerName_3_5);

			allPlayerName.Add(PlayerName_4_1);
			allPlayerName.Add(PlayerName_4_2);
			allPlayerName.Add(PlayerName_4_3);
			allPlayerName.Add(PlayerName_4_4);
			allPlayerName.Add(PlayerName_4_5);

			allPlayerName.Add(PlayerName_5_1);
			allPlayerName.Add(PlayerName_5_2);
			allPlayerName.Add(PlayerName_5_3);
			allPlayerName.Add(PlayerName_5_4);
			allPlayerName.Add(PlayerName_5_5);

			List<Image> allPlayerBack = new List<Image>();
			allPlayerBack.Add(ImageBack_1_3);

			allPlayerBack.Add(ImageBack_2_1);
			allPlayerBack.Add(ImageBack_2_2);
			allPlayerBack.Add(ImageBack_2_3);
			allPlayerBack.Add(ImageBack_2_4);
			allPlayerBack.Add(ImageBack_2_5);

			allPlayerBack.Add(ImageBack_3_1);
			allPlayerBack.Add(ImageBack_3_2);
			allPlayerBack.Add(ImageBack_3_3);
			allPlayerBack.Add(ImageBack_3_4);
			allPlayerBack.Add(ImageBack_3_5);

			allPlayerBack.Add(ImageBack_4_1);
			allPlayerBack.Add(ImageBack_4_2);
			allPlayerBack.Add(ImageBack_4_3);
			allPlayerBack.Add(ImageBack_4_4);
			allPlayerBack.Add(ImageBack_4_5);

			allPlayerBack.Add(ImageBack_5_1);
			allPlayerBack.Add(ImageBack_5_2);
			allPlayerBack.Add(ImageBack_5_3);
			allPlayerBack.Add(ImageBack_5_4);
			allPlayerBack.Add(ImageBack_5_5);



			if (deviceType == "iPad")
			{

				for (int i = 0; i < allPlayerName.Count(); i++)
				{
					allPlayerBack[i].WidthRequest = 100;
					allPlayerName[i].HeightRequest = 22;
					allPlayerName[i].FontSize = 12;

				}

				Gline1.Margin = new Thickness(20, 0, 20, 0);
				FanspickbannerImg.HeightRequest = 170;
				FanspickbannerImg.WidthRequest = 750;
				FanspickbannerImg.Margin = new Thickness(0, 0, 0, 60);

				Banner1.HeightRequest = 40;
				Banner2.HeightRequest = 40;
				Banner1.Margin = new Thickness(0, 0, 8, 0);
				Banner2.Margin = new Thickness(5, 0, 8, 0);

				BoxDivision1.Height = new GridLength(1.5, GridUnitType.Star);
				BoxDivision2.Height = new GridLength(6.45, GridUnitType.Star);
				BoxDivision3.Height = new GridLength(0.30, GridUnitType.Star);
				BoxDivision4.Height = new GridLength(1.1, GridUnitType.Star);

				BarCol1.Width = new GridLength(50);
				BarCol3.Width = new GridLength(50);
				BarCol4.Width = new GridLength(50);
                BarCol5.Width = new GridLength(60);

                NullImage.Margin = new Thickness(-15, -25, -15, 10);


                //StatsMin popup adjustment

                close.WidthRequest = 40;
                close.Margin = new Thickness(15);
                Type.FontSize = 25;

                pbYouVFans.HeightRequest = 100;
                pbYouVFans.WidthRequest = 100;
                textyPfP.FontSize = 21;
                YouVFansText.FontSize = 18;
                ypImage.Margin = new Thickness(20);
                fpImage.Margin = new Thickness(20);

				pbFanVManager.HeightRequest = 100;
				pbFanVManager.WidthRequest = 100;
				textfPmP.FontSize = 21;
				FanVManagerText.FontSize = 18;
                fpImage2.Margin = new Thickness(20);
                mpImage.Margin = new Thickness(20);

				pbYouVManager.HeightRequest = 100;
				pbYouVManager.WidthRequest = 100;
				textyPmP.FontSize = 21;
				YouVManagerText.FontSize = 18;
				ypImage2.Margin = new Thickness(20);
				mpImage2.Margin = new Thickness(20);

                UserSub.FontSize = 16;
                ManagerSub.FontSize = 16;
                Statsheader.FontSize = 16;

                label15.FontSize = 17;
                label30.FontSize = 17;
                label45.FontSize = 17;
                label60.FontSize = 17;
                label75.FontSize = 17;
                label90.FontSize = 17;

                Action15.HeightRequest = 80;
                Action45.HeightRequest = 80;
                Action30.HeightRequest = 80;
                Action60.HeightRequest = 80;
                Action75.HeightRequest = 80;
                Action90.HeightRequest = 80;

                NoPlayerText15.FontSize = 15;
                NoPlayerText30.FontSize = 15;
                NoPlayerText45.FontSize = 15;
                NoPlayerText60.FontSize = 15;
                NoPlayerText75.FontSize = 15;
                NoPlayerText90.FontSize = 15;

                clickConversation.FontSize = 20;

			}
			else
			{
				Gline1.Margin = new Thickness(0, 0, 0, 0);
				for (int i = 0; i < allPlayerName.Count(); i++)
				{
					allPlayerBack[i].WidthRequest = 60;
					allPlayerName[i].HeightRequest = 15;
					allPlayerName[i].FontSize = 10;

				}

				BoxDivision1.Height = new GridLength(1.5, GridUnitType.Star);
				BoxDivision2.Height = new GridLength(6.5, GridUnitType.Star);
				BoxDivision3.Height = new GridLength(0.25, GridUnitType.Star);
				BoxDivision4.Height = new GridLength(1.1, GridUnitType.Star);

				BarCol1.Width = new GridLength(35);
				BarCol3.Width = new GridLength(35);
				BarCol4.Width = new GridLength(35);
                BarCol5.Width = new GridLength(40);

				NoPlayerText15.FontSize = 10;
				NoPlayerText30.FontSize = 10;
				NoPlayerText45.FontSize = 10;
				NoPlayerText60.FontSize = 10;
				NoPlayerText75.FontSize = 10;
				NoPlayerText90.FontSize = 10;

                clickConversation.FontSize = 12;
			}


		}


		private async void GetUserPickVsManagerPickPreMatch(bool isPrime)
		{

			try
			{


				if (isPrime)
				{

					RequestUserPManagerPDTO getManagerPickPercentageRequest = new RequestUserPManagerPDTO();
					getManagerPickPercentageRequest.AccessToken = FanspickCache.UserData.AccessToken;
					getManagerPickPercentageRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
					getManagerPickPercentageRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;

					ServiceResponse serviceMResponse = await new FanspickServices<GetUserPickManagerPickPercentageService>().Run(getManagerPickPercentageRequest);

					if (serviceMResponse.OK)
					{
						ResponseUserPManagerPDTO userManagerPercentageResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseUserPManagerPDTO>(serviceMResponse.Data);
						youVsManager = Convert.ToDouble(userManagerPercentageResponse.Data.Percentage);

						if (YourpickPageMain.totalPlayersOnPitch != 21)
						{
							YouVManagerText.Text = "You have not created Your Pick yet";
							ypImage2.Source = "n-yp";
							mpImage2.Source = "mP";
							textyPmP.Text = "%";
							imtextyPmP.IsVisible = true;
						}
						else if (ManagerpickPageMain.totalPlayersOnPitch != 11)
						{
							YouVManagerText.Text = "Managerpick is not yet avaible for comparision";
							ypImage2.Source = "yP";
							mpImage2.Source = "n-mp";
							textyPmP.Text = "%";
							imtextyPmP.IsVisible = true;
						}
						else
						{
							pbYouVManager.Progress = youVsManager / 100;
							YouVManagerText.Text = youVsManager + "% of Your Pick matches the Manager Pick";
							textyPmP.Text = youVsManager + " %";
							ypImage2.Source = "yP";
							mpImage2.Source = "mP";
							imtextyPmP.IsVisible = false;
						}


					}
					else
					{
						if (YourpickPageMain.totalPlayersOnPitch != 21)
						{
							YouVManagerText.Text = "You have not created Your Pick yet";
							ypImage2.Source = "n-yp";
							mpImage2.Source = "mP";
						}
						else if (ManagerpickPageMain.totalPlayersOnPitch != 11)
						{
							YouVManagerText.Text = "Managerpick is not yet avaible for comparision";
							ypImage2.Source = "yP";
							mpImage2.Source = "n-mp";
						}
						else
						{
							YouVManagerText.Text = "Stats not available yet";
							ypImage2.Source = "n-yp";
							mpImage2.Source = "mP";
						}


						youVsManager = 0 / 100;
						pbYouVManager.Progress = youVsManager / 100;
						textyPmP.Text = "%";

						imtextyPmP.IsVisible = true;

					}


				}

				else
				{

					youVsManager = 0 / 100;
					pbYouVManager.Progress = youVsManager / 100;
					YouVManagerText.Text = "Selected team is not your favourite team so \n comparision cannot be made with this";
					YouVManagerText.HorizontalTextAlignment = TextAlignment.Center;
					textyPmP.Text = "%";
					imtextyPmP.IsVisible = true;
					ypImage2.Source = "n-yp";
				}

			}
			catch (Exception ex)
			{

				if (YourpickPageMain.totalPlayersOnPitch != 21)
				{
					YouVManagerText.Text = "You have not created Your Pick yet";
					ypImage2.Source = "n-yp";
					mpImage2.Source = "mP";
				}
				else if (ManagerpickPageMain.totalPlayersOnPitch != 11)
				{
					YouVManagerText.Text = "Managerpick is not yet avaible for comparision";
					ypImage2.Source = "yP";
					mpImage2.Source = "n-mp";
				}
				else
				{
					YouVManagerText.Text = "Stats not available yet";
					ypImage2.Source = "n-yp";
					mpImage2.Source = "mP";
				}

			}




			GetFansPickVsManagerPickPreMatch(isPrime);

		}

		private async void GetFansPickVsManagerPickPreMatch(bool isPrime)
		{

			try
			{


				RequestFansPManagerPDTO getManagerPickPercentageRequest = new RequestFansPManagerPDTO();
				getManagerPickPercentageRequest.AccessToken = FanspickCache.UserData.AccessToken;
				getManagerPickPercentageRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
				getManagerPickPercentageRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;

				ServiceResponse serviceMResponse = await new FanspickServices<GetFansPickManagerPickPercentageService>().Run(getManagerPickPercentageRequest);

				if (serviceMResponse.OK)
				{
					ResponseFansPManagerPDTO userManagerPercentageResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseFansPManagerPDTO>(serviceMResponse.Data);
					FansVsManager = Convert.ToDouble(userManagerPercentageResponse.Data.Percentage);
					pbFanVManager.Progress = FansVsManager / 100;


					if (totalPlayersOnPitch != 11)
					{
						FanVManagerText.Text = "None of the users have created their Pick";
						mpImage.Source = "mP";
						fpImage2.Source = "n-fp";
						textfPmP.Text = "%";
						imtextfPmP.IsVisible = true;
					}
					else if (ManagerpickPageMain.totalPlayersOnPitch != 11)
					{
						YouVManagerText.Text = "Managerpick is not yet avaible for comparision";
						mpImage.Source = "n-mp";
						fpImage2.Source = "fP";
						textfPmP.Text = "%";
						imtextfPmP.IsVisible = true;
					}

					else
					{
						FanVManagerText.Text = FansVsManager + "% of fans agree with Manager Pick";
						textfPmP.Text = FansVsManager + " %";
						mpImage.Source = "mP";
						fpImage2.Source = "fP";
						imtextfPmP.IsVisible = false;
					}


				}
				else
				{

					if (totalPlayersOnPitch != 11)
					{
						FanVManagerText.Text = "None of the users have created their Pick";
						mpImage.Source = "mP";
						fpImage2.Source = "n-fp";
					}
					else if (ManagerpickPageMain.totalPlayersOnPitch != 11)
					{
						YouVManagerText.Text = "Managerpick is not yet avaible for comparision";
						mpImage.Source = "n-mp";
						fpImage2.Source = "fP";
					}

					else
					{
						FanVManagerText.Text = "None of the users have created their Pick";
						mpImage.Source = "mP";
						fpImage2.Source = "n-fp";
					}

					pbFanVManager.Progress = 0 / 100;
					textfPmP.Text = "%";
					imtextfPmP.IsVisible = true;

				}


			}
			catch (Exception ex)
			{

				if (totalPlayersOnPitch != 11)
				{
					FanVManagerText.Text = "None of the users have created their Pick";
					mpImage.Source = "mP";
					fpImage2.Source = "n-fp";
				}
				else if (ManagerpickPageMain.totalPlayersOnPitch != 11)
				{
					YouVManagerText.Text = "Managerpick is not yet avaible for comparision";
					mpImage.Source = "n-mp";
					fpImage2.Source = "fP";
				}

				else
				{
					FanVManagerText.Text = "Stats not avalible yet";
					mpImage.Source = "mP";
					fpImage2.Source = "n-fp";
				}


			}





			GetUserPickVFansPickPreMatch(isPrime);
		}

		public async void GetUserPickVFansPickPreMatch(bool isPrime)
		{

			try
			{


				if (isPrime)
				{

					RequestGetUserPickPercentageDTO getManagerPickPercentageRequest = new RequestGetUserPickPercentageDTO();
					getManagerPickPercentageRequest.AccessToken = FanspickCache.UserData.AccessToken;
					getManagerPickPercentageRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
					getManagerPickPercentageRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;

					ServiceResponse serviceMResponse = await new FanspickServices<GetUserPickPercentageService>().Run(getManagerPickPercentageRequest);

					if (serviceMResponse.OK)
					{
						ResponseGetUserPickPercentageDTO userManagerPercentageResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetUserPickPercentageDTO>(serviceMResponse.Data);
						youVsFans = Convert.ToDouble(userManagerPercentageResponse.Data.Percentage);
						pbYouVFans.Progress = youVsFans / 100;


						if (YourpickPageMain.totalPlayersOnPitch != 21)
						{
							YouVFansText.Text = "You have not created Your Pick yet";
							ypImage.Source = "n-yp";
							fpImage.Source = "fP";
							textyPfP.Text = "%";
							imtextyPfP.IsVisible = true;
						}
						else if (totalPlayersOnPitch != 11)
						{
							YouVFansText.Text = "Fanspick is not yet available for comparasion";
							ypImage.Source = "yP";
							fpImage.Source = "n-fp";
							textyPfP.Text = "%";
							imtextyPfP.IsVisible = true;

						}
						else
						{

							YouVFansText.Text = youVsFans + "% of fans agree with Your Pick";
							textyPfP.Text = youVsFans + " %";
							ypImage.Source = "yP";
							fpImage.Source = "fP";
							imtextyPfP.IsVisible = false;

						}



					}
					else
					{

						if (YourpickPageMain.totalPlayersOnPitch != 21)
						{
							YouVFansText.Text = "You have not created Your Pick yet";
							ypImage.Source = "n-yp";
							fpImage.Source = "fP";
						}
						else if (totalPlayersOnPitch != 11)
						{
							YouVFansText.Text = "Fanspick is not yet available for comparasion";
							ypImage.Source = "yP";
							fpImage.Source = "n-fp";
						}
						else
						{
							YouVFansText.Text = "Stats not available yet...";
							ypImage.Source = "yP";
							fpImage.Source = "n-fp";
						}


						pbYouVFans.Progress = 0 / 100;
						textyPfP.Text = "%";
						imtextyPfP.IsVisible = true;
					}

				}
				else
				{
					pbYouVFans.Progress = 0 / 100;
					YouVFansText.Text = "Selected team is not your favourite team so \n comparision cannot be made with this";
					YouVFansText.HorizontalTextAlignment = TextAlignment.Center;
					textyPfP.Text = "%";
					ypImage.Source = "n-yp";
					imtextyPfP.IsVisible = true;
				}

			}
			catch (Exception e)
			{

				if (YourpickPageMain.totalPlayersOnPitch != 21)
				{
					YouVFansText.Text = "You have not created Your Pick yet";
					ypImage.Source = "n-yp";
					fpImage.Source = "fP";
				}
				else if (totalPlayersOnPitch != 11)
				{
					YouVFansText.Text = "Fanspick is not yet available for comparasion";
					ypImage.Source = "yP";
					fpImage.Source = "n-fp";
				}
				else
				{
					YouVFansText.Text = "Stats not available yet...";
					ypImage.Source = "yP";
					fpImage.Source = "n-fp";
				}

			}




		}

		private async void GetUserPickVsManagerPickLive(bool isPrime)
		{

			try
			{


				RequestUserPManagerPLiveDTO getManagerPickPercentageRequest = new RequestUserPManagerPLiveDTO();
				getManagerPickPercentageRequest.AccessToken = FanspickCache.UserData.AccessToken;
				getManagerPickPercentageRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
				getManagerPickPercentageRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;


				ServiceResponse serviceMResponse = await new FanspickServices<GetUserPickManagerPickPercentageServiceLive>().Run(getManagerPickPercentageRequest);

				if (serviceMResponse.OK)
				{
					UserSub.IsVisible = true;
					ManagerSub.IsVisible = true;
					ResponseUserPManagerPLiveDTO userManagerPercentageResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseUserPManagerPLiveDTO>(serviceMResponse.Data);
					UserPickSub = userManagerPercentageResponse.Data.userPickSub;
					ListIn.ItemsSource = UserPickSub;
					ManagerPickSub = userManagerPercentageResponse.Data.managerPickSub;
					ListOut.ItemsSource = ManagerPickSub;

					if (UserPickSub.Count == 0)
					{
						UserSub.Text = "You have not substituted any player";
						ListIn.IsVisible = false;
					}
					else
					{
						UserSub.Text = "You substituted the following";
						ListIn.IsVisible = true;
					}


					if (ManagerPickSub.Count == 0)
					{
						ManagerSub.Text = "Manager has not substituted any player";
						ListOut.IsVisible = false;
					}
					else
					{
						ManagerSub.Text = "Manager substituted the following";
						ListOut.IsVisible = true;
					}

				}
				else
				{
					UserSub.IsVisible = false;
					ManagerSub.IsVisible = false;
				}

			}
			catch (Exception ex)
			{

			}




			GetFansPickVsManagerPickLive(isPrime);

		}



		public List<ResponseSubstitute> UserPickSub { get; set; }
		public List<ResponseSubstitute> ManagerPickSub { get; set; }

		public List<ActionStats> action15 { get; set; }
		public List<ActionStats> action30 { get; set; }
		public List<ActionStats> action45 { get; set; }
		public List<ActionStats> action60 { get; set; }
		public List<ActionStats> action75 { get; set; }
		public List<ActionStats> action90 { get; set; }


		private async void GetFansPickVsManagerPickLive(bool isPrime)
		{


			RequestFansPManagerPLiveDTO getManagerPickPercentageRequest = new RequestFansPManagerPLiveDTO();
			getManagerPickPercentageRequest.AccessToken = FanspickCache.UserData.AccessToken;
			getManagerPickPercentageRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
			getManagerPickPercentageRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;

			ServiceResponse serviceMResponse = await new FanspickServices<GetFansPickManagerPickPercentageServiceLive>().Run(getManagerPickPercentageRequest);

			if (serviceMResponse.OK)
			{
				ResponseFansPManagerLivePDTO userManagerPercentageResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseFansPManagerLivePDTO>(serviceMResponse.Data);
				FansVsManager = Convert.ToDouble(userManagerPercentageResponse.Data.Percentage);
				pbFanVManager.Progress = FansVsManager / 100;

				if (totalPlayersOnPitch != 11)
				{
					FanVManagerText.Text = "None of the users have created their Pick";
					mpImage.Source = "mP";
					fpImage2.Source = "n-fp";
					textfPmP.Text = "%";
					imtextfPmP.IsVisible = true;
				}
				else if (ManagerpickPageMain.totalPlayersOnPitch != 11)
				{
					YouVManagerText.Text = "Managerpick is not yet avaible for comparision";
					mpImage.Source = "n-mp";
					fpImage2.Source = "fP";
					textfPmP.Text = "%";
					imtextfPmP.IsVisible = true;
				}

				else
				{
					FanVManagerText.Text = FansVsManager + "% of fans agree with Manager Pick";
					textfPmP.Text = FansVsManager + " %";
					mpImage.Source = "mP";
					fpImage2.Source = "fP";
					imtextfPmP.IsVisible = false;
				}



			}
			else
			{
				if (totalPlayersOnPitch != 11)
				{
					FanVManagerText.Text = "None of the users have created their Pick";
					mpImage.Source = "mP";
					fpImage2.Source = "n-fp";
				}

				else if (ManagerpickPageMain.totalPlayersOnPitch != 11)
				{
					YouVManagerText.Text = "Managerpick is not yet avaible for comparision";
					mpImage.Source = "n-mp";
					fpImage2.Source = "fP";
				}

				else
				{
					FanVManagerText.Text = "None of the users have created their Pick";
					mpImage.Source = "mP";
					fpImage2.Source = "n-fp";
				}

				pbFanVManager.Progress = 0 / 100;
				textfPmP.Text = "%";
				imtextfPmP.IsVisible = true;
			}


			GetUserPickVFansPickLive(isPrime);
		}


		private async void GetUserPickVFansPickLive(bool isPrime)
		{

			if (isPrime)
			{

				RequestUserPFansPLiveDTO getManagerPickPercentageRequest = new RequestUserPFansPLiveDTO();
				getManagerPickPercentageRequest.AccessToken = FanspickCache.UserData.AccessToken;
				getManagerPickPercentageRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
				getManagerPickPercentageRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;

				ServiceResponse serviceMResponse = await new FanspickServices<GetUserPickPercentageServiceLive>().Run(getManagerPickPercentageRequest);

				if (serviceMResponse.OK)
				{
					ResponseUserPFansPLiveDTO userManagerPercentageResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseUserPFansPLiveDTO>(serviceMResponse.Data);
					youVsFans = Convert.ToDouble(userManagerPercentageResponse.Data.Percentage);
					pbYouVFans.Progress = youVsFans / 100;


					if (YourpickPageMain.totalPlayersOnPitch != 21)
					{
						YouVFansText.Text = "You have not created Your Pick yet";
						ypImage.Source = "n-yp";
						fpImage.Source = "fP";
						textyPfP.Text = "%";
						imtextyPfP.IsVisible = true;
					}
					else if (totalPlayersOnPitch != 11)
					{
						YouVFansText.Text = "Fanspick is not yet available for comparasion";
						ypImage.Source = "yP";
						fpImage.Source = "n-fp";
						textyPfP.Text = "%";
						imtextyPfP.IsVisible = true;
					}
					else
					{
						YouVFansText.Text = youVsFans + "% of fans agree with Your Pick";
						textyPfP.Text = youVsFans + " %";
						ypImage.Source = "yP";
						fpImage.Source = "fP";
						imtextyPfP.IsVisible = false;
					}


				}
				else
				{

					if (YourpickPageMain.totalPlayersOnPitch != 21)
					{
						YouVFansText.Text = "You have not created Your Pick yet";
						ypImage.Source = "n-yp";
						fpImage.Source = "fP";
					}
					else if (totalPlayersOnPitch != 11)
					{
						YouVFansText.Text = "Fanspick is not yet available for comparasion";
						ypImage.Source = "yP";
						fpImage.Source = "n-fp";
					}
					else
					{
						YouVFansText.Text = "Stats not available yet...";
						ypImage.Source = "yP";
						fpImage.Source = "n-fp";
					}

					pbYouVFans.Progress = 0 / 100;
					textyPfP.Text = "%";
					imtextyPfP.IsVisible = true;
				}

			}

			else
			{
				pbYouVFans.Progress = 0 / 100;
				YouVFansText.Text = "Selected team is not your favourite team so \n comparision cannot be made with this";
				YouVFansText.HorizontalTextAlignment = TextAlignment.Center;
				textyPfP.Text = "%";
				ypImage.Source = "n-yp";
				imtextyPfP.IsVisible = true;
			}



			if (fanspickPitchEventHandler.MatchStatusText != null)
			{
				if (!fanspickPitchEventHandler.MatchStatusText.Contains("Half") && !fanspickPitchEventHandler.MatchStatusText.Contains("+") || fanspickPitchEventHandler.MatchStatusText.Contains("Full"))
				{
					GetFanspickUserActionStat();
				}
				else
				{
					await GetFanspickUserActionStatLive();
				}
			}
			else
			{
				GetFanspickUserActionStat();
			}
		}

		private async void GetFanspickUserActionStat()
		{

			RequestMinuteStatsDTO getManagerPickPercentageRequest = new RequestMinuteStatsDTO();
			getManagerPickPercentageRequest.AccessToken = FanspickCache.UserData.AccessToken;
			getManagerPickPercentageRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
			getManagerPickPercentageRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
			getManagerPickPercentageRequest.isLive = true;

			ServiceResponse serviceMResponse = await new FanspickServices<MinutesStatsService>().Run(getManagerPickPercentageRequest);

			if (serviceMResponse.OK)
			{
				ResponseMinuteStatsDTO userManagerPercentageResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseMinuteStatsDTO>(serviceMResponse.Data);

				if (userManagerPercentageResponse.Data == null)
				{
					StatsMin.IsVisible = false;
				}
				else
				{
					StatsMin.IsVisible = true;
					if (userManagerPercentageResponse.Data.Action15.Count != 0)
					{
						Action15.IsVisible = true; Action15.ItemsSource = userManagerPercentageResponse.Data.Action15;
						LabelAction15.IsVisible = false;
					}
					else
					{
						Action15.IsVisible = false;
						LabelAction15.IsVisible = true;
					}
					if (userManagerPercentageResponse.Data.Action30.Count != 0)
					{
						Action30.IsVisible = true; Action30.ItemsSource = userManagerPercentageResponse.Data.Action30;
						LabelAction30.IsVisible = false;
					}
					else
					{
						Action30.IsVisible = false;
						LabelAction30.IsVisible = true;
					}
					if (userManagerPercentageResponse.Data.Action45.Count != 0)
					{
						Action45.IsVisible = true; Action45.ItemsSource = userManagerPercentageResponse.Data.Action45;
						LabelAction45.IsVisible = false;
					}
					else
					{
						Action45.IsVisible = false;
						LabelAction45.IsVisible = true;
					}

					if (userManagerPercentageResponse.Data.Action60.Count != 0)
					{
						Action60.IsVisible = true; Action60.ItemsSource = userManagerPercentageResponse.Data.Action60;
						LabelAction60.IsVisible = false;
					}
					else
					{
						Action60.IsVisible = false;
						LabelAction60.IsVisible = true;
					}
					if (userManagerPercentageResponse.Data.Action75.Count != 0)
					{
						Action75.IsVisible = true; Action75.ItemsSource = userManagerPercentageResponse.Data.Action75;
						LabelAction75.IsVisible = false;
					}
					else
					{
						Action75.IsVisible = false;
						LabelAction75.IsVisible = true;

					}
					if (userManagerPercentageResponse.Data.Action90.Count != 0)
					{
						Action90.IsVisible = true; Action90.ItemsSource = userManagerPercentageResponse.Data.Action90;
						LabelAction90.IsVisible = false;
					}
					else
					{
						Action90.IsVisible = false;
						LabelAction90.IsVisible = true;

					}
				}




			}
			else
			{
				StatsMin.IsVisible = false;
			}
		}


		private async Task GetFanspickUserActionStatLive()
		{

			RequestMinuteStatsDTO getManagerPickPercentageRequest = new RequestMinuteStatsDTO();
			getManagerPickPercentageRequest.AccessToken = FanspickCache.UserData.AccessToken;
			getManagerPickPercentageRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
			getManagerPickPercentageRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
			getManagerPickPercentageRequest.isLive = true;

			ServiceResponse serviceMResponse = await new FanspickServices<MinutesStatsService>().Run(getManagerPickPercentageRequest);

			if (serviceMResponse.OK)
			{
				userManagerPercentageResponse1 = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseMinuteStatsDTO>(serviceMResponse.Data);

				if (userManagerPercentageResponse1.Data == null)
				{
					StatsMin.IsVisible = false;
				}
				else
				{

					StatsMin.IsVisible = false;
					Action15.IsVisible = false;
					LabelAction15.IsVisible = false;
					Action30.IsVisible = false;
					LabelAction30.IsVisible = false;
					Action60.IsVisible = false;
					LabelAction45.IsVisible = false;
					Action45.IsVisible = false;
					LabelAction60.IsVisible = false;
					Action75.IsVisible = false;
					LabelAction75.IsVisible = false;
					Action90.IsVisible = false;
					LabelAction90.IsVisible = false;
					Action15.ItemsSource = null;
					Action30.ItemsSource = null;
					Action45.ItemsSource = null;
					Action60.ItemsSource = null;
					Action75.ItemsSource = null;
					Action90.ItemsSource = null;
					label15.IsVisible = false;
					label30.IsVisible = false;
					label45.IsVisible = false;
					label60.IsVisible = false;
					label75.IsVisible = false;
					label90.IsVisible = false;

					TimeSpan RunEveryX = new TimeSpan(0, 0, 1);
					Device.StartTimer(RunEveryX, RaiseEvents);
					if (matchHalves == "0to15")
					{
						StatsMin.IsVisible = false;
						Action15.IsVisible = false;
						LabelAction15.IsVisible = false;
						Action30.IsVisible = false;
						LabelAction30.IsVisible = false;
						Action60.IsVisible = false;
						LabelAction45.IsVisible = false;
						Action45.IsVisible = false;
						LabelAction60.IsVisible = false;
						Action75.IsVisible = false;
						LabelAction75.IsVisible = false;
						Action90.IsVisible = false;
						LabelAction90.IsVisible = false;
						Action15.ItemsSource = null;
						Action30.ItemsSource = null;
						Action45.ItemsSource = null;
						Action60.ItemsSource = null;
						Action75.ItemsSource = null;
						Action90.ItemsSource = null;
						label15.IsVisible = false;
						label30.IsVisible = false;
						label45.IsVisible = false;
						label60.IsVisible = false;
						label75.IsVisible = false;
						label90.IsVisible = false;

					}
					if (matchHalves == "15to30")
					{
						label15.IsVisible = true;
						StatsMin.IsVisible = true;

						if (userManagerPercentageResponse1.Data.Action15.Count() == 0)
						{
							Action15.IsVisible = false;
							LabelAction15.IsVisible = true;
						}
						else
						{
							Action15.IsVisible = true; Action15.ItemsSource = userManagerPercentageResponse1.Data.Action15;
							LabelAction15.IsVisible = false;
						}

					}


					if (matchHalves == "30to45")
					{
						StatsMin.IsVisible = true;
						label15.IsVisible = true;
						label30.IsVisible = true;
						if (userManagerPercentageResponse1.Data.Action15.Count() == 0)
						{
							Action15.IsVisible = false;
							LabelAction15.IsVisible = true;
						}
						else
						{
							Action15.IsVisible = true; Action15.ItemsSource = userManagerPercentageResponse1.Data.Action15;
							LabelAction15.IsVisible = false;
						}
						if (userManagerPercentageResponse1.Data.Action30.Count != 0)
						{
							Action30.IsVisible = true; Action30.ItemsSource = userManagerPercentageResponse1.Data.Action30;
							LabelAction30.IsVisible = false;
						}
						else
						{
							Action30.IsVisible = false;
							LabelAction30.IsVisible = true;
						}

					}

					if (matchHalves == "45to60")
					{
						StatsMin.IsVisible = true;
						label15.IsVisible = true;
						label30.IsVisible = true;
						label45.IsVisible = true;

						if (userManagerPercentageResponse1.Data.Action15.Count() == 0)
						{
							Action15.IsVisible = false;
							LabelAction15.IsVisible = true;
						}
						else
						{
							Action15.IsVisible = true; Action15.ItemsSource = userManagerPercentageResponse1.Data.Action15;
							LabelAction15.IsVisible = false;
						}
						if (userManagerPercentageResponse1.Data.Action30.Count != 0)
						{
							Action30.IsVisible = true; Action30.ItemsSource = userManagerPercentageResponse1.Data.Action30;
							LabelAction30.IsVisible = false;
						}
						else
						{
							Action30.IsVisible = false;
							LabelAction30.IsVisible = true;
						}
						if (userManagerPercentageResponse1.Data.Action45.Count != 0)
						{
							Action45.IsVisible = true; Action45.ItemsSource = userManagerPercentageResponse1.Data.Action45;
							LabelAction45.IsVisible = false;
						}
						else
						{
							Action45.IsVisible = false;
							LabelAction45.IsVisible = true;
						}
					}



					if (matchHalves == "60to75")
					{
						StatsMin.IsVisible = true;
						label15.IsVisible = true;
						label30.IsVisible = true;
						label45.IsVisible = true;
						LabelAction45.IsVisible = true;
						LabelAction30.IsVisible = true;
						LabelAction15.IsVisible = true;
						if (userManagerPercentageResponse1.Data.Action15.Count != 0)
						{
							Action15.IsVisible = true; Action15.ItemsSource = userManagerPercentageResponse1.Data.Action15;
							LabelAction15.IsVisible = false;
						}
						else
						{
							Action15.IsVisible = false;
							LabelAction15.IsVisible = true;
						}
						if (userManagerPercentageResponse1.Data.Action30.Count != 0)
						{
							Action30.IsVisible = true; Action30.ItemsSource = userManagerPercentageResponse1.Data.Action30;
							LabelAction30.IsVisible = false;
						}
						else
						{
							Action30.IsVisible = false;
							LabelAction30.IsVisible = true;
						}
						if (userManagerPercentageResponse1.Data.Action45.Count != 0)
						{
							Action45.IsVisible = true; Action45.ItemsSource = userManagerPercentageResponse1.Data.Action45;
							LabelAction45.IsVisible = false;
						}
						else
						{
							Action45.IsVisible = false;
							LabelAction45.IsVisible = true;
						}


					}



					if (matchHalves == "75to90")
					{
						StatsMin.IsVisible = true;
						label15.IsVisible = true;
						label30.IsVisible = true;
						label45.IsVisible = true;
						label60.IsVisible = true;

						LabelAction60.IsVisible = true;
						if (userManagerPercentageResponse1.Data.Action15.Count != 0)
						{
							Action15.IsVisible = true; Action15.ItemsSource = userManagerPercentageResponse1.Data.Action15;
							LabelAction15.IsVisible = false;
						}
						else
						{
							Action15.IsVisible = false;
							LabelAction15.IsVisible = true;
						}
						if (userManagerPercentageResponse1.Data.Action30.Count != 0)
						{
							Action30.IsVisible = true; Action30.ItemsSource = userManagerPercentageResponse1.Data.Action30;
							LabelAction30.IsVisible = false;
						}
						else
						{
							Action30.IsVisible = false;
							LabelAction30.IsVisible = true;
						}
						if (userManagerPercentageResponse1.Data.Action45.Count != 0)
						{
							Action45.IsVisible = true; Action45.ItemsSource = userManagerPercentageResponse1.Data.Action45;
							LabelAction45.IsVisible = false;
						}
						else
						{
							Action45.IsVisible = false;
							LabelAction45.IsVisible = true;
						}

						if (userManagerPercentageResponse1.Data.Action60.Count != 0)
						{
							Action60.IsVisible = true; Action60.ItemsSource = userManagerPercentageResponse1.Data.Action60;
							LabelAction60.IsVisible = false;
						}
						else
						{
							Action60.IsVisible = false;
							LabelAction60.IsVisible = true;
						}


					}



					if (matchHalves == "90to105")
					{
						StatsMin.IsVisible = true;
						label15.IsVisible = true;
						label30.IsVisible = true;
						label45.IsVisible = true;
						label60.IsVisible = true;
						label75.IsVisible = true;



						if (userManagerPercentageResponse1.Data.Action15.Count != 0)
						{
							Action15.IsVisible = true; Action15.ItemsSource = userManagerPercentageResponse1.Data.Action15;
							LabelAction15.IsVisible = false;
						}
						else
						{
							Action15.IsVisible = false;
							LabelAction15.IsVisible = true;
						}
						if (userManagerPercentageResponse1.Data.Action30.Count != 0)
						{
							Action30.IsVisible = true; Action30.ItemsSource = userManagerPercentageResponse1.Data.Action30;
							LabelAction30.IsVisible = false;
						}
						else
						{
							Action30.IsVisible = false;
							LabelAction30.IsVisible = true;
						}
						if (userManagerPercentageResponse1.Data.Action45.Count != 0)
						{
							Action45.IsVisible = true; Action45.ItemsSource = userManagerPercentageResponse1.Data.Action45;
							LabelAction45.IsVisible = false;
						}
						else
						{
							Action45.IsVisible = false;
							LabelAction45.IsVisible = true;
						}

						if (userManagerPercentageResponse1.Data.Action60.Count != 0)
						{
							Action60.IsVisible = true; Action60.ItemsSource = userManagerPercentageResponse1.Data.Action60;
							LabelAction60.IsVisible = false;
						}
						else
						{
							Action60.IsVisible = false;
							LabelAction60.IsVisible = true;
						}
						if (userManagerPercentageResponse1.Data.Action75.Count != 0)
						{
							Action75.IsVisible = true; Action75.ItemsSource = userManagerPercentageResponse1.Data.Action75;
							LabelAction75.IsVisible = false;
						}
						else
						{
							Action75.IsVisible = false;
							LabelAction75.IsVisible = true;

						}


					}
					if (matchHalves == "105to120")
					{
						StatsMin.IsVisible = true;
						label15.IsVisible = true;
						label30.IsVisible = true;
						label45.IsVisible = true;
						label60.IsVisible = true;
						label75.IsVisible = true;

						label90.IsVisible = true;

						LabelAction90.IsVisible = false;

						if (userManagerPercentageResponse1.Data.Action15.Count != 0)
						{
							Action15.IsVisible = true; Action15.ItemsSource = userManagerPercentageResponse1.Data.Action15;
							LabelAction15.IsVisible = false;
						}
						else
						{
							Action15.IsVisible = false;
							LabelAction15.IsVisible = true;
						}
						if (userManagerPercentageResponse1.Data.Action30.Count != 0)
						{
							Action30.IsVisible = true; Action30.ItemsSource = userManagerPercentageResponse1.Data.Action30;
							LabelAction30.IsVisible = false;
						}
						else
						{
							Action30.IsVisible = false;
							LabelAction30.IsVisible = true;
						}
						if (userManagerPercentageResponse1.Data.Action45.Count != 0)
						{
							Action45.IsVisible = true; Action45.ItemsSource = userManagerPercentageResponse1.Data.Action45;
							LabelAction45.IsVisible = false;
						}
						else
						{
							Action45.IsVisible = false;
							LabelAction45.IsVisible = true;
						}

						if (userManagerPercentageResponse1.Data.Action60.Count != 0)
						{
							Action60.IsVisible = true; Action60.ItemsSource = userManagerPercentageResponse1.Data.Action60;
							LabelAction60.IsVisible = false;
						}
						else
						{
							Action60.IsVisible = false;
							LabelAction60.IsVisible = true;
						}
						if (userManagerPercentageResponse1.Data.Action75.Count != 0)
						{
							Action75.IsVisible = true; Action75.ItemsSource = userManagerPercentageResponse1.Data.Action75;
							LabelAction75.IsVisible = false;
						}
						else
						{
							Action75.IsVisible = false;
							LabelAction75.IsVisible = true;

						}

						if (userManagerPercentageResponse1.Data.Action90.Count != 0)
						{
							Action90.IsVisible = true; Action90.ItemsSource = userManagerPercentageResponse1.Data.Action90;
							LabelAction90.IsVisible = false;
						}
						else
						{
							Action90.IsVisible = false;
							LabelAction90.IsVisible = true;

						}


					}
				}
			}




			else
			{
				StatsMin.IsVisible = false;
			}
		}



	}


}