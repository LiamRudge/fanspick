﻿﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Badge.Plugin;
using Fanspick.Helper;
using Fanspick.PopUp;
using Fanspick.Shared.Helper;
using Fanspick.ViewModels;
using Plugin.Connectivity;
using Plugin.DeviceInfo;
using Plugin.Geolocator;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using static Fanspick.Shared.Models.DTO.UserNotificationDTO;

namespace Fanspick.Templates
{
	public partial class AlertScreen : ContentPage
	{
		AlertScreenModal vm;

		public AlertScreen()
		{
			InitializeComponent();
            vm = CacheStorage.ViewModels.AlertModal;
            BindingContext = vm;

			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{

                if(CrossConnectivity.Current.IsConnected){
                    InternetScreen.IsVisible = false;    
                }else{
                    InternetScreen.IsVisible = true;
                }

				
			};



		}

		protected async override void OnAppearing()
		{
			base.OnAppearing();
            await Navigation.PopAllPopupAsync(true);
			await FetchAlerts();
		}

		async void OnNotificationSelected(object sender, EventArgs e)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)e;
			ResponseUserNotificationData notificationAlerts = (ResponseUserNotificationData)eventArgs.Parameter;
			FanspickCache.CurrentFanspickScope.SelectedAlert = notificationAlerts;
            var _navPop = new NavigationPopUp();
            await Navigation.PushPopupAsync(_navPop);
			await updateNotificationToRead(notificationAlerts.notification._id, true, false);

			GetInfo("alertClicked", notificationAlerts.notification.NotificationMessage, notificationAlerts.notification._id);

			//  ((ListView)sender).SelectedItem = null;

		}


		async void ReadAllBtn(object sender, EventArgs e)
		{

			Shared.Models.Service.ServiceResponse response = await vm.ReadAllNotification();

			if (response.OK)
			{
				await FetchAlerts();
			}
			else
			{

			}

		}

		async void DeleteAllBtn(object sender, EventArgs e)
		{

			Shared.Models.Service.ServiceResponse response = await vm.DeleteAllNotification();

			if (response.OK)
			{
				await FetchAlerts();
			}
			else
			{

			}

		}

		private async Task updateNotificationToRead(string notficationID, bool isRead, bool isDelete)
		{
			Shared.Models.Service.ServiceResponse response = await vm.UpdateNotificationStatus(notficationID, isRead, isDelete);

			if (response.OK)
			{
				vm.Alerts.Clear();
				await FetchAlerts();
			}
			else
			{

			}
		}


		private async Task FetchAlerts()
		{
			Shared.Models.Service.ServiceResponse response = await vm.GetAlerts();

			if (response.OK)
			{
				Shared.Models.DTO.UserNotificationDTO.ResponseUserNotificationDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<Shared.Models.DTO.UserNotificationDTO.ResponseUserNotificationDTO>(response.Data);
				vm.Alerts = dto.Data;

				if (vm.Alerts.Count > 0)
				{
					vm.Alerts.Insert(0, new ResponseUserNotificationData()
					{
						isRead = true,
						notification = new NotificationDetails()
						{
							NotificationTitle = "Fanspick",
							NotificationMessage = "Win 2 tickets to a league match of your choice by sharing YourPick with the hashtag #Fanspick"
						}
					});
					alertbottom.IsVisible = true;

                    int unread = 0;
                    for (int i = 0; i < vm.Alerts.Count(); i++)
                    {

                        if(!vm.Alerts[i].isRead){
                            unread = unread + 1;
                        }


                    }

                    CrossBadge.Current.SetBadge(unread);

				}
				else
				{

					alertbottom.IsVisible = false;

					vm.Alerts.Insert(0, new ResponseUserNotificationData()
					{
						isRead = true,
						notification = new NotificationDetails()
						{
							NotificationTitle = "Fanspick",
							NotificationMessage = "Win 2 tickets to a league match of your choice by sharing YourPick with the hashtag #Fanspick"
						}
					});

					CrossBadge.Current.ClearBadge();


				}

			}
			else
			{
				Debug.WriteLine("", "" + response.ReasonPhrase);
			}
		}



		public async void GetInfo(String eventtype, String description, String additionalId)
		{
			try
			{


				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

				if (position == null)
				{
					Debug.WriteLine("null gps", "null gps");
					return;
				}

				if (position.Latitude.ToString() != null)
				{
					vm.requestEventLoggingDTO.Latitude = "" + position.Latitude;
				}
				else
				{
					vm.requestEventLoggingDTO.Latitude = "0";
				}

				if (position.Longitude.ToString() != null)
				{
					vm.requestEventLoggingDTO.Longitude = "" + position.Longitude;
				}
				else
				{
					vm.requestEventLoggingDTO.Longitude = "0";
				}
				vm.requestEventLoggingDTO.EventType = eventtype;
				vm.requestEventLoggingDTO.EventDescription = description;
				vm.requestEventLoggingDTO.EventAdditionalInfoID = additionalId;
				vm.requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
				vm.requestEventLoggingDTO.DeviceType = "IOS";
				vm.requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
				vm.requestEventLoggingDTO.AppVersion = "1";

				Shared.Models.Service.ServiceResponse response = await vm.EventLog();
				//  Application.Current.MainPage = new SplashScreen();


			}
			catch (Exception ex)
			{
				Debug.WriteLine(" " + ex);

			}

		}
	}
}
