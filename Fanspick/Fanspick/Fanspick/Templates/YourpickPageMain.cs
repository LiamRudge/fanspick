﻿using Fanspick.Helper;
using Fanspick.Shared.Helper;
using Fanspick.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Fanspick.Shared.Events.Events;
using System.Diagnostics;
using System.ComponentModel;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Fanspick.Shared.Models;
using static Fanspick.Shared.Models.DTO.GetTeamDataDTO;
using System.IO;
using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;
using Plugin.Geolocator;
using Plugin.DeviceInfo;
using static Fanspick.Shared.Models.DTO.GetPitchPlayersDTO;
using static Fanspick.Shared.Models.DTO.SwapPitchPlayersDTO;
using Rg.Plugins.Popup.Extensions;
using Fanspick.PopUp;
using static Fanspick.Shared.Models.DTO.MatchCommentriesDTO;
using Fanspick.Screens;
using Rg.Plugins.Popup.Services;
using Refit;
using Fanspick.RestAPI;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using Plugin.Connectivity;

namespace Fanspick.Templates
{
    public partial class YourpickPageMain : ContentPage, INotifyPropertyChanged
    {

        public new event PropertyChangedEventHandler PropertyChanged;
        Boolean isReplace = false;
        public void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        bool hasPreData { get; set; }

        public bool showShirts { get; set; } = false;
        public string currentFormation { get; set; }
        List<String> playerListRow1 = new List<String>();
        List<String> playerListRow2 = new List<String>();
        List<String> playerListRow3 = new List<String>();
        List<String> playerListRow4 = new List<String>();
        Boolean isLoad = false;
        App_MainViewModel vmApp_Main;
        FanspickPitchEventHandler fanspickPitchEventHandler;
        Boolean isShowLive = false;
        Rectangle rec;
        DateTime parsedFirstFixtureDate;
        DateTime parsedSecondFixtureDate;
        Boolean isGoalKeeper = false;
        Boolean is4Line;
        public int selPlayerCount;
        private List<Squad> selectablePlayers = new List<Squad>();
        public List<Squad> SelectablePlayers
        {
            get
            {
                return selectablePlayers;
            }
            set
            {
                selectablePlayers = value;
                foreach (var item in selectablePlayers)
                {
                    if (item.Role == "G")
                    {
                        item.shirt = "goalkeeper.png";
                    }
                    else
                    {
                        item.shirt = FanspickCache.CurrentFanspickScope.TeamShirtImage;
                    }
                }
            }
        }


        int tapCountSw = 0;
        bool isFirst = true;
        bool isSecond = true;

        public YourpickPageMain()
        {
            InitializeComponent();
            vmApp_Main = CacheStorage.ViewModels.App_Main;
            rec = new Rectangle(0, 0, 1, 1);
            selPlayerCount = 0;
            fanspickPitchEventHandler = new FanspickPitchEventHandler();
            fanspickPitchEventHandler.OnRunUp += FanspickPitchEventHandler_OnRunUp;
            fanspickPitchEventHandler.OnPreMatch += FanspickPitchEventHandler_OnPreMatch;
            fanspickPitchEventHandler.OnLiveMatchStart += FanspickPitchEventHandler_OnLiveMatchStart;
            fanspickPitchEventHandler.OnPreMatchEnd += FanspickPitchEventHandler_OnPreMatchEnd;
            fanspickPitchEventHandler.OnLiveMatchFinish += FanspickPitchEventHandler_OnLiveMatchFinish;
            fanspickPitchEventHandler.UpdateCounterText += FanspickPitchEventHandler_UpdateCounterText;
            fanspickPitchEventHandler.GetMatchStatus += FanspickPitchEventHandler_GetMatchStatus;
            fanspickPitchEventHandler.UpdateCounterTimeText += FanspickPitchEventHandler_UpdateCounterTimeText;
            BindingContext = this;

            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {

                if (CrossConnectivity.Current.IsConnected)
                {
                    InternetScreen.IsVisible = false;
                }
                else
                {
                    InternetScreen.IsVisible = true;
                }


            };

        }

        void FanspickPitchEventHandler_OnRunUp(object sender, EventArgs e)
        {
            if (FanspickPitchEventHandler.MatchStage == MatchStage.RunUp)
            {
                if (!hasUserPickData)
                {
                    hideAllPositions();

                    GetUserPick(false);
                }
                counterLabel.TextColor = Color.White;
            }
        }

        void FanspickPitchEventHandler_OnPreMatch(object sender, EventArgs e)
        {
            if (FanspickPitchEventHandler.MatchStage == MatchStage.PreMatch)
            {
                if (!hasUserPickData)
                {
                    hideAllPositions();

                    GetUserPick(false);
                }
                counterLabel.TextColor = Color.White;
            }
        }


        async Task<string> CheckOnlineVersion()
        {

            HttpClient _client = new HttpClient(); // you should reuse a HttpClient!
            String appStoreAppVersion = "";
            string url = "http://itunes.apple.com/lookup?bundleId=com.fanspick.fanspick";

            using (var response = await _client.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    var lookup = JsonConvert.DeserializeObject<Dictionary<string, object>>(response.Content.ReadAsStringAsync().Result);
                    // Horray it went well!
                    if (lookup != null
                        && lookup.Count >= 1
                        && lookup["resultCount"] != null
                        && Convert.ToInt32(lookup["resultCount"].ToString()) > 0)
                    {

                        var results = JsonConvert.DeserializeObject<List<object>>(lookup[@"results"].ToString());


                        if (results != null && results.Count > 0)
                        {
                            var values = JsonConvert.DeserializeObject<Dictionary<string, object>>(results[0].ToString());
                            appStoreAppVersion = values.ContainsKey("version") ? values["version"].ToString() : string.Empty;
                        }
                    }
                }
            }

            return appStoreAppVersion;
        }


        Animation CounterTextAnimation = new Animation();
        void FanspickPitchEventHandler_OnLiveMatchStart(object sender, EventArgs e)
        {

            if (fanspickPitchEventHandler.matchApistatus.Contains("Full") != true)
            {
                SwIcon.Source = "swtich_on";
                isShowLive = true;
                FanspickCache.showLIve = isShowLive;
                hideAllPositions();
                ClearALLPreviousAction();
                GetUserPick(isShowLive);
            }
            else
            {
                SwIcon.Source = "swtich_off";
                isShowLive = false;
                FanspickCache.showLIve = isShowLive;
                hideAllPositions();
                ClearALLPreviousAction();
                GetUserPick(isShowLive);
            }

            //hideAllPositions();

            // GetUserPick(true);

            OKToPlayAnimation = true;

            StartCounterTextAnimation();
            DependencyService.Get<IFanspickLocalNotification>().TriggerNotification("Fanspick", "We've now kicked-off! You are being taken to YourPick Live where you give your opinion on the live match.");

        }


        void FanspickPitchEventHandler_OnPreMatchEnd(object sender, EventArgs e)
        {

            SwIcon.Source = "swtich_on";
            isShowLive = true;
            FanspickCache.showLIve = isShowLive;
            hideAllPositions();
            ClearALLPreviousAction();
            GetUserPick(isShowLive);

            DependencyService.Get<IFanspickLocalNotification>().TriggerNotification("Fanspick", "We've now kicked-off! You are being taken to YourPick Live where you give your opinion on the live match.");

        }


        void FanspickPitchEventHandler_OnLiveMatchFinish(object sender, EventArgs e)
        {
            StopCounterTextAnimation();
            counterLabel.TextColor = Color.White;
            counterLabel.TextColor = Color.White;
            if (fanspickPitchEventHandler.matchApistatus.Contains("Full") != true)
            {
                SwIcon.Source = "swtich_on";
                isShowLive = true;
                FanspickCache.showLIve = isShowLive;
                hideAllPositions();
                ClearALLPreviousAction();
                GetUserPick(isShowLive);
            }
            else
            {
                SwIcon.Source = "swtich_off";
                isShowLive = false;
                FanspickCache.showLIve = isShowLive;
                hideAllPositions();
                ClearALLPreviousAction();
                GetUserPick(isShowLive);
            }
        }

        private void StartCounterTextAnimation()
        {
            CounterTextAnimation.Add(0, 0.5, new Animation((v) =>
            {
                counterLabel.Opacity = v;
            }, 0, 1, Easing.CubicInOut, () => { System.Diagnostics.Debug.WriteLine("ANIMATION A"); }));

            CounterTextAnimation.Add(0.5, 1, new Animation((v) =>
            {
                counterLabel.Opacity = v;
            }, 1, 0, Easing.CubicInOut, () => { System.Diagnostics.Debug.WriteLine("ANIMATION B"); }));

            CounterTextAnimation.Commit(counterLabel, "labelAnimation", 16, 2000, null, (d, f) =>
            {
                counterLabel.Opacity = 0;
                if (OKToPlayAnimation)
                {
                    StartCounterTextAnimation();
                }
                else
                {
                    StopCounterTextAnimation();
                }
            });
        }

        private void StopCounterTextAnimation()
        {
            OKToPlayAnimation = false;
        }

        void FanspickPitchEventHandler_UpdateCounterText(object sender, EventArgs e)
        {
            //CounterText = sender as string;
            //counterLabel.Text = CounterText;
            if (FanspickPitchEventHandler.MatchStage == MatchStage.Live)
            {
                counterLabel.TextColor = Color.FromHex("fff000");
            }
            else
            {
                counterLabel.TextColor = Color.White;
            }
            counterLabel.Text = sender as string;
        }

        void FanspickPitchEventHandler_UpdateCounterTimeText(object sender, EventArgs e)
        {
            //CounterTimeText = sender as string;
            //counterTimeLabel.Text = CounterTimeText;

            counterTimeLabel.Text = sender as string;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            String device = CrossDeviceInfo.Current.Model;
            ajustDevice(device);
        }

        protected async override void OnAppearing()
        {
            isLoad = true;

            base.OnAppearing();

            String device = CrossDeviceInfo.Current.Model;
            ajustDevice(device);


            if (FanspickCache.IsNewUser == "" || FanspickCache.IsNewUser == null || FanspickCache.IsNewUser == "false")
            {
                FanspickCache.IsNewUser = "true";
            }

            try
            {
                if (FanspickCache.UserData.OnceLogin == "false")
                {

                    var onBoard = new OnBoardingPopup();
                    await Navigation.PushPopupAsync(onBoard);

                }

                hideAllPositions();

                getPlayerShirt();

                var semiTransparentColor = new Color(0, 0, 0, 0.7);
                PopUpPlayers.BackgroundColor = semiTransparentColor;
                PopUpPlayers1.BackgroundColor = semiTransparentColor;
                PopUpFormation.BackgroundColor = semiTransparentColor;
                PopUpOptions.BackgroundColor = semiTransparentColor;

                RemoveSpacing();

                LoadGif();

                ImageLogo.Source = TeamLogo;

                if (FanspickCache.CurrentFanspickScope.SelectedFixture != null && FanspickCache.CurrentFanspickScope?.SelectedTeam != null)
                {


                    try
                    {

                        var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

                        RequestGetTeamSquadRefitDTO getTeamDataRequest = new RequestGetTeamSquadRefitDTO();
                        getTeamDataRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
                        getTeamDataRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
                        getTeamDataRequest.IsLive = false;
                        var header = "Bearer " + FanspickCache.UserData.AccessToken;
                        var response = await gitHubApi.GetTeamSquad(getTeamDataRequest, header);

                        if (response.StatusCode == 200)
                        {
                            FanspickCache.CurrentFanspickScope.TeamSquadData = response.Data;

                        }
                        else
                        {
                            FanspickCache.CurrentFanspickScope.TeamSquadData = null;
                        }

                    }
                    catch (ApiException ex)
                    {
                        Debug.WriteLine(ex.ToString());
                        FanspickCache.CurrentFanspickScope.TeamSquadData = null;
                    }

                }

                SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData;

                string currentVersion = DependencyService.Get<IScreen>().Version;
                string availableVersion = await CheckOnlineVersion();

                if (availableVersion != currentVersion)
                {

                    if (SplashScreen.isPopUpshown)
                    {
                    }
                    else
                    {

                        var answer = await DisplayAlert("Update available", "A new verion of Fanspick is available. Please update to " + availableVersion, "Update", "Later");
                        if (answer)
                        {
                            Device.OpenUri(new Uri("https://itunes.apple.com/us/app/fanspick/id1061898805?mt=8"));
                            SplashScreen.isPopUpshown = true;
                        }
                        else
                        {
                            SplashScreen.isPopUpshown = true;
                        }

                    }

                }



            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            //animate();
        }

        void animate()
        {
            var a = new Animation();

            a.Add(0, 0.5, new Animation((v) =>
            {
                Banner1.Opacity = v;
            }, 0, 1, Easing.CubicInOut, () => { System.Diagnostics.Debug.WriteLine("ANIMATION A"); }));

            a.Add(0.5, 1, new Animation((v) =>
            {
                Banner1.Opacity = v;
            }, 1, 0, Easing.CubicInOut, () => { System.Diagnostics.Debug.WriteLine("ANIMATION B"); }));

            a.Commit(Banner1, "animation", 16, 2000, null, (d, f) =>
            {
                Banner1.Opacity = 0;
                System.Diagnostics.Debug.WriteLine("ANIMATION ALL");
                animate();
            });
        }

        void Openbanner(object sender, EventArgs e)
        {
            FanspickCache.isPitch = false;
            var _billBoardPopup = new BillBoard();
            Navigation.PushPopupAsync(_billBoardPopup);
        }

        void OpenbannerPitch(object sender, EventArgs e)
        {
            FanspickCache.isPitch = true;
            var _billBoardPopup = new BillBoard();
            Navigation.PushPopupAsync(_billBoardPopup);
        }


        public async void GetInfo(String eventtype, String description)
        {
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;
                var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
                if (position == null)
                {
                    Debug.WriteLine("null gps", "null gps");
                    return;
                }
                if (position.Latitude.ToString() != null)
                {
                    vmApp_Main.requestEventLoggingDTO.Latitude = "" + position.Latitude;
                }
                else
                {
                    vmApp_Main.requestEventLoggingDTO.Latitude = "0";
                }

                if (position.Longitude.ToString() != null)
                {
                    vmApp_Main.requestEventLoggingDTO.Longitude = "" + position.Longitude;
                }
                else
                {
                    vmApp_Main.requestEventLoggingDTO.Longitude = "0";
                }
                vmApp_Main.requestEventLoggingDTO.EventType = eventtype;
                vmApp_Main.requestEventLoggingDTO.EventDescription = description;
                vmApp_Main.requestEventLoggingDTO.EventAdditionalInfoID = "" + FanspickCache.UserData.AccessToken;
                vmApp_Main.requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
                vmApp_Main.requestEventLoggingDTO.DeviceType = "IOS";
                vmApp_Main.requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
                vmApp_Main.requestEventLoggingDTO.AppVersion = "1";

                Shared.Models.Service.ServiceResponse response = await vmApp_Main.EventLog();
                //  Application.Current.MainPage = new SplashScreen();


            }
            catch (Exception ex)
            {
                Debug.WriteLine(" " + ex);

            }

        }
        protected async void GetMatchStatusAsync()
        {
            try
            {
                
                RequestMatchCommentriesDTO requestDto = new RequestMatchCommentriesDTO();
                requestDto.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
                requestDto.AccessToken = FanspickCache.UserData.AccessToken;
                ServiceResponse response = await new FanspickServices<MatchCommentryService>().Run(requestDto);
                if (response.OK)
                {
                    Shared.Models.DTO.MatchCommentriesDTO.ResponseMatchCommentries dto = Newtonsoft.Json.JsonConvert.DeserializeObject<Shared.Models.DTO.MatchCommentriesDTO.ResponseMatchCommentries>(response.Data);
                    if (dto.Data.MatchStatus != null)
                    {
                        if (dto.Data.MatchStatus.Contains("Half") || dto.Data.MatchStatus.Contains("+") || dto.Data.MatchStatus.Contains("Full"))
                        {
                            fanspickPitchEventHandler.isDelayed = false;
                            if (dto.Data.MatchStatus.Contains("First Half") && isFirst && dto.Data.StatusTime != null)
                            {
                                parsedFirstFixtureDate = DateTime.Parse(dto.Data.StatusTime);
                                TimeZoneInfo timeZoneInformation = TimeZoneInfo.Local;
                                DateTimeOffset timeOffset = new DateTimeOffset(parsedFirstFixtureDate, timeZoneInformation.GetUtcOffset(parsedFirstFixtureDate));
                                string newTime = parsedFirstFixtureDate.ToString("ddd dd MMM yyyy h:mm tt");
                                DateTime.TryParse(newTime, out fanspickPitchEventHandler.fixtureDateNew);
                                vmApp_Main.ApiFixtureDate = fanspickPitchEventHandler.fixtureDateNew;
                                vmApp_Main.ApiFirstFixtureDate = fanspickPitchEventHandler.fixtureDateNew;
                                isFirst = false;


                            }
                            if (dto.Data.MatchStatus.Contains("Second Half") && isSecond && dto.Data.StatusTime != null)
                            {
                                parsedSecondFixtureDate = DateTime.Parse(dto.Data.StatusTime);
                                TimeZoneInfo timeZoneInformation = TimeZoneInfo.Local;
                                DateTimeOffset timeOffset = new DateTimeOffset(parsedSecondFixtureDate, timeZoneInformation.GetUtcOffset(parsedSecondFixtureDate));
                                string newTime = parsedSecondFixtureDate.ToString("ddd dd MMM yyyy h:mm tt");
                                DateTime.TryParse(newTime, out fanspickPitchEventHandler.fixtureDateNew);
                                vmApp_Main.ApiFixtureDate = fanspickPitchEventHandler.fixtureDateNew;
                                vmApp_Main.ApiSecondFixtureDate = fanspickPitchEventHandler.fixtureDateNew;
                                isSecond = false;


                            }
                            if (dto.Data.MatchStatus.Contains("45+") || dto.Data.MatchStatus.Contains("Halftime") && dto.Data.StatusTime != null)
                            {
                                if (parsedFirstFixtureDate.ToString().Contains("0001"))
                                {

                                    DateTime d2 = DateTime.Parse(dto.Data.StatusTime).AddMinutes(-45);
                                    TimeZoneInfo timeZoneInformation = TimeZoneInfo.Local;
                                    DateTimeOffset timeOffset = new DateTimeOffset(d2, timeZoneInformation.GetUtcOffset(d2));
                                    string newTime = d2.ToString("ddd dd MMM yyyy h:mm tt");
                                    DateTime.TryParse(newTime, out fanspickPitchEventHandler.fixtureDateNew);
                                    vmApp_Main.ApiFixtureDate = fanspickPitchEventHandler.fixtureDateNew;


                                }
                                else
                                {
                                    string newTime = parsedFirstFixtureDate.ToString("ddd dd MMM yyyy h:mm tt");
                                    DateTime.TryParse(newTime, out vmApp_Main.ApiFixtureDate);

                                }


                            }
                            if (dto.Data.MatchStatus.Contains("90'+") || dto.Data.MatchStatus.Contains("Full") && dto.Data.StatusTime != null)
                            {
                                if (parsedSecondFixtureDate.ToString().Contains("0001"))
                                {


                                    DateTime d2 = DateTime.Parse(dto.Data.StatusTime).AddMinutes(-45);
                                    TimeZoneInfo timeZoneInformation = TimeZoneInfo.Local;
                                    DateTimeOffset timeOffset = new DateTimeOffset(d2, timeZoneInformation.GetUtcOffset(d2));
                                    string newTime = d2.ToString("ddd dd MMM yyyy h:mm tt");
                                    DateTime.TryParse(newTime, out fanspickPitchEventHandler.fixtureDateNew);
                                    vmApp_Main.ApiFixtureDate = fanspickPitchEventHandler.fixtureDateNew;
                                }
                                else
                                {
                                    string newTime = parsedSecondFixtureDate.ToString("ddd dd MMM yyyy h:mm tt");
                                    DateTime.TryParse(newTime, out vmApp_Main.ApiFixtureDate);
                                }


                            }

                        }
                        else
                        {
                            fanspickPitchEventHandler.isDelayed = true;

                        }
                    }


                    fanspickPitchEventHandler.matchApistatus = dto.Data.MatchStatus;
                    vmApp_Main.ApiMatchStatus = dto.Data.MatchStatus;


                }


                else
                {

                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }
        void FanspickPitchEventHandler_GetMatchStatus(object sender, EventArgs e)
        {

            GetMatchStatusAsync();


        }

        public async void getPlayerShirt()
        {
            try
            {
                var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

                RequestGetTeamDataRefitDTO getTeamDataRequest = new RequestGetTeamDataRefitDTO();
                getTeamDataRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
                var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.GetTeamData(getTeamDataRequest, header);

                if (response.StatusCode == 200)
                {
                    if (response.Data == null || response == null)
                    {
                        FanspickCache.CurrentFanspickScope.TeamShirtImage = "no_shirt";
                        actInd.IsVisible = false;
                    }
                    else
                    {
                        actInd.IsVisible = false;
                        if (response.Data.TeamShirtURL == null)
                        {
                            FanspickCache.CurrentFanspickScope.TeamShirtImage = "no_shirt";
                        }
                        else
                        {
                            FanspickCache.CurrentFanspickScope.TeamShirtImage = "http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamShirts/" + response.Data.TeamShirtURL.Replace(" ", "%20") + ".png";
                        }
                    }

                }
                else
                {
                    actInd.IsVisible = false;
                }

                GetUserPick(isShowLive);

            }
            catch (Exception e)
            {
                FanspickCache.CurrentFanspickScope.TeamShirtImage = "no_shirt";
                actInd.IsVisible = false;
                GetUserPick(isShowLive);
            }

        }





        private async void GetUserPick(Boolean isLive)
        {

            totalPlayersOnPitch = 0;
            FanspickCache.showLIve = isLive;
            actInd.IsVisible = true;

            playerListRow1.Clear();
            playerListRow2.Clear();
            playerListRow3.Clear();
            playerListRow4.Clear();
            ClearALLPreviousAction();

            try
            {
                var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

                RequestGetUserPickRefitDTO getTeamDataRequest = new RequestGetUserPickRefitDTO();
                getTeamDataRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
                getTeamDataRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
                getTeamDataRequest.IsLive = isLive;

                var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.GetUserPick(getTeamDataRequest, header);

                if (response.StatusCode == 200)
                {
                    YourPickNullImage.IsVisible = false;
                    hasUserPickData = true;
                    currentFormation = response.Data.CurrentFormation.Id;

                    if (!isShowLive)
                    {
                        selPlayerCount = response.Data.LineUpPlayers.Count();
                        LoadPlayers(response.Data.Positions);
                        ShowPlayers(response.Data.LineUpPlayers);

                    }
                    else
                    {
                        if (FanspickPitchEventHandler.MatchStage == MatchStage.RunUp)
                        {
                            hideAllPositions();
                            YourPickNullImage.IsVisible = true;
                            NullImageTitle.Text = "Come Back";
                            NullImageText.Text = "after kick off to make live substitutions";
                        }

                        else
                        {
                            if (response.Data.LineUpPlayers.Count == 0)
                            {
                                YourPickNullImage.IsVisible = true;
                                NullImageTitle.Text = "Come Back";
                                NullImageText.Text = "soon to make live substitutions";
                            }
                            else if (response.Data.LineUpPlayers.Count < 11)
                            {
                                YourPickNullImage.IsVisible = true;
                                NullImageTitle.Text = "Come Back";
                                NullImageText.Text = "soon to make live substitutions";
                            }

                            else
                            {

                                YourPickNullImage.IsVisible = false;
                                ShowPlayers(response.Data.LineUpPlayers);
                            }
                        }


                    }
                    actInd.IsVisible = false;
                }
            }
            catch (ApiException e)
            {
                hasUserPickData = false;

                YourPickNullImage.IsVisible = false;

                if (isShowLive)
                {
                    hideAllPositions();

                    YourPickNullImage.IsVisible = true;

                    if (FanspickPitchEventHandler.MatchStage == MatchStage.RunUp)
                    {
                        NullImageTitle.Text = "Come Back";

                        NullImageText.Text = "after kick off to make live substitutions";
                    }
                    else if (FanspickPitchEventHandler.MatchStage == MatchStage.PreMatch)
                    {
                        //NullImageTitle.Text = "We're just waiting on the Manager!";

                        //NullImageText.Text = "come back soon when we have the Managers Pick";
                        NullImageTitle.Text = "Come Back";

                        NullImageText.Text = "after kick off to make live substitutions";

                    }
                    else if (FanspickPitchEventHandler.MatchStage == MatchStage.Live)
                    {
                        //NullImageTitle.Text = "We're just waiting on the Manager!";

                        //NullImageText.Text = "come back soon when we have the Managers Pick";

                        NullImageTitle.Text = "Come Back";

                        NullImageText.Text = "soon to make live substitutions";

                    }
                    else if (FanspickPitchEventHandler.MatchStage >= MatchStage.Past)
                    {

                        if (counterLabel.Text != null)
                        {

                            if (counterLabel.Text.Contains("delayed"))
                            {
                                NullImageTitle.Text = "Match Delayed!";
                                NullImageText.Text = "come back soon to make live substitutions";

                            }
                            else
                            {
                                NullImageTitle.Text = "Full Time!";
                                NullImageText.Text = "this match has now finished";
                            }

                        }



                    }
                    else
                    {
                        NullImageTitle.Text = "Come Back";
                        NullImageText.Text = "soon to make live substitutions";
                    }
                }
                else
                {
                    //pre

                    if (FanspickPitchEventHandler.MatchStage == MatchStage.RunUp)
                    {
                        LoadDefaultPitch();
                    }
                    else if (FanspickPitchEventHandler.MatchStage == MatchStage.PreMatch)
                    {
                        YourPickNullImage.IsVisible = true;

                        NullImageTitle.Text = "Too Late for YourPick!";

                        NullImageText.Text = "but don't forget to make YourPick Live changes";
                    }
                    else if (FanspickPitchEventHandler.MatchStage == MatchStage.Live)
                    {
                        YourPickNullImage.IsVisible = true;

                        NullImageTitle.Text = "Too Late for YourPick!";

                        NullImageText.Text = "but don't forget to make YourPick Live changes";
                    }
                    else if (counterLabel.Text.Contains("delayed"))
                    {
                        YourPickNullImage.IsVisible = true;
                        YourPickNullImage.IsVisible = true;
                        NullImageTitle.Text = "Match Delayed!";
                        NullImageText.Text = "come back soon to make live substitutions";
                    }
                    else
                    {
                        YourPickNullImage.IsVisible = true;

                        YourPickNullImage.IsVisible = true;

                        NullImageTitle.Text = "Full Time!";

                        NullImageText.Text = "this match has now finished";
                    }


                }

                actInd.IsVisible = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }



        }

        protected override void OnDisappearing()
        {
            //YourPickNullImage.Opacity = 0;
            base.OnDisappearing();
        }


        private async void LoadDefaultPitch()
        {
            try
            {

                var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

                RequestGetFormationByTypeRefitDTO getTeamDataRequest = new RequestGetFormationByTypeRefitDTO();
                getTeamDataRequest.Type = FanspickCache.Formations.First().type; ;
                var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.GetFormationByType(getTeamDataRequest, header);

                if (response.StatusCode == 200)
                {
                    currentFormation = response.Data.CurrentFormation.Id;
                    LoadPlayers(response.Data.Positions);

                }

            }
            catch (Exception e)
            {

            }


        }


        public async void ChangePitchFormation(string SelectedType)
        {

            try
            {

                var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

                RequestGetFormationByTypeRefitDTO getTeamDataRequest = new RequestGetFormationByTypeRefitDTO();
                getTeamDataRequest.Type = SelectedType;
                var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.GetFormationByType(getTeamDataRequest, header);

                if (response.StatusCode == 200)
                {
                    playerListRow1.Clear();
                    playerListRow2.Clear();
                    playerListRow3.Clear();
                    playerListRow4.Clear();
                    currentFormation = response.Data.CurrentFormation.Id;
                    LoadPlayers(response.Data.Positions);

                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }

        }



        private void LoadPlayers(List<Position> players)
        {
            List<SelectUserPickPlayer> playerList = new List<SelectUserPickPlayer>();
            ClearALLPreviousAction();
            for (int i = 0; i < players.Count(); i++)
            {

                Debug.WriteLine("xY", "" + players[i].PosX + " " + players[i].PosY);

                // enablePlayers(players[i].PosX, players[i].PosY, "", null);
                SelectUserPickPlayer selectedPlayer = new SelectUserPickPlayer();
                selectedPlayer.PosX = players[i].PosX;
                selectedPlayer.PosY = players[i].PosY;
                selectedPlayer.PlayerName = "";
                selectedPlayer.PLayerId = "0";
                selectedPlayer.PositionId = players[i].Id;
                selectedPlayer.Action = "0";

                playerList.Add(selectedPlayer);
                enablePlayers(selectedPlayer.PosX, selectedPlayer.PosY, selectedPlayer.PlayerName, "gk_grey_plus", selectedPlayer.Action, "goalkeeper_plus");

                ArrangePlayerLine(players[i].PosX, players[i].PosY);

            }

            if (isLoad == false && selPlayerCount == 11 && !fanspickPitchEventHandler.MatchStatusText.Contains("Live") && !fanspickPitchEventHandler.MatchStatusText.Contains("Half") && !fanspickPitchEventHandler.MatchStatusText.Contains("Full"))
            {
                showSharebubbleAsync();
            }
            else
            {
                //Navigation.PopAllPopupAsync(true);
            }
            ArrangementOfPlayers(playerListRow1, playerListRow2, playerListRow3, playerListRow4);

            FanspickCache.SelectUserPick = playerList;

        }

        private void ShowPlayers(List<LineUpPlayerData> showPlayer)
        {

            Debug.WriteLine("", FanspickCache.SelectUserPick.ToString());
            List<SelectUserPickPlayer> playerList = new List<SelectUserPickPlayer>();
            ClearALLPreviousAction();
            for (int i = 0; i < showPlayer.Count(); i++)
            {
                if (showPlayer[i] != null && showPlayer[i].Position != null && showPlayer[i].Player != null && showPlayer[i].Player.KnownName != null)
                {

                    SelectUserPickPlayer selectedPlayer = new SelectUserPickPlayer();
                    selectedPlayer.PosX = showPlayer[i].Position.PosX;
                    selectedPlayer.PosY = showPlayer[i].Position.PosY;
                    selectedPlayer.PlayerName = showPlayer[i].Player.KnownName;
                    selectedPlayer.PLayerId = showPlayer[i].Player.Id;
                    selectedPlayer.PositionId = showPlayer[i].Position.Id;

                    if (showPlayer[i].Player.userActions.Count > 0)
                    {

                        if (showPlayer[i].Player.userActions.Count == 1)
                        {
                            selectedPlayer.Action = showPlayer[i].Player.userActions[0].Action;
                        }
                        else
                        {
                            selectedPlayer.Action = showPlayer[i].Player.userActions[0].Action + "-" + showPlayer[i].Player.userActions[1].Action;
                        }

                    }

                    else if (showPlayer[i].userActions.Count > 0)
                    {
                        if (showPlayer[i].userActions.Count == 1)
                        {
                            selectedPlayer.Action = showPlayer[i].userActions[0].Action;
                        }
                        else
                        {
                            selectedPlayer.Action = showPlayer[i].userActions[0].Action + "-" + showPlayer[i].userActions[1].Action;
                        }

                    }


                    else
                    {
                        selectedPlayer.Action = "0";
                    }

                    ImageSource shirtimage = "";

                    enablePlayers(selectedPlayer.PosX, selectedPlayer.PosY, selectedPlayer.PlayerName, FanspickCache.CurrentFanspickScope.TeamShirtImage, selectedPlayer.Action, "goalkeeper");
                    playerList.Add(selectedPlayer);

                    ArrangePlayerLine(showPlayer[i].Position.PosX, showPlayer[i].Position.PosY);
                }



                ArrangementOfPlayers(playerListRow1, playerListRow2, playerListRow3, playerListRow4);

            }

            if (isShowLive)
            {
                FanspickCache.SelectUserPickLive = playerList;
            }


            foreach (var item in playerList)
            {
                SelectUserPickPlayer itemInCache = FanspickCache.SelectUserPick.FirstOrDefault(ci => ci.PosX == item.PosX && ci.PosY == item.PosY);
                if (itemInCache != null)
                {
                    itemInCache.PLayerId = item.PLayerId;
                    itemInCache.PlayerName = item.PlayerName;
                    itemInCache.PositionId = item.PositionId;
                    itemInCache.PosX = item.PosX;
                    itemInCache.PosY = item.PosY;
                }
            }

            totalPlayersOnPitch = playerListRow1.Count + playerListRow2.Count + playerListRow3.Count + playerListRow4.Count + 1;

        }
        public async void showSharebubbleAsync()
        {

            ShareFixturePopUp popUp = new ShareFixturePopUp();
            await Task.Delay(2000);
            var y = sharePic.Y + 20;
            var x = shareIcon.X - 80;
            rec.X = x;
            rec.Y = y;
            rec.Height = 70;
            rec.Width = 100;
            popUp.BackgroundColor = Color.Transparent;
            popUp.Layout(rec);

            //  Navigation.PushPopupAsync(popUp); await Task.Delay(8000); Navigation.PopAllPopupAsync(true);

        }

        public void hideAllPositions()
        {
            ClearALLPreviousAction();

            Stack_1_3.IsVisible = false;

            Stack_2_1.IsVisible = false;
            Stack_2_2.IsVisible = false;
            Stack_2_3.IsVisible = false;
            Stack_2_4.IsVisible = false;
            Stack_2_5.IsVisible = false;

            Stack_3_1.IsVisible = false;
            Stack_3_2.IsVisible = false;
            Stack_3_3.IsVisible = false;
            Stack_3_4.IsVisible = false;
            Stack_3_5.IsVisible = false;

            Stack_4_1.IsVisible = false;
            Stack_4_2.IsVisible = false;
            Stack_4_3.IsVisible = false;
            Stack_4_4.IsVisible = false;
            Stack_4_5.IsVisible = false;

            Stack_5_1.IsVisible = false;
            Stack_5_2.IsVisible = false;
            Stack_5_3.IsVisible = false;
            Stack_5_4.IsVisible = false;
            Stack_5_5.IsVisible = false;

        }

        public static ImageSource ConvertToImage(string imageBase64)
        {
            try
            {
                ImageSource image;

                if (string.IsNullOrEmpty(imageBase64))
                {
                    image = Xamarin.Forms.ImageSource.FromFile("no_shirt.png");
                    return image;
                }
                else
                {
                    //  image = Xamarin.Forms.ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(imageBase64)));
                    using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(imageBase64)))
                    {
                        return Xamarin.Forms.ImageSource.FromStream(() => ms);
                    }
                }

            }
            catch (Exception ex)
            {
                return Xamarin.Forms.ImageSource.FromFile("no_shirt.png");
            }
        }

        public void enablePlayers(String Posx, String Posy, String PlayerName, ImageSource playerImage, String action, String ImageGoalKeeper)
        {

            if (Posx == "1" && Posy == "3")
            {
                Stack_1_3.IsVisible = true;
                PlayerName_1_3.Text = PlayerName;
                PlayerImage_1_3.Source = ImageGoalKeeper;
                if (action != "0")
                {
                    PlayerImagePop_1_3.IsVisible = true;
                    if (action == "star") { PlayerImagePop_1_3.Source = "shirt_st"; }
                    else if (action == "hairdryer") { PlayerImagePop_1_3.Source = "shirt_hd"; }
                    else if (action == "manofmatch") { PlayerImagePop_1_3.Source = "shirt_mom"; }
                    else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_1_3.Source = "shirt_mom+s"; }
                    else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_1_3.Source = "shirt_mom+h"; }
                }

            }

            if (Posx == "2")
            {
                if (Posy == "1")
                {
                    Stack_2_1.IsVisible = true;
                    PlayerImage_2_1.Source = playerImage;
                    PlayerName_2_1.Text = PlayerName;
                    PlayerImage_2_1.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_2_1.IsVisible = true;
                        if (action == "star") { PlayerImagePop_2_1.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_2_1.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_2_1.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_1.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_1.Source = "shirt_mom+h"; }
                    }

                }
                if (Posy == "2")
                {
                    Stack_2_2.IsVisible = true;
                    PlayerImage_2_2.Source = playerImage;
                    PlayerName_2_2.Text = PlayerName;
                    PlayerImage_2_2.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_2_2.IsVisible = true;
                        if (action == "star") { PlayerImagePop_2_2.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_2_2.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_2_2.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_2.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_2.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "3")
                {
                    Stack_2_3.IsVisible = true;
                    PlayerImage_2_3.Source = playerImage;
                    PlayerName_2_3.Text = PlayerName;
                    PlayerImage_2_3.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_2_3.IsVisible = true;
                        if (action == "star") { PlayerImagePop_2_3.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_2_3.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_2_3.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_3.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_3.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "4")
                {
                    Stack_2_4.IsVisible = true;
                    PlayerImage_2_4.Source = playerImage;
                    PlayerName_2_4.Text = PlayerName;
                    PlayerImage_2_4.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_2_4.IsVisible = true;
                        if (action == "star") { PlayerImagePop_2_4.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_2_4.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_2_4.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_4.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_4.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "5")
                {
                    Stack_2_5.IsVisible = true;
                    PlayerImage_2_5.Source = playerImage;
                    PlayerName_2_5.Text = PlayerName;
                    PlayerImage_2_5.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_2_5.IsVisible = true;
                        if (action == "star") { PlayerImagePop_2_5.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_2_5.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_2_5.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_5.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_5.Source = "shirt_mom+h"; }
                    }
                }

            }

            if (Posx == "3")
            {
                if (Posy == "1")
                {
                    Stack_3_1.IsVisible = true;
                    PlayerImage_3_1.Source = playerImage;
                    PlayerName_3_1.Text = PlayerName;
                    PlayerImage_3_1.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_3_1.IsVisible = true;
                        if (action == "star") { PlayerImagePop_3_1.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_3_1.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_3_1.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_1.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_1.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "2")
                {
                    Stack_3_2.IsVisible = true;
                    PlayerImage_3_2.Source = playerImage;
                    PlayerName_3_2.Text = PlayerName;
                    PlayerImage_3_2.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_3_2.IsVisible = true;
                        if (action == "star") { PlayerImagePop_3_2.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_3_2.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_3_2.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_2.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_2.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "3")
                {
                    Stack_3_3.IsVisible = true;
                    PlayerImage_3_3.Source = playerImage;
                    PlayerName_3_3.Text = PlayerName;
                    PlayerImage_3_3.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_3_3.IsVisible = true;
                        if (action == "star") { PlayerImagePop_3_3.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_3_3.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_3_3.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_3.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_3.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "4")
                {
                    Stack_3_4.IsVisible = true;
                    PlayerImage_3_4.Source = playerImage;
                    PlayerName_3_4.Text = PlayerName;
                    PlayerImage_3_4.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_3_4.IsVisible = true;
                        if (action == "star") { PlayerImagePop_3_4.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_3_4.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_3_4.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_4.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_4.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "5")
                {
                    Stack_3_5.IsVisible = true;
                    PlayerImage_3_5.Source = playerImage;
                    PlayerName_3_5.Text = PlayerName;
                    PlayerImage_3_5.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_3_5.IsVisible = true;
                        if (action == "star") { PlayerImagePop_3_5.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_3_5.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_3_5.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_5.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_5.Source = "shirt_mom+h"; }
                    }
                }

            }

            if (Posx == "4")
            {
                if (Posy == "1")
                {
                    Stack_4_1.IsVisible = true;
                    PlayerImage_4_1.Source = playerImage;
                    PlayerName_4_1.Text = PlayerName;
                    PlayerImage_4_1.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_4_1.IsVisible = true;
                        if (action == "star") { PlayerImagePop_4_1.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_4_1.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_4_1.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_1.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_1.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "2")
                {
                    Stack_4_2.IsVisible = true;
                    PlayerImage_4_2.Source = playerImage;
                    PlayerName_4_2.Text = PlayerName;
                    PlayerImage_4_2.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_4_2.IsVisible = true;
                        if (action == "star") { PlayerImagePop_4_2.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_4_2.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_4_2.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_2.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_2.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "3")
                {
                    Stack_4_3.IsVisible = true;
                    PlayerImage_4_3.Source = playerImage;
                    PlayerName_4_3.Text = PlayerName;
                    PlayerImage_4_3.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_4_3.IsVisible = true;
                        if (action == "star") { PlayerImagePop_4_3.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_4_3.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_4_3.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_3.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_3.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "4")
                {
                    Stack_4_4.IsVisible = true;
                    PlayerImage_4_4.Source = playerImage;
                    PlayerName_4_4.Text = PlayerName;
                    PlayerImage_4_4.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_4_4.IsVisible = true;
                        if (action == "star") { PlayerImagePop_4_4.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_4_4.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_4_4.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_4.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_4.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "5")
                {
                    Stack_4_5.IsVisible = true;
                    PlayerImage_4_5.Source = playerImage;
                    PlayerName_4_5.Text = PlayerName;
                    PlayerImage_4_5.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_4_5.IsVisible = true;
                        if (action == "star") { PlayerImagePop_4_5.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_4_5.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_4_5.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_5.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_5.Source = "shirt_mom+h"; }
                    }
                }

            }

            if (Posx == "5")
            {
                if (Posy == "1")
                {
                    Stack_5_1.IsVisible = true;
                    PlayerImage_5_2.Source = playerImage;
                    PlayerName_5_1.Text = PlayerName;
                    PlayerImage_5_1.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_5_1.IsVisible = true;
                        if (action == "star") { PlayerImagePop_5_1.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_5_1.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_5_1.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_1.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_1.Source = "shirt_mom+h"; }
                    }

                }
                if (Posy == "2")
                {
                    Stack_5_2.IsVisible = true;
                    PlayerImage_5_2.Source = playerImage;
                    PlayerName_5_2.Text = PlayerName;
                    PlayerImage_5_2.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_5_2.IsVisible = true;
                        if (action == "star") { PlayerImagePop_5_2.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_5_2.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_5_2.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_2.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_2.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "3")
                {
                    Stack_5_3.IsVisible = true;
                    PlayerImage_5_3.Source = playerImage;
                    PlayerName_5_3.Text = PlayerName;
                    PlayerImage_5_3.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_5_3.IsVisible = true;
                        if (action == "star") { PlayerImagePop_5_3.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_5_3.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_5_3.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_3.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_3.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "4")
                {
                    Stack_5_4.IsVisible = true;
                    PlayerImage_5_4.Source = playerImage;
                    PlayerName_5_4.Text = PlayerName;
                    PlayerImage_5_4.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_5_4.IsVisible = true;
                        if (action == "star") { PlayerImagePop_5_4.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_5_4.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_5_4.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_4.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_4.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "5")
                {
                    Stack_5_5.IsVisible = true;
                    PlayerImage_5_5.Source = playerImage;
                    PlayerName_5_5.Text = PlayerName;
                    PlayerImage_5_5.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_5_5.IsVisible = true;
                        if (action == "star") { PlayerImagePop_5_5.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_5_5.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_5_5.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_5.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_5.Source = "shirt_mom+h"; }
                    }
                }

            }
        }

        #region On Tapped

        public void OnTapped_Position_1_3(object sender, EventArgs e)
        {

            MakeActivePosition(1, 3);

            var y = Stack_1_3.Y; var parent = Stack_1_3.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_1_3.X; var parent1 = Stack_1_3.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;

            LoadPopUpPosition(0, 0, 0, 300, rec);
            PopUpOptions.BackgroundColor = Color.Transparent;


        }

        void OnTapped_Position_2_1(object sender, EventArgs e)
        {

            MakeActivePosition(2, 1);
            var y = Stack_2_1.Y; var parent = Stack_2_1.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_2_1.X; var parent1 = Stack_2_1.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;

            if (is4Line)
            {
                LoadPopUpPosition(0, 0, 400, 70, rec);
            }
            else
            {
                LoadPopUpPosition(0, 0, 400, 50, rec);
            }

        }

        void OnTapped_Position_2_2(object sender, EventArgs e)
        {

            MakeActivePosition(2, 2);
            var y = Stack_2_2.Y; var parent = Stack_2_2.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_2_2.X; var parent1 = Stack_2_2.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;

            if (is4Line)
            {
                LoadPopUpPosition(0, 0, 250, 70, rec);
            }
            else
            {
                LoadPopUpPosition(0, 0, 250, 50, rec);
            }


        }
        void OnTapped_Position_2_3(object sender, EventArgs e)
        {

            MakeActivePosition(2, 3);
            if (Stack_2_1.IsVisible == false || Stack_2_2.IsVisible == false)
            {
                var x = 140;
                rec.X = x;
            }
            else
            {

                var x = Stack_2_3.X; var parent1 = Stack_2_3.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
                rec.X = x;

            }
            var y = Stack_2_3.Y; var parent = Stack_2_3.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }

            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(0, 0, 0, 70, rec);
            }
            else
            {
                LoadPopUpPosition(0, 0, 0, 50, rec);
            }

        }

        void OnTapped_Position_2_4(object sender, EventArgs e)
        {

            MakeActivePosition(2, 4);
            var y = Stack_2_4.Y; var parent = Stack_2_4.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_2_4.X; var parent1 = Stack_2_4.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(250, 0, 0, 70, rec);
            }
            else
            {
                LoadPopUpPosition(250, 0, 0, 50, rec);
            }
        }

        void OnTapped_Position_2_5(object sender, EventArgs e)
        {

            MakeActivePosition(2, 5);
            var y = Stack_2_5.Y; var parent = Stack_2_5.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_2_5.X; var parent1 = Stack_2_5.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(400, 0, 0, 70, rec);
            }
            else
            {
                LoadPopUpPosition(400, 0, 0, 50, rec);
            }
        }


        void OnTapped_Position_3_1(object sender, EventArgs e)
        {

            MakeActivePosition(3, 1);
            var y = Stack_3_1.Y; var parent = Stack_3_1.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_3_1.X; var parent1 = Stack_3_1.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(0, 0, 400, -80, rec);
            }
            else
            {
                LoadPopUpPosition(0, 0, 400, -80, rec);
            }

        }

        void OnTapped_Position_3_2(object sender, EventArgs e)
        {

            MakeActivePosition(3, 2);
            var y = Stack_3_2.Y; var parent = Stack_3_2.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_3_2.X; var parent1 = Stack_3_2.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(0, 0, 250, -80, rec);
            }
            else
            {
                LoadPopUpPosition(0, 0, 250, -80, rec);
            }
        }

        void OnTapped_Position_3_3(object sender, EventArgs e)
        {

            MakeActivePosition(3, 3);
            if (Stack_3_1.IsVisible == false || Stack_3_2.IsVisible == false)
            {
                var x = 140;
                rec.X = x;
            }
            else
            {

                var x = Stack_3_3.X; var parent1 = Stack_3_3.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
                rec.X = x;

            }
            var y = Stack_3_3.Y; var parent = Stack_3_3.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }

            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(0, 0, 0, -80, rec);
            }
            else
            {
                LoadPopUpPosition(0, 0, 0, -80, rec);
            }
        }

        void OnTapped_Position_3_4(object sender, EventArgs e)
        {

            MakeActivePosition(3, 4);
            var y = Stack_3_4.Y; var parent = Stack_3_4.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_3_4.X; var parent1 = Stack_3_4.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(0, 0, -250, -80, rec);
            }
            else
            {
                LoadPopUpPosition(0, 0, -250, -80, rec);
            }
        }

        void OnTapped_Position_3_5(object sender, EventArgs e)
        {

            MakeActivePosition(3, 5);
            var y = Stack_3_5.Y; var parent = Stack_3_5.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_3_5.X; var parent1 = Stack_3_5.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(0, 0, -400, -80, rec);
            }
            else
            {
                LoadPopUpPosition(0, 0, -400, -80, rec);
            }
        }

        void OnTapped_Position_4_1(object sender, EventArgs e)
        {

            MakeActivePosition(4, 1);
            var y = Stack_4_1.Y; var parent = Stack_4_1.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_4_1.X; var parent1 = Stack_4_1.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(0, 280, 400, 0, rec);
            }
            else
            {
                LoadPopUpPosition(0, 280, 400, 0, rec);
            }
        }

        void OnTapped_Position_4_2(object sender, EventArgs e)
        {

            MakeActivePosition(4, 2);
            var y = Stack_4_2.Y; var parent = Stack_4_2.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_4_2.X; var parent1 = Stack_4_2.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            if (is4Line)
            {
                LoadPopUpPosition(0, 280, 280, 0, rec);
            }
            else
            {
                LoadPopUpPosition(0, 280, 280, 0, rec);
            }
        }

        void OnTapped_Position_4_3(object sender, EventArgs e)
        {

            MakeActivePosition(4, 3);
            if (Stack_4_1.IsVisible == false || Stack_4_2.IsVisible == false)
            {
                var x = 140;
                rec.X = x;
            }
            else
            {

                var x = Stack_4_3.X; var parent1 = Stack_4_3.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
                rec.X = x;

            }
            var y = Stack_4_3.Y; var parent = Stack_4_3.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(0, 280, 0, 0, rec);
            }
            else
            {
                LoadPopUpPosition(0, 280, 0, 0, rec);
            }
        }

        void OnTapped_Position_4_4(object sender, EventArgs e)
        {

            MakeActivePosition(4, 4);
            var y = Stack_4_4.Y; var parent = Stack_4_4.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_4_4.X; var parent1 = Stack_4_4.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(250, 280, 0, 0, rec);
            }
            else
            {
                LoadPopUpPosition(250, 280, 0, 0, rec);
            }
        }

        void OnTapped_Position_4_5(object sender, EventArgs e)
        {

            MakeActivePosition(4, 5);
            var y = Stack_4_5.Y; var parent = Stack_4_5.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_4_5.X; var parent1 = Stack_4_5.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(400, 280, 0, 0, rec);
            }
            else
            {
                LoadPopUpPosition(450, 280, 0, 0, rec);
            }
        }

        void OnTapped_Position_5_1(object sender, EventArgs e)
        {

            MakeActivePosition(5, 1);
            var y = Stack_5_1.Y; var parent = Stack_5_1.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_5_1.X; var parent1 = Stack_5_1.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(0, 550, 400, 0, rec);
            }
            else
            {
                LoadPopUpPosition(0, 550, 400, 0, rec);
            }
        }

        void OnTapped_Position_5_2(object sender, EventArgs e)
        {

            MakeActivePosition(5, 2);
            var y = Stack_5_2.Y; var parent = Stack_5_2.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_5_2.X; var parent1 = Stack_5_2.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(0, 550, 280, 0, rec);
            }
            else
            {
                LoadPopUpPosition(0, 550, 280, 0, rec);
            }
        }

        void OnTapped_Position_5_3(object sender, EventArgs e)
        {

            MakeActivePosition(5, 3);
            if (Stack_5_1.IsVisible == false || Stack_5_2.IsVisible == false)
            {
                var x = 140;
                rec.X = x;
            }
            else
            {
                var x = Stack_5_3.X; var parent1 = Stack_5_3.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
                rec.X = x;

            }
            var y = Stack_5_3.Y; var parent = Stack_5_3.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }

            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(0, 550, 0, 0, rec);
            }
            else
            {
                LoadPopUpPosition(0, 550, 0, 0, rec);
            }
        }

        void OnTapped_Position_5_4(object sender, EventArgs e)
        {
            MakeActivePosition(5, 4);
            var y = Stack_5_4.Y; var parent = Stack_5_4.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_5_4.X; var parent1 = Stack_5_4.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(280, 550, 0, 0, rec);
            }
            else
            {
                LoadPopUpPosition(280, 550, 0, 0, rec);
            }
        }

        void OnTapped_Position_5_5(object sender, EventArgs e)
        {

            MakeActivePosition(5, 5);
            var y = Stack_5_5.Y; var parent = Stack_5_5.ParentView; while (parent != null) { y += parent.Y; parent = parent.ParentView; }
            var x = Stack_5_5.X; var parent1 = Stack_5_5.ParentView; while (parent1 != null) { x += parent1.X; parent1 = parent1.ParentView; }
            rec.X = x;
            rec.Y = y;
            rec.Height = 60;
            rec.Width = 90;
            PopUpOptions.BackgroundColor = Color.Transparent;
            if (is4Line)
            {
                LoadPopUpPosition(400, 550, 0, 0, rec);
            }
            else
            {
                LoadPopUpPosition(400, 550, 0, 0, rec);
            }

        }


        #endregion
        private async void MakeActivePosition(int row, int col)
        {
            try
            {

                if (row == 1 && col == 3) { isGoalKeeper = true; } else { isGoalKeeper = false; }
                if (isShowLive)
                {
                    FanspickCache.SelectUserPick = FanspickCache.SelectUserPickLive;
                }

                int positionNumber = 0;
                if (FanspickCache.SelectUserPick != null)
                {
                    if (FanspickCache.SelectUserPick.Count() > 0)
                    {
                        for (int i = 0; i < FanspickCache.SelectUserPick.Count(); i++)
                        {
                            if (row.ToString() == FanspickCache.SelectUserPick[i].PosX && col.ToString() == FanspickCache.SelectUserPick[i].PosY)
                            {
                                positionNumber = i;
                                break;
                            }

                        }

                        Debug.WriteLine("", positionNumber.ToString());
                        selectedPlayer = FanspickCache.SelectUserPick[positionNumber];
                        currentPositionId = selectedPlayer.PositionId;
                        FanspickCache.CurrentFanspickScope.SelectedPlayerId = selectedPlayer.PLayerId;
                    }
                }

                if (!isShowLive)
                {
                    if (FanspickPitchEventHandler.MatchStage == MatchStage.RunUp)
                    {
                        if (selectedPlayer.PLayerId == "0")
                        {
                            if (FanspickCache.CurrentFanspickScope.SelectedFixture != null && FanspickCache.CurrentFanspickScope?.SelectedTeam != null)
                            {
                                try
                                {

                                    var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

                                    RequestGetTeamSquadRefitDTO getTeamDataRequest = new RequestGetTeamSquadRefitDTO();
                                    getTeamDataRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
                                    getTeamDataRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
                                    getTeamDataRequest.IsLive = false;
                                    var header = "Bearer " + FanspickCache.UserData.AccessToken;
                                    var response = await gitHubApi.GetTeamSquad(getTeamDataRequest, header);

                                    if (response.StatusCode == 200)
                                    {
                                        FanspickCache.CurrentFanspickScope.TeamSquadData = response.Data;

                                    }
                                    else
                                    {
                                        FanspickCache.CurrentFanspickScope.TeamSquadData = null;
                                    }

                                }
                                catch (ApiException ex)
                                {
                                    FanspickCache.CurrentFanspickScope.TeamSquadData = null;
                                }
                                catch (Exception ex)
                                {
                                    Debug.WriteLine(ex.ToString());
                                }


                            }
                            if (FanspickCache.CurrentFanspickScope.TeamSquadData != null)
                            {
                                if (FanspickCache.CurrentFanspickScope.TeamSquadData.Count() > 0)
                                {
                                    if (selectedPlayer.PosX == "1")
                                    {
                                        SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(p => p.Role == "G" && FanspickCache.SelectUserPick.All(up => up.PLayerId != p.PlayerId)).ToList();
                                    }
                                    else if (selectedPlayer.PosX == "2")
                                    {
                                        SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(p => (p.Role == "D" || p.Role == "M") && FanspickCache.SelectUserPick.All(up => up.PLayerId != p.PlayerId)).ToList();
                                    }
                                    else if (selectedPlayer.PosX == "3")
                                    {
                                        SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(p => (p.Role == "D" || p.Role == "M" || p.Role == "A") && FanspickCache.SelectUserPick.All(up => up.PLayerId != p.PlayerId)).ToList();
                                    }
                                    else if (selectedPlayer.PosX == "4")
                                    {
                                        SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(p => (p.Role == "D" || p.Role == "M" || p.Role == "A") && FanspickCache.SelectUserPick.All(up => up.PLayerId != p.PlayerId)).ToList();
                                    }
                                    else if (selectedPlayer.PosX == "5")
                                    {
                                        SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(p => (p.Role == "M" || p.Role == "A") && FanspickCache.SelectUserPick.All(up => up.PLayerId != p.PlayerId)).ToList();
                                    }

                                    lvPlayerPop.ItemsSource = SelectablePlayers;


                                    iconStar.IsVisible = false;
                                    iconHairDryer.IsVisible = false;
                                    iconMom.IsVisible = false;
                                    iconRemove.IsVisible = false;
                                    iconChange.IsVisible = false;
                                    PopUpPlayers.IsVisible = true;
                                }
                            }



                        }
                        else
                        {
                            iconRemove.IsVisible = true;
                            FanspickCache.CurrentFanspickScope.SelectedPlayerId = selectedPlayer.PLayerId.ToString();
                            PopUpOptions.IsVisible = true;
                        }

                    }
                    else
                    {

                        if (selectedPlayer.PLayerId == "0")
                        {
                            PopUpOptions.IsVisible = false;
                            await DisplayAlert("Deadline passed", "You cannot select yourpick for a pre match after the deadline has passed", "Close");
                        }
                        else
                        {
                            FanspickCache.CurrentFanspickScope.SelectedPlayerId = selectedPlayer.PLayerId.ToString();
                            PopUpOptions.IsVisible = false;
                            var page = new PlayerInformationPage(selectedPlayer.PLayerId.ToString());
                            await Navigation.PushPopupAsync(page);
                        }


                    }


                }
                else
                {
                    FanspickCache.CurrentFanspickScope.SelectedPlayerId = selectedPlayer.PLayerId.ToString();
                    //Live Player is selected

                    if (FanspickPitchEventHandler.MatchStage == MatchStage.Live)
                    {

                        if (isShowLive)
                        {


                            if (FanspickPitchEventHandler.CanDoManOfTheMatch)
                            {
                                iconMom.IsVisible = true;
                            }
                            else
                            {
                                iconMom.IsVisible = false;
                            }

                            iconStar.IsVisible = true;
                            iconHairDryer.IsVisible = true;
                            iconChange.IsVisible = true;

                            if (isGoalKeeper)
                            { iconChange.IsVisible = false; }
                            else { iconChange.IsVisible = true; }

                            if (fanspickPitchEventHandler.MatchStatusText == "Half Time")
                            {
                                PopUpOptions.IsVisible = false;
                            }
                            else
                            {
                                PopUpOptions.IsVisible = true;
                            }
                        }

                        else
                        {

                            PopUpOptions.IsVisible = false;
                            var page = new PlayerInformationPage(selectedPlayer.PLayerId.ToString());
                            await Navigation.PushPopupAsync(page);

                        }


                    }
                    else
                    {
                        PopUpOptions.IsVisible = false;
                        FanspickCache.CurrentFanspickScope.SelectedPlayerId = selectedPlayer.PLayerId.ToString();
                        var page = new PlayerInformationPage(selectedPlayer.PLayerId.ToString());
                        await Navigation.PushPopupAsync(page);

                    }

                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }


        }

        #region private functions
        private void OpenFormation(object sender, EventArgs e)
        { vmApp_Main.ActiveDetailsPage(ScreenType.FormationPop); }
        #endregion



        public async void ActionOnPlayer(string action)
        {


            RequestCreateActionDTO requestCreateAction = new RequestCreateActionDTO();
            requestCreateAction.AccessToken = FanspickCache.UserData.AccessToken;
            requestCreateAction.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
            requestCreateAction.PlayerId = FanspickCache.CurrentFanspickScope.SelectedPlayerId;
            requestCreateAction.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
            requestCreateAction.userId = FanspickCache.UserData.Id;
            requestCreateAction.Action = action;

            DateTime current = DateTime.Now;
            DateTime convertedFixtureDate = TimeZoneInfo.ConvertTime(current, TimeZoneInfo.Utc);
            String nowTime = convertedFixtureDate.ToString("yyyy-MM-dd HH:MM:ss");//YYYY - MM - DD HH: MM: SS

            requestCreateAction.Time = nowTime;

            ServiceResponse useraction = await new FanspickServices<CreateUserActionService>().Run(requestCreateAction);

            if (useraction.OK)
            {
                PopUpOptions.IsVisible = false;
                ClearALLPreviousAction();
                GetUserPick(isShowLive);
            }
            else
            {
                await DisplayAlert("Something wasn't right there?", useraction.ReasonPhrase, "Close");
            }


        }



        public List<Formation> lstFormations
        {
            get
            {
                return Fanspick.Shared.Helper.FanspickCache.Formations;
            }
        }

        public string currentPositionId { get; private set; }
        public SelectUserPickPlayer selectedPlayer { get; private set; }

        private bool hasData;
        private bool hasUserPickData;
        private List<Squad> lstSelectablePlayers;
        public string CounterText { get; set; }

        //private string counterTimeText;
        //public string CounterTimeText { get { return counterTimeText; } set { counterTimeText = value; NotifyPropertyChanged(); } }
        public string CounterTimeText { get; set; }

        void ClickStar(object sender, EventArgs e)
        {
            ActionOnPlayer("star");
        }

        void ClickHairdryer(object sender, EventArgs e)
        {
            ActionOnPlayer("hairdryer");
        }

        void ClickMom(object sender, EventArgs e)
        {
            ActionOnPlayer("manofmatch");
        }

        void ShowFormation(object sender, EventArgs e)
        {

            if (FanspickPitchEventHandler.MatchStage == MatchStage.RunUp)
            {
                if (!isShowLive)
                {
                    PopUpFormation.IsVisible = true;
                }
                else
                {
                    DisplayAlert("Wait For Kick Off", "You can't make live changes until the match has kicked off.", "OK");
                }
            }
            else if (FanspickPitchEventHandler.MatchStage == MatchStage.PreMatch || FanspickPitchEventHandler.MatchStage == MatchStage.Live)
            {
                if (!isShowLive)
                {
                    DisplayAlert("Deadline passed", "You cannot change your formation after the deadline has passed", "OK");
                }
                else
                {
                    if (fanspickPitchEventHandler.MatchStatusText == "Half Time" || FanspickPitchEventHandler.MatchStage == MatchStage.PreMatch)
                    {
                        PopUpFormation.IsVisible = false;
                    }
                    else
                    {
                        if (hasUserPickData)
                        {

                            PopUpFormation.IsVisible = true;
                        }
                        else
                        {
                            DisplayAlert("Waiting on The Manager", "We're just waiting for the Managers Pick. After that, you can change YourPick Live formations.", "OK");
                        }
                    }
                }
            }
            else if (FanspickPitchEventHandler.MatchStage >= MatchStage.Past)
            {
                DisplayAlert("Full Time", "You cannot make formation changes to either YourPick or YourPick Live after the match has finished", "OK");
            }
        }

        void OnCloseFormation(object sender, EventArgs e)
        {
            PopUpFormation.IsVisible = false;
        }

        void OnClosePlayerPop(object sender, EventArgs e)
        {
            PopUpPlayers.IsVisible = false;
        }

        void OnClosePopUpOptions(object sender, EventArgs e)
        {
            PopUpOptions.IsVisible = false;
        }
        void OnClosePlayerPop1(object sender, EventArgs e)
        {
            PopUpPlayers1.IsVisible = false;
        }


        void OpenPlayer(object sender, EventArgs e)
        {
            PopUpOptions.IsVisible = false;
            var page = new PlayerInformationPage(selectedPlayer.PLayerId.ToString());
            Navigation.PushPopupAsync(page);
        }

        async void SwapPlayer(object sender, EventArgs e)
        {

            //   unSetPlayer();

            if (FanspickCache.CurrentFanspickScope.SelectedFixture != null && FanspickCache.CurrentFanspickScope?.SelectedTeam != null)
            {

                if (isShowLive)
                {
                    // getTeamSubstitutes
                    ServiceResponse serviceResponse = await APIService.Run(new RequestGetTeamSubsDTO
                    {
                        AccessToken = FanspickCache.UserData.AccessToken,
                        FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id,
                        TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id,
                        IsLive = true
                    });
                    if (serviceResponse.Data != null)
                    {

                        FanspickCache.CurrentFanspickScope.TeamSquadData = null;
                        ResponseGetTeamSubsDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetTeamSubsDTO>(serviceResponse.Data);
                        FanspickCache.CurrentFanspickScope.TeamSquadData = response.Data;
                    }
                    else
                    {
                        FanspickCache.CurrentFanspickScope.TeamSquadData = null;
                    }

                    if (selectedPlayer.PosX == "1")
                    {
                        SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(p => p.Role == "G" && FanspickCache.SelectUserPick.All(up => up.PLayerId != p.PlayerId)).ToList();
                    }
                    else
                    {
                        SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(p => p.Role != "G" && FanspickCache.SelectUserPick.All(up => up.PLayerId != p.PlayerId)).ToList();
                    }



                }
                else
                {

                    ServiceResponse serviceResponse = await APIService.Run(new RequestGetTeamSquadDTO
                    {
                        AccessToken = FanspickCache.UserData.AccessToken,
                        FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id,
                        TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id,
                        IsLive = false
                    });
                    if (serviceResponse.Data != null)
                    {
                        FanspickCache.CurrentFanspickScope.TeamSquadData = null;

                        ResponseGetTeamSquadDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetTeamSquadDTO>(serviceResponse.Data);
                        FanspickCache.CurrentFanspickScope.TeamSquadData = response.Data;
                    }
                    else
                    {
                        FanspickCache.CurrentFanspickScope.TeamSquadData = null;
                    }

                    if (selectedPlayer.PosX == "1")
                    {
                        SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(p => p.Role == "G" && FanspickCache.SelectUserPick.All(up => up.PLayerId != p.PlayerId)).ToList();
                    }
                    else if (selectedPlayer.PosX == "2")
                    {
                        SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(p => (p.Role == "D" || p.Role == "M") && FanspickCache.SelectUserPick.All(up => up.PLayerId != p.PlayerId)).ToList();
                    }
                    else if (selectedPlayer.PosX == "3")
                    {
                        SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(p => (p.Role == "D" || p.Role == "M" || p.Role == "A") && FanspickCache.SelectUserPick.All(up => up.PLayerId != p.PlayerId)).ToList();
                    }
                    else if (selectedPlayer.PosX == "4")
                    {
                        SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(p => (p.Role == "D" || p.Role == "M" || p.Role == "A") && FanspickCache.SelectUserPick.All(up => up.PLayerId != p.PlayerId)).ToList();
                    }
                    else if (selectedPlayer.PosX == "5")
                    {
                        SelectablePlayers = FanspickCache.CurrentFanspickScope.TeamSquadData.Where(p => (p.Role == "M" || p.Role == "A") && FanspickCache.SelectUserPick.All(up => up.PLayerId != p.PlayerId)).ToList();
                    }

                }


            }

            lvPlayerPop.ItemsSource = SelectablePlayers;
            isReplace = true;
            PopUpPlayers.IsVisible = true;

            OnClosePopUpOptions(this, EventArgs.Empty);
        }

        async void ChangePlayer(object sender, EventArgs e)
        {
            List<ResponsePitchPlayersData> lstPitchData = null;
            List<pitchPlayersDetails> lstPitchPlayers = new List<pitchPlayersDetails>();
            pitchPlayersDetails pd = null;

            //   unSetPlayer();

            if (FanspickCache.CurrentFanspickScope.SelectedFixture != null && FanspickCache.CurrentFanspickScope?.SelectedTeam != null)
            {

                if (fanspickPitchEventHandler.MatchStatusText.Contains("Live"))
                {
                    // getTeamSubstitutes
                    ServiceResponse serviceResponse = await APIService.Run(new RequestGetPitchPlayersDataDTO
                    {
                        AccessToken = FanspickCache.UserData.AccessToken,
                        FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id,
                        TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id,
                        SelPlayerId = FanspickCache.CurrentFanspickScope.SelectedPlayerId,
                        IsLive = true
                    });
                    if (serviceResponse.Data != null)
                    {

                        ResponsePitchPlayersDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponsePitchPlayersDTO>(serviceResponse.Data);
                        lstPitchData = dto.Data;
                        for (int i = 0; i < dto.Data.Count(); i++)
                        {
                            pd = new pitchPlayersDetails();
                            pd.Id = lstPitchData[i].pitchPlayersDetail.Id;
                            pd.Name = lstPitchData[i].pitchPlayersDetail.Name;
                            if (lstPitchData[i].positionValues.Role == "G")
                            {
                                pd.shirt = "goalkeeper.png";
                            }
                            else
                            {
                                pd.shirt = FanspickCache.CurrentFanspickScope.TeamShirtImage;
                            }
                            lstPitchPlayers.Insert(i, pd);
                        }
                    }
                    else
                    {
                        lstPitchPlayers = null;
                    }
                }

            }

            lvPitchPlayerPop.ItemsSource = lstPitchPlayers;
            isReplace = true;
            PopUpPlayers1.IsVisible = true;
            OnClosePopUpOptions(this, EventArgs.Empty);
        }


        private void lstSelectFormation_OnSelectedItem(object sender, XLabs.EventArgs<object> e)
        {
            Formation selectedFormation = (Formation)e.Value;

            if (isShowLive)
            {
                hideAllPositions();
                ChangeFormationToNew(selectedFormation);
            }
            else
            {

                if (FanspickCache.SelectUserPickLive.Count > 0)
                {
                    ChangeFormationToNew(selectedFormation);
                }
                else
                {
                    hideAllPositions();
                    // ChangePitchFormation(selectedFormation.type);
                    ChangeFormationToNew(selectedFormation);

                }

            }


            OnCloseFormation(this, EventArgs.Empty);
        }

        private async void ChangeFormationToNew(Formation selectedFormation)
        {


            if (hasUserPickData)
            {

                try
                {

                    var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
                    RequestChangeFormationRefitDTO requestChangeFormation = new RequestChangeFormationRefitDTO();
                    requestChangeFormation.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
                    requestChangeFormation.IsLive = isShowLive;
                    requestChangeFormation.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
                    requestChangeFormation.NewFormationId = selectedFormation._id;

                    var header = "Bearer " + FanspickCache.UserData.AccessToken;
                    var response = await gitHubApi.GetNewFormationWithPlayers(requestChangeFormation, header);

                    if (response.StatusCode == 200)
                    {
                        GetUserPick(isShowLive);

                    }

                }
                catch (ApiException ex)
                {
                    var content = ex.GetContentAs<Dictionary<String, String>>();
                    await DisplayAlert("Fanspick", content.Values.Last().ToString(), "ok");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }


            }
            else
            {
                ChangePitchFormation(selectedFormation.type);
            }
        }

        private async void unSetPlayer(Squad item)
        {


            try
            {

                var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
                RequestUnSetUserPickRefitDTO unSetUserPickRequest = new RequestUnSetUserPickRefitDTO();
                unSetUserPickRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
                unSetUserPickRequest.IsLive = isShowLive;
                unSetUserPickRequest.PlayerId = FanspickCache.CurrentFanspickScope.SelectedPlayerId;
                unSetUserPickRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;

                var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.UnsetUserPick(unSetUserPickRequest, header);

                ErrorMessageUserPick dto = JsonConvert.DeserializeObject<ErrorMessageUserPick>(response);

                if (dto.StatusCode == 200)
                {
                    if (!isReplace)
                    {
                        GetUserPick(isShowLive);
                    }
                    else
                    {
                        isReplace = false;
                        setPlayer(item);
                    }

                }

            }
            catch (ApiException ex)
            {
                var content = ex.GetContentAs<Dictionary<String, String>>();
                if (content.Values.Last().ToString() == "Substitiue limit exceeded")
                {
                    await DisplayAlert("", content.Values.Last().ToString(), "Close");
                }
                else
                {
                    await DisplayAlert("Sorry", "you're only allowed to make 3 substitutions!", "Close");
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }


        }

        private async void setPlayer(Squad item)
        {

            isLoad = false;
            try
            {

                var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
                RequestSetUserPickRefitDTO updateUserPick = new RequestSetUserPickRefitDTO();
                updateUserPick.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
                updateUserPick.Formation = currentFormation;
                updateUserPick.IsLive = isShowLive;
                updateUserPick.OldPlayerId = FanspickCache.CurrentFanspickScope.SelectedPlayerId;
                updateUserPick.NewPlayerId = item.PlayerId;
                updateUserPick.PositionId = currentPositionId;
                updateUserPick.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;


                var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.SetUserPick(updateUserPick, header);

                ErrorMessageUserPick dto = JsonConvert.DeserializeObject<ErrorMessageUserPick>(response);

                if (dto.StatusCode == 200)
                {
                    this.GetUserPick(isShowLive);
                }

            }
            catch (ApiException ex)
            {
                var content = ex.GetContentAs<Dictionary<String, String>>();
                await DisplayAlert("", content.Values.Last().ToString(), "Close");

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }


            //RequestSetUserPickRefitDTO


        }


        private void lvPlayerPop_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            Squad item = e.SelectedItem as Squad;

            if (selectedPlayer.PLayerId == "0")
            {
                isReplace = false;
                setPlayer(item);

            }
            else
            {
                isReplace = true;
                unSetPlayer(item);

            }

            ((ListView)sender).SelectedItem = null;

            OnClosePlayerPop(this, EventArgs.Empty);


        }


        private async void lvPitchPlayerPop_ItemSelectedAsync(object sender, ItemTappedEventArgs e)
        {

            pitchPlayersDetails item = e.Item as pitchPlayersDetails;
            FanspickCache.CurrentFanspickScope.NewPlayerId = item.Id;

            try
            {

                var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

                RequestSwapPitchPlayersDataRefitDTO swapPlayerRequest = new RequestSwapPitchPlayersDataRefitDTO();
                swapPlayerRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
                swapPlayerRequest.IsLive = isShowLive;
                swapPlayerRequest.OldPlayerId = FanspickCache.CurrentFanspickScope.SelectedPlayerId;
                swapPlayerRequest.NewPlayerId = FanspickCache.CurrentFanspickScope.NewPlayerId;
                swapPlayerRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;

                var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.SwapPlayers(swapPlayerRequest, header);

                if (response.StatusCode == 200)
                {
                    GetUserPick(isShowLive);
                }

            }
            catch (ApiException ex)
            {

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            OnClosePlayerPop1(this, EventArgs.Empty);
            ((ListView)sender).SelectedItem = null;

        }


        public void ArrangementOfPlayers(List<String> line1, List<String> line2, List<String> line3, List<String> line4)
        {

            setBakgroundPlayer();

            if (line2.Count == 0)
            {
                UpdateRowHeight(25, 25, 1, 25, 25);
                set3Line();
            }
            else if (line3.Count == 0)
            {
                UpdateRowHeight(25, 25, 25, 1, 25);
                set3Line();
            }
            else
            {
                UpdateRowHeight(20, 20, 20, 20, 20);
                set4Line();
            }


            int denominator1 = line1.Where(l => l != "0").Count();
            denominator1 = denominator1 == 0 ? 1 : denominator1;
            int percentForEachLine1 = (100 / denominator1);
            percentForEachLine1 = percentForEachLine1 == 0 ? 1 : percentForEachLine1;
            List<int> methodValues1 = new List<int>();
            for (int i = 1; i <= 5; i++)
            {
                string columnPos = line1.FirstOrDefault(c => Convert.ToInt32(c) == i);
                if (!string.IsNullOrEmpty(columnPos))
                {
                    methodValues1.Add(percentForEachLine1);
                }
                else
                {
                    methodValues1.Add(1);
                }
            }
            UpdateRowLine1(methodValues1[0], methodValues1[1], methodValues1[2], methodValues1[3], methodValues1[4]);

            int denominator2 = line2.Where(l => l != "0").Count();
            denominator2 = denominator2 == 0 ? 1 : denominator2;
            int percentForEachLine2 = (100 / denominator2);
            percentForEachLine2 = percentForEachLine2 == 0 ? 1 : percentForEachLine2;
            List<int> methodValues2 = new List<int>();
            for (int i = 1; i <= 5; i++)
            {
                string columnPos = line2.FirstOrDefault(c => Convert.ToInt32(c) == i);
                if (!string.IsNullOrEmpty(columnPos))
                {
                    methodValues2.Add(percentForEachLine2);
                }
                else
                {
                    methodValues2.Add(1);
                }
            }
            UpdateRowLine2(methodValues2[0], methodValues2[1], methodValues2[2], methodValues2[3], methodValues2[4]);

            int denominator3 = line3.Where(l => l != "0").Count();
            denominator3 = denominator3 == 0 ? 1 : denominator3;
            int percentForEachLine3 = (100 / denominator3);
            percentForEachLine3 = percentForEachLine3 == 0 ? 1 : percentForEachLine3;
            List<int> methodValues3 = new List<int>();
            for (int i = 1; i <= 5; i++)
            {
                string columnPos = line3.FirstOrDefault(c => Convert.ToInt32(c) == i);
                if (!string.IsNullOrEmpty(columnPos))
                {
                    methodValues3.Add(percentForEachLine3);
                }
                else
                {
                    methodValues3.Add(1);
                }
            }
            UpdateRowLine3(methodValues3[0], methodValues3[1], methodValues3[2], methodValues3[3], methodValues3[4]);

            int denominator4 = line4.Where(l => l != "0").Count();
            denominator4 = denominator4 == 0 ? 1 : denominator4;
            int percentForEachLine4 = (100 / denominator4);
            percentForEachLine4 = percentForEachLine4 == 0 ? 1 : percentForEachLine4;
            List<int> methodValues4 = new List<int>();
            for (int i = 1; i <= 5; i++)
            {
                string columnPos = line4.FirstOrDefault(c => Convert.ToInt32(c) == i);
                if (!string.IsNullOrEmpty(columnPos))
                {
                    methodValues4.Add(percentForEachLine4);
                }
                else
                {
                    methodValues4.Add(1);
                }
            }
            UpdateRowLine4(methodValues4[0], methodValues4[1], methodValues4[2], methodValues4[3], methodValues4[4]);


        }



        public void UpdateRowHeight(int a, int b, int c, int d, int e)
        {
            Row1.Height = new GridLength(a, GridUnitType.Star);
            Row2.Height = new GridLength(b, GridUnitType.Star);
            Row3.Height = new GridLength(c, GridUnitType.Star);
            Row4.Height = new GridLength(d, GridUnitType.Star);
            Row5.Height = new GridLength(e, GridUnitType.Star);
        }

        public void UpdateRowLine1(int a, int b, int c, int d, int e)
        {
            Row2_Column1.Width = new GridLength(a, GridUnitType.Star);
            Row2_Column2.Width = new GridLength(b, GridUnitType.Star);
            Row2_Column3.Width = new GridLength(c, GridUnitType.Star);
            Row2_Column4.Width = new GridLength(d, GridUnitType.Star);
            Row2_Column5.Width = new GridLength(e, GridUnitType.Star);
        }

        public void UpdateRowLine2(int a, int b, int c, int d, int e)
        {
            Row3_Column1.Width = new GridLength(a, GridUnitType.Star);
            Row3_Column2.Width = new GridLength(b, GridUnitType.Star);
            Row3_Column3.Width = new GridLength(c, GridUnitType.Star);
            Row3_Column4.Width = new GridLength(d, GridUnitType.Star);
            Row3_Column5.Width = new GridLength(e, GridUnitType.Star);
        }

        public void UpdateRowLine3(int a, int b, int c, int d, int e)
        {
            Row4_Column1.Width = new GridLength(a, GridUnitType.Star);
            Row4_Column2.Width = new GridLength(b, GridUnitType.Star);
            Row4_Column3.Width = new GridLength(c, GridUnitType.Star);
            Row4_Column4.Width = new GridLength(d, GridUnitType.Star);
            Row4_Column5.Width = new GridLength(e, GridUnitType.Star);
        }

        public void UpdateRowLine4(int a, int b, int c, int d, int e)
        {
            Row5_Column2.Width = new GridLength(a, GridUnitType.Star);
            Row5_Column2.Width = new GridLength(b, GridUnitType.Star);
            Row5_Column3.Width = new GridLength(c, GridUnitType.Star);
            Row5_Column4.Width = new GridLength(d, GridUnitType.Star);
            Row5_Column5.Width = new GridLength(e, GridUnitType.Star);
        }

        public void ArrangePlayerLine(String x, String y)
        {

            //var c = new ColumnDefinition(); c.Width = new GridLength(,GridUnitType.Star);

            if (x == "2")
            {
                playerListRow1.Add(y);
            }

            if (x == "3")
            {
                playerListRow2.Add(y);
            }
            if (x == "4")
            {
                playerListRow3.Add(y);
            }

            if (x == "5")
            {
                playerListRow4.Add(y);
            }


        }

        public void setBakgroundPlayer()
        {
            var semiTransparentColor = new Color(0, 0, 0, 0.5);

            List<Label> allPlayerName = new List<Label>();
            allPlayerName.Add(PlayerName_1_3);

            allPlayerName.Add(PlayerName_2_1);
            allPlayerName.Add(PlayerName_2_2);
            allPlayerName.Add(PlayerName_2_3);
            allPlayerName.Add(PlayerName_2_4);
            allPlayerName.Add(PlayerName_2_5);

            allPlayerName.Add(PlayerName_3_1);
            allPlayerName.Add(PlayerName_3_2);
            allPlayerName.Add(PlayerName_3_3);
            allPlayerName.Add(PlayerName_3_4);
            allPlayerName.Add(PlayerName_3_5);

            allPlayerName.Add(PlayerName_4_1);
            allPlayerName.Add(PlayerName_4_2);
            allPlayerName.Add(PlayerName_4_3);
            allPlayerName.Add(PlayerName_4_4);
            allPlayerName.Add(PlayerName_4_5);

            allPlayerName.Add(PlayerName_5_1);
            allPlayerName.Add(PlayerName_5_2);
            allPlayerName.Add(PlayerName_5_3);
            allPlayerName.Add(PlayerName_5_4);
            allPlayerName.Add(PlayerName_5_5);

            List<Image> allPlayerBack = new List<Image>();
            allPlayerBack.Add(ImageBack_1_3);

            allPlayerBack.Add(ImageBack_2_1);
            allPlayerBack.Add(ImageBack_2_2);
            allPlayerBack.Add(ImageBack_2_3);
            allPlayerBack.Add(ImageBack_2_4);
            allPlayerBack.Add(ImageBack_2_5);

            allPlayerBack.Add(ImageBack_3_1);
            allPlayerBack.Add(ImageBack_3_2);
            allPlayerBack.Add(ImageBack_3_3);
            allPlayerBack.Add(ImageBack_3_4);
            allPlayerBack.Add(ImageBack_3_5);

            allPlayerBack.Add(ImageBack_4_1);
            allPlayerBack.Add(ImageBack_4_2);
            allPlayerBack.Add(ImageBack_4_3);
            allPlayerBack.Add(ImageBack_4_4);
            allPlayerBack.Add(ImageBack_4_5);

            allPlayerBack.Add(ImageBack_5_1);
            allPlayerBack.Add(ImageBack_5_2);
            allPlayerBack.Add(ImageBack_5_3);
            allPlayerBack.Add(ImageBack_5_4);
            allPlayerBack.Add(ImageBack_5_5);


            for (int i = 0; i < allPlayerName.Count(); i++)
            {

                if (allPlayerName[i].Text == "")
                {
                    allPlayerBack[i].Source = "";
                    allPlayerName[i].BackgroundColor = Color.Transparent;
                }
                else
                {


                    var midTransparentColor = new Color(0, 0, 0, 0.7);
                    //  allPlayerName[i].BackgroundColor = midTransparentColor;
                    allPlayerName[i].TextColor = Color.White;
                    allPlayerName[i].WidthRequest = 60;
                    allPlayerName[i].VerticalTextAlignment = TextAlignment.Center;
                    allPlayerName[i].HorizontalTextAlignment = TextAlignment.Center;
                    allPlayerName[i].VerticalOptions = LayoutOptions.FillAndExpand;
                    allPlayerName[i].HorizontalOptions = LayoutOptions.FillAndExpand;
                    allPlayerName[i].FontSize = 8;


                    allPlayerBack[i].Source = "back_player_diffused";
                    allPlayerBack[i].VerticalOptions = LayoutOptions.FillAndExpand;
                    allPlayerBack[i].HorizontalOptions = LayoutOptions.FillAndExpand;
                    allPlayerBack[i].Aspect = Aspect.Fill;
                    //allPlayerBack[i].WidthRequest = 58;
                    //allPlayerBack[i].HeightRequest = 20;

                }

            }

            String device = CrossDeviceInfo.Current.Model;
            ajustDevice(device);

        }




        public void RemoveSpacing()
        {

            List<StackLayout> allPlayerLayout = new List<StackLayout>();
            allPlayerLayout.Add(Stack_1_3);

            allPlayerLayout.Add(Stack_2_1);
            allPlayerLayout.Add(Stack_2_2);
            allPlayerLayout.Add(Stack_2_3);
            allPlayerLayout.Add(Stack_2_4);
            allPlayerLayout.Add(Stack_2_5);

            allPlayerLayout.Add(Stack_3_1);
            allPlayerLayout.Add(Stack_3_2);
            allPlayerLayout.Add(Stack_3_3);
            allPlayerLayout.Add(Stack_3_4);
            allPlayerLayout.Add(Stack_3_5);

            allPlayerLayout.Add(Stack_4_1);
            allPlayerLayout.Add(Stack_4_2);
            allPlayerLayout.Add(Stack_4_3);
            allPlayerLayout.Add(Stack_4_4);
            allPlayerLayout.Add(Stack_4_5);

            allPlayerLayout.Add(Stack_5_1);
            allPlayerLayout.Add(Stack_5_2);
            allPlayerLayout.Add(Stack_5_3);
            allPlayerLayout.Add(Stack_5_4);
            allPlayerLayout.Add(Stack_5_5);

            for (int i = 0; i < allPlayerLayout.Count(); i++)
            {
                allPlayerLayout[i].Spacing = 0;

            }

        }

        public void LoadGif()
        {

            if (FanspickCache.BannerImage == null || FanspickCache.PitchImage == null || FanspickCache.BannerImage.Contains(".jpg") || FanspickCache.PitchImage.Contains(".jpg"))
            {
                
                FanspickCache.PitchImage = "banner_FP_pitch.gif";
                FanspickCache.PitchPopUp = "PopUp_Page_v4.png";
                FanspickCache.PitchUrl = "http://fanspick.com";

                FanspickCache.BannerImage = "Banner_FP_with-Billboard.gif.gif";
                FanspickCache.BannerPopUp = "PopUp_Page_v4.png";
                FanspickCache.BannerUrl = "http://fanspick.com";
            }


            String htmlsource = FanspickCache.PitchImage;
            String htmlMain = FanspickCache.BannerImage.Replace(".gif.gif", ".gif");

            HtmlWebViewSource html = new HtmlWebViewSource();
            html.Html = String.Format
                (@"<html style='width:" + BannerWidthSize() + ";height=" + BannerheightSize() + "; margin-left:-6px;'><body style='width:100%;height=100%; background: #ffffff;'><img src='{0}' style='width:100%;height=100%;margin-top:-8px;'  /></body></html>",
                 htmlsource);
            Banner1.Source = html;
            Banner2.Source = html;
            YourPickbannerImg.Source = htmlMain;


            YourPickbannerImg.Source = htmlMain;


            if (FanspickCache.CurrentFanspickScope.SelectedFixture.CompetitionName == "UEFA Champions League")
            {
                skyBack.IsVisible = true;
                skyBack.Source = "start_bg_night";

            }
            else if (FanspickCache.CurrentFanspickScope.SelectedFixture.CompetitionName == "UEFA Europa League")
            {

                skyBack.IsVisible = true;
                skyBack.Source = "start_bg_europa copy";
            }
            else
            {
                skyBack.IsVisible = false;
            }


        }


        public ImageSource TeamLogo
        {
            get
            {

                string ImageString = "";

                if (FanspickCache.CurrentFanspickScope.SelectedTeam.Id == FanspickCache.CurrentFanspickScope.SelectedFixture.LocalTeam.FanspickTeam.Id)
                {
                    ImageString = FanspickCache.CurrentFanspickScope.SelectedFixture.LocalTeam.FanspickTeam.ImageURL;
                }
                else
                {
                    ImageString = FanspickCache.CurrentFanspickScope.SelectedFixture.VisitorTeam.FanspickTeam.ImageURL;
                }

                //  Uri location = new Uri();
                // return Xamarin.Forms.ImageSource.FromUri(location);
                if (ImageString != null)
                {
                    return @"http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamLogos/" + ImageString;
                }
                else
                {
                    return "no_image";
                }
            }
        }

        public bool OKToPlayAnimation { get; private set; }

        public void SwIconOption(object sender, EventArgs e)
        {
            if (tapCountSw % 2 == 0)
            {
                tapCountSw++;
                SwIcon.Source = "swtich_on";
                isShowLive = true;
                hideAllPositions();
                ClearALLPreviousAction();
                GetUserPick(isShowLive);
            }
            else
            {
                tapCountSw++;
                SwIcon.Source = "swtich_off";
                isShowLive = false;
                hideAllPositions();
                ClearALLPreviousAction();
                GetUserPick(isShowLive);
            }
        }

        public void ClearALLPreviousAction()
        {

            PlayerImagePop_1_3.IsVisible = false;

            PlayerImagePop_2_1.IsVisible = false;
            PlayerImagePop_2_2.IsVisible = false;
            PlayerImagePop_2_3.IsVisible = false;
            PlayerImagePop_2_4.IsVisible = false;
            PlayerImagePop_2_5.IsVisible = false;

            PlayerImagePop_3_1.IsVisible = false;
            PlayerImagePop_3_2.IsVisible = false;
            PlayerImagePop_3_3.IsVisible = false;
            PlayerImagePop_3_4.IsVisible = false;
            PlayerImagePop_3_5.IsVisible = false;

            PlayerImagePop_4_1.IsVisible = false;
            PlayerImagePop_4_2.IsVisible = false;
            PlayerImagePop_4_3.IsVisible = false;
            PlayerImagePop_4_4.IsVisible = false;
            PlayerImagePop_4_5.IsVisible = false;

            PlayerImagePop_5_1.IsVisible = false;
            PlayerImagePop_5_2.IsVisible = false;
            PlayerImagePop_5_3.IsVisible = false;
            PlayerImagePop_5_4.IsVisible = false;
            PlayerImagePop_5_5.IsVisible = false;
        }
        public static int totalPlayersOnPitch;
        public async void SharePick(object sender, EventArgs e)
        {
            totalPlayersOnPitch = playerListRow1.Count + playerListRow2.Count + playerListRow3.Count + playerListRow4.Count + 1;
            if (totalPlayersOnPitch == 21)
            {
                await DependencyService.Get<IScreenCapture>().Share("Fanspick!", "Check out my YourPick and make your own with Fanspick.", null);
            }
            else
            {
                await DisplayAlert("Warning!", "Pitch does not have all players to share the pitch", "ok");
            }
        }

        public void RemovePlayer(object sender, EventArgs e)
        {
            isReplace = false;
            unSetPlayer(null);
            PopUpOptions.IsVisible = false;
        }


        public void set3Line()
        {
            LoadAllImagePop();
            is4Line = false;
            String device = CrossDeviceInfo.Current.Model;

            double playerShirtSize = 37;

            if (device == "iPad")
            {
                playerShirtSize = 85;
            }

            for (int i = 0; i < allImagePopUp.Count(); i++)
            {
                allImagePopUp[i].WidthRequest = playerShirtSize;
                allImage[i].WidthRequest = playerShirtSize;

                allImagePopUp[i].HeightRequest = playerShirtSize;
                allImage[i].HeightRequest = playerShirtSize;
            }

        }

        public void set4Line()
        {
            String device = CrossDeviceInfo.Current.Model;

            double playerShirtSize = 32;

            if (device == "iPad")
            {
                playerShirtSize = 85;
            }
            is4Line = true;

            LoadAllImagePop();
            for (int i = 0; i < allImagePopUp.Count(); i++)
            {
                allImagePopUp[i].WidthRequest = playerShirtSize;
                allImage[i].WidthRequest = playerShirtSize;

                allImagePopUp[i].HeightRequest = playerShirtSize;
                allImage[i].HeightRequest = playerShirtSize;
            }

        }

        List<Image> allImagePopUp = new List<Image>();
        List<Image> allImage = new List<Image>();
        public void LoadAllImagePop()
        {

            if (allImagePopUp.Count > 0)
            {
                allImagePopUp.Clear();
            }

            if (allImage.Count > 0)
            {
                allImage.Clear();
            }


            allImagePopUp.Add(PlayerImagePop_1_3);

            allImagePopUp.Add(PlayerImagePop_2_1);
            allImagePopUp.Add(PlayerImagePop_2_2);
            allImagePopUp.Add(PlayerImagePop_2_3);
            allImagePopUp.Add(PlayerImagePop_2_4);
            allImagePopUp.Add(PlayerImagePop_2_5);

            allImagePopUp.Add(PlayerImagePop_3_1);
            allImagePopUp.Add(PlayerImagePop_3_2);
            allImagePopUp.Add(PlayerImagePop_3_3);
            allImagePopUp.Add(PlayerImagePop_3_4);
            allImagePopUp.Add(PlayerImagePop_3_5);

            allImagePopUp.Add(PlayerImagePop_4_1);
            allImagePopUp.Add(PlayerImagePop_4_2);
            allImagePopUp.Add(PlayerImagePop_4_3);
            allImagePopUp.Add(PlayerImagePop_4_4);
            allImagePopUp.Add(PlayerImagePop_4_5);

            allImagePopUp.Add(PlayerImagePop_5_1);
            allImagePopUp.Add(PlayerImagePop_5_2);
            allImagePopUp.Add(PlayerImagePop_5_3);
            allImagePopUp.Add(PlayerImagePop_5_4);
            allImagePopUp.Add(PlayerImagePop_5_5);


            allImage.Add(PlayerImage_1_3);

            allImage.Add(PlayerImage_2_1);
            allImage.Add(PlayerImage_2_2);
            allImage.Add(PlayerImage_2_3);
            allImage.Add(PlayerImage_2_4);
            allImage.Add(PlayerImage_2_5);

            allImage.Add(PlayerImage_3_1);
            allImage.Add(PlayerImage_3_2);
            allImage.Add(PlayerImage_3_3);
            allImage.Add(PlayerImage_3_4);
            allImage.Add(PlayerImage_3_5);

            allImage.Add(PlayerImage_4_1);
            allImage.Add(PlayerImage_4_2);
            allImage.Add(PlayerImage_4_3);
            allImage.Add(PlayerImage_4_4);
            allImage.Add(PlayerImage_4_5);

            allImage.Add(PlayerImage_5_1);
            allImage.Add(PlayerImage_5_2);
            allImage.Add(PlayerImage_5_3);
            allImage.Add(PlayerImage_5_4);
            allImage.Add(PlayerImage_5_5);

        }



        public void ajustDevice(string deviceType)
        {
            List<Label> allPlayerName = new List<Label>();
            allPlayerName.Add(PlayerName_1_3);

            allPlayerName.Add(PlayerName_2_1);
            allPlayerName.Add(PlayerName_2_2);
            allPlayerName.Add(PlayerName_2_3);
            allPlayerName.Add(PlayerName_2_4);
            allPlayerName.Add(PlayerName_2_5);

            allPlayerName.Add(PlayerName_3_1);
            allPlayerName.Add(PlayerName_3_2);
            allPlayerName.Add(PlayerName_3_3);
            allPlayerName.Add(PlayerName_3_4);
            allPlayerName.Add(PlayerName_3_5);

            allPlayerName.Add(PlayerName_4_1);
            allPlayerName.Add(PlayerName_4_2);
            allPlayerName.Add(PlayerName_4_3);
            allPlayerName.Add(PlayerName_4_4);
            allPlayerName.Add(PlayerName_4_5);

            allPlayerName.Add(PlayerName_5_1);
            allPlayerName.Add(PlayerName_5_2);
            allPlayerName.Add(PlayerName_5_3);
            allPlayerName.Add(PlayerName_5_4);
            allPlayerName.Add(PlayerName_5_5);

            List<Image> allPlayerBack = new List<Image>();
            allPlayerBack.Add(ImageBack_1_3);

            allPlayerBack.Add(ImageBack_2_1);
            allPlayerBack.Add(ImageBack_2_2);
            allPlayerBack.Add(ImageBack_2_3);
            allPlayerBack.Add(ImageBack_2_4);
            allPlayerBack.Add(ImageBack_2_5);

            allPlayerBack.Add(ImageBack_3_1);
            allPlayerBack.Add(ImageBack_3_2);
            allPlayerBack.Add(ImageBack_3_3);
            allPlayerBack.Add(ImageBack_3_4);
            allPlayerBack.Add(ImageBack_3_5);

            allPlayerBack.Add(ImageBack_4_1);
            allPlayerBack.Add(ImageBack_4_2);
            allPlayerBack.Add(ImageBack_4_3);
            allPlayerBack.Add(ImageBack_4_4);
            allPlayerBack.Add(ImageBack_4_5);

            allPlayerBack.Add(ImageBack_5_1);
            allPlayerBack.Add(ImageBack_5_2);
            allPlayerBack.Add(ImageBack_5_3);
            allPlayerBack.Add(ImageBack_5_4);
            allPlayerBack.Add(ImageBack_5_5);


            if (deviceType == "iPad")
            {

                for (int i = 0; i < allPlayerName.Count(); i++)
                {
                    allPlayerBack[i].WidthRequest = 100;
                    allPlayerName[i].HeightRequest = 22;
                    allPlayerName[i].FontSize = 12;

                }

                Gline1.Margin = new Thickness(20, 0, 20, 0);
                YourPickbannerImg.HeightRequest = 170;
                YourPickbannerImg.WidthRequest = 750;
                YourPickbannerImg.Margin = new Thickness(0, 0, 0, 60);
                Banner1.HeightRequest = 40;
                Banner2.HeightRequest = 40;
                Banner1.Margin = new Thickness(0, 0, 8, 0);
                Banner2.Margin = new Thickness(5, 0, 8, 0);

                BoxDivision1.Height = new GridLength(1.5, GridUnitType.Star);
                BoxDivision2.Height = new GridLength(6.45, GridUnitType.Star);
                BoxDivision3.Height = new GridLength(0.30, GridUnitType.Star);
                BoxDivision4.Height = new GridLength(1.1, GridUnitType.Star);

                BarCol1.Width = new GridLength(50);
                BarCol3.Width = new GridLength(50);
                BarCol4.Width = new GridLength(50);
                BarCol5.Width = new GridLength(60);

                counterTimeLabel.FontSize = 20;
                counterLabel.FontSize = 16;

                NullImageTitle.FontSize = 16;
                NullImageText.FontSize = 14;

                NullImage.Margin = new Thickness(-20, 0, -20, -12);

                PopIcon1.WidthRequest = 40;
                PopIcon2.WidthRequest = 40;
                PopIcon3.WidthRequest = 40;
                iconRemove.WidthRequest = 40;
                iconStar.WidthRequest = 40;
                iconHairDryer.WidthRequest = 40;
                iconMom.WidthRequest = 40;
                iconChange.WidthRequest = 40;




            }
            else
            {
                Gline1.Margin = new Thickness(0, 0, 0, 0);
                for (int i = 0; i < allPlayerName.Count(); i++)
                {
                    allPlayerBack[i].WidthRequest = 60;
                    allPlayerName[i].HeightRequest = 15;
                    allPlayerName[i].FontSize = 10;

                }

                BoxDivision1.Height = new GridLength(1.5, GridUnitType.Star);
                BoxDivision2.Height = new GridLength(6.5, GridUnitType.Star);
                BoxDivision3.Height = new GridLength(0.25, GridUnitType.Star);
                BoxDivision4.Height = new GridLength(1.1, GridUnitType.Star);

                BarCol1.Width = new GridLength(30);
                BarCol3.Width = new GridLength(30);
                BarCol4.Width = new GridLength(30);
                BarCol5.Width = new GridLength(40);

                counterTimeLabel.FontSize = 10;
                counterLabel.FontSize = 12;


                PopIcon1.WidthRequest = 25;
                PopIcon2.WidthRequest = 25;
                PopIcon3.WidthRequest = 25;
                iconRemove.WidthRequest = 25;
                iconStar.WidthRequest = 25;
                iconHairDryer.WidthRequest = 25;
                iconMom.WidthRequest = 25;
                iconChange.WidthRequest = 25;

            }

        }

        public String BannerheightSize()
        {

            String size = "50px";
            String device = CrossDeviceInfo.Current.Model;
            if (device == "iPad")
            {
                size = "100px";
            }

            return size;
        }


        public String BannerWidthSize()
        {

            String size = "100px";
            String device = CrossDeviceInfo.Current.Model;
            if (device == "iPad")
            {
                size = "200px";
            }

            return size;
        }

        public void LoadPopUpPosition(double left, double top, double right, double bottom, Rectangle rec)
        {

            String device = CrossDeviceInfo.Current.Model;
            if (device == "iPad")
            {
                PopUpOptions.VerticalOptions = LayoutOptions.CenterAndExpand;
                PopUpOptions.HorizontalOptions = LayoutOptions.CenterAndExpand;

                PopUpOptions.Margin = new Thickness(left, top, right, bottom);
                AbsoluteLayout.SetLayoutBounds(PopUpOptions, new Rectangle(0, 0, 1, 1));
            }
            else
            {
                AbsoluteLayout.SetLayoutBounds(PopUpOptions, new Rectangle(0, 0, 0, 0));
                PopUpOptions.Layout(rec);
            }
        }

    }
}