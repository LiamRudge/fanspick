﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Fanspick.Screens;
using Fanspick.ViewModels;
using Fanspick.Helper;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using System.Diagnostics;
using Fanspick.Shared.Models;
using System.IO;
using static Fanspick.Shared.Models.DTO.GetTeamDataDTO;
using Fanspick.Shared.Events.Events;
using Plugin.Geolocator;
using Plugin.DeviceInfo;
using Fanspick.PopUp;
using Refit;
using Fanspick.RestAPI;
using Plugin.Connectivity;

namespace Fanspick.Templates
{
    public partial class ManagerpickPageMain : ContentPage
    {

        public async void SharePick(object sender, EventArgs e)
        {
            if(totalPlayersOnPitch==11){
             await DependencyService.Get<IScreenCapture>().Share("Fanspick!", "Check out the ManagersPick with Fanspick.", null);   
            }else{
               await DisplayAlert("Warning!", "Pitch does not have all players to share the pitch", "ok");
            }

        }

        App_MainViewModel vmApp_Main;   
        public static int totalPlayersOnPitch;
        public string currentFormation { get; set; }
       
        List<String> playerListRow1 = new List<String>();
        List<String> playerListRow2 = new List<String>();
        List<String> playerListRow3 = new List<String>();
        List<String> playerListRow4 = new List<String>();


        private List<Squad> selectablePlayers;
        public List<Squad> SelectablePlayers
        {
            get
            {
                return selectablePlayers;
            }
            set
            {
                selectablePlayers = value;
                foreach (var item in selectablePlayers)
                {
                    if (item.Role == "G")
                    {
                        item.shirt = "goalkeeper.png";
                    }
                    else
                    {
                        item.shirt = FanspickCache.CurrentFanspickScope.TeamShirtImage;
                    }
                }
            }
        }

        public ManagerpickPageMain()
        {
            InitializeComponent();
            vmApp_Main = CacheStorage.ViewModels.App_Main;
            BindingContext = FanspickCache.CurrentFanspickScope.SelectedFixture;

			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{

				if (CrossConnectivity.Current.IsConnected)
				{
					InternetScreen.IsVisible = false;
				}
				else
				{
					InternetScreen.IsVisible = true;
				}


			};

        }

        private void OnTapGestureCommentry(object sender, EventArgs e)
        {
            //Application.Current.MainPage = new MatchCommentry();
            Navigation.PushAsync(new Screens.MatchCommentry(), true);
        }

        async void OpenContextMenuCommentry(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Screens.MatchCommentry(), true);
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await Navigation.PopAllPopupAsync(true);
            hideAllPositions();
            getPlayerShirt();
            RemoveSpacing();
            LoadGif();

			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);

        }

        private async void GetManagerPick()
        {

                      
            GetInfo("managersPick", "managerpick access for fixture " + FanspickCache.CurrentFanspickScope.SelectedFixture.Id);

            actInd.IsVisible = true;
            playerListRow1.Clear();
            playerListRow2.Clear();
            playerListRow3.Clear();
            playerListRow4.Clear();

            try
            {
                var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);

                RequestGetManagerPickRefitDTO getTeamDataRequest = new RequestGetManagerPickRefitDTO();
				getTeamDataRequest.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
				getTeamDataRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;

                var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.GetManagerPick(getTeamDataRequest, header);

                if (response.StatusCode==200)
				{
                    
					if (FanspickPitchEventHandler.MatchStage == MatchStage.RunUp)
					{
						ManagerNullImage.IsVisible = true;
						actInd.IsVisible = false;
					}
					else
					{
                        currentFormation = response.Data?.CurrentFormation?.Id;
                        if (response.Data.LineUpPlayers.Count == 0)
						{
							ManagerNullImage.IsVisible = true;
						}
						else if (response.Data.LineUpPlayers.Count < 11)
						{
							ManagerNullImage.IsVisible = true;
						}
                        else
						{
							ManagerNullImage.IsVisible = false;
							ShowPlayers(response.Data.LineUpPlayers);
						}
						actInd.IsVisible = false;

					}

				}

            }
            catch(ApiException ex){
				ManagerNullImage.IsVisible = true;
				actInd.IsVisible = false;

                var content = ex.GetContentAs<Dictionary<String, String>>();
            
            }

			catch (Exception ex)
			{
				ManagerNullImage.IsVisible = true;
				actInd.IsVisible = false;
				Debug.WriteLine(ex.ToString());
			}

			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);

        }

        protected override void OnDisappearing()
        {
            //ManagerNullImage.Opacity = 0;
            base.OnDisappearing();
        }


        public async void GetInfo(String eventtype, String description)
        {
            try
            {
                
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;

                var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

                if (position == null)
                {
                    Debug.WriteLine("null gps", "null gps");
                    return;
                }

                if (position.Latitude.ToString() != null)
                {
                    vmApp_Main.requestEventLoggingDTO.Latitude = "" + position.Latitude;
                }
                else
                {
                    vmApp_Main.requestEventLoggingDTO.Latitude = "0";
                }

                if (position.Longitude.ToString() != null)
                {
                    vmApp_Main.requestEventLoggingDTO.Longitude = "" + position.Longitude;
                }
                else
                {
                    vmApp_Main.requestEventLoggingDTO.Longitude = "0";
                }
                vmApp_Main.requestEventLoggingDTO.EventType = eventtype;
                vmApp_Main.requestEventLoggingDTO.EventDescription = description;
                vmApp_Main.requestEventLoggingDTO.EventAdditionalInfoID = "" + FanspickCache.UserData.AccessToken;
                vmApp_Main.requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
                vmApp_Main.requestEventLoggingDTO.DeviceType = "IOS";
                vmApp_Main.requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
                vmApp_Main.requestEventLoggingDTO.AppVersion = "1";

                Shared.Models.Service.ServiceResponse response = await vmApp_Main.EventLog();
                //  Application.Current.MainPage = new SplashScreen();


            }
            catch (Exception ex)
            {
                Debug.WriteLine(" " + ex);

            }

        }


        private async void ShowPlayers(List<LineUpPlayerData> showPlayer)
        {

            List<SelectUserPickPlayer> playerList = new List<SelectUserPickPlayer>();
            for (int i = 0; i < showPlayer.Count(); i++)
            {
				if (showPlayer[i] != null && showPlayer[i].Position != null && showPlayer[i].Player != null && showPlayer[i].Player.KnownName != null)
                {

                    SelectUserPickPlayer selectedPlayer = new SelectUserPickPlayer();
                    selectedPlayer.PosX = showPlayer[i].Position.PosX;
                    selectedPlayer.PosY = showPlayer[i].Position.PosY;
                    selectedPlayer.PlayerName = showPlayer[i].Player.KnownName;
                    selectedPlayer.PLayerId = showPlayer[i].Player.Id;
                    selectedPlayer.PositionId = showPlayer[i].Position.Id;

                    if (showPlayer[i].Player.userActions.Count > 1)
                    {

                        if (showPlayer[i].Player.userActions.Count == 1)
                        {

                            selectedPlayer.Action = showPlayer[i].Player.userActions[0].Action;

                        }
                        else
                        {
                            selectedPlayer.Action = showPlayer[i].Player.userActions[0].Action + "-" + showPlayer[i].Player.userActions[1].Action;
                        }


                    }
                    else
                    {
                        selectedPlayer.Action = "0";
                    }

                    ImageSource shirtimage = "";

                    enablePlayers(selectedPlayer.PosX, selectedPlayer.PosY, selectedPlayer.PlayerName, FanspickCache.CurrentFanspickScope.TeamShirtImage, selectedPlayer.Action, "goalkeeper");
                    playerList.Add(selectedPlayer);

                    ArrangePlayerLine(showPlayer[i].Position.PosX, showPlayer[i].Position.PosY);
                }

                FanspickCache.SelectManagerPick = playerList;

                ArrangementOfPlayers(playerListRow1, playerListRow2, playerListRow3, playerListRow4);
				String device = CrossDeviceInfo.Current.Model;
				ajustDevice(device);

            }

            totalPlayersOnPitch = playerListRow1.Count + playerListRow2.Count + playerListRow3.Count + playerListRow4.Count + 1 ;


			if (totalPlayersOnPitch == 0)
			{
                hideAllPositions();
				ManagerNullImage.IsVisible = true;
			}
			else if (totalPlayersOnPitch < 11)
			{
                hideAllPositions();
				ManagerNullImage.IsVisible = true; 

			}
			else
			{
				ManagerNullImage.IsVisible = false;
			}

        }


        public async void getPlayerShirt()
        {

            try
            {
                actInd.IsVisible = true;
                RequestGetTeamDataDTO getTeamDataRequest = new RequestGetTeamDataDTO();
                getTeamDataRequest.AccessToken = FanspickCache.UserData.AccessToken;
                getTeamDataRequest.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
                ServiceResponse getTeamDataResponse = await new FanspickServices<GetTeamDataService>().Run(getTeamDataRequest);
                ResponseGetTeamDataDTO teamDataResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetTeamDataDTO>(getTeamDataResponse.Data);

                if (teamDataResponse.Data.TeamShirtURL == null)
                {
                    FanspickCache.CurrentFanspickScope.TeamShirtImage = "no_shirt";
                    actInd.IsVisible = false;

                }
                else
                {
                    FanspickCache.CurrentFanspickScope.TeamShirtImage = "http://fanspick-admin-test.5sol.co.uk/resources/season/2017_18/teamShirts/" + teamDataResponse.Data.TeamShirtURL.Replace(" ", "%20") + ".png";
                    actInd.IsVisible = false;
                }
                GetManagerPick();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                FanspickCache.CurrentFanspickScope.TeamShirtImage = "no_shirt";
                GetManagerPick();
            }

        }


        public void enablePlayers(String Posx, String Posy, String PlayerName, ImageSource playerImage, String action, String ImageGoalKeeper)
        {

            if (Posx == "1" && Posy == "3")
            {
                Stack_1_3.IsVisible = true;
                PlayerName_1_3.Text = PlayerName;
                PlayerImage_1_3.Source = ImageGoalKeeper;
                if (action != "0")
                {
                    PlayerImagePop_1_3.IsVisible = true;
                    if (action == "star") { PlayerImagePop_1_3.Source = "shirt_st"; }
                    else if (action == "hairdryer") { PlayerImagePop_1_3.Source = "shirt_hd"; }
                    else if (action == "manofmatch") { PlayerImagePop_1_3.Source = "shirt_mom"; }
                    else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_1_3.Source = "shirt_mom+s"; }
                    else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_1_3.Source = "shirt_mom+h"; }
                }


            }

            if (Posx == "2")
            {
                if (Posy == "1")
                {
                    Stack_2_1.IsVisible = true;
                    PlayerImage_2_1.Source = playerImage;
                    PlayerName_2_1.Text = PlayerName;
                    PlayerImage_2_1.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_2_1.IsVisible = true;
                        if (action == "star") { PlayerImagePop_2_1.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_2_1.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_2_1.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_1.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_1.Source = "shirt_mom+h"; }
                    }

                }
                if (Posy == "2")
                {
                    Stack_2_2.IsVisible = true;
                    PlayerImage_2_2.Source = playerImage;
                    PlayerName_2_2.Text = PlayerName;
                    PlayerImage_2_2.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_2_2.IsVisible = true;
                        if (action == "star") { PlayerImagePop_2_2.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_2_2.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_2_2.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_2.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_2.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "3")
                {
                    Stack_2_3.IsVisible = true;
                    PlayerImage_2_3.Source = playerImage;
                    PlayerName_2_3.Text = PlayerName;
                    PlayerImage_2_3.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_2_3.IsVisible = true;
                        if (action == "star") { PlayerImagePop_2_3.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_2_3.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_2_3.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_3.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_3.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "4")
                {
                    Stack_2_4.IsVisible = true;
                    PlayerImage_2_4.Source = playerImage;
                    PlayerName_2_4.Text = PlayerName;
                    PlayerImage_2_4.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_2_4.IsVisible = true;
                        if (action == "star") { PlayerImagePop_2_4.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_2_4.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_2_4.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_4.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_4.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "5")
                {
                    Stack_2_5.IsVisible = true;
                    PlayerImage_2_5.Source = playerImage;
                    PlayerName_2_5.Text = PlayerName;
                    PlayerImage_2_5.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_2_5.IsVisible = true;
                        if (action == "star") { PlayerImagePop_2_5.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_2_5.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_2_5.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_2_5.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_2_5.Source = "shirt_mom+h"; }
                    }
                }

            }

            if (Posx == "3")
            {
                if (Posy == "1")
                {
                    Stack_3_1.IsVisible = true;
                    PlayerImage_3_1.Source = playerImage;
                    PlayerName_3_1.Text = PlayerName;
                    PlayerImage_3_1.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_3_1.IsVisible = true;
                        if (action == "star") { PlayerImagePop_3_1.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_3_1.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_3_1.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_1.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_1.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "2")
                {
                    Stack_3_2.IsVisible = true;
                    PlayerImage_3_2.Source = playerImage;
                    PlayerName_3_2.Text = PlayerName;
                    PlayerImage_3_2.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_3_2.IsVisible = true;
                        if (action == "star") { PlayerImagePop_3_2.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_3_2.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_3_2.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_2.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_2.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "3")
                {
                    Stack_3_3.IsVisible = true;
                    PlayerImage_3_3.Source = playerImage;
                    PlayerName_3_3.Text = PlayerName;
                    PlayerImage_3_3.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_3_3.IsVisible = true;
                        if (action == "star") { PlayerImagePop_3_3.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_3_3.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_3_3.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_3.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_3.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "4")
                {
                    Stack_3_4.IsVisible = true;
                    PlayerImage_3_4.Source = playerImage;
                    PlayerName_3_4.Text = PlayerName;
                    PlayerImage_3_4.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_3_4.IsVisible = true;
                        if (action == "star") { PlayerImagePop_3_4.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_3_4.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_3_4.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_4.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_4.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "5")
                {
                    Stack_3_5.IsVisible = true;
                    PlayerImage_3_5.Source = playerImage;
                    PlayerName_3_5.Text = PlayerName;
                    PlayerImage_3_5.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_3_5.IsVisible = true;
                        if (action == "star") { PlayerImagePop_3_5.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_3_5.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_3_5.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_3_5.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_3_5.Source = "shirt_mom+h"; }
                    }
                }

            }

            if (Posx == "4")
            {
                if (Posy == "1")
                {
                    Stack_4_1.IsVisible = true;
                    PlayerImage_4_1.Source = playerImage;
                    PlayerName_4_1.Text = PlayerName;
                    PlayerImage_4_1.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_4_1.IsVisible = true;
                        if (action == "star") { PlayerImagePop_4_1.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_4_1.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_4_1.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_1.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_1.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "2")
                {
                    Stack_4_2.IsVisible = true;
                    PlayerImage_4_2.Source = playerImage;
                    PlayerName_4_2.Text = PlayerName;
                    PlayerImage_4_2.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_4_2.IsVisible = true;
                        if (action == "star") { PlayerImagePop_4_2.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_4_2.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_4_2.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_2.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_2.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "3")
                {
                    Stack_4_3.IsVisible = true;
                    PlayerImage_4_3.Source = playerImage;
                    PlayerName_4_3.Text = PlayerName;
                    PlayerImage_4_3.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_4_3.IsVisible = true;
                        if (action == "star") { PlayerImagePop_4_3.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_4_3.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_4_3.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_3.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_3.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "4")
                {
                    Stack_4_4.IsVisible = true;
                    PlayerImage_4_4.Source = playerImage;
                    PlayerName_4_4.Text = PlayerName;
                    PlayerImage_4_4.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_4_4.IsVisible = true;
                        if (action == "star") { PlayerImagePop_4_4.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_4_4.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_4_4.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_4.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_4.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "5")
                {
                    Stack_4_5.IsVisible = true;
                    PlayerImage_4_5.Source = playerImage;
                    PlayerName_4_5.Text = PlayerName;
                    PlayerImage_4_5.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_4_5.IsVisible = true;
                        if (action == "star") { PlayerImagePop_4_5.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_4_5.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_4_5.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_4_5.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_4_5.Source = "shirt_mom+h"; }
                    }
                }

            }

            if (Posx == "5")
            {
                if (Posy == "1")
                {
                    Stack_5_1.IsVisible = true;
                    PlayerImage_5_1.Source = playerImage;
                    PlayerName_5_1.Text = PlayerName;
                    PlayerImage_5_1.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_5_1.IsVisible = true;
                        if (action == "star") { PlayerImagePop_5_1.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_5_1.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_5_1.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_1.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_1.Source = "shirt_mom+h"; }
                    }

                }
                if (Posy == "2")
                {
                    Stack_5_2.IsVisible = true;
                    PlayerImage_5_2.Source = playerImage;
                    PlayerName_5_2.Text = PlayerName;
                    PlayerImage_5_2.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_5_2.IsVisible = true;
                        if (action == "star") { PlayerImagePop_5_2.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_5_2.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_5_2.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_2.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_2.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "3")
                {
                    Stack_5_3.IsVisible = true;
                    PlayerImage_5_3.Source = playerImage;
                    PlayerName_5_3.Text = PlayerName;
                    PlayerImage_5_3.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_5_3.IsVisible = true;
                        if (action == "star") { PlayerImagePop_5_3.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_5_3.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_5_3.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_3.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_3.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "4")
                {
                    Stack_5_4.IsVisible = true;
                    PlayerImage_5_4.Source = playerImage;
                    PlayerName_5_4.Text = PlayerName;
                    PlayerImage_5_4.Source = playerImage;
                    if (action != "0")
                    {
                        PlayerImagePop_5_4.IsVisible = true;
                        if (action == "star") { PlayerImagePop_5_4.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_5_4.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_5_4.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_4.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_4.Source = "shirt_mom+h"; }
                    }
                }
                if (Posy == "5")
                {
                    Stack_5_5.IsVisible = true;
                    PlayerImage_5_5.Source = playerImage;
                    PlayerName_5_5.Text = PlayerName;
                    PlayerImage_5_5.Source = playerImage;

                    if (action != "0")
                    {
                        PlayerImagePop_5_5.IsVisible = true;
                        if (action == "star") { PlayerImagePop_5_5.Source = "shirt_st"; }
                        else if (action == "hairdryer") { PlayerImagePop_5_5.Source = "shirt_hd"; }
                        else if (action == "manofmatch") { PlayerImagePop_5_5.Source = "shirt_mom"; }
                        else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_5_5.Source = "shirt_mom+s"; }
                        else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_5_5.Source = "shirt_mom+h"; }
                    }
                }

            }
        }

        #region On Tapped


        public void OnTapped_Position_1_3(object sender, EventArgs e)
        {

            MakeActivePosition(1, 3);
        }

        void OnTapped_Position_2_1(object sender, EventArgs e)
        {

            MakeActivePosition(2, 1);
        }

        void OnTapped_Position_2_2(object sender, EventArgs e)
        {

            MakeActivePosition(2, 2);
        }
        void OnTapped_Position_2_3(object sender, EventArgs e)
        {

            MakeActivePosition(2, 3);
        }

        void OnTapped_Position_2_4(object sender, EventArgs e)
        {

            MakeActivePosition(2, 4);
        }

        void OnTapped_Position_2_5(object sender, EventArgs e)
        {

            MakeActivePosition(2, 5);
        }


        void OnTapped_Position_3_1(object sender, EventArgs e)
        {

            MakeActivePosition(3, 1);
        }

        void OnTapped_Position_3_2(object sender, EventArgs e)
        {

            MakeActivePosition(3, 2);
        }

        void OnTapped_Position_3_3(object sender, EventArgs e)
        {

            MakeActivePosition(3, 3);
        }

        void OnTapped_Position_3_4(object sender, EventArgs e)
        {

            MakeActivePosition(3, 4);
        }

        void OnTapped_Position_3_5(object sender, EventArgs e)
        {

            MakeActivePosition(3, 5);
        }

        void OnTapped_Position_4_1(object sender, EventArgs e)
        {

            MakeActivePosition(4, 1);
        }

        void OnTapped_Position_4_2(object sender, EventArgs e)
        {

            MakeActivePosition(4, 2);
        }

        void OnTapped_Position_4_3(object sender, EventArgs e)
        {

            MakeActivePosition(4, 3);
        }

        void OnTapped_Position_4_4(object sender, EventArgs e)
        {

            MakeActivePosition(4, 4);
        }

        void OnTapped_Position_4_5(object sender, EventArgs e)
        {

            MakeActivePosition(4, 5);
        }

        void OnTapped_Position_5_1(object sender, EventArgs e)
        {

            MakeActivePosition(5, 1);
        }

        void OnTapped_Position_5_2(object sender, EventArgs e)
        {

            MakeActivePosition(5, 2);
        }

        void OnTapped_Position_5_3(object sender, EventArgs e)
        {

            MakeActivePosition(5, 3);
        }

        void OnTapped_Position_5_4(object sender, EventArgs e)
        {

            MakeActivePosition(5, 4);
        }

        void OnTapped_Position_5_5(object sender, EventArgs e)
        {

            MakeActivePosition(5, 5);
        }

        #endregion

        async void MakeActivePosition(int row, int col)
        {

            int positionNumber = 0;

            for (int i = 0; i < FanspickCache.SelectManagerPick.Count(); i++)
            {

                if (row.ToString() == FanspickCache.SelectManagerPick[i].PosX && col.ToString() == FanspickCache.SelectManagerPick[i].PosY)
                {
                    positionNumber = i;
                    break;
                }

            }

            Debug.WriteLine("", positionNumber.ToString());

            selectedPlayer = FanspickCache.SelectManagerPick[positionNumber];

            FanspickCache.CurrentFanspickScope.SelectedPlayerId = selectedPlayer.PLayerId.ToString();
			var _optionPop = new PlayerInformationPage(selectedPlayer.PLayerId.ToString());
            await Navigation.PushPopupAsync(_optionPop);


        }

        public SelectUserPickPlayer selectedPlayer { get; private set; }


        void Openbanner(object sender, EventArgs e)
        {
            FanspickCache.isPitch = false;
			var _billBoardPopup = new BillBoard();
			Navigation.PushPopupAsync(_billBoardPopup);
        }

		void OpenbannerPitch(object sender, EventArgs e)
		{
			FanspickCache.isPitch = true;
			var _billBoardPopup = new BillBoard();
			Navigation.PushPopupAsync(_billBoardPopup);
		}


		public void hideAllPositions()
        {
            Stack_1_3.IsVisible = false;

            Stack_2_1.IsVisible = false;
            Stack_2_2.IsVisible = false;
            Stack_2_3.IsVisible = false;
            Stack_2_4.IsVisible = false;
            Stack_2_5.IsVisible = false;

            Stack_3_1.IsVisible = false;
            Stack_3_2.IsVisible = false;
            Stack_3_3.IsVisible = false;
            Stack_3_4.IsVisible = false;
            Stack_3_5.IsVisible = false;

            Stack_4_1.IsVisible = false;
            Stack_4_2.IsVisible = false;
            Stack_4_3.IsVisible = false;
            Stack_4_4.IsVisible = false;
            Stack_4_5.IsVisible = false;

            Stack_5_1.IsVisible = false;
            Stack_5_2.IsVisible = false;
            Stack_5_3.IsVisible = false;
            Stack_5_4.IsVisible = false;
            Stack_5_5.IsVisible = false;

            PlayerImagePop_1_3.IsVisible = false;

            PlayerImagePop_2_1.IsVisible = false;
            PlayerImagePop_2_2.IsVisible = false;
            PlayerImagePop_2_3.IsVisible = false;
            PlayerImagePop_2_4.IsVisible = false;
            PlayerImagePop_2_5.IsVisible = false;

            PlayerImagePop_3_1.IsVisible = false;
            PlayerImagePop_3_2.IsVisible = false;
            PlayerImagePop_3_3.IsVisible = false;
            PlayerImagePop_3_4.IsVisible = false;
            PlayerImagePop_3_5.IsVisible = false;

            PlayerImagePop_4_1.IsVisible = false;
            PlayerImagePop_4_2.IsVisible = false;
            PlayerImagePop_4_3.IsVisible = false;
            PlayerImagePop_4_4.IsVisible = false;
            PlayerImagePop_4_5.IsVisible = false;

            PlayerImagePop_5_1.IsVisible = false;
            PlayerImagePop_5_2.IsVisible = false;
            PlayerImagePop_5_3.IsVisible = false;
            PlayerImagePop_5_4.IsVisible = false;
            PlayerImagePop_5_5.IsVisible = false;


        }

        public void ArrangementOfPlayers(List<String> line1, List<String> line2, List<String> line3, List<String> line4)
        {

            setBakgroundPlayer();

            if (line2.Count == 0)
            {
                UpdateRowHeight(25, 25, 1, 25, 25);
                set3Line();
            }
            else if (line3.Count == 0)
            {
                UpdateRowHeight(25, 25, 25, 1, 25);
                set3Line();
            }
            else
            {
                UpdateRowHeight(20, 20, 20, 20, 20);
                set4Line();
             //   Abs_1_3.HeightRequest=30;
            }

            Debug.WriteLine("playerListRow1", "" + line1);
            Debug.WriteLine("playerListRow1", "" + line2);
            Debug.WriteLine("playerListRow1", "" + line3);
            Debug.WriteLine("playerListRow1", "" + line4);


            int denominator1 = line1.Where(l => l != "0").Count();
            denominator1 = denominator1 == 0 ? 1 : denominator1;
            int percentForEachLine1 = (100 / denominator1);
            percentForEachLine1 = percentForEachLine1 == 0 ? 1 : percentForEachLine1;
            List<int> methodValues1 = new List<int>();
            for (int i = 1; i <= 5; i++)
            {
                string columnPos = line1.FirstOrDefault(c => Convert.ToInt32(c) == i);
                if (!string.IsNullOrEmpty(columnPos))
                {
                    methodValues1.Add(percentForEachLine1);
                }
                else
                {
                    methodValues1.Add(1);
                }
            }
            UpdateRowLine1(methodValues1[0], methodValues1[1], methodValues1[2], methodValues1[3], methodValues1[4]);

            int denominator2 = line2.Where(l => l != "0").Count();
            denominator2 = denominator2 == 0 ? 1 : denominator2;
            int percentForEachLine2 = (100 / denominator2);
            percentForEachLine2 = percentForEachLine2 == 0 ? 1 : percentForEachLine2;
            List<int> methodValues2 = new List<int>();
            for (int i = 1; i <= 5; i++)
            {
                string columnPos = line2.FirstOrDefault(c => Convert.ToInt32(c) == i);
                if (!string.IsNullOrEmpty(columnPos))
                {
                    methodValues2.Add(percentForEachLine2);
                }
                else
                {
                    methodValues2.Add(1);
                }
            }
            UpdateRowLine2(methodValues2[0], methodValues2[1], methodValues2[2], methodValues2[3], methodValues2[4]);

            int denominator3 = line3.Where(l => l != "0").Count();
            denominator3 = denominator3 == 0 ? 1 : denominator3;
            int percentForEachLine3 = (100 / denominator3);
            percentForEachLine3 = percentForEachLine3 == 0 ? 1 : percentForEachLine3;
            List<int> methodValues3 = new List<int>();
            for (int i = 1; i <= 5; i++)
            {
                string columnPos = line3.FirstOrDefault(c => Convert.ToInt32(c) == i);
                if (!string.IsNullOrEmpty(columnPos))
                {
                    methodValues3.Add(percentForEachLine3);
                }
                else
                {
                    methodValues3.Add(1);
                }
            }
            UpdateRowLine3(methodValues3[0], methodValues3[1], methodValues3[2], methodValues3[3], methodValues3[4]);

            int denominator4 = line4.Where(l => l != "0").Count();
            denominator4 = denominator4 == 0 ? 1 : denominator4;
            int percentForEachLine4 = (100 / denominator4);
            percentForEachLine4 = percentForEachLine4 == 0 ? 1 : percentForEachLine4;
            List<int> methodValues4 = new List<int>();
            for (int i = 1; i <= 5; i++)
            {
                string columnPos = line4.FirstOrDefault(c => Convert.ToInt32(c) == i);
                if (!string.IsNullOrEmpty(columnPos))
                {
                    methodValues4.Add(percentForEachLine4);
                }
                else
                {
                    methodValues4.Add(1);
                }
            }
            UpdateRowLine4(methodValues4[0], methodValues4[1], methodValues4[2], methodValues4[3], methodValues4[4]);


        }



        public void UpdateRowHeight(int a, int b, int c, int d, int e)
        {
            Row1.Height = new GridLength(a, GridUnitType.Star);
            Row2.Height = new GridLength(b, GridUnitType.Star);
            Row3.Height = new GridLength(c, GridUnitType.Star);
            Row4.Height = new GridLength(d, GridUnitType.Star);
            Row5.Height = new GridLength(e, GridUnitType.Star);
        }

        public void UpdateRowLine1(int a, int b, int c, int d, int e)
        {
            Row2_Column1.Width = new GridLength(a, GridUnitType.Star);
            Row2_Column2.Width = new GridLength(b, GridUnitType.Star);
            Row2_Column3.Width = new GridLength(c, GridUnitType.Star);
            Row2_Column4.Width = new GridLength(d, GridUnitType.Star);
            Row2_Column5.Width = new GridLength(e, GridUnitType.Star);
        }

        public void UpdateRowLine2(int a, int b, int c, int d, int e)
        {
            Row3_Column1.Width = new GridLength(a, GridUnitType.Star);
            Row3_Column2.Width = new GridLength(b, GridUnitType.Star);
            Row3_Column3.Width = new GridLength(c, GridUnitType.Star);
            Row3_Column4.Width = new GridLength(d, GridUnitType.Star);
            Row3_Column5.Width = new GridLength(e, GridUnitType.Star);
        }

        public void UpdateRowLine3(int a, int b, int c, int d, int e)
        {
            Row4_Column1.Width = new GridLength(a, GridUnitType.Star);
            Row4_Column2.Width = new GridLength(b, GridUnitType.Star);
            Row4_Column3.Width = new GridLength(c, GridUnitType.Star);
            Row4_Column4.Width = new GridLength(d, GridUnitType.Star);
            Row4_Column5.Width = new GridLength(e, GridUnitType.Star);
        }

        public void UpdateRowLine4(int a, int b, int c, int d, int e)
        {
            Row5_Column1.Width = new GridLength(a, GridUnitType.Star);
            Row5_Column2.Width = new GridLength(b, GridUnitType.Star);
            Row5_Column3.Width = new GridLength(c, GridUnitType.Star);
            Row5_Column4.Width = new GridLength(d, GridUnitType.Star);
            Row5_Column5.Width = new GridLength(e, GridUnitType.Star);
        }

        public void ArrangePlayerLine(String x, String y)
        {

            //var c = new ColumnDefinition(); c.Width = new GridLength(,GridUnitType.Star);

            if (x == "2")
            {
                playerListRow1.Add(y);
            }

            if (x == "3")
            {
                playerListRow2.Add(y);
            }
            if (x == "4")
            {
                playerListRow3.Add(y);
            }

            if (x == "5")
            {
                playerListRow4.Add(y);
            }


        }

        public void setBakgroundPlayer()
        {
            var semiTransparentColor = new Color(0, 0, 0, 0.5);

            List<Label> allPlayerName = new List<Label>();
            allPlayerName.Add(PlayerName_1_3);

            allPlayerName.Add(PlayerName_2_1);
            allPlayerName.Add(PlayerName_2_2);
            allPlayerName.Add(PlayerName_2_3);
            allPlayerName.Add(PlayerName_2_4);
            allPlayerName.Add(PlayerName_2_5);

            allPlayerName.Add(PlayerName_3_1);
            allPlayerName.Add(PlayerName_3_2);
            allPlayerName.Add(PlayerName_3_3);
            allPlayerName.Add(PlayerName_3_4);
            allPlayerName.Add(PlayerName_3_5);

            allPlayerName.Add(PlayerName_4_1);
            allPlayerName.Add(PlayerName_4_2);
            allPlayerName.Add(PlayerName_4_3);
            allPlayerName.Add(PlayerName_4_4);
            allPlayerName.Add(PlayerName_4_5);

            allPlayerName.Add(PlayerName_5_1);
            allPlayerName.Add(PlayerName_5_2);
            allPlayerName.Add(PlayerName_5_3);
            allPlayerName.Add(PlayerName_5_4);
            allPlayerName.Add(PlayerName_5_5);

            List<Image> allPlayerBack = new List<Image>();
            allPlayerBack.Add(ImageBack_1_3);

            allPlayerBack.Add(ImageBack_2_1);
            allPlayerBack.Add(ImageBack_2_2);
            allPlayerBack.Add(ImageBack_2_3);
            allPlayerBack.Add(ImageBack_2_4);
            allPlayerBack.Add(ImageBack_2_5);

            allPlayerBack.Add(ImageBack_3_1);
            allPlayerBack.Add(ImageBack_3_2);
            allPlayerBack.Add(ImageBack_3_3);
            allPlayerBack.Add(ImageBack_3_4);
            allPlayerBack.Add(ImageBack_3_5);

            allPlayerBack.Add(ImageBack_4_1);
            allPlayerBack.Add(ImageBack_4_2);
            allPlayerBack.Add(ImageBack_4_3);
            allPlayerBack.Add(ImageBack_4_4);
            allPlayerBack.Add(ImageBack_4_5);

            allPlayerBack.Add(ImageBack_5_1);
            allPlayerBack.Add(ImageBack_5_2);
            allPlayerBack.Add(ImageBack_5_3);
            allPlayerBack.Add(ImageBack_5_4);
            allPlayerBack.Add(ImageBack_5_5);


            for (int i = 0; i < allPlayerName.Count(); i++)
            {

                if (allPlayerName[i].Text != null || allPlayerName[i].Text == "")
                {

                    var midTransparentColor = new Color(0, 0, 0, 0.7);
                    //	allPlayerName[i].BackgroundColor = midTransparentColor;
                    allPlayerName[i].TextColor = Color.White;
                    allPlayerName[i].WidthRequest = 60;
                    allPlayerName[i].VerticalTextAlignment = TextAlignment.Center;
                    allPlayerName[i].HorizontalTextAlignment = TextAlignment.Center;
                    allPlayerName[i].VerticalOptions = LayoutOptions.FillAndExpand;
                    allPlayerName[i].HorizontalOptions = LayoutOptions.FillAndExpand;
                    allPlayerName[i].FontSize = 8;


                    allPlayerBack[i].Source = "back_player_diffused";
                    allPlayerBack[i].VerticalOptions = LayoutOptions.FillAndExpand;
                    allPlayerBack[i].HorizontalOptions = LayoutOptions.FillAndExpand;
                    allPlayerBack[i].Aspect = Aspect.Fill;
                    //allPlayerBack[i].WidthRequest = 58;
                   // allPlayerBack[i].HeightRequest = 30;
                }
            }

			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);

        }


       

        public void RemoveSpacing()
        {

            List<StackLayout> allPlayerLayout = new List<StackLayout>();
            allPlayerLayout.Add(Stack_1_3);

            allPlayerLayout.Add(Stack_2_1);
            allPlayerLayout.Add(Stack_2_2);
            allPlayerLayout.Add(Stack_2_3);
            allPlayerLayout.Add(Stack_2_4);
            allPlayerLayout.Add(Stack_2_5);

            allPlayerLayout.Add(Stack_3_1);
            allPlayerLayout.Add(Stack_3_2);
            allPlayerLayout.Add(Stack_3_3);
            allPlayerLayout.Add(Stack_3_4);
            allPlayerLayout.Add(Stack_3_5);

            allPlayerLayout.Add(Stack_4_1);
            allPlayerLayout.Add(Stack_4_2);
            allPlayerLayout.Add(Stack_4_3);
            allPlayerLayout.Add(Stack_4_4);
            allPlayerLayout.Add(Stack_4_5);

            allPlayerLayout.Add(Stack_5_1);
            allPlayerLayout.Add(Stack_5_2);
            allPlayerLayout.Add(Stack_5_3);
            allPlayerLayout.Add(Stack_5_4);
            allPlayerLayout.Add(Stack_5_5);

            for (int i = 0; i < allPlayerLayout.Count(); i++)
            {
                allPlayerLayout[i].Spacing = 0;

            }

        }

        public void LoadGif()
        {

			String htmlsource = FanspickCache.PitchImage;
			String htmlMain = FanspickCache.BannerImage.Replace(".gif.gif", ".gif");

            if (FanspickCache.CurrentFanspickScope.SelectedFixture.CompetitionName == "UEFA Champions League")
            {
                //  htmlsource = "Heineken_pitch.gif";
                //  htmlMain = "Heineken_top_banner.gif";
                skyBack.IsVisible = true;
                skyBack.Source = "start_bg_night";

            }
            else if (FanspickCache.CurrentFanspickScope.SelectedFixture.CompetitionName == "UEFA Europa League")
            {
                //  htmlsource = "Heineken_pitch.gif";
                //  htmlMain = "Heineken_top_banner.gif";
                skyBack.IsVisible = true;
                skyBack.Source = "start_bg_europa copy";
            }
            else
            {

                //  htmlsource = "Oddbins_banner.gif";
                //  htmlMain = "oddbin_bigbanner_F.gif";
                skyBack.IsVisible = false;

            }


			HtmlWebViewSource html = new HtmlWebViewSource();
			html.Html = String.Format
				(@"<html style='width:" + BannerWidthSize() + ";height=" + BannerheightSize() + "; margin-left:-6px;'><body style='width:100%;height=100%; background: #ffffff;'><img src='{0}' style='width:100%;height=100%;margin-top:-8px;'  /></body></html>",
				 htmlsource);
			Banner1.Source = html;
			Banner2.Source = html;

            ManagerbannerImg.Source = htmlMain;

			//HtmlWebViewSource htmlmain = new HtmlWebViewSource();
			//htmlmain.Html = String.Format
			//             (@"<html><body style='overflow:hidden;'><img src='{0}' style='width:190px;height:42px;margin-left:-12px;margin-right:13px;margin-top:-9px;background: transparent;'/></body></html>",
			//     htmlMain);
			// string imgSource= $"http://fanspick-admin-test.5sol.co.uk/resources/Banners/ {htmlMain}";
			// imgSource = imgSource.Replace("\\", "/");           

		}

        public void set3Line()
		{
			LoadAllImagePop();

			String device = CrossDeviceInfo.Current.Model;

			double playerShirtSize = 37;

			if (device == "iPad")
			{
				playerShirtSize = 85;
			}


			for (int i = 0; i < allImagePopUp.Count(); i++)
			{
				allImagePopUp[i].WidthRequest = playerShirtSize;
				allImage[i].WidthRequest = playerShirtSize;

				allImagePopUp[i].HeightRequest = playerShirtSize;
				allImage[i].HeightRequest = playerShirtSize;
			}

		}

		public void set4Line()
		{
			LoadAllImagePop();

			String device = CrossDeviceInfo.Current.Model;

			double playerShirtSize = 32;

			if (device == "iPad")
			{
				playerShirtSize = 85;
			}


			for (int i = 0; i < allImagePopUp.Count(); i++)
			{
				allImagePopUp[i].WidthRequest = playerShirtSize;
				allImage[i].WidthRequest = playerShirtSize;

				allImagePopUp[i].HeightRequest = playerShirtSize;
				allImage[i].HeightRequest = playerShirtSize;
			}

		}

        List<Image> allImagePopUp = new List<Image>();
        List<Image> allImage = new List<Image>();
        public void LoadAllImagePop (){

            if(allImagePopUp.Count>0){
                allImagePopUp.Clear();
            }

			if (allImage.Count > 0)
			{
				allImage.Clear();
			}
                

			allImagePopUp.Add(PlayerImagePop_1_3);

			allImagePopUp.Add(PlayerImagePop_2_1);
			allImagePopUp.Add(PlayerImagePop_2_2);
			allImagePopUp.Add(PlayerImagePop_2_3);
			allImagePopUp.Add(PlayerImagePop_2_4);
			allImagePopUp.Add(PlayerImagePop_2_5);

			allImagePopUp.Add(PlayerImagePop_3_1);
			allImagePopUp.Add(PlayerImagePop_3_2);
			allImagePopUp.Add(PlayerImagePop_3_3);
			allImagePopUp.Add(PlayerImagePop_3_4);
			allImagePopUp.Add(PlayerImagePop_3_5);

			allImagePopUp.Add(PlayerImagePop_4_1);
			allImagePopUp.Add(PlayerImagePop_4_2);
			allImagePopUp.Add(PlayerImagePop_4_3);
			allImagePopUp.Add(PlayerImagePop_4_4);
			allImagePopUp.Add(PlayerImagePop_4_5);

			allImagePopUp.Add(PlayerImagePop_5_1);
			allImagePopUp.Add(PlayerImagePop_5_2);
			allImagePopUp.Add(PlayerImagePop_5_3);
			allImagePopUp.Add(PlayerImagePop_5_4);
			allImagePopUp.Add(PlayerImagePop_5_5);


            allImage.Add(PlayerImage_1_3);

			allImage.Add(PlayerImage_2_1);
			allImage.Add(PlayerImage_2_2);
			allImage.Add(PlayerImage_2_3);
			allImage.Add(PlayerImage_2_4);
			allImage.Add(PlayerImage_2_5);

			allImage.Add(PlayerImage_3_1);
			allImage.Add(PlayerImage_3_2);
			allImage.Add(PlayerImage_3_3);
			allImage.Add(PlayerImage_3_4);
			allImage.Add(PlayerImage_3_5);

			allImage.Add(PlayerImage_4_1);
			allImage.Add(PlayerImage_4_2);
			allImage.Add(PlayerImage_4_3);
			allImage.Add(PlayerImage_4_4);
			allImage.Add(PlayerImage_4_5);

			allImage.Add(PlayerImage_5_1);
			allImage.Add(PlayerImage_5_2);
			allImage.Add(PlayerImage_5_3);
			allImage.Add(PlayerImage_5_4);
			allImage.Add(PlayerImage_5_5);

        }

		public String BannerheightSize()
		{

			String size = "50px";
			String device = CrossDeviceInfo.Current.Model;
			if (device == "iPad")
			{
				size = "100px";
			}

			return size;
		}


		public String BannerWidthSize()
		{

			String size = "100px";
			String device = CrossDeviceInfo.Current.Model;
			if (device == "iPad")
			{
				size = "200px";
			}

			return size;
		}


        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
        }

		public void ajustDevice(string deviceType)
		{
			List<Label> allPlayerName = new List<Label>();
			allPlayerName.Add(PlayerName_1_3);

			allPlayerName.Add(PlayerName_2_1);
			allPlayerName.Add(PlayerName_2_2);
			allPlayerName.Add(PlayerName_2_3);
			allPlayerName.Add(PlayerName_2_4);
			allPlayerName.Add(PlayerName_2_5);

			allPlayerName.Add(PlayerName_3_1);
			allPlayerName.Add(PlayerName_3_2);
			allPlayerName.Add(PlayerName_3_3);
			allPlayerName.Add(PlayerName_3_4);
			allPlayerName.Add(PlayerName_3_5);

			allPlayerName.Add(PlayerName_4_1);
			allPlayerName.Add(PlayerName_4_2);
			allPlayerName.Add(PlayerName_4_3);
			allPlayerName.Add(PlayerName_4_4);
			allPlayerName.Add(PlayerName_4_5);

			allPlayerName.Add(PlayerName_5_1);
			allPlayerName.Add(PlayerName_5_2);
			allPlayerName.Add(PlayerName_5_3);
			allPlayerName.Add(PlayerName_5_4);
			allPlayerName.Add(PlayerName_5_5);

			List<Image> allPlayerBack = new List<Image>();
			allPlayerBack.Add(ImageBack_1_3);

			allPlayerBack.Add(ImageBack_2_1);
			allPlayerBack.Add(ImageBack_2_2);
			allPlayerBack.Add(ImageBack_2_3);
			allPlayerBack.Add(ImageBack_2_4);
			allPlayerBack.Add(ImageBack_2_5);

			allPlayerBack.Add(ImageBack_3_1);
			allPlayerBack.Add(ImageBack_3_2);
			allPlayerBack.Add(ImageBack_3_3);
			allPlayerBack.Add(ImageBack_3_4);
			allPlayerBack.Add(ImageBack_3_5);

			allPlayerBack.Add(ImageBack_4_1);
			allPlayerBack.Add(ImageBack_4_2);
			allPlayerBack.Add(ImageBack_4_3);
			allPlayerBack.Add(ImageBack_4_4);
			allPlayerBack.Add(ImageBack_4_5);

			allPlayerBack.Add(ImageBack_5_1);
			allPlayerBack.Add(ImageBack_5_2);
			allPlayerBack.Add(ImageBack_5_3);
			allPlayerBack.Add(ImageBack_5_4);
			allPlayerBack.Add(ImageBack_5_5);



			if (deviceType == "iPad")
			{

				for (int i = 0; i < allPlayerName.Count(); i++)
				{
					allPlayerBack[i].WidthRequest = 100;
					allPlayerName[i].HeightRequest = 22;
					allPlayerName[i].FontSize = 12;

				}

				Gline1.Margin = new Thickness(20, 0, 20, 0);
				ManagerbannerImg.HeightRequest = 170;
				ManagerbannerImg.WidthRequest = 750;
				ManagerbannerImg.Margin = new Thickness(0, 0, 0, 60);

				Banner1.HeightRequest = 40;
				Banner2.HeightRequest = 40;
				Banner1.Margin = new Thickness(0, 0, 8, 0);
				Banner2.Margin = new Thickness(5, 0, 8, 0);

				BoxDivision1.Height = new GridLength(1.5, GridUnitType.Star);
				BoxDivision2.Height = new GridLength(6.45, GridUnitType.Star);
				BoxDivision3.Height = new GridLength(0.30, GridUnitType.Star);
				BoxDivision4.Height = new GridLength(1.1, GridUnitType.Star);

				BarCol1.Width = new GridLength(50);
				BarCol3.Width = new GridLength(50);
				BarCol4.Width = new GridLength(50);

                localTeamImage.Margin = new Thickness(10);
                visitorTeamImage.Margin = new Thickness(10);

                HTeamScore.FontSize = 20;
                VTeamScore.FontSize = 20;
                ScoreGap.FontSize = 20;


			}
			else
			{
				Gline1.Margin = new Thickness(0, 0, 0, 0);
				for (int i = 0; i < allPlayerName.Count(); i++)
				{
					allPlayerBack[i].WidthRequest = 60;
					allPlayerName[i].HeightRequest = 15;
					allPlayerName[i].FontSize = 10;

				}

				BoxDivision1.Height = new GridLength(1.5, GridUnitType.Star);
				BoxDivision2.Height = new GridLength(6.5, GridUnitType.Star);
				BoxDivision3.Height = new GridLength(0.25, GridUnitType.Star);
				BoxDivision4.Height = new GridLength(1.1, GridUnitType.Star);

				BarCol1.Width = new GridLength(35);
				BarCol3.Width = new GridLength(35);
				BarCol4.Width = new GridLength(35);

				
			}


		}

	}
}
