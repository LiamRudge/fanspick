﻿using Fanspick.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fanspick.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SocialWebView : ContentPage
    {
        public SocialWebView()
        {
            InitializeComponent();
            LocalWebView.Source = CacheStorage.ViewModels.HomePage.SocialVM.SelectedSocialFeed.Expanded_URL;


        }
    }
}