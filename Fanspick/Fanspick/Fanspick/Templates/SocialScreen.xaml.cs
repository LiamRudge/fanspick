﻿using Fanspick.Helper;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.ViewModels;
using Plugin.Share;
using Plugin.Share.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using System.Diagnostics;
using Plugin.Geolocator;
using static Fanspick.Shared.Models.DTO.EventLoggingDTO;
using Plugin.DeviceInfo;
using Fanspick.Shared.Services;
using Fanspick.Shared.Models.Service;
using Rg.Plugins.Popup.Extensions;
using Plugin.Connectivity;

namespace Fanspick.Templates
{
	public partial class SocialScreen : ContentPage
	{
		SocialViewModel vm;
		RequestEventLoggingDTO requestEventLoggingDTO = new RequestEventLoggingDTO();
		public SocialScreen()
		{
			InitializeComponent();

			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{
                if (CrossConnectivity.Current.IsConnected)
				{
					InternetScreen.IsVisible = false;
				}
				else
				{
					InternetScreen.IsVisible = true;
				}

			};

		}

		void OnItemAppearing(object sender, ItemVisibilityEventArgs e)
		{
            var currentItem = e.Item as SocialFeed;
            var lastItem = vm.SocialFeedData[vm.SocialFeedData.Count() - 1];
			if (currentItem == lastItem)
			{
                vm.ShowLoader();
				PullMoreAroundYouData(); // Make the rest api call to get more data to list
			}
		}

        private async void PullMoreAroundYouData()
		{
            actIndBottom.IsVisible = true;
            await vm.LoadDataLastitem();	
            vm.HideLoader();
            actIndBottom.IsVisible = false;

		}
		protected override async void OnAppearing()
		{
			vm = CacheStorage.ViewModels.HomePage.SocialVM;
            await Navigation.PopAllPopupAsync(true);
			if (vm.SocialFeedData.Count() == 0)
			{
				await CacheStorage.ViewModels.HomePage.SocialVM.LoadData();
			}
			if (vm.SocialFeedData.Count() > 0)
			{
				acIndicator.IsVisible = true;
				listView.ItemsSource = vm.SocialFeedData;
				listView.IsVisible = true;
				lblNoData.IsVisible = false;
			}
			else
			{
				acIndicator.IsVisible = false;
				listView.IsVisible = false;
				lblNoData.IsVisible = true;
			}

		}



		private void ListView_ItemSelected(object sender, ItemTappedEventArgs e)
		{
			vm.SelectedSocialFeed = e.Item as SocialFeed;

            Navigation.PushAsync(new SocialWebView(), true);
			//CacheStorage.ViewModels.App_Main.ActiveDetailsPage(ScreenType.SocialWebView);
			((ListView)sender).SelectedItem = null;
			GetInfo("socialClick", "social event clicked " + vm.SelectedSocialFeed.Title);

		}

		private async void ShareImage_OnTapped(object sender, EventArgs e)
		{
			TappedEventArgs te = (TappedEventArgs)e;
			SocialFeed sf = te.Parameter as SocialFeed;


			//await Plugin.Share.CrossShare.Current.Share(new Plugin.Share.Abstractions.ShareMessage
			//{
			//    Url = sf.Expanded_URL,
			//    Title = "Fanspick",
			//    Text = ""
			//}, new ShareOptions() { ExcludedUIActivityTypes = new[] { ShareUIActivityType.PostToFacebook } });

			await Plugin.Share.CrossShare.Current.Share(new Plugin.Share.Abstractions.ShareMessage
			{
				Url = sf.Expanded_URL,
				Title = "Fanspick",
				Text = ""
			}, new Plugin.Share.Abstractions.ShareOptions { ChooserTitle = "Fanspick" });
		}

		public async void GetInfo(String eventtype, String description)
		{
			try
			{

				var locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

				if (position == null)
				{
					Debug.WriteLine("null gps", "null gps");
					return;
				}

				if (position.Latitude.ToString() != null)
				{
					requestEventLoggingDTO.Latitude = "" + position.Latitude;
				}
				else
				{
					requestEventLoggingDTO.Latitude = "0";
				}

				if (position.Longitude.ToString() != null)
				{
					requestEventLoggingDTO.Longitude = "" + position.Longitude;
				}
				else
				{
					requestEventLoggingDTO.Longitude = "0";
				}
				requestEventLoggingDTO.EventType = eventtype;
				requestEventLoggingDTO.EventDescription = description;
				requestEventLoggingDTO.EventAdditionalInfoID = "" + FanspickCache.UserData.AccessToken;
				requestEventLoggingDTO.UserAgent = CrossDeviceInfo.Current.Platform + " " + CrossDeviceInfo.Current.Version;
				requestEventLoggingDTO.DeviceType = "IOS";
				requestEventLoggingDTO.DeviceToken = CrossDeviceInfo.Current.Id;
				requestEventLoggingDTO.AppVersion = "1";

                await EventLog();
				//  Application.Current.MainPage = new SplashScreen();


			}
			catch (Exception ex)
			{
				Debug.WriteLine(" " + ex);

			}

		}

		public async Task<ServiceResponse> EventLog()
		{
			ServiceResponse serviceResponse = await new FanspickServices<EventLogService>().Run(requestEventLoggingDTO);
			return serviceResponse;
		}
	}
}