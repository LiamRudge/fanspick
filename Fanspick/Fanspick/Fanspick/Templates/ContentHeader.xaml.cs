﻿﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Fanspick.Helper;
using Fanspick.Screens;
using Fanspick.Shared.Events.Events;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Fanspick.ViewModels;
using Plugin.DeviceInfo;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace Fanspick.Templates
{
	public partial class ContentHeader : ContentView
	{

		Fixture vmFixturePage;
		FanspickPitchEventHandler fanspickPitchEventHandler;
		
		public ContentHeader()
		{
			InitializeComponent();
		
			vmFixturePage = FanspickCache.CurrentFanspickScope.SelectedFixture;
			BindingContext = vmFixturePage;
			GetTickerBar();
			//animate();

			community.GestureRecognizers.Add(
			new TapGestureRecognizer()
			{
				Command = new Command(() =>
				{
					//Navigation.PushAsync(new Screens.TeamCommunities(), true);
                    Navigation.PushAsync(new TeamCommunities(), true);
				})
			}
		);

			Header.GestureRecognizers.Add(
		new TapGestureRecognizer()
		{
			Command = new Command(() =>
			{
				iconArrow.Rotation = 180;
				Navigation.PushAsync(new Screens.FixturePage(), true);

			})
		}
		);


			menu.GestureRecognizers.Add(
		new TapGestureRecognizer()
		{
			Command = new Command(() =>
			{
				/* Handle the click here */

				Navigation.PopAllPopupAsync(true);

                    OptimisedFiles.MasterPage appMain = Application.Current.MainPage as OptimisedFiles.MasterPage;
				    appMain.IsPresented = true;

			})
		}
		);
			lblFixtureName.GestureRecognizers.Add(new TapGestureRecognizer()
			{
				Command = new Command(() =>
{
	Navigation.PopAllPopupAsync(true);


})
			});

			callTimerTicker();
		}
		private void callTimerTicker()
		{

			try
			{
				var minutes = TimeSpan.FromSeconds(120);

				Device.StartTimer(minutes, () => { GetTickerBar(); return true; });
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.ToString());
			}
		}

		public async void GetTickerBar()
		{
			try
			{
				RequestTickerBarDTO requestTickerBar = new RequestTickerBarDTO();
				requestTickerBar.AccessToken = FanspickCache.UserData.AccessToken;
				requestTickerBar.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;

				ServiceResponse serviceResponse = await new FanspickServices<TickerBarService>().Run(requestTickerBar);

				if (serviceResponse.OK)
				{
					ResponseTickerBarDTO teamDataResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseTickerBarDTO>(serviceResponse.Data);

					//            var summary = $"<html style='margin:0;'>{style}<body style='width:100%; height:100%; margin:0px; padding:0 0 10px; background:{teamDataResponse.Data.Bgcolour}; font-color:#000; font-family: HelveticaNeue-Thin; font-size:12px; line-height:18px;'><marquee behavior='scroll' direction='left' scrollamount={speed}>{teamDataResponse.Data.NewsText}</marquee></body></html>";

					var htmlSource = new HtmlWebViewSource();
					htmlSource.Html = "<style type=\"text/css\">" +
						"@font-face {" +
						"font-family: HelveticaNeue-Thin;" +
						"src: url('http://fanspick-admin-test.5sol.co.uk/resources/Font/HLL__.TTF')" +
						"}" +
					"body {" +
						"font-family: HelveticaNeue-Thin;" +
                        "font-size:"+labelTextSize()+"; width:100%; height:100%; margin-top:"+PaddingFromTop()+"; padding:0 0 10px 0; background:" + teamDataResponse.Data.Bgcolour + "; font-color:#000; line-height:"+LineHeight()+";" +
						"}" +
						"</style>" +
					"<body><marquee behavior='scroll' direction='left' scrollamount=" + Speed() + " >" + teamDataResponse.Data.NewsText + "</marquee></body>";
					htmlSource.BaseUrl = "http://fanspick-admin-test.5sol.co.uk/resources/Font/HLL__.TTF";
					TickerWebView.Source = htmlSource;
				}

			}
			catch (Exception e)
			{
				Debug.WriteLine(e.ToString());
			}

		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			String device = CrossDeviceInfo.Current.Model;
			ajustDevice(device);
		}

		public void ajustDevice(string deviceType)
		{

			if (deviceType == "iPad")
			{

                menu.WidthRequest = 40;
                menu.HeightRequest = 40;
                menu.Margin = new Thickness(5);

				community.WidthRequest = 40;
				community.HeightRequest = 40;
				community.Margin = new Thickness(6);

                TickerWebView.HeightRequest = 40;

				lblFixtureName.FontSize = 15;
				FixtureDate.FontSize = 14;

			}
			else
			{
				menu.WidthRequest = 25;
				menu.HeightRequest = 25;
                menu.Margin = new Thickness(10);

				community.WidthRequest = 25;
				community.HeightRequest = 25;
				community.Margin = new Thickness(10);

                TickerWebView.HeightRequest = 20;

                lblFixtureName.FontSize = 11;
                FixtureDate.FontSize = 10;
			}


		}

		public String PaddingFromTop()
		{

			String defaultsize = "0px";
			String device = CrossDeviceInfo.Current.Model;

			if (device == "iPad")
			{
				defaultsize = "8px";
			}

			return defaultsize;

		}

        public String labelTextSize(){

            String defaultsize = "12px";
            String device = CrossDeviceInfo.Current.Model;

            if (device == "iPad")
            {
                defaultsize = "24px";
            }

				return defaultsize;

        }

        public String Speed(){
            
			String defaultspeed = "2";
			String device = CrossDeviceInfo.Current.Model;

			if (device == "iPad")
			{
				defaultspeed = "4";
			}

			return defaultspeed;
        }

		public String LineHeight()
		{

			String defaultspeed = "18px";
			String device = CrossDeviceInfo.Current.Model;

			if (device == "iPad")
			{
				defaultspeed = "22px";
			}

			return defaultspeed;
		}
	}
}
