﻿using System;
using System.Collections.Generic;
using Fanspick.Helper;
using Fanspick.ViewModels;
using Xamarin.Forms;

namespace Fanspick.Templates
{
    public partial class PitchView : ContentView
    {

        App_MainViewModel vmApp_Main;

        public PitchView()
        {
            InitializeComponent();
            vmApp_Main = CacheStorage.ViewModels.App_Main;
        }

		#region On Tapped

		void OnTapped_Position_1_3(object sender, EventArgs e)
		{
			MakeActivePosition(1, 3);
		}

		void OnTapped_Position_2_1(object sender, EventArgs e)
		{
			MakeActivePosition(2, 1);
		}

		void OnTapped_Position_2_2(object sender, EventArgs e)
		{
			MakeActivePosition(2, 2);
		}

		void OnTapped_Position_2_3(object sender, EventArgs e)
		{
			MakeActivePosition(2, 3);
		}

		void OnTapped_Position_2_4(object sender, EventArgs e)
		{
			MakeActivePosition(2, 4);
		}

		void OnTapped_Position_2_5(object sender, EventArgs e)
		{
			MakeActivePosition(2, 5);
		}


		void OnTapped_Position_3_1(object sender, EventArgs e)
		{
			MakeActivePosition(3, 1);
		}

		void OnTapped_Position_3_2(object sender, EventArgs e)
		{
			MakeActivePosition(3, 2);
		}

		void OnTapped_Position_3_3(object sender, EventArgs e)
		{
			MakeActivePosition(3, 3);
		}

		void OnTapped_Position_3_4(object sender, EventArgs e)
		{
			MakeActivePosition(3, 4);
		}

		void OnTapped_Position_3_5(object sender, EventArgs e)
		{
			MakeActivePosition(3, 5);
		}

		void OnTapped_Position_4_1(object sender, EventArgs e)
		{
			MakeActivePosition(4, 1);
		}

		void OnTapped_Position_4_2(object sender, EventArgs e)
		{
			MakeActivePosition(4, 2);
		}

		void OnTapped_Position_4_3(object sender, EventArgs e)
		{
			MakeActivePosition(4, 3);
		}

		void OnTapped_Position_4_4(object sender, EventArgs e)
		{
			MakeActivePosition(4, 4);
		}

		void OnTapped_Position_4_5(object sender, EventArgs e)
		{
			MakeActivePosition(4, 5);
		}

		void OnTapped_Position_5_1(object sender, EventArgs e)
		{
			MakeActivePosition(5, 1);
		}

		void OnTapped_Position_5_2(object sender, EventArgs e)
		{
			MakeActivePosition(5, 2);
		}

		void OnTapped_Position_5_3(object sender, EventArgs e)
		{
			MakeActivePosition(5, 3);
		}

		void OnTapped_Position_5_4(object sender, EventArgs e)
		{
			MakeActivePosition(5, 4);
		}

		void OnTapped_Position_5_5(object sender, EventArgs e)
		{
			MakeActivePosition(5, 5);
		}



		#endregion


		private void MakeActivePosition(int row, int col)
		{
			PitchViewModel pitchViewModel = (PitchViewModel)this.BindingContext;
			pitchViewModel.InitializeTeamSquadData(row, col);
			vmApp_Main.ActiveDetailsPage(ScreenType.TeamSquad);
		}
    }
}
