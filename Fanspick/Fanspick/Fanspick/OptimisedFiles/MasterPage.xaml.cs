﻿
using System;
using System.Threading.Tasks;
using MasterDetailPageNavigation;
using Xamarin.Forms;

namespace Fanspick.OptimisedFiles
{
    public partial class MasterPage : MasterDetailPage
    {
        public MasterPage()
        {
            InitializeComponent();
			masterPage.ListView.ItemSelected += async delegate (object sender, SelectedItemChangedEventArgs e)
            {
                await OnItemSelectedAsync(sender, e);
            };
        }

        async Task OnItemSelectedAsync(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterPageItem;
            if (item != null)
            {
                if (item.Title == "Logout")
                {
                    var answer2 = await DisplayAlert("Logout", " Are you sure you want to Logout this app?", "YES", "NO");
                    switch(answer2)
                        {

                    	case true:
                    		Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                    		masterPage.ListView.SelectedItem = null;
                    		IsPresented = false;
                    		break;

                        case false:
                            masterPage.ListView.SelectedItem = null;
                            break;

                    }

                }
                else
                {

                    Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                    masterPage.ListView.SelectedItem = null;
                    IsPresented = false;

                }

            }
        }


       

    }
}
