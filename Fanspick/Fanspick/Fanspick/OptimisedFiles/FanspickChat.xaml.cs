﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DK.SlidingPanel.Interface;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using Quobject.SocketIoClientDotNet.Client;
using Xamarin.Forms;

namespace Fanspick.OptimisedFiles
{
    public partial class FanspickChat : ContentView
    {
        SlidingUpPanel spTest;
		Boolean isScrollAvailable;
		int totalPlayersOnPitch;
        String selectedTopicId;

        public FanspickChat(SlidingUpPanel sp)
        {
            InitializeComponent();
            BindingContext = this;
            spTest = sp;
            GetTopicIdService();

			var keyboardService = Xamarin.Forms.DependencyService.Get<IKeyboardService>();

			keyboardService.KeyboardIsShown += keyBoardShown;
			keyboardService.KeyboardIsHidden += keyBoardHide;


			socket = IO.Socket(CreateUri());
			socket.Connect();

			socket.On(Socket.EVENT_CONNECT, (data) =>
		{


		});

			socket.On(Socket.EVENT_DISCONNECT, (data) =>
		   {
		   });

			socket.On(Socket.EVENT_MESSAGE, (data) =>
		   {
			   GetChatHistory();
		   });

			socket.On("messageFromServer", (data) =>
		   {

		   });

			socket.On("onOtherMessage", (data) =>
		   {

			   MessageFromSocket dto = Newtonsoft.Json.JsonConvert.DeserializeObject<MessageFromSocket>(data.ToString());
			   DateTime current = DateTime.Now;
			   String utcTime = current.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");

			   CommunityPosts getMessage = new CommunityPosts();
			   User userid = new User();

			   userid.Username = dto.MsgSocket.userName;
			   userid.Sender_id = dto.MsgSocket.uID;

			   getMessage.Id = dto.MsgSocket.topicId;
			   getMessage.Message = dto.MsgSocket.message;
			   getMessage.UserId = userid;
			   getMessage.CurrentUserId = FanspickCache.UserData.Id;
			   getMessage.Time = utcTime;
			   getMessage.isConnect = true;
			   CommunityPosts.Add(getMessage);

			   if (!isScrollAvailable)
			   {

				   //Task.Delay(3000);
				   //newMsg.IsVisible = false;

			   }



			   ScrollToLast();

		   });

        }

		void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
		{
			isScrollAvailable = true;
		}

		void Handle_FocusedList(object sender, Xamarin.Forms.FocusEventArgs e)
		{
			isScrollAvailable = false;
		}

		private void OnListClicked(object sender, EventArgs e)
		{
			isScrollAvailable = false;
		}


		void keyBoardShown(object sender, EventArgs e)
		{
			Debug.WriteLine("keyboardVisible");
			CommunityList.Margin = new Thickness(0, 50, 0, 0);
			labelGap.IsVisible = false;
		}

		void keyBoardHide(object sender, EventArgs e)
		{
			Debug.WriteLine("keyboardhidden");
			CommunityList.Margin = new Thickness(0, 200, 0, 0);
			labelGap.IsVisible = false;
		}

        private void OnClosePanel(object sender, EventArgs e)
        {
              spTest.HidePanel();
        }

		ObservableCollection<CommunityPosts> communityPosts = null;
		public ObservableCollection<CommunityPosts> CommunityPosts
		{
			get
			{
				if (communityPosts == null)
					communityPosts = new ObservableCollection<CommunityPosts>();
				//socialFeedData.Shuffle();
				return communityPosts;
			}
			set
			{
				this.communityPosts = value;
			}
		}

		public async Task<ServiceResponse> GetChatHistory()
		{
			ServiceResponse serviceResponse = await new FanspickServices<ChatHistoryService>().Run(requestChatHistoryDTO);
			return serviceResponse;
		}


		public async Task<ServiceResponse> GetTopicByFixture()
		{
			ServiceResponse serviceResponse = await new FanspickServices<TopicByFixtureService>().Run(requestTopicByFixtureDTO);
			return serviceResponse;
		}
		public RequestChatHistoryDTO requestChatHistoryDTO { get; set; } = new RequestChatHistoryDTO();
		public RequestTopicByFixtureDTO requestTopicByFixtureDTO { get; set; } = new RequestTopicByFixtureDTO();

		private async void GetTopicIdService()
		{

			requestTopicByFixtureDTO.TeamId = FanspickCache.CurrentFanspickScope.SelectedTeam.Id;
			requestTopicByFixtureDTO.FixtureId = FanspickCache.CurrentFanspickScope.SelectedFixture.Id;
			requestTopicByFixtureDTO.AccessToken = FanspickCache.UserData.AccessToken;

			Shared.Models.Service.ServiceResponse response = await GetTopicByFixture();

			if (response.OK)
			{
				ResponseTopicByFixtureDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseTopicByFixtureDTO>(response.Data);

				if (dto.Data.Count != 0)
				{
					GetChatHistoryService(dto.Data.First()._id);
					selectedTopicId = dto.Data.First()._id;
				}

			}

			JObject o = new JObject();
			o.Add("topicId", selectedTopicId);
			o.Add("userId", FanspickCache.UserData.AccessToken);

			socket.Emit("createRoom", o);


		}


		private async void GetChatHistoryService(String topicID)
		{

			CommunityPosts.Clear();

			requestChatHistoryDTO.TopicId = topicID;
			requestChatHistoryDTO.AccessToken = FanspickCache.UserData.AccessToken;

			Shared.Models.Service.ServiceResponse response = await GetChatHistory();

			if (response.OK)
			{
				ResponseChatHistoryDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseChatHistoryDTO>(response.Data);

				foreach (CommunityPosts sf in dto.Data[0].FanspickPosts)
				{

					sf.isConnect = true;
					sf.CurrentUserId = FanspickCache.UserData.Id;

					if (sf.UserId.Sender_id == sf.CurrentUserId)
					{
						sf.UserPic = "http://fanspick-admin-test.5sol.co.uk/Uploads/" + FanspickCache.UserData.Photo;
					}

					CommunityPosts.Add(sf);

					var v = CommunityList.ItemsSource.Cast<object>().LastOrDefault();
					CommunityList.ScrollTo(v, ScrollToPosition.End, true);

				}

			}


		}

		Socket socket;


		void Handle_Clicked(object sender, System.EventArgs e)
		{
			if (!String.IsNullOrEmpty(txtMessage.Text))
			{

				DateTime current = DateTime.Now;
				DateTime convertedFixtureDate = TimeZoneInfo.ConvertTime(current, TimeZoneInfo.Utc);
				String nowTime = convertedFixtureDate.ToString("yyyy-MM-dd HH:MM");
				String utcTime = current.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");

				JObject o = new JObject();
				o.Add("topicId", selectedTopicId);
				o.Add("userId", FanspickCache.UserData.AccessToken);
				o.Add("type", "message");
				o.Add("chatType", "fanspick");
				o.Add("message", txtMessage.Text);
				o.Add("time", nowTime);

				socket.Emit("new message", o);

				CommunityPosts getMessage = new CommunityPosts();
				User userid = new User();

				userid.Username = FanspickCache.UserData.Username;
				userid.Sender_id = FanspickCache.UserData.Id;

				getMessage.Id = "0";
				getMessage.Message = txtMessage.Text;
				getMessage.UserId = userid;
				getMessage.CurrentUserId = FanspickCache.UserData.Id;
				getMessage.Time = utcTime;
				getMessage.UserPic = "http://fanspick-admin-test.5sol.co.uk/Uploads/" + FanspickCache.UserData.Photo;


				if (CrossConnectivity.Current.IsConnected)
				{
					getMessage.isConnect = true;
				}
				else
				{
					getMessage.isConnect = false;
				}


				CommunityPosts.Add(getMessage);
				txtMessage.Text = "";
				ScrollToLast();

			}
		}

		public class MessageFromSocket
		{
			[JsonProperty(PropertyName = "message")]
			public Message MsgSocket { get; set; }

		}

		public void ScrollToLast()
		{

			try
			{
				Device.BeginInvokeOnMainThread(() =>
				{
					try
					{

						if (isScrollAvailable)
						{
							var last = CommunityList.ItemsSource.Cast<object>().LastOrDefault();
							CommunityList.ScrollTo(last, ScrollToPosition.MakeVisible, false);
						}
						else
						{

						}

					}
					catch (Exception ex)
					{
						Debug.WriteLine(ex.ToString());
					}

				});


			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.ToString());
			}

		}

		public class Message
		{
			[JsonProperty(PropertyName = "message")]
			public string message { get; set; }

			[JsonProperty(PropertyName = "topicId")]
			public string topicId { get; set; }

			[JsonProperty(PropertyName = "userName")]
			public string userName { get; set; }

			[JsonProperty(PropertyName = "userId")]
			public string uID { get; set; }

		}

		private void ScrollViewLast()
		{

			Device.BeginInvokeOnMainThread(() =>
			{

				try
				{
					var v = CommunityList.ItemsSource.Cast<object>().LastOrDefault();
					CommunityList.ScrollTo(v, ScrollToPosition.End, true);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
				}



			});


		}

		public string CreateUri()
		{
			var options = CreateOptions();
			var uri = string.Format("{0}://{1}:{2}", options.Secure ? "https" : "http", options.Hostname, options.Port);
			return uri;
		}


		public IO.Options CreateOptions()
		{


			var config = ConfigBase.Load();
			var options = new IO.Options();
			options.Port = config.server.port;
			options.Hostname = config.server.hostname;
			options.ForceNew = true;

			return options;
		}


		public class ConfigBase
		{
			public ConfigServer server { get; set; }

			public static ConfigBase Load()
			{
				var result = new ConfigBase()
				{
					server = new ConfigServer()
				};
				result.server.hostname = "52.163.48.178";
				result.server.port = 8000;

				return result;
			}
		}

		// fplive.ukwest.cloudapp.azure.com
		// 52.163.48.178

		public class ConfigServer
		{
			public string hostname { get; set; }
			public int port { get; set; }
		}

	}
}
