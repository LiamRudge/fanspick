﻿using System;
using System.Collections.Generic;
using DK.SlidingPanel.Interface;
using Fanspick.Shared.Helper;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace Fanspick.OptimisedFiles
{
    public partial class Fanspick : ContentPage
    {
        public Fanspick()
        {
            InitializeComponent();
			CommonPitch pitch = new CommonPitch(2);
			PitchView.Children.Add(pitch);
			PitchView.Spacing = 0;

			SlidingPanelConfig config = new SlidingPanelConfig();
			config.MainView = GetMainStackLayout();
			spTest.ApplyConfig(config);

            FanspickChat chatBlock = new FanspickChat(spTest);
            StackChat.Children.Add(chatBlock);


			CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
			{

				if (CrossConnectivity.Current.IsConnected)
				{
					InternetScreen.IsVisible = false;
				}
				else
				{
					InternetScreen.IsVisible = true;
				}


			};

		}

		private AbsoluteLayout GetMainStackLayout()
		{
			AbsoluteLayout mainStackLayout = new AbsoluteLayout();
			mainStackLayout.VerticalOptions = LayoutOptions.FillAndExpand;
			mainStackLayout.HorizontalOptions = LayoutOptions.FillAndExpand;

			return (mainStackLayout);
		}

		private void ShowPanel(object sender, EventArgs e)
		{
			spTest.ShowExpandedPanel();
		}

		private void OnClosePanel(object sender, EventArgs e)
		{
			spTest.HidePanel();
		}
    }
}
