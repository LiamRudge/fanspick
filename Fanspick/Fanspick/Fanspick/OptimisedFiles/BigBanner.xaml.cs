﻿using System;
using System.Collections.Generic;
using Fanspick.PopUp;
using Fanspick.Shared.Helper;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace Fanspick.OptimisedFiles
{
    public partial class BigBanner : ContentView
    {
        public BigBanner()
        {
            InitializeComponent();

			if (FanspickCache.BannerImage == null || FanspickCache.PitchImage == null || FanspickCache.BannerImage.Contains(".jpg") || FanspickCache.PitchImage.Contains(".jpg"))
			{

				FanspickCache.PitchImage = "banner_FP_pitch.gif";
				FanspickCache.PitchPopUp = "PopUp_Page_v4.png";
				FanspickCache.PitchUrl = "http://fanspick.com";

				FanspickCache.BannerImage = "Banner_FP_with-Billboard.gif.gif";
				FanspickCache.BannerPopUp = "PopUp_Page_v4.png";
				FanspickCache.BannerUrl = "http://fanspick.com";
			}

			String htmlMain = FanspickCache.BannerImage.Replace(".gif.gif", ".gif");
			YourPickbannerImg.Source = htmlMain;

        }

		void Openbanner(object sender, EventArgs e)
		{
			FanspickCache.isPitch = false;
			var _billBoardPopup = new BillBoard();
			Navigation.PushPopupAsync(_billBoardPopup);
		}
    }
}
