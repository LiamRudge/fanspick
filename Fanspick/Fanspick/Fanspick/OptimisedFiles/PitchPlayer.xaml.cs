﻿using System;
using System.Threading.Tasks;
using Fanspick.Screens;
using Fanspick.Shared.Models;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace Fanspick.OptimisedFiles
{
    public partial class PitchPlayer : ContentView
    {
        Player SelectedPlayer;
        public PitchPlayer()
        {
            InitializeComponent();


            PlayerName_1_3.HeightRequest = 15;
            PlayerName_1_3.FontSize = 10;

            PlayerImage_1_3.WidthRequest = 32;
            PlayerImage_1_3.HeightRequest = 32;
            PlayerImagePop_1_3.WidthRequest = 32;
            PlayerImagePop_1_3.HeightRequest = 32;

            ImageBack_1_3.WidthRequest = 60;
            ImageBack_1_3.Source = "back_player_diffused";
            ImageBack_1_3.VerticalOptions = LayoutOptions.FillAndExpand;
            ImageBack_1_3.HorizontalOptions = LayoutOptions.FillAndExpand;
            ImageBack_1_3.Aspect = Aspect.Fill;

            PlayerName_1_3.TextColor = Color.White;
            PlayerName_1_3.WidthRequest = 60;
            PlayerName_1_3.VerticalTextAlignment = TextAlignment.Center;
            PlayerName_1_3.HorizontalTextAlignment = TextAlignment.Center;
            PlayerName_1_3.VerticalOptions = LayoutOptions.FillAndExpand;
            PlayerName_1_3.HorizontalOptions = LayoutOptions.FillAndExpand;
            PlayerName_1_3.FontSize = 8;
        }

        public void SetPlayerDetails(String PlayerShirt, Player details)
        {

            SelectedPlayer = details;

            PlayerImage_1_3.Source = PlayerShirt;
            if (details == null)
            {
                PlayerName_1_3.Text = "Player N/A";
            }
            else
            {
                PlayerName_1_3.Text = details.LastName;

            }

            String action = "";
            if (details.userActions != null)
            {

                if (details.userActions.Count > 0)
                {
                    if (details.userActions.Count == 1)
                    {
                        action = details.userActions[0].Action;
                    }
                    else
                    {
                        action = details.userActions[0].Action + "-" + details.userActions[1].Action;
                    }
                }

				if (action == "star") { PlayerImagePop_1_3.Source = "shirt_st"; }
				else if (action == "hairdryer") { PlayerImagePop_1_3.Source = "shirt_hd"; }
				else if (action == "manofmatch") { PlayerImagePop_1_3.Source = "shirt_mom"; }
				else if (action.Contains("manofmatch") && action.Contains("star")) { PlayerImagePop_1_3.Source = "shirt_mom+s"; }
				else if (action.Contains("manofmatch") && action.Contains("hairdryer")) { PlayerImagePop_1_3.Source = "shirt_mom+h"; }

            }



        }

        async void SelectPlayer(object sender, System.EventArgs e)
        {

            if (SelectedPlayer != null)
            {
                var page = new PlayerInformationPage(SelectedPlayer.Id);
                await Navigation.PushPopupAsync(page);
            }
            else
            {
                // await DisplayAlert("Deadline passed", "You cannot select yourpick for a pre match after the deadline has passed", "Close");

            }

        }
    }
}
