﻿using System;
using System.Collections.Generic;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Xamarin.Forms;

namespace Fanspick.OptimisedFiles
{
    public partial class ManagerPick : ContentPage
    {
        public ManagerPick()
        {
            InitializeComponent();
            BindingContext = FanspickCache.CurrentFanspickScope.SelectedFixture;
            // FanspickCache.PageIndex = 1;

            CommonPitch pitch = new CommonPitch(1);
            PitchView.Children.Add(pitch);
            PitchView.Spacing = 0;


		}

        private void OnTapGestureCommentry(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Screens.MatchCommentry(), true);
        }

		
    }
}
