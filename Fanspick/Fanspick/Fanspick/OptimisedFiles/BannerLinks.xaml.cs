﻿using System;
using System.Collections.Generic;
using Fanspick.Shared.Helper;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace Fanspick.OptimisedFiles
{
    public partial class BannerLinks : ContentView
    {
        public BannerLinks()
        {
            InitializeComponent();
            LoadGif();
        }

		public void LoadGif()
		{

			if (FanspickCache.BannerImage == null || FanspickCache.PitchImage == null || FanspickCache.BannerImage.Contains(".jpg") || FanspickCache.PitchImage.Contains(".jpg"))
			{

				FanspickCache.PitchImage = "banner_FP_pitch.gif";
				FanspickCache.PitchPopUp = "PopUp_Page_v4.png";
				FanspickCache.PitchUrl = "http://fanspick.com";

				FanspickCache.BannerImage = "Banner_FP_with-Billboard.gif.gif";
				FanspickCache.BannerPopUp = "PopUp_Page_v4.png";
				FanspickCache.BannerUrl = "http://fanspick.com";
			}


			String htmlsource = FanspickCache.PitchImage;
			String htmlMain = FanspickCache.BannerImage.Replace(".gif.gif", ".gif");

			HtmlWebViewSource html = new HtmlWebViewSource();
			html.Html = String.Format
				(@"<html style='width:" + BannerWidthSize() + ";height=" + BannerheightSize() + "; margin-left:-6px;'><body style='width:100%;height=100%; background: #ffffff;'><img src='{0}' style='width:100%;height=100%;margin-top:-8px;'  /></body></html>",
				 htmlsource);
			Banner1.Source = html;

		}

		public String BannerheightSize()
		{

			String size = "50px";
			String device = CrossDeviceInfo.Current.Model;
			if (device == "iPad")
			{
				size = "100px";
			}

			return size;
		}


		public String BannerWidthSize()
		{

			String size = "100px";
			String device = CrossDeviceInfo.Current.Model;
			if (device == "iPad")
			{
				size = "200px";
			}

			return size;
		}
    }
}
