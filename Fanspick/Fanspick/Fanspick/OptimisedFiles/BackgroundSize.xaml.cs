﻿using System;
using System.Collections.Generic;
using Fanspick.Shared.Helper;
using Xamarin.Forms;


namespace Fanspick.OptimisedFiles
{
    public partial class BackgroundSize : ContentView
    {
        public BackgroundSize()
        {
            InitializeComponent();
            LoadSky();
        }

        public void LoadSky()
        {
            if (FanspickCache.CurrentFanspickScope.SelectedFixture.CompetitionName == "UEFA Champions League")
            {
                skyBack.IsVisible = true;
                skyBack.Source = "start_bg_night";
            }
            else if (FanspickCache.CurrentFanspickScope.SelectedFixture.CompetitionName == "UEFA Europa League")
            {
                skyBack.IsVisible = true;
                skyBack.Source = "start_bg_europa copy";
            }
            else
            {
                skyBack.IsVisible = false;
            }


        }
    }
}
