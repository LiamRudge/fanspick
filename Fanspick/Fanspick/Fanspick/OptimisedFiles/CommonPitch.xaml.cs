﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Fanspick.PopUp;
using Fanspick.RestAPI;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Newtonsoft.Json.Linq;
using Refit;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace Fanspick.OptimisedFiles
{
    public partial class CommonPitch : ContentView
    {
        int totalPlayersOnPitch = 0;
        int tapCountSw = 0;
        public CommonPitch(int index)
        {
            InitializeComponent();

            if(index==1){
                Mic.IsVisible = true;
                Swtich.IsVisible = false;
				GetManagerPick();
			}
			else if (index == 2)
			{
                Mic.IsVisible = false;
                Swtich.IsVisible = true;
                tapCountSw = 0;
				Swtich.Source = "swtich_off";
				GetFanspick(false);
			}

         

        }

        void OpenbannerPitch(object sender, EventArgs e)
        {
            FanspickCache.isPitch = true;
            var _billBoardPopup = new BillBoard();
            Navigation.PushPopupAsync(_billBoardPopup);
        }

        private async void GetManagerPick()
        {
            RemoveAllChildViews(StackForPlayers);
            try
            {
                var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
                RequestGetManagerPickRefitDTO getTeamDataRequest = new RequestGetManagerPickRefitDTO();
                getTeamDataRequest.FixtureId = "5968b5e571f4e76ba53ef52f";
                getTeamDataRequest.TeamId = "59688e4b71f4e76ba53ef4a0";

                var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.GetManagerPick(getTeamDataRequest, header);

                if (response.StatusCode == 200)
                {
                    List<LineUpPlayerData> playerLineup = response.Data.LineUpPlayers;
                    UpdatePlayersOnPitch(playerLineup, response.Data.LineUpPlayers.Count);
                    NullImageLayout.IsVisible = false;

                }

            }
            catch (ApiException ex)
            {
                var content = ex.GetContentAs<Dictionary<String, String>>();
                NullImageLayout.IsVisible = true;
                totalPlayersOnPitch = 0;
            }

            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                NullImageLayout.IsVisible = true;
                totalPlayersOnPitch = 0;
            }

        }

        private async void GetFanspick(bool islive)
		{
            RemoveAllChildViews(StackForPlayers);
			try
			{
				var gitHubApi = RestService.For<IGitHubApi>(FanspickSettings.ApiUrl);
				RequestGetFanspickRefitDTO getTeamDataRequest = new RequestGetFanspickRefitDTO();
				getTeamDataRequest.FixtureId = "596c71a7a8ea4a7faec75343";
				getTeamDataRequest.TeamId = "596c6b5da8ea4a7faec74e9c";
                getTeamDataRequest.IsLive = islive;

				var header = "Bearer " + FanspickCache.UserData.AccessToken;
                var response = await gitHubApi.GetFanspick(getTeamDataRequest, header);

				if (response.StatusCode == 200)
				{
					List<LineUpPlayerData> playerLineup = response.Data.LineUpPlayers;
					UpdatePlayersOnPitch(playerLineup, response.Data.LineUpPlayers.Count);
					NullImageLayout.IsVisible = false;

				}

			}
			catch (ApiException ex)
			{
				var content = ex.GetContentAs<Dictionary<String, String>>();
				NullImageLayout.IsVisible = true;
                totalPlayersOnPitch = 0;
			}

			catch (Exception ex)
			{
				Debug.WriteLine(ex.ToString());
				NullImageLayout.IsVisible = true;
                totalPlayersOnPitch = 0;
			}

		}


		public void SwIconOption(object sender, EventArgs e)
		{
			if (tapCountSw % 2 == 0)
			{
				tapCountSw++;
                Swtich.Source = "swtich_on";
                GetFanspick(true);
			}
			else
			{
				tapCountSw++;
                Swtich.Source = "swtich_off";
                GetFanspick(false);
			}
		}

        public void RemoveAllChildViews(StackLayout view){
            for (int i = 0; i < view.Children.Count; i++)
            {
                view.Children.RemoveAt(i);
            }
            if(GolKeeperStack.Children.Count !=0){
            GolKeeperStack.Children.RemoveAt(0);    
            }

        }


        public void UpdatePlayersOnPitch(List<LineUpPlayerData> lineUpPlayers, int totalPlayerCount)
        {
            totalPlayersOnPitch = totalPlayerCount;
            List<int> playersXValue = new List<int>();
            List<int> playersY2 = new List<int>();
            List<int> playersY3 = new List<int>();
            List<int> playersY4 = new List<int>();
            List<int> playersY5 = new List<int>();

            List<Player> PlayerDetailsY2 = new List<Player>();
            List<Player> PlayerDetailsY3 = new List<Player>();
            List<Player> PlayerDetailsY4 = new List<Player>();
            List<Player> PlayerDetailsY5 = new List<Player>();

			//Set GolKeeper
			for (int i = 0; i < totalPlayerCount; i++)
			{
				int x = int.Parse(lineUpPlayers[i].Position.PosX);
				int y = int.Parse(lineUpPlayers[i].Position.PosY);
                if(x==1 && y==3){
					//GolKeeperStack
					PitchPlayer playerOnPitch = new PitchPlayer();
                    playerOnPitch.SetPlayerDetails("goalkeeper", lineUpPlayers[i].Player);
					GolKeeperStack.Children.Add(playerOnPitch);
					break;
                }

			}


            // Get Number of rows to be made
            for (int i = 0; i < totalPlayerCount; i++)
            {
                int x = int.Parse(lineUpPlayers[i].Position.PosX);
                if (!playersXValue.Contains(x) && x != 1)
                {
                    playersXValue.Add(x);
                }
            }

			for (int i = 0; i < totalPlayerCount; i++)
			{
				int x = int.Parse(lineUpPlayers[i].Position.PosX);
                int y = int.Parse(lineUpPlayers[i].Position.PosY);
                Player details = lineUpPlayers[i].Player;
                if (x == 2) { if(!playersY2.Contains(y)){playersY2.Add(y); PlayerDetailsY2.Add(details);}}
              
			}

			for (int i = 0; i < totalPlayerCount; i++)
			{
				int x = int.Parse(lineUpPlayers[i].Position.PosX);
				int y = int.Parse(lineUpPlayers[i].Position.PosY);
                Player details = lineUpPlayers[i].Player;
				if (x == 3) { if (!playersY3.Contains(y)) { playersY3.Add(y); PlayerDetailsY3.Add(details); } }

			}

			for (int i = 0; i < totalPlayerCount; i++)
			{
				int x = int.Parse(lineUpPlayers[i].Position.PosX);
				int y = int.Parse(lineUpPlayers[i].Position.PosY);
                Player details = lineUpPlayers[i].Player;
				if (x == 4) { if (!playersY4.Contains(y)) { playersY4.Add(y); PlayerDetailsY4.Add(details); } }

			}

			for (int i = 0; i < totalPlayerCount; i++)
			{
				int x = int.Parse(lineUpPlayers[i].Position.PosX);
				int y = int.Parse(lineUpPlayers[i].Position.PosY);
                Player details = lineUpPlayers[i].Player;
				if (x == 5) { if (!playersY5.Contains(y)) { playersY5.Add(y); PlayerDetailsY5.Add(details); } }

			}

            Debug.WriteLine(playersY2);
            Debug.WriteLine(playersY3);
            Debug.WriteLine(playersY4);
            Debug.WriteLine(playersY5);

			StackLayout internalRows = new StackLayout();

			StackLayout addedRow2 = new StackLayout();
			StackLayout addedRow3 = new StackLayout();
			StackLayout addedRow4 = new StackLayout();
			StackLayout addedRow5 = new StackLayout();

            addedRow2.Orientation = StackOrientation.Horizontal;
            addedRow3.Orientation = StackOrientation.Horizontal;
            addedRow4.Orientation = StackOrientation.Horizontal;
            addedRow5.Orientation = StackOrientation.Horizontal;

            addedRow2.VerticalOptions = LayoutOptions.CenterAndExpand; addedRow2.HorizontalOptions = LayoutOptions.CenterAndExpand;
            addedRow3.VerticalOptions = LayoutOptions.CenterAndExpand; addedRow3.HorizontalOptions = LayoutOptions.CenterAndExpand;
            addedRow4.VerticalOptions = LayoutOptions.CenterAndExpand; addedRow4.HorizontalOptions = LayoutOptions.CenterAndExpand;
            addedRow5.VerticalOptions = LayoutOptions.CenterAndExpand; addedRow5.HorizontalOptions = LayoutOptions.CenterAndExpand;
            internalRows.VerticalOptions = LayoutOptions.FillAndExpand; internalRows.HorizontalOptions = LayoutOptions.FillAndExpand;
            internalRows.Spacing = 0;
			//Add dynamic stackLayout based on number of rows from server
			for (int i = 0; i < playersXValue.Count; i++)
			{
                if(playersXValue[i] == 2){
                    for (int j = 0; j < playersY2.Count; j++)
                    {
			        PitchPlayer playerOnPitch = new PitchPlayer();
                    playerOnPitch.SetPlayerDetails("no_shirt", PlayerDetailsY2[j]);
			        addedRow2.Children.Add(playerOnPitch);
                    }
				
                }
				if (playersXValue[i] == 3)
				{
					for (int j = 0; j < playersY3.Count; j++)
					{
						PitchPlayer playerOnPitch = new PitchPlayer();
                        playerOnPitch.SetPlayerDetails("no_shirt", PlayerDetailsY3[j]);
						addedRow3.Children.Add(playerOnPitch);
					}

				}
				if (playersXValue[i] == 4)
				{
					for (int j = 0; j < playersY4.Count; j++)
					{
						PitchPlayer playerOnPitch = new PitchPlayer();
                        playerOnPitch.SetPlayerDetails("no_shirt", PlayerDetailsY4[j]);
						addedRow4.Children.Add(playerOnPitch);
					}

				}
				if (playersXValue[i] == 5)
				{
					for (int j = 0; j < playersY5.Count; j++)
					{
						PitchPlayer playerOnPitch = new PitchPlayer();
                        playerOnPitch.SetPlayerDetails("no_shirt", PlayerDetailsY5[j]);
						addedRow5.Children.Add(playerOnPitch);
					}

				}
                	
			}
			//"no_shirt"
			internalRows.Children.Add(addedRow2);
            internalRows.Children.Add(addedRow3);
            internalRows.Children.Add(addedRow4);
            internalRows.Children.Add(addedRow5);
            StackForPlayers.Children.Add(internalRows);

        }


		private void OnTapGestureCommentry(object sender, EventArgs e)
		{
			Navigation.PushAsync(new Screens.MatchCommentry(), true);
		}

		public async void SharePick(object sender, EventArgs e)
		{
			if (totalPlayersOnPitch == 11)
			{
				await DependencyService.Get<IScreenCapture>().Share("Fanspick!", "Check out the ManagersPick with Fanspick.", null);
			}
			else
			{
				//DisplayAlert("Warning!", "Pitch does not have all players to share the pitch", "ok");
	
			}

		}

    }
}
