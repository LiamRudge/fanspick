﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Fanspick.Templates;
using Fanspick.Helper;
using MasterDetailPageNavigation;
using Fanspick.Screens;

namespace Fanspick.OptimisedFiles
{
	public partial class SideMenu : ContentPage
	{
		public ListView ListView { get { return listView; } }

		public SideMenu()
		{
		  InitializeComponent();

		  var navigationPages = new List<MasterPageItem>();

		  navigationPages.Add(new MasterPageItem()
		  {
		      Title = "Home",
		      TargetType = typeof(HomePage),
		      IconSource = "icon_home.png"

		  });

		  navigationPages.Add(new MasterPageItem()
		  {
		      Title = "My Profile",
		      TargetType = typeof(UserProfilePage),
		      IconSource = "slider_icon_my_teams.png",
		  });


		  navigationPages.Add(new MasterPageItem()
		  {
		      Title = "My Team(s)",
		      TargetType = typeof(MyTeamPage),
		      IconSource = "slider_icon_my_teams.png",
		  });

		  navigationPages.Add(new MasterPageItem()
		  {
		      Title = "Teams",
		      TargetType = typeof(App_TeamSelector),        
		      IconSource = "slider_icon_team.png",
		  });

		  navigationPages.Add(new MasterPageItem()
		  {
		      Title = "Europa League",
		      TargetType = typeof(EuropaLeague),
		      IconSource = "uefa-europa-league.png",
		  });

		  navigationPages.Add(new MasterPageItem()
		  {
		      Title = "Champions League",
		      TargetType = typeof(ChampionLeague),
		      IconSource = "Champions-League-logo.png",
		  });

		  navigationPages.Add(new MasterPageItem()
		  {
		      Title = "Standings",
		      TargetType = typeof(MenuStanding),
		      IconSource = "menu_icon_standings.png",
		  });

		  navigationPages.Add(new MasterPageItem()
		  {
		      Title = "Chat with Friends",
		      TargetType = typeof(ChatContacts),
		      IconSource = "chat_icon.png",
		  });

		  navigationPages.Add(new MasterPageItem()
		  {
		      Title = "Sports",
		      TargetType = typeof(SportsPage),
		      IconSource = "slider_icon_sports.png",
		  });

		  navigationPages.Add(new MasterPageItem()
		  {
		      Title = "How it works",
		      TargetType = typeof(HelpPage),
		      IconSource = "icon_help2.png",
		  });

		  navigationPages.Add(new MasterPageItem()
		  {
		      Title = "Logout",
		      TargetType = typeof(App_Logout),
		      IconSource = "icon_logout.png",
		  });

		  listView.ItemsSource = navigationPages;
		}

	}
}
