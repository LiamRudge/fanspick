﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Fanspick.Shared.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

[assembly: Dependency(typeof(Fanspick.iOS.PlacesAutocomplete))]
namespace Fanspick.iOS
{
	public class PlacesAutocomplete : IPlacesAutocomplete
	{
		private readonly PlacesHttpProvider httpProvider;
		private readonly PlacesConfig authConfig;

		private const string GooglePlacesUrl = "https://maps.googleapis.com/maps/api/place/autocomplete/json";
		private const string GoogleMapsUrl = "https://maps.googleapis.com/maps/api/geocode/json";
		List<string> postalList;
		/// <summary>
		/// Initializes a new instance of the <see cref="PlacesAutocomplete"/> class.
		/// </summary>
		/// <param name="apikey"> The google api key. </param>

		public PlacesAutocomplete()
		{
			this.authConfig = new PlacesConfig("AIzaSyA5sM1pLhG4Eg0-bAnxQyWJaK-gr37cmDA");
			this.httpProvider = new PlacesHttpProvider(authConfig);
		}

		/// <summary>
		/// Creates post body content for an autocomplete request
		/// </summary>
		/// <param name="input"> The search params. </param>
		public async Task<Predictions> GetAutocomplete(string input, string code)
		{
			string content = "input=" + input + "&components=country:" + code + "&types=(cities)&sensor=false";

			string responseData = await httpProvider.FetchPostContentAsync(GooglePlacesUrl, content).ConfigureAwait(false);

			var predictions = JsonConvert.DeserializeObject<Predictions>(responseData);
			return predictions;
		}
		
		public List<string> PostalCodes(string cCode, string city, string input)
		{
			postalList = new List<string>();
			using (StreamReader reader = new StreamReader("Flags/allCountries.txt"))
				while (!reader.EndOfStream)
				{
					string line = reader.ReadLine();
					if (line.Contains(cCode))
					{
						string[] splitstring = line.Split('\t');
						if (splitstring != null)
						{
							if (splitstring.Length == 12)
							{
								if (splitstring[0] == cCode && splitstring[5].Contains(city) || splitstring[7].Contains(city))
								{
									if (splitstring[5].StartsWith(input) || splitstring[7].StartsWith(input))
										postalList.Add(splitstring[1]);
								}
							}

						}

					}

				}

			return postalList;
		}
	}
}
