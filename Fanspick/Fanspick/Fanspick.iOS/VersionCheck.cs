﻿using Fanspick.iOS;
using Fanspick.Shared.Models;
using Foundation;

[assembly: Xamarin.Forms.Dependency(typeof(VersionCheck))]

namespace Fanspick.iOS
{
	public class VersionCheck : IScreen
	{

		public string Version
		{
			get
			{
				NSObject ver = NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"];
				return ver.ToString();
			}
		}

	}
}
