﻿using Fanspick.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TabbedPage), typeof(TabbedPageRenderer))]
namespace Fanspick.iOS
{
	public class TabbedPageRenderer : TabbedRenderer
	{

		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);

            TabBar.TintColor = Color.FromRgb(8,163,222).ToUIColor();
            TabBar.BarTintColor = Color.FromHex("#1c2e43").ToUIColor();
            TabBar.UnselectedItemTintColor = UIColor.White;

            //TabBar.LayoutMargins = UIKit.UIEdgeInsets
		}
	}
}
