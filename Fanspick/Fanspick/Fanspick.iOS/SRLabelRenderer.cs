﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using System.Drawing;
using Fanspick;
using Fanspick.iOS;

[assembly: ExportRenderer(typeof(SRLabel), typeof(SRLabelRenderer))]

namespace Fanspick.iOS
{
    public class SRLabelRenderer : LabelRenderer
    {
        // Override the OnElementChanged method so
        // we can tweak this renderer post-initial setup
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            var label = Control as UILabel;
            if (label != null)
            {
                label.AdjustsFontSizeToFitWidth = true;
                label.Lines = 1;
                label.BaselineAdjustment = UIBaselineAdjustment.AlignCenters;
                label.LineBreakMode = UILineBreakMode.Clip;
            }
        }
    }
}
