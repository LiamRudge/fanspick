﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Fanspick.Shared.Models;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(Fanspick.iOS.ScreenCapture_iOS))]

namespace Fanspick.iOS
{
    public class ScreenCapture_iOS : IScreenCapture
    {
        public async Task<bool> Share(string subject, string message, ImageSource image)
        {
			var handler = new ImageLoaderSourceHandler();

			UIImage screenShot = UIScreen.MainScreen.Capture();

			var img = NSObject.FromObject(screenShot);

			var mess = NSObject.FromObject(message);

            var sub = NSObject.FromObject(subject);

            var activityItems = new NSObject[] { img };

			var activityController = new UIActivityViewController(activityItems, null);

			var topController = UIApplication.SharedApplication.KeyWindow.RootViewController;

			while (topController.PresentedViewController !=null)
			{
			    topController = topController.PresentedViewController;
			}

			topController.PresentViewController(activityController, true, () => { });


			return true;
        }

		UIViewController GetVisibleViewController()
		{
			var rootController = UIApplication.SharedApplication.KeyWindow.RootViewController;

			if (rootController.PresentedViewController == null)
				return rootController;

			if (rootController.PresentedViewController is UINavigationController)
			{
				return ((UINavigationController)rootController.PresentedViewController).TopViewController;
			}

			if (rootController.PresentedViewController is UITabBarController)
			{
				return ((UITabBarController)rootController.PresentedViewController).SelectedViewController;
			}

			return rootController.PresentedViewController;
		}
    }
}
