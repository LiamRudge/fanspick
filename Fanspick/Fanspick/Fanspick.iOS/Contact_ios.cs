﻿using System;
using System.Collections.Generic;
using AddressBook;
using Contacts;
using Fanspick.iOS;
using Fanspick.Shared.Models;
using Foundation;
using Xamarin.Forms;

[assembly: Dependency(typeof(Contact_ios))]

namespace Fanspick.iOS
{
    public class Contact_ios : IPhoneService
    {
        static List<ContactModal> contactsToReturn;

        public Contact_ios()
        {
            if (contactsToReturn == null)
                contactsToReturn = new List<ContactModal>();
        }

        public List<ContactModal> GetContacts()
        {

            ABAddressBook iPhoneAddressBook = new ABAddressBook();

            NSError error;

            iPhoneAddressBook = ABAddressBook.Create(out error);

            if (iPhoneAddressBook != null)
            {
                iPhoneAddressBook.RequestAccess(delegate (bool granted, NSError accessError)
                {
                    if (granted)
                    {
                        ABPerson[] Contacts = iPhoneAddressBook.GetPeople();

                        if (contactsToReturn.Count != 0)
                        {
                            contactsToReturn.Clear();
                        }
						foreach (ABPerson contact in Contacts)
						{
							ABMultiValue<string> myContact = contact.GetPhones();
							foreach (ABMultiValueEntry<string> cont in myContact)
							{

								ContactModal cm = new ContactModal();
								cm.Name = contact.FirstName;
								cm.PhoneNumber = cont.Value;
								contactsToReturn.Add(cm);
							}


						}
                    }
                });
            }


            return contactsToReturn;
        }
    }
}