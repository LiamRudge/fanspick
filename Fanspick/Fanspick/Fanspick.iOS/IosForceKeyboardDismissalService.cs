﻿using System;
using Fanspick.iOS;
using Fanspick.Shared.Models;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(IosForceKeyboardDismissalService))]
namespace Fanspick.iOS
{
    public class IosForceKeyboardDismissalService: IForceKeyboardDismissalService
    {
		public void DismissKeyboard()
		{
			UIApplication.SharedApplication.InvokeOnMainThread(() =>
			{
				var window = UIApplication.SharedApplication.KeyWindow;
				var vc = window.RootViewController;
				while (vc.PresentedViewController != null)
				{
					vc = vc.PresentedViewController;
				}

				vc.View.EndEditing(true);
			});

		}
    }
}
