﻿using System;
using Fanspick.Shared.Models;
using Foundation;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(Fanspick.iOS.FanspickLocalNotification_iOS))]

namespace Fanspick.iOS
{
    public class FanspickLocalNotification_iOS : IFanspickLocalNotification
    {
        public FanspickLocalNotification_iOS()
        {
        }

        public void TriggerNotification(String Title, String Message)
        {
			UILocalNotification notification = new UILocalNotification();

			notification.AlertAction =Title;

			notification.AlertBody = Message;

			UIApplication.SharedApplication.ScheduleLocalNotification(notification);
        }
    }
}
