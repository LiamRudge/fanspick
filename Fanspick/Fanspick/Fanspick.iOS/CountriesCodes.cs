﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Fanspick.Shared.Models;
using Foundation;
using Newtonsoft.Json;
using Xamarin.Forms;

[assembly: Dependency(typeof(Fanspick.iOS.CountriesCodes))]
namespace Fanspick.iOS
{
    public class CountriesCodes: ICountryCodes
    {
        List<CCode> ccCodes;
        CCode cCode = null;
      
        public List<CCode>  DisplayCountryCodes(string input)
        {
			ccCodes = new List<CCode>();
			NSLocale current = NSLocale.CurrentLocale;
			var path = NSBundle.MainBundle.PathForResource("DialingCodes", "plist");
            if(path != null)
            {
				var dict =	NSDictionary.FromFile(path);

				if (dict != null)
				{
					foreach (var city in dict)
					{
						cCode = new CCode();

						cCode.Code = city.Key.ToString().ToUpper();
						cCode.CodeP = "+" + city.Value.ToString();
						cCode.CodeF = "Flags/" + cCode.Code + ".png";
						cCode.CodeN = current.GetCountryCodeDisplayName(city.Key.ToString());
						if (input != "")
						{
							if (cCode.CodeN != null)
							{
								if (cCode.CodeN.StartsWith(input))
								{
									if (cCode.Code == "GB" && cCode.CodeP == "+44")
									{
										ccCodes.Insert(0, cCode);
									}
									else
									{
										ccCodes.Add(cCode);
									}
								}
							}
						}

						else
						{
							cCode = new CCode();

							cCode.Code = city.Key.ToString().ToUpper();
							cCode.CodeP = "+" + city.Value.ToString();
							cCode.CodeF = "Flags/" + cCode.Code + ".png";
							if (cCode.Code == "GB" && cCode.CodeP == "+44")
							{
								ccCodes.Insert(0, cCode);
							}
							else
							{
								ccCodes.Add(cCode);
							}
						}
					}
				}
            }
  
			
            return ccCodes;
		}


    }
}
