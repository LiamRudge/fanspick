﻿using System;
namespace Fanspick.iOS
{
    public class PlacesConfig
    {
		
		public PlacesConfig(string apiKey)
		{
			this.ApiKey = apiKey;
		}

		/// <summary>
		/// Gets or sets the api key of your Firebase app. 
		/// </summary>
		public string ApiKey
		{
			get;
			set;
		}
    }
}
