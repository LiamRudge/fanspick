﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Foundation;
using UIKit;

using CarouselView.FormsPlugin.iOS;
using ImageCircle.Forms.Plugin.iOS;
using DLToolkit.Forms.Controls;
using XLabs.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.Threading.Tasks;
using FFImageLoading.Forms.Touch;
using XLabs.Ioc;
using XLabs.Platform.Services.Media;
using Firebase.CloudMessaging;
using Firebase.InstanceID;
using UserNotifications;
using Fanspick.Shared.Helper;
using Plugin.Toasts;
using HockeyApp.iOS;
using Google.SignIn;

namespace Fanspick.iOS
{

	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	[Register("AppDelegate")]
	public partial class AppDelegate : XFormsApplicationDelegate, IUNUserNotificationCenterDelegate
	{
		//
		// This method is invoked when the application has loaded and is ready to run. In this 
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//

		public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
		{
			// Convert NSUrl to Uri
			var uri = new Uri(url.AbsoluteString);

			// Load redirectUrl page
			AuthenticationState.Authenticator.OnPageLoading(uri);

			return true;
		}

		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{


			var manager = BITHockeyManager.SharedHockeyManager;
			manager.Configure("81616d187584400a828032148ba45b66");
			manager.DebugLogEnabled = true;
			manager.StartManager();

			global::Xamarin.Forms.Forms.Init();
   
            global::Xamarin.Auth.Presenters.XamarinIOS.AuthenticationConfiguration.Init();

			var container = new SimpleContainer();

			container.Register<IMediaPicker, MediaPicker>();

			Resolver.SetResolver(container.GetResolver());

			var r = new XLabs.Forms.Controls.GridViewRenderer();

			var cv = typeof(Xamarin.Forms.CarouselView);

			var assembly = Assembly.Load(cv.FullName);

			CarouselViewRenderer.Init();

			ImageCircleRenderer.Init();

			CachedImageRenderer.Init();

			FlowListView.Init();

			ProgressRingRenderer.Init();

            GoogleLogin.Init();

			MessagingCenter.Subscribe<ImageSource>(this, "Share", Share, null);

			Forms9Patch.iOS.Settings.Initialize("NZPK-RMP4-PJVV-Z7LP-78JF-GNXB-CDJZ-SRYA-BLR2-WBZC-G64K-QJZW-65DB");

            XamForms.Controls.iOS.Calendar.Init();

			LoadApplication(new App());



			var googleServiceDictionary = NSDictionary.FromFile("GoogleService-Info.plist");
			SignIn.SharedInstance.ClientID = googleServiceDictionary["CLIENT_ID"].ToString();


			// get permission for notification
			if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
			{
				// iOS 10
				var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
				UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (granted, error) =>
				{
					System.Diagnostics.Debug.WriteLine(granted);
				});

				// For iOS 10 display notification (sent via APNS)
				// UNUserNotificationCenter.Current.Delegate = this;
			}
			else
			{
				// iOS 9 <=
				var allNotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
				var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);
				UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
			}

			UIApplication.SharedApplication.RegisterForRemoteNotifications();

			// Firebase component initialize
			Firebase.Analytics.App.Configure();


			Firebase.InstanceID.InstanceId.Notifications.ObserveTokenRefresh((sender, e) =>
			{
				var newToken = Firebase.InstanceID.InstanceId.SharedInstance.Token;
				// if you want to send notification per user, use this token
				System.Diagnostics.Debug.WriteLine(newToken);

				FanspickCache.CurrentFanspickScope.FCMId = newToken as string;

				//connectFCM();
			});

			return base.FinishedLaunching(app, options);
		}



		async void Share(ImageSource imageSource)
		{
			var handler = new ImageLoaderSourceHandler();
			var uiImage = await handler.LoadImageAsync(imageSource);

			var item = NSObject.FromObject(uiImage);
			var activityItems = new[] { item };
			var activityController = new UIActivityViewController(activityItems, null);

			var topController = UIApplication.SharedApplication.KeyWindow.RootViewController;

			while (topController.PresentedViewController != null)
			{
				topController = topController.PresentedViewController;
			}

			topController.PresentViewController(activityController, true, () => { });
		}


		public override void DidEnterBackground(UIApplication uiApplication)
		{
			Messaging.SharedInstance.Disconnect();
		}

		public override void OnActivated(UIApplication uiApplication)
		{
            
			//connectFCM();
			base.OnActivated(uiApplication);
		}

		public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
		{
#if DEBUG
            Firebase.InstanceID.InstanceId.SharedInstance.SetApnsToken(deviceToken, Firebase.InstanceID.ApnsTokenType.Sandbox);
#endif
#if RELEASE
            Firebase.InstanceID.InstanceId.SharedInstance.SetApnsToken(deviceToken, Firebase.InstanceID.ApnsTokenType.Prod);
#endif
		}

		// iOS 9 <=, fire when recieve notification foreground

		public override void DidReceiveRemoteNotification(
	    UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{
			//NSDictionary aps = userInfo.ObjectForKey(new NSString("aps")) as NSDictionary;

			//string alert = string.Empty;
			//if (aps.ContainsKey(new NSString("alert")))
				//alert = (aps[new NSString("alert")] as NSString).ToString();
		}


		// iOS 10, fire when recieve notification foreground
		[Export("userNotificationCenter:willPresentNotification:withCompletionHandler:")]
		public void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
		{
			
			//debugAlert(title, body);
		}

		private void connectFCM()
		{
			Messaging.SharedInstance.Connect((error) =>
			{
				if (error == null)
				{
					Messaging.SharedInstance.Subscribe("/topics/all");
				}
				System.Diagnostics.Debug.WriteLine(error != null ? "error occured" : "connect success");
			});
		}

		private void debugAlert(string title, string message)
		{
			var alert = new UIAlertView(title ?? "Title", message ?? "Message", null, "Cancel", "OK");
			alert.Show();
		}
	}
}