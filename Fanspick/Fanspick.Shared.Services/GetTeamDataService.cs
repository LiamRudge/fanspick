﻿using System;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;
using static Fanspick.Shared.Models.DTO.GetTeamDataDTO;

namespace Fanspick.Shared.Services
{
	public class GetTeamDataService : IFanspickService
	{
		public async Task<ServiceResponse> Run<ServiceData>(ServiceData serviceData)
		{
			ServiceResponse serviceResponse = new ServiceResponse();

            RequestGetTeamDataDTO request = serviceData as RequestGetTeamDataDTO;

			FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

			apiController.AccessToken = request.AccessToken;

 			apiController.Target = "fanspick/getTeamData";

            apiController.Method = Models.DTO.MethodType.Post;

            apiController.Data = request;

			Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

			if (apiResponse.OK)
			{
				serviceResponse.Data = apiResponse.DataAsJsonString;

				serviceResponse.OK = true;
			}
			else
			{
				serviceResponse.ReasonPhrase = apiResponse.ReasonPhrase;

				serviceResponse.OK = false;
			}

			return serviceResponse;
		}
	}
}
