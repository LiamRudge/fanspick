﻿using System;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;
using static Fanspick.Shared.Models.DTO.UserNotificationDTO;
using static Fanspick.Shared.Models.DTO.ViewProfileDTO;

namespace Fanspick.Shared.Services
{
    public class UserNotificationService : IFanspickService
	{
		public async Task<ServiceResponse> Run<ServiceData>(ServiceData serviceData)
		{
			ServiceResponse serviceResponse = new ServiceResponse();

			RequestUserNotificationDTO request = serviceData as RequestUserNotificationDTO;


			FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

			apiController.AccessToken = request.AccessToken;

			apiController.Method = request.Method;

			apiController.Target = "fanspick/getUserNotifications";

			apiController.Method = Models.DTO.MethodType.Get;

			Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

			if (apiResponse.OK)
			{
				serviceResponse.Data = apiResponse.DataAsJsonString;

				serviceResponse.OK = true;
			}
			else
			{
				serviceResponse.ReasonPhrase = apiResponse.ReasonPhrase;

				serviceResponse.OK = false;
			}

			return serviceResponse;
		}

	}
}