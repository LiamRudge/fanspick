﻿
using System;
using System.Threading.Tasks;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using static Fanspick.Shared.Models.DTO.MyCommunityDTO;

namespace Fanspick.Shared.Services
{
	public class GetTopicByFixture : IFanspickService
	{
		public async Task<ServiceResponse> Run<ServiceData>(ServiceData serviceData)
		{
			ServiceResponse serviceResponse = new ServiceResponse();

			RequestGetTopicByFixtureDTO request = serviceData as RequestGetTopicByFixtureDTO;

			FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

			apiController.AccessToken = request.AccessToken;

			apiController.Target = "fanspick/getTopicsByFixture";

			apiController.Method = MethodType.Post;

			apiController.Data = request;

			Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

			if (apiResponse.OK)
			{
				serviceResponse.Data = apiResponse.DataAsJsonString;

				serviceResponse.OK = true;
			}
			else
			{
				serviceResponse.ReasonPhrase = apiResponse.ReasonPhrase;

				serviceResponse.OK = false;
			}

			return serviceResponse;
		}

	}
}