﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Services
{
    public class FanspickServices<FanspickService> where FanspickService : IFanspickService, new()
    {
        public async Task<Models.Service.ServiceResponse> Run<ServiceData>(ServiceData FanspickServiceData)
        {
            FanspickService service = new FanspickService();

            return await service.Run<ServiceData>(FanspickServiceData);
        }
    }
}
