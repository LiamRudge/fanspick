﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Models.DTO;

namespace Fanspick.Shared.Services
{
    public class AllTeamsForCompetitionService : IFanspickService
    {
        public async Task<ServiceResponse> Run<ServiceData>(ServiceData serviceData)
        {
            ServiceResponse serviceResponse = new ServiceResponse();

            RequestGetTeamsForCompetitionDTO requestDTO = serviceData as RequestGetTeamsForCompetitionDTO;

            FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

            apiController.AccessToken = requestDTO.AccessToken;

            apiController.Target = "fanspick/getTeamsForCompetition";

            apiController.Data = requestDTO;

			apiController.Method = MethodType.Post;

            Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

            if (apiResponse.OK)
            {
                serviceResponse.Data = apiResponse.DataAsJsonString;

                serviceResponse.OK = true;
            }
            else
            {
                serviceResponse.ReasonPhrase = apiResponse.ReasonPhrase;

                serviceResponse.OK = false;
            }

            return serviceResponse;
        }
    }
}
