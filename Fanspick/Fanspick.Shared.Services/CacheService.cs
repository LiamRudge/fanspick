﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;

namespace Fanspick.Shared.Services
{

    public class CacheService : IFanspickService
    {
        public Task<ServiceResponse> Run<ServiceData>(ServiceData FanspickServiceData)
        {
            throw new NotImplementedException();
        }
    }
    //public class CacheService : IFanspickService
    //{
    //    public async Task<ServiceResponse> Run<ServiceData>(ServiceData FanspickServiceData)
    //    {
    //        ServiceResponse serviceResponse = new ServiceResponse();

    //        // Cache user profile

    //        try
    //        {
    //            CacheUserProfile(FanspickServiceData);
    //        }
    //        catch (Exception error)
    //        {
    //            serviceResponse.MessageStack.Add(error.Message);
    //        }

    //        // Cache all countries

    //        try
    //        {
    //            await CacheAllCountries();
    //        }
    //        catch (Exception error)
    //        {
    //            serviceResponse.MessageStack.Add(error.Message);
    //        }



    //        try
    //        {
    //            await CacheAllLeagues();
    //        }
    //        catch (Exception error)
    //        {
    //            serviceResponse.MessageStack.Add(error.Message);
    //        }

    //        // Cache all teams

    //        try
    //        {
    //            await CacheAllTeams();
    //        }
    //        catch (Exception error)
    //        {
    //            serviceResponse.MessageStack.Add(error.Message);
    //        }


    //        try
    //        {
    //            await SetDefaultFanspickScope();
    //        }
    //        catch (Exception error)
    //        {
    //            serviceResponse.MessageStack.Add(error.Message);
    //        }

    //        if (serviceResponse.MessageStack.Count > 0)
    //        {
    //            // has errored

    //            serviceResponse.OK = false;
    //        }
    //        else
    //        {
    //            serviceResponse.OK = true;
    //        }

    //        return serviceResponse;
    //    }

    //    #region Caching methods

    //    private async Task CacheAllCountries()
    //    {
    //        ServiceResponse allCountriesForSportsResponse = await new FanspickServices<AllCountriesForSportService>()
    //                       .Run(Helper.FanspickCache.UserProfile.accessToken);

    //        Models.FanspickAPI.ResponseModels.Country.GetCountriesForSportModel countryModel = Newtonsoft.Json.JsonConvert
    //            .DeserializeObject<Models.FanspickAPI.ResponseModels.Country.GetCountriesForSportModel>(allCountriesForSportsResponse.Data);

    //        Helper.FanspickCache.Countries = countryModel.data;
    //    }

    //    private async Task CacheAllLeagues()
    //    {
    //        Helper.FanspickCache.Leagues.Clear();

    //        foreach (Models.FanspickAPI.ResponseModels.Country.Datum country in Helper.FanspickCache.Countries)
    //        {
    //            Models.AppModels.LeagueCollectionPackage leagueCollectionPackage = new Models.AppModels.LeagueCollectionPackage();

    //            leagueCollectionPackage.AccessToken = Helper.FanspickCache.UserProfile.accessToken;
    //            leagueCollectionPackage.CountryId = country._id;

    //            ServiceResponse allLeaguesForCountry = await new FanspickServices<AllLeaguesForCountry>()
    //                           .Run(leagueCollectionPackage);

    //            Models.FanspickAPI.ResponseModels.Leagues.GetLeaguesForCountryModel leagueModel = Newtonsoft.Json.JsonConvert
    //                .DeserializeObject<Models.FanspickAPI.ResponseModels.Leagues.GetLeaguesForCountryModel>(allLeaguesForCountry.Data);

    //            if (leagueModel.data.Count > 0)
    //                Helper.FanspickCache.Leagues.AddRange(leagueModel.data);
    //        }
    //    }

    //    private async Task CacheAllTeams()
    //    {
    //        foreach (Models.FanspickAPI.ResponseModels.Leagues.Datum league in Helper.FanspickCache.Leagues)
    //        {
    //            Models.AppModels.TeamCollectionPackage teamCollectionPackage = new Models.AppModels.TeamCollectionPackage();

    //            teamCollectionPackage.AccessToken = Helper.FanspickCache.UserProfile.accessToken;
    //            teamCollectionPackage.CompetitionId = league._id;

    //            ServiceResponse allTeamsForCompetitionService = await new FanspickServices<AllTeamsForCompetitionService>()
    //                           .Run(teamCollectionPackage);

    //            Models.FanspickAPI.ResponseModels.Teams.AllTeamsForCompetitionModel teamsModel = Newtonsoft.Json.JsonConvert
    //                .DeserializeObject<Models.FanspickAPI.ResponseModels.Teams.AllTeamsForCompetitionModel>(allTeamsForCompetitionService.Data);


    //            // manually relate teams to league

    //            foreach (Models.FanspickAPI.ResponseModels.Teams.Datum team in teamsModel.data)
    //            {
    //                Models.FanspickAPI.ResponseModels.Teams.Datum existingTeam = Helper.FanspickCache.Teams
    //                    .FirstOrDefault(t => t._id == team._id);

    //                if (existingTeam != null)
    //                {
    //                    existingTeam.RelatedLeagues.Add(league);
    //                }
    //                else
    //                {
    //                    team.RelatedLeagues.Add(league);

    //                    Helper.FanspickCache.Teams.Add(team);
    //                }
    //            }
    //        }
    //    }



    //    private void CacheUserProfile<ServiceData>(ServiceData FanspickServiceData)
    //    {
    //        String loginResponse = FanspickServiceData as String;

    //        Models.FanspickAPI.ResponseModels.Login.LoginModel loginModel = Newtonsoft.Json.JsonConvert
    //            .DeserializeObject<Models.FanspickAPI.ResponseModels.Login.LoginModel>(loginResponse);

    //        Helper.FanspickCache.UserProfile = loginModel.data;
    //    }

    //    private async Task SetDefaultFanspickScope()
    //    {
    //        Models.FanspickAPI.ResponseModels.Teams.Datum targetTeam = null;

    //        if (Helper.FanspickCache.CurrentFanspickScope.Team == null)
    //        {
    //            dynamic defaultTeam = Helper.FanspickCache.UserProfile.userDetails.defaultTeam.FirstOrDefault();

    //            String favouriteTeamId = defaultTeam.favouriteTeam;

    //            targetTeam = Helper.FanspickCache.Teams.FirstOrDefault(t => t._id == favouriteTeamId);
    //        }
    //        else
    //        {
    //            targetTeam = Helper.FanspickCache.CurrentFanspickScope.Team;
    //        }


    //        Helper.FanspickCache.CurrentFanspickScope.Team = Helper.FanspickCache.Teams
    //            .FirstOrDefault(t => t._id == targetTeam._id);

    //        Models.AppModels.UpcomingFixtureCollectionPackage upcomingFixtureCollectionPackage = new Models.AppModels.UpcomingFixtureCollectionPackage();

    //        upcomingFixtureCollectionPackage.AccessToken = Helper.FanspickCache.UserProfile.accessToken;
    //        upcomingFixtureCollectionPackage.TeamId = targetTeam._id;

    //        ServiceResponse getUpcomingFixturesService = await new FanspickServices<GetUpcomingFixturesService>()
    //                       .Run(upcomingFixtureCollectionPackage);

    //        Models.FanspickAPI.ResponseModels.UpcomingFixtures.GetUpcomingFixturesModel upcomingFixturesModel = Newtonsoft.Json.JsonConvert
    //            .DeserializeObject<Models.FanspickAPI.ResponseModels.UpcomingFixtures.GetUpcomingFixturesModel>(getUpcomingFixturesService.Data);

    //        // order the list
    //        upcomingFixturesModel.data = upcomingFixturesModel.data.OrderBy(f => DateTime.Parse(f.fixtureDate))
    //            .ToList();

    //        if (upcomingFixturesModel.data.Count > 0)
    //            Helper.FanspickCache.CurrentFanspickScope.Fixtures = upcomingFixturesModel.data;

    //        Models.AppModels.UserPickCollectionPackage userPickCollectionPackage = new Models.AppModels.UserPickCollectionPackage();

    //        userPickCollectionPackage.AccessToken = Shared.Helper.FanspickCache.UserProfile.accessToken;

    //        userPickCollectionPackage.TeamId = Shared.Helper.FanspickCache.CurrentFanspickScope.SelectedFixture.localTeam.franspickTeamId._id;

    //        userPickCollectionPackage.FixtureId = Shared.Helper.FanspickCache.CurrentFanspickScope.SelectedFixture._id;

    //        List<Models.AppModels.FanspickPitch> pitches = new List<Models.AppModels.FanspickPitch>();

    //        Shared.Models.Service.ServiceResponse getUserPick;

    //        // get YourPick runup

    //        //userPickCollectionPackage.IsLive = false;
    //        //getUserPick = await new Shared.Services.FanspickServices<Shared.Services.GetUserPickService>().Run(userPickCollectionPackage);
    //        //Models.FanspickAPI.ResponseModels.YourPick.GetUserPickModel yourPickRunup = null;
    //        //if (getUserPick.OK)
    //        //{
    //        //    yourPickRunup = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.FanspickAPI.ResponseModels.YourPick.GetUserPickModel>(getUserPick.Data);
    //        //}
    //        //Shared.Helper.FanspickCache.CurrentFanspickScope.SelectedFixturesPitches.Add(CreateYourPickPitch(yourPickRunup));

    //        // get YourPick live

    //        userPickCollectionPackage.IsLive = true;
    //        getUserPick = await new Shared.Services.FanspickServices<Shared.Services.GetUserPickService>().Run(userPickCollectionPackage);
    //        Models.FanspickAPI.ResponseModels.YourPick.GetUserPickModel yourPickLive = null;
    //        if (getUserPick.OK)
    //        {
    //            yourPickLive = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.FanspickAPI.ResponseModels.YourPick.GetUserPickModel>(getUserPick.Data);
    //        }
    //        Shared.Helper.FanspickCache.CurrentFanspickScope.SelectedFixturesPitches.Add(CreateYourPickPitch(yourPickLive));

    //        // Managers Pick

    //        // get Fanspick Runup

    //        // get Fanspick Match

    //    }

    //    private Models.AppModels.FanspickPitch CreateYourPickPitch(Models.FanspickAPI.ResponseModels.YourPick.GetUserPickModel yourPick)
    //    {
    //        Models.AppModels.FanspickPitch pitch = new Models.AppModels.FanspickPitch();

    //        if (yourPick != null)
    //        {
    //            pitch.CollectedFromServer = true;

    //            pitch.Formation = yourPick.data.formation;

    //            pitch.Players = yourPick.data.lineUpPlayers;
    //        }
    //        else
    //        {
    //            pitch.CollectedFromServer = false;

    //            Fanspick.Shared.Models.FanspickAPI.ResponseModels.YourPick.Formation formation = new Fanspick.Shared.Models.FanspickAPI.ResponseModels.YourPick.Formation();

    //            pitch.Formation = new Models.FanspickAPI.ResponseModels.YourPick.Formation().Empty;
    //        }

    //        return pitch;
    //    }
    //    #endregion

    //}
}
