﻿using System;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;
using static Fanspick.Shared.Models.DTO.GetTopTeamDTO;

namespace Fanspick.Shared.Services
{
	public class GetTopTeamService : IFanspickService
	{
		public async Task<ServiceResponse> Run<ServiceData>(ServiceData serviceData)
		{
			ServiceResponse serviceResponse = new ServiceResponse();

			RequestGetTopTeamDTO request = serviceData as RequestGetTopTeamDTO;


			FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

			apiController.AccessToken = request.AccessToken;

			apiController.Target = "fanspick/getTeamsDetailsForCompetitionId";

			apiController.Queries.Add("competitionId", request.CompetitionId);

            apiController.Method = Models.DTO.MethodType.Get;

			Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

			if (apiResponse.OK)
			{
				serviceResponse.Data = apiResponse.DataAsJsonString;

				serviceResponse.OK = true;
			}
			else
			{
				serviceResponse.ReasonPhrase = apiResponse.ReasonPhrase;

				serviceResponse.OK = false;
			}

			return serviceResponse;
		}
	}
}
