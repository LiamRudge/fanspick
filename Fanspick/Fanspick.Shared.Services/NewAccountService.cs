﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Helper;

namespace Fanspick.Shared.Services
{
    public class NewAccountService : IFanspickService
    {
        public async Task<ServiceResponse> Run<ServiceData>(ServiceData FanspickServiceData)
        {
            ServiceResponse serviceResponse = new ServiceResponse();

            Models.AppModels.SignUpModel signUpModel = FanspickServiceData as Models.AppModels.SignUpModel;

            ServiceResponse validationServiceResponse = await new FanspickServices<NewAccountValidationService>()
                .Run(signUpModel);

            if (validationServiceResponse.OK)
            {
                ServiceResponse createAccountServiceResponse = await new FanspickServices<CreateAccountService>()
                    .Run(signUpModel);

                if (createAccountServiceResponse.OK)
                {
                    Models.FanspickAPI.ResponseModels.Account.RegisterModel registerModel = Newtonsoft.Json.JsonConvert
                        .DeserializeObject<Models.FanspickAPI.ResponseModels.Account.RegisterModel>(createAccountServiceResponse.Data);

                    Shared.Models.Service.ServiceResponse loginServiceResponse = await new Shared.Services.FanspickServices<Shared.Services.AuthenticationService>()
                        .Run(new Shared.Models.AppModels.Credentials() { Username = signUpModel.EmailAddress, Password = signUpModel.Password });

                    if (loginServiceResponse.OK)
                    {
                        Shared.Models.Service.ServiceResponse cacheService = await new Shared.Services.FanspickServices<Shared.Services.CacheService>()
                           .Run(loginServiceResponse.Data);

                        if (cacheService.OK)
                        {
                            
                        }
                        else
                        {
                            serviceResponse.OK = false;
                        }
                    }
                    else
                    {
                        serviceResponse.OK = false;
                    }
                }
            }
            else
            {
                serviceResponse.OK = false;

                //validationServiceResponse.MessageStack.Insert(0, FanspickStrings.Validation_Failed);

                serviceResponse.MessageStack = validationServiceResponse.MessageStack;
            }

            return serviceResponse;
        }
    }
}
