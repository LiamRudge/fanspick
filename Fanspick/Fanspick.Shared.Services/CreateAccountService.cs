﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Models.DTO;

namespace Fanspick.Shared.Services
{
    public class CreateAccountService : IFanspickService
    {
        public async Task<ServiceResponse> Run<ServiceData>(ServiceData serviceData)
        {

            ServiceResponse serviceResponse = new ServiceResponse();

            FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

            apiController.Target = "fanspick/register";


            apiController.Data = serviceData as RequestSignUpDTO;

			apiController.Method = MethodType.Post;

            Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

            if (apiResponse.OK)
            {
                serviceResponse.Data = apiResponse.DataAsJsonString;

                serviceResponse.OK = true;
            }
            else
            {
                serviceResponse.OK = false;
            }

            return serviceResponse;
        }
    }
}
