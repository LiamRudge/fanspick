﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Helper;
using Fanspick.Shared.Models.Service;

namespace Fanspick.Shared.Services
{
    public class GetUserProfileGrade : IFanspickService
    {
        public async Task<ServiceResponse> Run<ServiceData>(ServiceData FanspickServiceData)
        {
            ServiceResponse serviceResponse = new ServiceResponse();

            FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

            apiController.AccessToken = FanspickCache.UserData.AccessToken;

            apiController.Target = "fanspick/getGrade";

			apiController.Method = Models.DTO.MethodType.Get;

            Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

            serviceResponse.OK = apiResponse.OK;

            serviceResponse.Data = apiResponse.DataAsJsonString;

            return serviceResponse;
        }
    }
}
