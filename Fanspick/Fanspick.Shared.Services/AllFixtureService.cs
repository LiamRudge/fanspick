﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;

namespace Fanspick.Shared.Services
{
    public class AllFixtureService : IFanspickService
    {
        public async Task<ServiceResponse> Run<ServiceData>(ServiceData FanspickServiceData)
        {
            ServiceResponse serviceResponse = new ServiceResponse();

            String authorizationKey = FanspickServiceData as String;

            Models.AppModels.Credentials credentials = FanspickServiceData as Models.AppModels.Credentials;

            FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

            apiController.AccessToken = authorizationKey;

            apiController.Target = "fanspick/getAllFixtures";

			apiController.Method = Models.DTO.MethodType.Post;

            apiController.Data = new
            {
                authorization = $"bearer {authorizationKey}"
            };

            Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

            if (apiResponse.OK)
            {
                serviceResponse.Data = apiResponse.DataAsJsonString;

                serviceResponse.OK = true;
            }
            else
            {
                serviceResponse.ReasonPhrase = apiResponse.ReasonPhrase;

                serviceResponse.OK = false;
            }

            return serviceResponse;
        }
    }
}
