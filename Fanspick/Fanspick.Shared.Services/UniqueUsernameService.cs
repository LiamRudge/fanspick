﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;

namespace Fanspick.Shared.Services
{
    public class UniqueUsernameService : IFanspickService
    {
        public async Task<ServiceResponse> Run<ServiceData>(ServiceData FanspickServiceData)
        {
            ServiceResponse serviceResponse = new ServiceResponse();

            String username = FanspickServiceData as String;

            FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

            apiController.Target = "fanspick/checkUsername";

			apiController.Method = Models.DTO.MethodType.Post;

            apiController.Data = new
            {
                username = username
            };

            Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

            serviceResponse.OK = apiResponse.OK;

            return serviceResponse;
        }
    }
}
