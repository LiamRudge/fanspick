﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Models.DTO;

namespace Fanspick.Shared.Services
{
    public static class APIService
    {
        public static async Task<ServiceResponse> Run<ServiceData>(ServiceData serviceData)
        {
            ServiceResponse serviceResponse = new ServiceResponse();

            RequestBaseDTO requestDTO = serviceData as RequestBaseDTO;

            FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

            apiController.AccessToken = requestDTO.AccessToken;

            apiController.Target = requestDTO.APITarget;

            apiController.Api = requestDTO.API;

            apiController.Method = requestDTO.Method;

            apiController.Data = serviceData;

            Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

            if (apiResponse.OK)
            {
                serviceResponse.Data = apiResponse.DataAsJsonString;
            }
            else
            {
                serviceResponse.ReasonPhrase = apiResponse.ReasonPhrase;
            }

            serviceResponse.OK = apiResponse.OK;

            return serviceResponse;
        }
    }
}
