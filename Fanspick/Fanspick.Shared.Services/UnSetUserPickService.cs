﻿using System;
using System.Threading.Tasks;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;

namespace Fanspick.Shared.Services
{
	public class UnSetUserPickService : IFanspickService
	{
		public async Task<ServiceResponse> Run<ServiceData>(ServiceData serviceData)
		{
			ServiceResponse serviceResponse = new ServiceResponse();

            RequestUnSetUserPickDTO request = serviceData as RequestUnSetUserPickDTO;

			FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

			apiController.AccessToken = request.AccessToken;

			apiController.Target = "fanspick/unsetUserPick";

			apiController.Data = request;

			apiController.Method = MethodType.Post;

			Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

			if (apiResponse.OK)
			{
				serviceResponse.Data = apiResponse.DataAsJsonString;

				serviceResponse.OK = true;
			}
			else
			{
				serviceResponse.ReasonPhrase = apiResponse.ReasonPhrase;

				serviceResponse.OK = false;
			}

			return serviceResponse;
		}


	}
}