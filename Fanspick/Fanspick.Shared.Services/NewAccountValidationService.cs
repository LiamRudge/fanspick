﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;

namespace Fanspick.Shared.Services
{
    public class NewAccountValidationService : IFanspickService
    {
        public ServiceResponse ServiceResponse { get; set; } = new ServiceResponse();

        public async Task<ServiceResponse> Run<ServiceData>(ServiceData FanspickServiceData)
        {
            // TODO refactor with [Services.ValidationEngine.Conditions.IsRequired] implementation

            // set to true initially and mark as false when validation fails
            ServiceResponse.OK = true;

            Models.AppModels.SignUpModel signUpModel = FanspickServiceData as Models.AppModels.SignUpModel;

            if (signUpModel == null)
            {
                Failed("SignUpModel equals null");
            }
            else
            {
                if (String.IsNullOrEmpty(signUpModel.Username))
                {
                    Failed("Username is empty");
                }
                else
                {
                    ServiceResponse uniqueUsernameResponse = await new Services.FanspickServices<UniqueUsernameService>()
                        .Run(signUpModel.Username);

                    if (!uniqueUsernameResponse.OK)
                    {
                        Failed("Username is already in use");
                    }
                }

                if (String.IsNullOrEmpty(signUpModel.EmailAddress))
                {
                    Failed("Eamil is empty");
                }

                if (String.IsNullOrEmpty(signUpModel.Password))
                {
                    Failed("Password is empty");
                }
                else
                {
                    if (signUpModel.Password.Length < 6)
                    {
                        Failed("Password must be greater than 6 characters");
                    }
                }

                if (String.IsNullOrEmpty(signUpModel.PasswordConfirmation))
                {
                    Failed("Password confirmation not provided");
                }
                else
                {
                    if (signUpModel.Password != signUpModel.PasswordConfirmation)
                    {
                        Failed("The passwords do not match");
                    }
                }
            }

            return ServiceResponse;
        }

        private void Failed(String Reason)
        {
            ServiceResponse.OK = false;

            String safeReason = Reason == null ? String.Empty : Reason;

            ServiceResponse.MessageStack.Add(safeReason);
        }
    }
}
