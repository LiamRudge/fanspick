﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Models.DTO;

namespace Fanspick.Shared.Services
{
    public class NotificationResponseService : IFanspickService
	{
		public async Task<ServiceResponse> Run<ServiceData>(ServiceData serviceData)
		{
			ServiceResponse serviceResponse = new ServiceResponse();

			RequestNotificationResponseDTO requestDTO = serviceData as RequestNotificationResponseDTO;

			FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

			apiController.AccessToken = requestDTO.AccessToken;

			apiController.Target = "fanspick/createNotificationResponse";

			apiController.Method = MethodType.Post;

			apiController.Data = requestDTO;

			Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

			if (apiResponse.OK)
			{
				serviceResponse.Data = apiResponse.DataAsJsonString;
			}
			else
			{
				serviceResponse.ReasonPhrase = apiResponse.ReasonPhrase;
			}

			serviceResponse.OK = apiResponse.OK;
			return serviceResponse;
		}
	}
}
