﻿using System;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;
using static Fanspick.Shared.Models.DTO.RecentChatContactDTO;

namespace Fanspick.Shared.Services                                 
{
	public class RecentChatContactService : IFanspickService
	{
		public async Task<ServiceResponse> Run<ServiceData>(ServiceData serviceData)
		{
			ServiceResponse serviceResponse = new ServiceResponse();

			RequestRecentChatContactDTO request = serviceData as RequestRecentChatContactDTO;
			FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();
			apiController.AccessToken = request.AccessToken;
            apiController.Method = request.Method;
            apiController.Target = "fanspick/getRecentChats";
            apiController.Method = Models.DTO.MethodType.Post;

			Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

			if (apiResponse.OK)
			{
				serviceResponse.Data = apiResponse.DataAsJsonString;

				serviceResponse.OK = true;
			}
			else
			{
				serviceResponse.ReasonPhrase = apiResponse.ReasonPhrase;

				serviceResponse.OK = false;
			}

			return serviceResponse;
		}

	}
}