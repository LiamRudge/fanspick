﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanspick.Shared.Services
{
    public interface IFanspickService
    {
        Task<Models.Service.ServiceResponse> Run<ServiceData>(ServiceData FanspickServiceData);
    }
}
