﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Models.DTO;

namespace Fanspick.Shared.Services
{
    public class GetManagerPickService : IFanspickService
    {
        public async Task<ServiceResponse> Run<ServiceData>(ServiceData serviceData)
        {
            ServiceResponse serviceResponse = new ServiceResponse();

            RequestGetManagerPickDTO requestDTO = serviceData as RequestGetManagerPickDTO;

            FanspickAPI.FanspickAPIController apiController = new FanspickAPI.FanspickAPIController();

            apiController.AccessToken = requestDTO.AccessToken;

            apiController.Target = "fanspick/getManagerPick_V2";

			apiController.Method = MethodType.Post;

            apiController.Data = requestDTO;

            Models.FanspickAPI.FanspickAPIResponse apiResponse = await apiController.Call();

            if (apiResponse.OK)
            {
                serviceResponse.Data = apiResponse.DataAsJsonString;
            }
            else
            {
                serviceResponse.ReasonPhrase = apiResponse.ReasonPhrase;
            }

            serviceResponse.OK = apiResponse.OK;
            return serviceResponse;
        }
    }
}
