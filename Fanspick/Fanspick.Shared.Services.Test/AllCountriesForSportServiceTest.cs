﻿using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services.Test.MockData;
using NUnit.Framework;
using System.Threading.Tasks;
using Xamarin.UITest;

namespace Fanspick.Shared.Services.Test
{
    
    [TestFixture]
    public class AllCountriesForSportServiceTest
    {   

        public AllCountriesForSportServiceTest()
        {   

        }

        [SetUp]
        public void BeforeEachTest()
        {
            
        }

        [Test]
        public async Task AllCountriesForSportService()
        {
            RequestGetCountriesForSportDTO requestDTO = new RequestGetCountriesForSportDTO();
            requestDTO.AccessToken = UserData.AccessToken;
            requestDTO.SportId = "58906bc577a25834fa86fed3";

            ServiceResponse allCountriesForSportsResponse = await new FanspickServices<AllCountriesForSportService>().Run(requestDTO);

            ResponseGetCountriesForSportDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetCountriesForSportDTO>(allCountriesForSportsResponse.Data);
            Assert.AreEqual(200, dto.StatusCode);
        }
    }

}

