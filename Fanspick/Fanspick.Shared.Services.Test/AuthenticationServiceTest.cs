﻿using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services;
using Fanspick.Shared.Services.Test.MockData;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Xamarin.UITest;


namespace Fanspick.Shared.Services.Test
{
    [TestFixture]
    public class AuthenticationServiceTest
    {

        public AuthenticationServiceTest()
        {
        }

        [Test]
        public async Task Login_Normal_Test()
        {
            ServiceResponse serviceResponse = await new FanspickServices<AuthenticationService>()
                    .Run(new RequestLoginDTO()
                    {
                        Email = "sunil@gmail.com",
                        Password = "123456",
                        AppVersion = "1.0",
                        LoginType = "SIMPLE",
                        DeviceType = "IOS",
                        DeviceToken = Guid.NewGuid().ToString()                        
                    });

            ResponseLoginDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseLoginDTO>(serviceResponse.Data);
            Assert.AreEqual(200, response.StatusCode);



        }
    }
}
