﻿using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services.Test.MockData;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Xamarin.UITest;


namespace Fanspick.Shared.Services.Test
{
    [TestFixture]
    public class GetFormationByTypeTest
    {

        public GetFormationByTypeTest()
        {
        }

        [Test]
        public async Task GetFormationByType_Test()
        {
            ServiceResponse serviceResponse = await new FanspickServices<GetFormationByTypeService>()
                    .Run(new RequestGetFormationByTypeDTO
                    {
                        AccessToken = UserData.AccessToken1,
                        Type="4321"
                    });

            ResponseGetFormationByTypeDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetFormationByTypeDTO>(serviceResponse.Data);
            Assert.AreEqual(200, response.StatusCode);
        }
    }
}
