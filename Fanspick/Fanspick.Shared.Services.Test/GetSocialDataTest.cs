﻿using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services.Test.MockData;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Xamarin.UITest;


namespace Fanspick.Shared.Services.Test
{
    [TestFixture]
    public class GetSocialDataTest
    {

        public GetSocialDataTest()
        {
        }

        [Test]
        public async Task GetSocialData_Test()
        {
            ServiceResponse serviceResponse = await APIService.Run(new RequestGetSocialFeedDTO
            {
                AccessToken = string.Empty,
                TeamId = "58d4e9b14dcb492d8d0495d5"
            });

            ResponseGetSocialFeedDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetSocialFeedDTO>(serviceResponse.Data);
           // Assert.Greater(response.Data.Count, 0);
        }
    }
}
