﻿using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services.Test.MockData;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Xamarin.UITest;


namespace Fanspick.Shared.Services.Test
{
    [TestFixture]
    public class GetManagerPickServiceTest
    {

        public GetManagerPickServiceTest()
        {
        }

        [Test]
        public async Task GetManagerPickService_Test()
        {
            ServiceResponse serviceResponse = await new FanspickServices<GetManagerPickService>()
                    .Run(new RequestGetManagerPickDTO
                    {
                        AccessToken = UserData.AccessToken1,
                        FixtureId = "58d4f0e94dcb492d8d04a139",
                        TeamId = "58d4e9b14dcb492d8d0495d5"
                    });

            ResponseGetManagerPickDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetManagerPickDTO>(serviceResponse.Data);
            Assert.AreEqual(200, response.StatusCode);
        }
    }
}
