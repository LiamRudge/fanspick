﻿using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services.Test.MockData;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.UITest;

namespace Fanspick.Shared.Services.Test
{

    [TestFixture]
    public class GetUpcomingFixturesServiceTest
    {

        public GetUpcomingFixturesServiceTest()
        {

        }

        [SetUp]
        public void BeforeEachTest()
        {

        }

        [Test]
        public async Task GetUpcomingFixturesService_Test()
        {

            ServiceResponse authenticationServiceResponse = await new FanspickServices<AuthenticationService>()
                 .Run(new RequestLoginDTO()
                 {
                     Email = "sunil@gmail.com",
                     Password = "123456",
                     AppVersion = "1.0",
                     LoginType = "SIMPLE",
                     DeviceType = "IOS",
                     DeviceToken = Guid.NewGuid().ToString()
                 });

            ResponseLoginDTO responseLoginDTO = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseLoginDTO>(authenticationServiceResponse.Data);

            RequestGetUpcomingFixturesDTO requestDTO = new RequestGetUpcomingFixturesDTO();
            requestDTO.AccessToken = UserData.AccessToken;
            requestDTO.TeamId = responseLoginDTO.Data.UserDetails.DefaultTeam.First().FavouriteTeam;


            ServiceResponse getUpcomingFixturesServiceResponse = await new FanspickServices<GetUpcomingFixturesService>().Run(requestDTO);

            ResponseGetUpcomingFixturesDTO responseGetUpcomingFixturesDTO = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetUpcomingFixturesDTO>(getUpcomingFixturesServiceResponse.Data);
            Assert.AreEqual(true, getUpcomingFixturesServiceResponse.OK);

        }
    }
}

