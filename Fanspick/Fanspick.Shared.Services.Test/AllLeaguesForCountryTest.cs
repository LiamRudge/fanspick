﻿using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services.Test.MockData;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.UITest;

namespace Fanspick.Shared.Services.Test
{

    [TestFixture]
    public class AllLeaguesForCountryTest
    {

        public AllLeaguesForCountryTest()
        {

        }

        [SetUp]
        public void BeforeEachTest()
        {

        }

        [Test]
        public async Task AllLeaguesForCountry_Test()
        {
            RequestGetCountriesForSportDTO requestDTO = new RequestGetCountriesForSportDTO();
            requestDTO.AccessToken = UserData.AccessToken;
            requestDTO.SportId = "58906bc577a25834fa86fed3";

            ServiceResponse allCountriesForSportsResponse = await new FanspickServices<AllCountriesForSportService>().Run(requestDTO);

            ResponseGetCountriesForSportDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetCountriesForSportDTO>(allCountriesForSportsResponse.Data);           

            
            foreach (Country country in dto.Data)
            {
                RequestAllLeaguesForCountryDTO requestAllLeaguesForCountryDTO = new RequestAllLeaguesForCountryDTO();
                requestAllLeaguesForCountryDTO.AccessToken = UserData.AccessToken;
                requestAllLeaguesForCountryDTO.CountryId = country.Id;

                ServiceResponse allLeaguesForCountryResponse = await new FanspickServices<AllLeaguesForCountry>().Run(requestAllLeaguesForCountryDTO);
                if (allLeaguesForCountryResponse.OK)
                {
                    ResponseAllLeaguesForCountryDTO responseDTO = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseAllLeaguesForCountryDTO>(allLeaguesForCountryResponse.Data);

                    Assert.AreEqual(200, responseDTO.StatusCode);
                }
            }
        }
    }

}

