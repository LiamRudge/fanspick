﻿using Fanspick.Shared.Helper;
using Fanspick.Shared.Models;
using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services.Test.MockData;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.UITest;

namespace Fanspick.Shared.Services.Test
{

    [TestFixture]
    public class AllTeamsForCompetitionServiceTest
    {

        public AllTeamsForCompetitionServiceTest()
        {

        }

        [SetUp]
        public void BeforeEachTest()
        {

        }

        [Test]
        public async Task AllTeamsForCompetitionService_Test()
        {
            RequestGetCountriesForSportDTO requestDTO = new RequestGetCountriesForSportDTO();
            requestDTO.AccessToken = UserData.AccessToken;
            requestDTO.SportId = "58906bc577a25834fa86fed3";

            ServiceResponse allCountriesForSportsResponse = await new FanspickServices<AllCountriesForSportService>().Run(requestDTO);

            ResponseGetCountriesForSportDTO dto = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetCountriesForSportDTO>(allCountriesForSportsResponse.Data);

            List<Competition> competitions = new List<Competition>();

            FanspickCache.CompetitionData.Clear();


            Competition competition;

            foreach (Country country in dto.Data)
            {
                foreach (CompetitionMini competitionMini in country.CompetitionMini)
                {
                    RequestGetTeamsForCompetitionDTO requestGetTeamsForCompetitionDTO = new RequestGetTeamsForCompetitionDTO();
                    requestGetTeamsForCompetitionDTO.AccessToken = UserData.AccessToken;
                    requestGetTeamsForCompetitionDTO.CompetitionId = competitionMini.competitionId;

                    ServiceResponse allTeamsForCompetitionServiceResponse = await new FanspickServices<AllTeamsForCompetitionService>().Run(requestGetTeamsForCompetitionDTO);
                    Assert.AreEqual(true, allTeamsForCompetitionServiceResponse.OK);
                    ResponseGetTeamsForCompetitionDTO responseDTO = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseGetTeamsForCompetitionDTO>(allTeamsForCompetitionServiceResponse.Data);
                    competition = new Competition();
                    competition.Id = competitionMini.competitionId;
                    competition.CountryId = country.Id;
                    competition.Teams.AddRange(responseDTO.Data);
                    //competitions.Add(competition);
                    FanspickCache.CompetitionData.Add(competition);

                }
            }

        }
    }

}

