﻿using Fanspick.Shared.Models.DTO;
using Fanspick.Shared.Models.Service;
using Fanspick.Shared.Services.Test.MockData;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Xamarin.UITest;

namespace Fanspick.Shared.Services.Test
{
    
    [TestFixture]
    public class CreateAccountServiceTest
    {   

        public CreateAccountServiceTest()
        {   

        }

        [SetUp]
        public void BeforeEachTest()
        {
            
        }

        [Test]
        public async Task CreateAccountService_Test()
        {
            RequestSignUpDTO dto = new RequestSignUpDTO();
            dto.AppVersion = "1.0";
            dto.DeviceType = "IOS";
            dto.DeviceToken = Guid.NewGuid().ToString();
            dto.UserName = "nishant126"; //username should be unique
            dto.EmailAddress = "nishantkumar.126@fivesol.com"; //email address should be unique
            dto.Password = "123456";
            dto.Gender = "MALE";

            ServiceResponse createAccountServiceResponse = await new FanspickServices<CreateAccountService>().Run(dto);

            ResponseLoginDTO response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseLoginDTO>(createAccountServiceResponse.Data);
            
        }
    }

}

